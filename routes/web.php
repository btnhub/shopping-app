<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    //return view('welcome');
    return view('layouts.organica.homepage');    
});

Auth::routes();

/*Route::get('emails', function(){
	$user = App\User::inRandomOrder()->first();
	$subject = "Failed Job test";
	$email_view = 'emails.notify_admin';

	//dispatch(new App\Jobs\SendAdminEmail($user, $subject, $email_view));
	dispatch(new App\Jobs\SendAdminEmail($subject, $email_view, "TESTING 24141"));
    echo('done');
});
*/

Route::get('/fb/data-feed', [
	'as' => 'facebook.data.feed',
	'uses' => 'ProductController@facebookDataFeedCSV'
]);

Route::get('/search', [
	'as' => 'search.products',
	'uses' => 'ProductController@searchProducts'
]);

//Route::get('/home', 'HomeController@index');

//Webhooks
Route::post('/membership/stripe-webhooks', [
	'uses' => 'StripeWebhooksController@processWebhook'
	]);

Route::get('/contact-us', function () {
    return view('pages.contact');    
});

Route::get('/terms', function () {
    return view('pages.terms');    
});

Route::get('/privacy-policy', function () {
    return view('pages.privacy');    
});

Route::get('/faqs', function () {
    return view('pages.faqs');    
});

Route::get('/giving-back', function () {
    return view('pages.donations');    
});

Route::get('/shipping', function () {
    return view('pages.shipping');    
});

Route::get('/refunds', function () {
    return view('pages.refunds');    
});

Route::get('/gift-cards', function () {
    return view('gift_cards.index');    
});

Route::get('/subscription', function () {
    return view('pages.subscription');    
});

Route::post('gift-cards', [
			'as' => 'purchase.gift.card',
			'uses' =>'CreditsController@purchaseGiftCard']);

Route::post('credits/redeem', [
			'as' => 'redeem.credit.code',
			'uses' =>'CreditsController@redeemCreditCode']);

Route::get('/{cat_slug}/sub/{subcategory_id}/{sub_slug}/', [
			'as' => 'list.products',
			'uses' =>'SubCategoryController@listProducts']);

Route::get('/{cat_slug}/{sub_slug}/pr/{product_id}/{prod_slug}', [
			'as' => 'product.details',
			'uses'=> 'ProductController@productDetails']);

Route::delete('/cart/empty', [
			'as' =>'empty.cart', 
			'uses' => 'CartController@emptyCart',
		]);
Route::resource('/cart', 'CartController');

//For Account
Route::group(['prefix' => 'my-account', 'middleware' => 'auth'], function () {
	Route::get('/', [
		'as' => 'my.account',
		function () {
			if (auth()->user()->hasRole('admin')) {
				return redirect('admin');
			}

			return redirect()->route('user.account', ['user_ref_id' => auth()->user()->ref_id]);
		}]);

	Route::get('/{user_ref_id}', [
		'as' =>'user.account', 
		'uses' => 'UsersController@myAccount',
	]);

	Route::get('/{user_ref_id}/edit', [
		'as' =>'edit.account', 
		'uses' => 'UsersController@editAccount',
	]);

	Route::post('/{user_ref_id}/edit', [
		'as' =>'update.account', 
		'uses' => 'UsersController@updateAccount',
	]);

	Route::post('/cart/reorder/{order_ref_id}', [
				'as' => 'reorder.items',
				'uses' =>'CartController@reorderItems']);	
	
	Route::delete('/cancel-order/{order_ref_id}', [
				'as' => 'cancel.order',
				'uses' => 'OrdersController@cancelOrder'
				]);
	
	Route::get('/credits/{user_ref_id}', [
		'as' =>'user.credits', 
		'uses' => 'CreditsController@myCredits',
	]);

	Route::post('/payment-settings/add/{user_ref_id}', [
		'as' =>'add.card', 
		'uses' => 'UsersController@storeCreditCard',
	]);

	Route::get('/payment-settings/{user_ref_id}', [
		'as' =>'edit.card', 
		'uses' => 'UsersController@editCreditCard',
	]);

	Route::post('/payment-settings/{user_ref_id}', [
		'as' =>'update.card', 
		'uses' => 'UsersController@updateCreditCard',
	]);
});

Route::group(['prefix' => 'my-membership'], function () {
	Route::get('/{user_ref_id}', [
		'as' =>'user.memberships', 
		'uses' => 'UsersController@myMemberships',
	]);
});

Route::group(['prefix' => 'membership'], function () {
	Route::get('/', [
		'as' => 'membership.pricing',
		'uses' => 'MembershipsController@membershipPricing'
		]);

	Route::get('/purchase', [
		'as' => 'select.membership',
		'uses' => 'MembershipsController@selectMembership'
		])->middleware('auth');

	Route::post('/purchase', [
		'as' => 'purchase.membership',
		'uses' => 'MembershipsController@purchaseMembership'
		])->middleware('auth');

	Route::get('/update/{stripe_subscription_id}', [
		'as' => 'change.membership',
		'uses' => 'MembershipsController@changeMembership'
		])->middleware('auth');

	Route::post('/update', [
		'as' => 'update.membership',
		'uses' => 'MembershipsController@updateMembership'
		])->middleware('auth');

	Route::delete('/cancel/{subscription_id}', [
		'as' => 'cancel.membership',
		'uses' => 'MembershipsController@cancelMembership'
		])->middleware('auth');

	Route::post('/reactivate/{subscription_id}', [
		'as' => 'reactivate.membership',
		'uses' => 'MembershipsController@reactivateMembership'
		])->middleware('auth');

});

Route::group(['prefix' => 'subscriptions', 'middleware' => 'auth'], function () {
	Route::get('/', [
		'as' => 'my.subscriptions',
		function () {
		return redirect()->route('user.subscriptions', ['user_ref_id' => auth()->user()->ref_id]);
		}]);

	Route::get('/{user_ref_id}', [
		'as' => 'user.subscriptions',
		'uses' => 'RecurringOrdersController@subscriptions'
		]);

	Route::delete('/delete_item/{recurring_order_ref_id}/{item_ref_id}', [
		'as' => 'remove.subscription.item',
		'uses' => 'RecurringOrdersController@removeItem'
		]);

	Route::delete('/delete/{ref_id}', [
		'as' => 'subscription.destroy',
		'uses' => 'RecurringOrdersController@destroySubscription'
		]);
});

Route::group(['prefix' => 'wishlist', 'middleware' => 'auth'], function () {
	Route::get('/', [
		'as' => 'my.wishlist',
		function () {
		return redirect()->route('user.wishlist', ['user_ref_id' => auth()->user()->ref_id]);
		}]);

	Route::get('/{user_ref_id}', [
			'as' =>'user.wishlist', 
			'uses' => 'WishlistController@getWishlist',
		]);	

	Route::post('/{product_ref_id}', [
			'as' =>'wishlist.add.product', 
			'uses' => 'WishlistController@addProduct',
		]);	

	Route::delete('/remove/{product_ref_id}', [
			'as' =>'wishlist.remove.product', 
			'uses' => 'WishlistController@removeProduct',
		]);	
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function() {
		Route::get('/', [
			'as' => 'admin.index',
			'uses' => 'AdminController@index']);

		Route::post('/orders/{id}/add-tracking-number/', [
					'as' => 'add.tracking.no',
					'uses' => 'ShipmentsController@addTrackingNumber'
			]);

		Route::post('/orders/{id}/mark-as-unreviewed/', [
					'as' => 'mark.order.unreviewed',
					'uses' => 'OrdersController@markAsUnreviewed'
			]);
		
		Route::get('/orders/{id}/packing-slip/', [
					'as' => 'create.packing.slip',
					'uses' => 'ShipmentsController@createPackingSlip'
			]);

		Route::get('/payments/{payment_id}/refund', [
					'as' => 'create.payment.refund',
					'uses' => 'PaymentsController@createRefund'
			]);

		Route::get('clone-user/{user_id}', [
			'as' => 'clone.user',
			'uses' => 'AdminController@cloneUser'
			]);

		Route::get('products/ingredient-proportions/{product_id}', [
			'as' => 'create.recipe',
			'uses' => 'ProductController@createRecipe'
			]);

		Route::post('products/ingredient-proportions/{product_id}', [
			'as' => 'store.recipe',
			'uses' => 'ProductController@storeRecipe'
			]);

		Route::get('products/ingredient-proportions/{product_id}/edit', [
			'as' => 'edit.recipe',
			'uses' => 'ProductController@editRecipe'
			]);

		Route::patch('products/ingredient-proportions/{product_id}/update', [
			'as' => 'update.recipe',
			'uses' => 'ProductController@updateRecipe'
			]);

		Route::post('/recurring_orders/fill/{recurring_order_id}', [
			'as' => 'fill.recurring.order',
			'uses' => 'RecurringOrdersController@fillRecurringOrder'
			]);

		Route::post('/products/copy-items/{new_product_id}', [
			'as' => 'copy.items',
			'uses' => 'ProductController@copyItems'
			]);

		Route::post('/users/{user_id}/membership', [
			'as' => 'add.user.membership',
			'uses' => 'AdminController@addUserToMembership'
			]);

		Route::post('/credits/{credit_id}/approve', [
			'as' => 'approve.credit.code',
			'uses' => 'CreditsController@approveCreditCode'
			]);

		Route::resource('users', 'UsersController');
		Route::resource('memberships', 'MembershipsController');
		Route::resource('credits', 'CreditsController');
		Route::resource('suppliers', 'SupplierController');
		Route::resource('categories', 'CategoryController');
		Route::resource('subcategories', 'SubCategoryController');
		Route::resource('products', 'ProductController');
		Route::resource('orders', 'OrdersController');
		Route::resource('shipments', 'ShipmentsController');
		Route::resource('payments', 'PaymentsController');
		Route::resource('refunds', 'RefundsController');
		Route::resource('raw_materials', 'RawMaterialController');
		Route::resource('recurring_orders', 'RecurringOrdersController');
		Route::resource('items', 'ItemController');
		Route::resource('purchases', 'PurchasesController');
    });

Route::group(['prefix' => 'checkout'], function() {
		Route::get('/', [
			'as' => 'get.checkout',
			'uses' => 'CheckoutController@getCheckoutPage']);
		
		Route::post('/shipping', [
			'as' => 'shipping.rates',
			'uses' => 'CheckoutController@getShippingRates']);

		Route::post('/payment-form', [
			'as' => 'payment.form',
			'uses' => 'CheckoutController@getPaymentForm']);

		Route::post('/payment', [
			'as' => 'submit.payment',
			'uses' => 'CheckoutController@submitPayment']);
    });




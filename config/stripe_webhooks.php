<?php
  
  //EXAMPLE REQUESTS sent when testing Stripe Webhooks (sent from dashboard)
  //Uncomment to test, but will cause an error when php artisan config:cache, so always keep COMMENTED!
  
  /*return
    [
      'event_example' => json_decode('{
            "id": "evt_1Ajs8F2eZvKYlo2CcPgw9a2t",
            "object": "event",
            "api_version": "2017-06-05",
            "created": 1501085587,
            "data": {
              "object": {
                "object": "invoice",
                "amount_due": 999,
                "application_fee": null,
                "attempt_count": 0,
                "attempted": false,
                "charge": null,
                "closed": false,
                "currency": "usd",
                "customer": "cus_8vxPfLICcTD7rQ",
                "date": 1501690382,
                "description": null,
                "discount": null,
                "ending_balance": null,
                "forgiven": false,
                "lines": {
                  "object": "list",
                  "data": [
                    {
                      "id": "sub_8vxQ52b8iEEfIQ",
                      "object": "line_item",
                      "amount": 999,
                      "currency": "usd",
                      "description": null,
                      "discountable": true,
                      "livemode": false,
                      "metadata": {
                      },
                      "period": {
                        "start": 1501690382,
                        "end": 1504368782
                      },
                      "plan": {
                        "id": "quartz-professional-550",
                        "object": "plan",
                        "amount": 999,
                        "created": 1470154367,
                        "currency": "usd",
                        "interval": "month",
                        "interval_count": 1,
                        "livemode": false,
                        "metadata": {
                        },
                        "name": "Quartz Professional",
                        "statement_descriptor": null,
                        "trial_period_days": null
                      },
                      "proration": false,
                      "quantity": 1,
                      "subscription": null,
                      "subscription_item": "si_18e5Vq2eZvKYlo2CbGDHOrZK",
                      "type": "subscription"
                    }
                  ],
                  "has_more": false,
                  "total_count": 1,
                  "url": "/v1/invoices/in_1Ajs8F2eZvKYlo2CpN1Zmoj8/lines"
                },
                "livemode": false,
                "metadata": {
                },
                "next_payment_attempt": 1501693982,
                "paid": false,
                "period_end": 1501690382,
                "period_start": 1499011982,
                "receipt_number": null,
                "starting_balance": 0,
                "statement_descriptor": null,
                "subscription": "sub_8vxQ52b8iEEfIQ",
                "subtotal": 999,
                "tax": null,
                "tax_percent": null,
                "total": 999,
                "webhooks_delivered_at": null
              }
            },
            "livemode": false,
            "pending_webhooks": 0,
            "request": {
              "id": null,
              "idempotency_key": null
            },
            "type": "invoice.upcoming"
          }'),

      'customer_subscription_created' => json_decode('{
            "created": 1326853478,
            "livemode": false,
            "id": "evt_00000000000000",
            "type": "customer.subscription.created",
            "object": "event",
            "request": null,
            "pending_webhooks": 1,
            "api_version": null,
            "data": {
              "object": {
                "id": "sub_00000000000000",
                "object": "subscription",
                "application_fee_percent": null,
                "cancel_at_period_end": true,
                "canceled_at": 1501033859,
                "created": 1501033738,
                "current_period_end": 1503712138,
                "current_period_start": 1501033738,
                "customer": "cus_00000000000000",
                "discount": null,
                "ended_at": null,
                "items": {
                  "object": "list",
                  "data": [
                    {
                      "id": "si_1AjedyHkbHZB95BRVsHPdle4",
                      "object": "subscription_item",
                      "created": 1501033738,
                      "metadata": {
                      },
                      "plan": {
                        "id": "balanced",
                        "object": "plan",
                        "amount": 1000,
                        "created": 1498254907,
                        "currency": "usd",
                        "interval": "month",
                        "interval_count": 1,
                        "livemode": false,
                        "metadata": {
                        },
                        "name": "Balanced",
                        "statement_descriptor": "Mountain Myst",
                        "trial_period_days": null
                      },
                      "quantity": 1
                    }
                  ],
                  "has_more": false,
                  "total_count": 1,
                  "url": "/v1/subscription_items?subscription=sub_B5qKzq9Agt6dEj"
                },
                "livemode": false,
                "metadata": {
                },
                "plan": {
                  "id": "balanced_00000000000000",
                  "object": "plan",
                  "amount": 1000,
                  "created": 1498254907,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Balanced",
                  "statement_descriptor": "Mountain Myst",
                  "trial_period_days": null
                },
                "quantity": 1,
                "start": 1501033738,
                "status": "active",
                "tax_percent": null,
                "trial_end": null,
                "trial_start": null
              }
            }
          }'),

      'customer_subscription_deleted' => json_decode('{
            "created": 1326853478,
            "livemode": false,
            "id": "evt_00000000000000",
            "type": "customer.subscription.deleted",
            "object": "event",
            "request": null,
            "pending_webhooks": 1,
            "api_version": null,
            "data": {
              "object": {
                "id": "sub_B5qKzq9Agt6dEj",
                "object": "subscription",
                "application_fee_percent": null,
                "cancel_at_period_end": true,
                "canceled_at": 1501033859,
                "created": 1501033738,
                "current_period_end": 1503712138,
                "current_period_start": 1501033738,
                "customer": "cus_00000000000000",
                "discount": null,
                "ended_at": 1501098648,
                "items": {
                  "object": "list",
                  "data": [
                    {
                      "id": "si_1AjedyHkbHZB95BRVsHPdle4",
                      "object": "subscription_item",
                      "created": 1501033738,
                      "metadata": {
                      },
                      "plan": {
                        "id": "balanced",
                        "object": "plan",
                        "amount": 1000,
                        "created": 1498254907,
                        "currency": "usd",
                        "interval": "month",
                        "interval_count": 1,
                        "livemode": false,
                        "metadata": {
                        },
                        "name": "Balanced",
                        "statement_descriptor": "Mountain Myst",
                        "trial_period_days": null
                      },
                      "quantity": 1
                    }
                  ],
                  "has_more": false,
                  "total_count": 1,
                  "url": "/v1/subscription_items?subscription=sub_B5qKzq9Agt6dEj"
                },
                "livemode": false,
                "metadata": {
                },
                "plan": {
                  "id": "balanced_00000000000000",
                  "object": "plan",
                  "amount": 1000,
                  "created": 1498254907,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Balanced",
                  "statement_descriptor": "Mountain Myst",
                  "trial_period_days": null
                },
                "quantity": 1,
                "start": 1501033738,
                "status": "canceled",
                "tax_percent": null,
                "trial_end": null,
                "trial_start": null
              }
            }
          }'),
      
      'customer_subscription_updated' => json_decode('{
            "created": 1326853478,
            "livemode": false,
            "id": "evt_00000000000000",
            "type": "customer.subscription.updated",
            "object": "event",
            "request": null,
            "pending_webhooks": 1,
            "api_version": null,
            "data": {
              "object": {
                "id": "sub_B5qKzq9Agt6dEj",
                "object": "subscription",
                "application_fee_percent": null,
                "cancel_at_period_end": true,
                "canceled_at": 1501033859,
                "created": 1501033738,
                "current_period_end": 1503712138,
                "current_period_start": 1501033738,
                "customer": "cus_00000000000000",
                "discount": null,
                "ended_at": null,
                "items": {
                  "object": "list",
                  "data": [
                    {
                      "id": "si_1AjedyHkbHZB95BRVsHPdle4",
                      "object": "subscription_item",
                      "created": 1501033738,
                      "metadata": {
                      },
                      "plan": {
                        "id": "balanced",
                        "object": "plan",
                        "amount": 1000,
                        "created": 1498254907,
                        "currency": "usd",
                        "interval": "month",
                        "interval_count": 1,
                        "livemode": false,
                        "metadata": {
                        },
                        "name": "Balanced",
                        "statement_descriptor": "Mountain Myst",
                        "trial_period_days": null
                      },
                      "quantity": 1
                    }
                  ],
                  "has_more": false,
                  "total_count": 1,
                  "url": "/v1/subscription_items?subscription=sub_B5qKzq9Agt6dEj"
                },
                "livemode": false,
                "metadata": {
                },
                "plan": {
                  "id": "balanced_00000000000000",
                  "object": "plan",
                  "amount": 1000,
                  "created": 1498254907,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Balanced",
                  "statement_descriptor": "Mountain Myst",
                  "trial_period_days": null
                },
                "quantity": 1,
                "start": 1501033738,
                "status": "active",
                "tax_percent": null,
                "trial_end": null,
                "trial_start": null
              },
              "previous_attributes": {
                "plan": {
                  "id": "OLD_PLAN_ID",
                  "object": "plan",
                  "amount": 2500,
                  "created": 1498254959,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Old plan",
                  "statement_descriptor": null,
                  "trial_period_days": null
                }
              }
            }
          }'),

      'invoice_created' => json_decode('{
                              "created": 1326853478,
                              "livemode": false,
                              "id": "evt_00000000000000",
                              "type": "invoice.created",
                              "object": "event",
                              "request": null,
                              "pending_webhooks": 1,
                              "api_version": null,
                              "data": {
                                "object": {
                                  "id": "in_00000000000000",
                                  "object": "invoice",
                                  "amount_due": 1500,
                                  "application_fee": null,
                                  "attempt_count": 1,
                                  "attempted": false,
                                  "charge": "ch_00000000000000",
                                  "closed": true,
                                  "currency": "usd",
                                  "customer": "cus_00000000000000",
                                  "date": 1500348922,
                                  "description": null,
                                  "discount": null,
                                  "ending_balance": 0,
                                  "forgiven": false,
                                  "lines": {
                                    "data": [
                                      {
                                        "id": "sub_B5qKzq9Agt6dEj",
                                        "object": "line_item",
                                        "amount": 1000,
                                        "currency": "usd",
                                        "description": null,
                                        "discountable": true,
                                        "livemode": true,
                                        "metadata": {
                                        },
                                        "period": {
                                          "start": 1503712138,
                                          "end": 1506390538
                                        },
                                        "plan": {
                                          "id": "enlightened",
                                          "object": "plan",
                                          "amount": 2500,
                                          "created": 1498254959,
                                          "currency": "usd",
                                          "interval": "month",
                                          "interval_count": 1,
                                          "livemode": false,
                                          "metadata": {
                                          },
                                          "name": "Enlightened",
                                          "statement_descriptor": null,
                                          "trial_period_days": null
                                        },
                                        "proration": false,
                                        "quantity": 1,
                                        "subscription": null,
                                        "subscription_item": "si_1AjedyHkbHZB95BRVsHPdle4",
                                        "type": "subscription"
                                      }
                                    ],
                                    "total_count": 1,
                                    "object": "list",
                                    "url": "/v1/invoices/in_1AgmUYHkbHZB95BRnWM25UgX/lines"
                                  },
                                  "livemode": false,
                                  "metadata": {
                                  },
                                  "next_payment_attempt": null,
                                  "paid": true,
                                  "period_end": 1500348874,
                                  "period_start": 1497756874,
                                  "receipt_number": null,
                                  "starting_balance": 0,
                                  "statement_descriptor": null,
                                  "subscription": "sub_00000000000000",
                                  "subtotal": 1500,
                                  "tax": null,
                                  "tax_percent": null,
                                  "total": 1500,
                                  "webhooks_delivered_at": null
                                }
                              }
                            }'),
    
      'invoice_payment_failed' => json_decode('{
            "created": 1326853478,
            "livemode": false,
            "id": "evt_00000000000000",
            "type": "invoice.payment_failed",
            "object": "event",
            "request": null,
            "pending_webhooks": 1,
            "api_version": null,
            "data": {
              "object": {
                "id": "in_00000000000000",
                "object": "invoice",
                "amount_due": 1500,
                "application_fee": null,
                "attempt_count": 1,
                "attempted": true,
                "charge": "ch_00000000000000",
                "closed": false,
                "currency": "usd",
                "customer": "cus_00000000000000",
                "date": 1500348922,
                "description": null,
                "discount": null,
                "ending_balance": 0,
                "forgiven": false,
                "lines": {
                  "data": [
                    {
                      "id": "sub_B5qKzq9Agt6dEj",
                      "object": "line_item",
                      "amount": 1000,
                      "currency": "usd",
                      "description": null,
                      "discountable": true,
                      "livemode": true,
                      "metadata": {
                      },
                      "period": {
                        "start": 1503712138,
                        "end": 1506390538
                      },
                      "plan": {
                        "id": "enlightened",
                        "object": "plan",
                        "amount": 2500,
                        "created": 1498254959,
                        "currency": "usd",
                        "interval": "month",
                        "interval_count": 1,
                        "livemode": false,
                        "metadata": {
                        },
                        "name": "Enlightened",
                        "statement_descriptor": null,
                        "trial_period_days": null
                      },
                      "proration": false,
                      "quantity": 1,
                      "subscription": null,
                      "subscription_item": "si_1AjedyHkbHZB95BRVsHPdle4",
                      "type": "subscription"
                    }
                  ],
                  "total_count": 1,
                  "object": "list",
                  "url": "/v1/invoices/in_1AgmUYHkbHZB95BRnWM25UgX/lines"
                },
                "livemode": false,
                "metadata": {
                },
                "next_payment_attempt": null,
                "paid": false,
                "period_end": 1500348874,
                "period_start": 1497756874,
                "receipt_number": null,
                "starting_balance": 0,
                "statement_descriptor": null,
                "subscription": "sub_00000000000000",
                "subtotal": 1500,
                "tax": null,
                "tax_percent": null,
                "total": 1500,
                "webhooks_delivered_at": null
              }
            }
          }'),

      'invoice_payment_succeeded' => json_decode('{
              "created": 1326853478,
              "livemode": false,
              "id": "evt_00000000000000",
              "type": "invoice.payment_succeeded",
              "object": "event",
              "request": null,
              "pending_webhooks": 1,
              "api_version": null,
              "data": {
                "object": {
                  "id": "in_00000000000000",
                  "object": "invoice",
                  "amount_due": 1500,
                  "application_fee": null,
                  "attempt_count": 1,
                  "attempted": true,
                  "charge": "_00000000000000",
                  "closed": true,
                  "currency": "usd",
                  "customer": "cus_00000000000000",
                  "date": 1500348922,
                  "description": null,
                  "discount": null,
                  "ending_balance": 0,
                  "forgiven": false,
                  "lines": {
                    "data": [
                      {
                        "id": "sub_B5qKzq9Agt6dEj",
                        "object": "line_item",
                        "amount": 1000,
                        "currency": "usd",
                        "description": null,
                        "discountable": true,
                        "livemode": true,
                        "metadata": {
                        },
                        "period": {
                          "start": 1503712138,
                          "end": 1506390538
                        },
                        "plan": {
                          "id": "enlightened",
                          "object": "plan",
                          "amount": 2500,
                          "created": 1498254959,
                          "currency": "usd",
                          "interval": "month",
                          "interval_count": 1,
                          "livemode": false,
                          "metadata": {
                          },
                          "name": "Enlightened",
                          "statement_descriptor": null,
                          "trial_period_days": null
                        },
                        "proration": false,
                        "quantity": 1,
                        "subscription": null,
                        "subscription_item": "si_1AjedyHkbHZB95BRVsHPdle4",
                        "type": "subscription"
                      }
                    ],
                    "total_count": 1,
                    "object": "list",
                    "url": "/v1/invoices/in_1AgmUYHkbHZB95BRnWM25UgX/lines"
                  },
                  "livemode": false,
                  "metadata": {
                  },
                  "next_payment_attempt": null,
                  "paid": true,
                  "period_end": 1500348874,
                  "period_start": 1497756874,
                  "receipt_number": null,
                  "starting_balance": 0,
                  "statement_descriptor": null,
                  "subscription": "sub_00000000000000",
                  "subtotal": 1500,
                  "tax": null,
                  "tax_percent": null,
                  "total": 1500,
                  "webhooks_delivered_at": null
                }
              }
            }'),

      'invoice_payment_succeeded_upgrade' => json_decode('{
        "object": {
          "id": "in_1Al1IEHkbHZB95BRXTcFwbO3",
          "object": "invoice",
          "amount_due": 947,
          "application_fee": null,
          "attempt_count": 1,
          "attempted": true,
          "charge": "ch_1Al1IEHkbHZB95BR1sCcQe6Q",
          "closed": true,
          "currency": "usd",
          "customer": "cus_B6d8hyA5UA1HBA",
          "date": 1501359130,
          "description": "Mountain Myst Membership Upgrade",
          "discount": null,
          "ending_balance": 0,
          "forgiven": false,
          "lines": {
            "object": "list",
            "data": [
              {
                "id": "ii_1Al1IDHkbHZB95BRzqvNsO9p",
                "object": "line_item",
                "amount": -1419,
                "currency": "usd",
                "description": "Unused time on Holistic after 29 Jul 2017",
                "discountable": false,
                "livemode": false,
                "metadata": {
                },
                "period": {
                  "start": 1501359129,
                  "end": 1503893700
                },
                "plan": {
                  "id": "holistic",
                  "object": "plan",
                  "amount": 1500,
                  "created": 1498254942,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Holistic",
                  "statement_descriptor": null,
                  "trial_period_days": null
                },
                "proration": true,
                "quantity": 1,
                "subscription": "sub_B6d89yNefUPYS1",
                "subscription_item": "si_1AkPsOHkbHZB95BRBcuVkj9W",
                "type": "invoiceitem"
              },
              {
                "id": "ii_1Al1IDHkbHZB95BRZKhkAKRj",
                "object": "line_item",
                "amount": 2366,
                "currency": "usd",
                "description": "Remaining time on Enlightened after 29 Jul 2017",
                "discountable": false,
                "livemode": false,
                "metadata": {
                },
                "period": {
                  "start": 1501359129,
                  "end": 1503893700
                },
                "plan": {
                  "id": "enlightened",
                  "object": "plan",
                  "amount": 2500,
                  "created": 1498254959,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Enlightened",
                  "statement_descriptor": null,
                  "trial_period_days": null
                },
                "proration": true,
                "quantity": 1,
                "subscription": "sub_B6d89yNefUPYS1",
                "subscription_item": "si_1AkPsOHkbHZB95BRBcuVkj9W",
                "type": "invoiceitem"
              }
            ],
            "has_more": false,
            "total_count": 2,
            "url": "/v1/invoices/in_1Al1IEHkbHZB95BRXTcFwbO3/lines"
          },
          "livemode": false,
          "metadata": {
          },
          "next_payment_attempt": null,
          "paid": true,
          "period_end": 1501359130,
          "period_start": 1501215300,
          "receipt_number": null,
          "starting_balance": 0,
          "statement_descriptor": null,
          "subscription": "sub_B6d89yNefUPYS1",
          "subtotal": 947,
          "tax": null,
          "tax_percent": null,
          "total": 947,
          "webhooks_delivered_at": null
        }
      }'),

      'invoice_created_upgrade' => json_decode('{
        "object": {
          "id": "in_1Al1IEHkbHZB95BRXTcFwbO3",
          "object": "invoice",
          "amount_due": 947,
          "application_fee": null,
          "attempt_count": 0,
          "attempted": false,
          "charge": null,
          "closed": false,
          "currency": "usd",
          "customer": "cus_B6d8hyA5UA1HBA",
          "date": 1501359130,
          "description": "Mountain Myst Membership Upgrade",
          "discount": null,
          "ending_balance": null,
          "forgiven": false,
          "lines": {
            "object": "list",
            "data": [
              {
                "id": "ii_1Al1IDHkbHZB95BRzqvNsO9p",
                "object": "line_item",
                "amount": -1419,
                "currency": "usd",
                "description": "Unused time on Holistic after 29 Jul 2017",
                "discountable": false,
                "livemode": false,
                "metadata": {
                },
                "period": {
                  "start": 1501359129,
                  "end": 1503893700
                },
                "plan": {
                  "id": "holistic",
                  "object": "plan",
                  "amount": 1500,
                  "created": 1498254942,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Holistic",
                  "statement_descriptor": null,
                  "trial_period_days": null
                },
                "proration": true,
                "quantity": 1,
                "subscription": "sub_B6d89yNefUPYS1",
                "subscription_item": "si_1AkPsOHkbHZB95BRBcuVkj9W",
                "type": "invoiceitem"
              },
              {
                "id": "ii_1Al1IDHkbHZB95BRZKhkAKRj",
                "object": "line_item",
                "amount": 2366,
                "currency": "usd",
                "description": "Remaining time on Enlightened after 29 Jul 2017",
                "discountable": false,
                "livemode": false,
                "metadata": {
                },
                "period": {
                  "start": 1501359129,
                  "end": 1503893700
                },
                "plan": {
                  "id": "enlightened",
                  "object": "plan",
                  "amount": 2500,
                  "created": 1498254959,
                  "currency": "usd",
                  "interval": "month",
                  "interval_count": 1,
                  "livemode": false,
                  "metadata": {
                  },
                  "name": "Enlightened",
                  "statement_descriptor": null,
                  "trial_period_days": null
                },
                "proration": true,
                "quantity": 1,
                "subscription": "sub_B6d89yNefUPYS1",
                "subscription_item": "si_1AkPsOHkbHZB95BRBcuVkj9W",
                "type": "invoiceitem"
              }
            ],
            "has_more": false,
            "total_count": 2,
            "url": "/v1/invoices/in_1Al1IEHkbHZB95BRXTcFwbO3/lines"
          },
          "livemode": false,
          "metadata": {
          },
          "next_payment_attempt": 1501362730,
          "paid": false,
          "period_end": 1501359130,
          "period_start": 1501215300,
          "receipt_number": null,
          "starting_balance": 0,
          "statement_descriptor": null,
          "subscription": "sub_B6d89yNefUPYS1",
          "subtotal": 947,
          "tax": null,
          "tax_percent": null,
          "total": 947,
          "webhooks_delivered_at": null
        }
      }')
    ];
*/
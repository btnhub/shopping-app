<?php

return [

    /*
        Other Constants For Application
     */
    'recipe_units' => [
    		'oz' => 'oz',
    		'g' => 'g',
    		'ml' => 'ml',
    	],
    //Cutoff For Free Shipping
    'free_shipping_amount' => 75,
    'free_shipping_provider' => "Free Shipping",
    'free_shipping_servicelevel' => '',
    'free_shipping_days' => '',

    //Glass container cost
    'glass_cost' => 2, //$2 added to price if packaged in glass

    //Reorder frequency
    'reorder_frequency' => [15 => '15 Days', 30 => '30 Days', 45 => '45 Days', 60 => '60 Days']
];

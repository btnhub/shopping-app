<?php

return [

    /*
        Constants for Calculating Sales Tax & Use Tax
     */

    // 07/30/2020 - OLD!  NOT USED.  Used TaxState & TaxCity models instead
    //REF: http://www.taxrates.com/state-rates/colorado/cities/boulder/
    'state_nexus' => "CO",
    'state_tax_rate' => 2.9 / 100,
    'county_tax_rate' => 0.985 / 100, // collected by state in CO
    'city_tax_rate' => 3.86 / 100,
    'special_tax_rate' => 1.1 / 100,

    //Zip Codes Within City for local taxes
    'local_zip_codes' => [80301,80302,80303,80304,80305,80306,80307,80308,80309,80310,80314],
];

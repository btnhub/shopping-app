<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\StoreCartSession;

use Carbon;
use DB;
use Cart;

class RestoreCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;

        //Restore recent cart if current cart is empty
        if (!Cart::count()) {
            //Find stored cart that is less than 1 week old
            $stored_cart = DB::table('shoppingcart')
                                ->where('user_id', $user->id)
                                ->where('created_at', '>', Carbon\Carbon::now()->subWeek())
                                ->whereNull('order_id')
                                ->orderBy('created_at', 'desc')
                                ->first();

            if ($stored_cart) 
            {
                $identifier = $stored_cart->identifier;
                Cart::restore($identifier);
                session(['cart_identifier' => $identifier]);

                //Recreate destroyed cart row in shopping cart table
                dispatch(new StoreCartSession($identifier));
            }
        }
    }
}

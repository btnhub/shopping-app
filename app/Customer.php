<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class Customer extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Accessor to display last 4 digits of card
     * @return string 
     */
    public function getCardLastFourAttribute($value)
    {
        return sprintf("%04d", $value);
    }

    /**
     * Accessor to display exp month with leading zero
     * @return string 
     */
    public function getExpMonthAttribute($value)
    {
        return sprintf("%02d", $value);
    }

    /**
     * Relationships
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Methods
     */
    
    public function isCardValid(){
        $month = $this->exp_month;
        $year = $this->exp_year;

        $expiration_date = Carbon\Carbon::create($this->exp_year, $this->exp_month,15,23,59,59,'America/Denver');

        return $expiration_date->isFuture();
    }
}

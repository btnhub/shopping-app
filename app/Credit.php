<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Credit extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at', 'expiration_date', 'approval_date'];
    
    public function user(){
    	return $this->belongsTo('App\User', 'recipient_id');
    }

    public function provider(){
       return $this->belongsTo('App\User', 'provider_id'); 
    }
    /**
     * Orders where Credit was applied
     * @return [type] [description]
     */
    public function orders(){
    	return $this->belongsToMany('App\Order')
    				->whereNull('credit_order.deleted_at')
    				->withPivot('user_id', 'amount', 'notes', 'deleted_at')
                    ->withTimestamps();
    }

    /**
     * Balance of specific credit line
     * @return [type] [description]
     */
    public function balance()
    {
        if (!$this->isValid()) {
            return 0;
        }
        
        $credit_amount = $this->amount;
        $amount_used = $this->orders->sum('pivot.amount');

        $balance = $credit_amount - $amount_used;
        
        return number_format($balance, 2);
    }

    /**
     * Check if credit is valid
     */
    public function isValid(){
        if ($this->expiration_date->isPast() || !$this->approved) {
            return false;
        }
        return true;
    }

    public function createdByAdmin(){
        if ($this->provider && $this->provider->isAdmin()) {
            return true;
        }
        return false;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class PickupLocation extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['start_date', 'end_date', 'deleted_at'];

    /**
     * Relationships
     */
    
    /**
     * Methods
     */
    
    public function scopeCurrent($query)
    {   
        return $query->where('active', 1)
        				->where('start_date', '<', Carbon\Carbon::now())
        				->where('end_date', '>', Carbon\Carbon::now());
    }
}

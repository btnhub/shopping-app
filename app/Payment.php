<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class Payment extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function refunds(){
        return $this->hasMany('App\Refund');
    }

    public function collected_tax(){
        return $this->hasOne('App\CollectedTax');
    }
    /**
     * Methods
     */

    public function payment_method(){
        $payment_method = DB::table('payment_methods')->where('id', $this->payment_method_id)->first();

        return $payment_method->title;
    }

}


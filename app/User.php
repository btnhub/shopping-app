<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Role;
use App\Membership;
use Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'ref_id', 'first_name', 'last_name', 'email', 'phone', 'shipping_address', 'shipping_address_2', 'city', 'state', 'zip', 'country', 
                'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at', 'last_login_date'];

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Relationships
     */
    
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function customer(){
        return $this->hasOne('App\Customer');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function payments(){
        return $this->hasMany('App\Payment');
    }

    public function refunds(){
        return $this->hasMany('App\Refund');
    }

    public function memberships(){
        return $this->belongsToMany('App\Membership')
                        ->whereNull('membership_user.deleted_at')
                        ->withPivot('stripe_subscription_id', 'active', 'cancelled', 'price', 'frequency', 'start_date', 'end_date',  'current_period_start', 'current_period_end', 'pause_start_date', 'pause_end_date')
                        ->withTimestamps();
    }

    public function membership_payments(){
        return $this->hasMany('App\MembershipPayment');
    }

    public function wishlist(){
        return $this->belongsToMany('App\Product')->withTimestamps();
    }

    public function recurring_orders(){
        return $this->hasMany('App\RecurringOrder');
    }

    public function credits(){
        return $this->hasMany('App\Credit', 'recipient_id');
    }

    public function approved_credits(){
        return $this->hasMany('App\Credit', 'recipient_id')
                        ->where('approved', 1);
    }

    public function referrals(){
        return $this->hasMany('App\Credit', 'recipient_id')
                        ->where('type', 'referral');
    }

    public function gift_cards(){
        return $this->hasMany('App\Credit', 'recipient_id')
                        ->where('type', 'gift_card');
    }

    /**
     * Methods
     */
    
    public function recentOrders()
    {
        return $this->orders->sortByDesc('id');
    }

    public function hasRole($roles) 
    { 
        if (is_array($roles)) {
            foreach ($roles as $role) 
            {
                if ($this->roles->contains('role', $role))
                    return true;
            }
        }
        
        return $this->roles->contains('role', $roles);
    }

    public function isAdmin()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }

        return $this->hasRole(['super-user', 'admin']);
    }

    public function addRole($role) 
    { 
        if (!$this->hasRole($role)) {
            return $this->roles()->save(Role::whereRole($role)->firstOrFail());     
        }  
    }

    public function removeRole($role) 
    { 
        if ($this->hasRole($role)) {
            $role_id = Role::whereRole($role)->firstOrFail()->id;
            return $this->roles()->detach($role_id);     
        }  
    }

    /**
     * Return the shipping data for a user. Source: https://dotdev.co/easily-handle-shipping-in-laravel-with-shippo-12055c903704#.z03s1yi2a
     * 
     * @return array
     */
    public function shippingAddress()
    {
        return [
            'name' => $this->name,
            /*'company' => $this->company,*/
            'street1' => $this->shipping_address,
            'street2' => $this->shipping_address_2,
            'city' => $this->city,
            'state' => $this->state,
            'zip' => $this->zip,
            'country' => $this->country,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }

    /**
     * Calculates the available balance form referral credits
     * @return number $balance calculated balance
     */
    
    public function credit_balance($credit_type = null){
        $balance = $total_earned = $total_used = 0;

        $credits = $this->approved_credits
                        ->filter(function($credit){
                                return $credit->isValid();
                            });

        if ($credit_type == null){
            //Sum all types of approved credits
            $total_earned = $credits->sum('amount');
            
            foreach ($credits as $credit) {
                $total_used += $credit->orders->sum('pivot.amount');
            }
        }
        
        else {
            $specified_credits = $credits->where('type', $credit_type);

            $total_earned = $specified_credits->sum('amount');
            
            foreach ($specified_credits as $specified_credit) {
                $total_used += $specified_credit->orders->sum('pivot.amount');           
            }
        }

        $balance = $total_earned - $total_used;

        return number_format($balance, 2);
    }

    public function best_membership(){
        $active_memberships = $this->memberships->filter(function ($membership){      
                                    //return only active memberships
                                    return $membership->active();
                            });

        $best_membership = $active_memberships->sortByDesc('discount')->first();

        return $best_membership;
    }

    public function current_membership($stripe_subscription_id){
        if ($stripe_subscription_id == null) {
            return null;
        }

        //Call Stripe to get the users current membership
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        
        try{
            $subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

            $matching_memberships = $this->memberships
                            ->where('stripe_plan', $subscription->plan->id)
                            ->filter(function ($membership) use ($stripe_subscription_id){      
                                    //return memberships with matching subscription id
                                    return $membership->pivot->stripe_subscription_id == $stripe_subscription_id;
                            });

            return $matching_memberships->first();

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();
        }
    }
}

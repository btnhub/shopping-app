<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Refund extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function payment(){
        return $this->belongsTo('App\Payment');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}

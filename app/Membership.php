<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class Membership extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */

    public function products(){
        return $this->belongsToMany('App\Item');
    }

    public function users(){
        return $this->belongsToMany('App\User')
                        ->whereNull('membership_user.deleted_at')
                        ->withPivot('stripe_subscription_id', 'active', 'cancelled', 'price', 'frequency', 'start_date', 'end_date',  'current_period_start', 'current_period_end', 'pause_start_date', 'pause_end_date')
                        ->withTimestamps();
    }

    public function membership_payments(){
        return $this->hasMany('App\MembershipPayment');
    }


    public function active(){
        //Whether a membership is currently active (and paid for)
        if ($this->pivot->active
                && $this->pivot->start_date < Carbon\Carbon::now()
                && ($this->pivot->end_date > Carbon\Carbon::now() || $this->pivot->end_date == NULL)

                && ($this->pivot->current_period_start < Carbon\Carbon::now() || $this->pivot->current_period_start == NULL)
                && ($this->pivot->current_period_end > Carbon\Carbon::now() || $this->pivot->current_period_end == NULL)) {

            if (!$this->isStudentMembership()) {
                //if plan isn't a student membership, then return true
                return true;
            }

            elseif ($this->isStudentMembership() && auth()->user()->hasRole('student')) {
                //If a student plan, make sure user is a student
                return true;
            }
            
        }

        return false;

    }

    public function isStudentMembership()
    {
        if (stripos($this->name, "student") !== false) {
                return true;
            }

        return false;
    }
}

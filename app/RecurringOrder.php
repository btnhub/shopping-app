<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Parcel;
use App\Shipping;
use App\CollectedTax;

class RecurringOrder extends Model
{
     use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at', 'start_date', 'end_date', 'last_charge', 'next_charge'];

    /**
     * Relationships
     */
    
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function items(){
        return $this->belongsToMany('App\Item')
                    ->whereNull('item_recurring_order.deleted_at')
                    ->withPivot('price', 'quantity', 'details', 'organic')
                    ->withTimestamps();
    }

    public function subtotal_cost(){
        $subtotal_cost = 0;
        
        foreach ($this->items as $recurring_item) {
            
            $subtotal_cost += $recurring_item->pivot->quantity * $recurring_item->pivot->price;
        }

        $membership = $this->user->best_membership();
        if ($membership) {
            $member_discount = $membership->discount;
            $subtotal_cost *= (1-$member_discount);
        }
        
        return number_format($subtotal_cost, 2);
    }

    public function shipping_price()
    {
        if (($this->subtotal_cost() >= config('other_constants.free_shipping_amount') || ($this->user->best_membership() && $this->user->best_membership()->free_shipping)) && in_array($this->user->country, ['US']) && !in_array($this->user->state, ['AK', 'HI'])) {
            return 0;
        }

        return $this->shipping_cost;
    }

    public function tax(){
        $tax_model = new CollectedTax();
        
        $user = $this->user;
        $subtotal = $this->subtotal_cost();
        $shipping_cost = $this->shipping_cost;
        $taxable_sales = $subtotal + $shipping_cost;

        $taxes_array = $tax_model->calculate_taxes($taxable_sales, $user->state, $user->zip);
        
        return $taxes_array['combined_tax'];
    }

    public function total_cost(){
        return number_format($this->subtotal_cost() + $this->shipping_price() + $this->tax(), 2);
    }

    public function shipping_details(){
        //NOT USED (08/15/17)
        //If subtotal is more than Free Shipping Cutoff
        if ($this->subtotal_cost() >= config('other_constants.free_shipping_amount')) {
            
            return (object)['provider' => config('other_constants.free_shipping_provider'),
                    'servicelevel_name' => config('other_constants.free_shipping_servicelevel'),
                    'amount' => 0];
        }

        $user = $this->user;
        $cart_items = collect([]);
        
        //build collection of items for order
        foreach ($this->items as $item) {
                $cart_items->push(unserialize($item->pivot->details));
        }

        $order = new Order ([
                "user_id" => $user->id,
                "first_name" => $user->first_name,
                "last_name" => $user->last_name,
                "email" => $user->email,
                "phone" => $user->phone,
                "shipping_address" => $user->shipping_address,
                "shipping_address_2" => $user->shipping_address_2,
                "city" => $user->city,
                "state" => $user->state,
                "zip" => $user->zip,
                "country" => $user->country,
                "details" => serialize($cart_items)
            ]);

        $parcel = new Parcel;
        $shipping = new Shipping;
        $shippo = $parcel->shippingRates($order, $cart_items);

        return $shipping->shipping_estimate($shippo->rates);
    }
}

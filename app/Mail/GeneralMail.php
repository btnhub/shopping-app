<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class GeneralMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $subject;
    public $email_view;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $subject, $email_view)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->email_view = $email_view;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->view($this->email_view);
    }
}

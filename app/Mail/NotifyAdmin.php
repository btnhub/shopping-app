<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $email_view;
    public $admin_message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $email_view, $admin_message)
    {
        $this->subject = $subject;
        $this->email_view = $email_view;
        $this->admin_message = $admin_message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->view($this->email_view)
                    ->with(['admin_message' => $this->admin_message]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Credit;

class CreditsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $credit;
    public $subject;
    public $email_view;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Credit $credit, $subject, $email_view)
    {
        $this->credit = $credit;
        $this->subject = $subject;
        $this->email_view = $email_view;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->view($this->email_view);
    }
}

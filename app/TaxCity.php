<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class TaxCity extends Model
{
	use SoftDeletes;
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function tax_state(){
        return $this->belongsTo('App\TaxState');
    }

    public function collected_taxes(){
        return $this->hasMany('App\CollectedTax');
    }
    
    /**	
     * Methods
     */
    
    public function scopeActive($query){
    	return $query->where('start_date', '<=', Carbon\Carbon::now())
    					->where(function ($q) {
    						$q->whereNull('end_date')
    							->orWhere('end_date', '>', Carbon\Carbon::now());
    					});
    }
}

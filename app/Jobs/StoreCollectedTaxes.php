<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

use Exception;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyAdmin;

use App\CollectedTax;
use App\Order;

class StoreCollectedTaxes
{
    use Dispatchable, Queueable;

    public $order;
    public $taxable_sales;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, $taxable_sales)
    {
        $this->order = $order;
        $this->taxable_sales = $taxable_sales;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tax_model = new CollectedTax();
        $order = $this->order;
        $taxable_sales = $this->taxable_sales;
        
        $collected_taxes = $tax_model->calculate_taxes($taxable_sales, $order->state, $order->zip);

        CollectedTax::create([
            'payment_id' => $order->payment->id,
            'tax_state_id' => $collected_taxes['tax_state_id'],
            'tax_city_id' => $collected_taxes['tax_city_id'],
            'taxable_sales' => $taxable_sales,
            'state_tax' => $collected_taxes['state_tax'],
            'county_tax' => $collected_taxes['county_tax'],
            'city_tax' => $collected_taxes['city_tax'],
            'special_tax' => $collected_taxes['special_tax']
            ]);
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $tax_model = new CollectedTax();
        $order = $this->order;
        $taxable_sales = $this->taxable_sales;

        $collected_taxes = $tax_model->calculate_taxes($taxable_sales, $order->state, $order->zip);

        $state_tax = $collected_taxes['state_tax'];
        $county_tax = $collected_taxes['county_tax'];
        $city_tax = $collected_taxes['city_tax'];
        $special_tax = $collected_taxes['special_tax'];

        $subject = "StoreCollectedTaxes Failed";
        $email_view = 'emails.notify_admin';
        $admin_message = "
                CollectedTax::create([
                    'payment_id' => {$order->payment->id}, <br>
                    'taxable_sales' => $taxable_sales, <br>
                    'state_tax' => $state_tax, <br>
                    'county_tax' => $county_tax, <br>
                    'city_tax' => $city_tax, <br>
                    'special_tax' => $special_tax<br>
                    ]);";

        Mail::to(config('mail.from.address'))->send(new NotifyAdmin($subject, $email_view, $admin_message));
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Exception;
use Illuminate\Support\Facades\Mail;

use App\Mail\CreditsMail;

use App\Credit;

class SendCreditsMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $credit;
    public $send_to_email;
    public $subject;
    public $email_view;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Credit $credit, $send_to_email, $subject, $email_view)
    {
        $this->credit = $credit;
        $this->send_to_email = $send_to_email;
        $this->subject = $subject;
        $this->email_view = $email_view; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        Mail::to($this->send_to_email)->send(new CreditsMail($this->credit, $this->subject, $this->email_view));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $admin_subject = "SendGeneralMail Failed: To: $this->send_to_email, Subject: $this->subject";

        Mail::to(config('mail.from.address'))->send(new CreditsMail($this->credit, $admin_subject, $this->email_view));
    }
}

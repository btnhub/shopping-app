<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Exception;
use Illuminate\Support\Facades\Mail;

use App\Mail\GeneralMail;

use App\User;

class SendGeneralMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $subject;
    public $email_view;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $subject, $email_view)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->email_view = $email_view; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->email)->send(new GeneralMail($this->user, $this->subject, $this->email_view));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $admin_subject = "SendGeneralMail Failed: To: $user->email, Subject: $this->subject";


        Mail::to(config('mail.from.address'))->send(new GeneralMail($this->user, $admin_subject, $this->email_view));
    }
}

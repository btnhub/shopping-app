<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

use Cart;
use DB;
use Carbon;

class StoreCartSession
{
    use Dispatchable, Queueable;

    public $identifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $identifier = $this->identifier;
        $cart_exists = DB::table('shoppingcart')->where('identifier', $identifier)->first();

        if (!Cart::count() && $cart_exists) {
            //When Cart is emptied, delete from database
            DB::table('shoppingcart')->where('identifier', $identifier)->delete();
        }

        else {
            $user_id = $email = $first_name = $last_name = null;
            $user = auth()->user() ?? null;

            if ($user) {
                $user_id = $user->id;
                $email = $user->email;
                $first_name = $user->first_name;
                $last_name = $user->last_name;
            }

            if (session('email')) {
                $email = session('email');
                $first_name = session('first_name') ?? $first_name;
                $last_name = session('last_name') ?? $last_name;
            }


            if ($cart_exists) {
                DB::table('shoppingcart')
                    ->where('identifier', $identifier)
                    ->update([
                        'user_id' => $user_id,
                        'email' => $email,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'content' => serialize(Cart::content()),
                        'updated_at' => Carbon\Carbon::now()
                        ]);             
            }

            else
            {
               Cart::store($identifier);

                //Add Customer Info to stored cart in database table
                DB::table('shoppingcart')
                    ->where('identifier', $identifier)
                    ->update([
                        'user_id' => $user_id,
                        'email' => $email,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'created_at' => Carbon\Carbon::now(),
                        'updated_at' => Carbon\Carbon::now()
                        ]); 
            }       
        }
    }
}

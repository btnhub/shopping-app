<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Exception;
use Illuminate\Support\Facades\Mail;

use App\Mail\NotifyAdmin;

class SendAdminEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $subject;
    public $email_view;
    public $admin_message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subject, $email_view, $admin_message)
    {
       $this->subject = $subject;
       $this->email_view = $email_view; 
       $this->admin_message = $admin_message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(config('mail.from.address'))->send(new NotifyAdmin($this->subject, $this->email_view, $this->admin_message));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Mail::to(config('mail.from.address'))->send(new NotifyAdmin("$this->subject (SendAdmin Failed Job)", $this->email_view, $this->admin_message));
    }
}

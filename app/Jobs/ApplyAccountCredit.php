<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

use Exception;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyAdmin;

use App\Credit;
use App\Order;

use DB;
use Carbon;

class ApplyAccountCredit
{
    use Dispatchable, Queueable;

    public $order;
    public $order_balance;
    public $taxable_sales;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, $order_balance, $taxable_sales)
    {
        $this->order = $order;
        $this->order_balance = $order_balance;
        $this->taxable_sales = $taxable_sales;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;
        $order_balance = $this->order_balance;
        $taxable_sales = $this->taxable_sales;

        $credit_applied = 0;

        if ($order->user) {
            //If User is logged in, find all credits
            $credits = $order->user->approved_credits->sortBy('created_at');   
        }

        else
        {
            // if user not logged in, find gift card & referral codes submitted
            $credits = Credit::whereIn('credit_code', session('credit_code'))->get()->sortBy('created_at');
        }   

        foreach ($credits as $credit) {
            if ($credit->balance() > 0 && $order_balance > 0) {
                if ($order_balance >= $credit->balance()) {
                    //Completely use up credit if Order balance is greater than the amount of the individual credit line
                    $order_balance -= $credit->balance();
                    $credit_applied += $credit->balance();

                    DB::table('credit_order')->insert(['order_id' => $order->id,
                                                        'credit_id' => $credit->id,
                                                        'user_id' => $order->user_id,
                                                        'amount' => $credit->balance(),
                                                        'created_at' => Carbon\Carbon::now(),
                                                        'updated_at' => Carbon\Carbon::now()
                                                        ]);

                    //If credit is not taxable, adjust taxable sales to new order balance
                    if (!$credit->taxable) {
                        $taxable_sales = $order_balance;    
                    }
                    
                }

                else {
                    $credit_applied += $order_balance;

                    DB::table('credit_order')->insert(['order_id' => $order->id,
                                                        'credit_id' => $credit->id,
                                                        'user_id' => $order->user_id,
                                                        'amount' => $order_balance,
                                                        'created_at' => Carbon\Carbon::now(),
                                                        'updated_at' => Carbon\Carbon::now()
                                                        ]);

                    $order_balance = 0;

                    //If credit is not taxable, adjust taxable sales to new order balance
                    if (!$credit->taxable) {
                        $taxable_sales = $order_balance;    
                    }
                }
            }
        }

        return ["credit_applied" => $credit_applied,
                "order_balance" => $order_balance,
                "taxable_sales" => $taxable_sales];
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $order = $this->order;
        $order_balance = $this->order_balance;
        $taxable_sales = $this->taxable_sales;

        if ($order->user) {
            //If User is logged in, find all credits
            $credits = $order->user->approved_credits->sortBy('created_at');   
        }

        else
        {
            // if user not logged in, find gift card & referral codes submitted
            $credits = Credit::whereIn('credit_code', session('credit_code'))->get()->sortBy('created_at');
        }

        $credit_available = 0;

        foreach ($credits as $credit) {
            if ($credit->approved) {
                $credit_available += $credit->balance();
            }
        }

        $subject = "ApplyAccountCredit Failed";
        $email_view = 'emails.notify_admin';
        $admin_message = "
                <b>Order #:</b> $order->id ($order->ref_id) <br>
                <b>Customer Name:</b> $order->full_name <br>
                <b>Customer Email:</b> $order->email <br>
                <b>Order Balance:</b> $$order_balance <br>
                <b>Credit Balance:</b> $credit_available<br>
                <b>Taxable Sales:</b> $taxable_sales";

        Mail::to(config('mail.from.address'))->send(new NotifyAdmin($subject, $email_view, $admin_message));
    }
}

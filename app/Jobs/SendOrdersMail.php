<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Exception;
use Illuminate\Support\Facades\Mail;

use App\Mail\OrdersMail;

use App\Order;

class SendOrdersMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;
    public $cart_items;
    public $send_to;
    public $subject;
    public $email_view;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, $cart_items, $send_to, $subject, $email_view)
    {
        $this->order = $order;
        $this->cart_items = $cart_items;
        $this->send_to = $send_to;
        $this->subject = $subject;
        $this->email_view = $email_view; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->send_to)->send(new OrdersMail($this->order, $this->cart_items, $this->subject, $this->email_view));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $admin_subject = "SendOrdersMail Failed: To: {$this->order->email}, Subject: $this->subject";

        Mail::to(config('mail.from.address'))->send(new OrdersMail($this->order, $this->cart_items, $admin_subject, $this->email_view));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

use Carbon;

class Product extends Model
{
    use SoftDeletes;
    use Searchable;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];


    /**
     * SCOUT / ALGOLIA: Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $product_array = [
            'name' => $this->name,
            'ref_id' => $this->ref_id,
            'description' => $this->description,
            'website_description' => $this->website_description,
            'detailed_description' => $this->detailed_description,
            'subcategory' => $this->subcategory->name,
            'category' => $this->subcategory->category->name,
            'available' => $this->available(),
            ];

        if ($this->recipe) {
            $product_array['ingredients'] = $this->recipe->ingredients->map(function ($ingredient) {
                             return $ingredient['name'];
                           })->toArray();
        }
        

        return $product_array;
    }

    /**
     * Products visible on the front end
     * Must be set to visible and have items that are visible
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeFrontVisible($query)
    {
        return $query->where('is_visible', 1)
                        ->whereHas('subcategory', function($subcat){
                            $subcat->where('is_visible', 1);
                        })
                        ->whereHas('items', function($item){
                            $item->frontVisible();
                        });
    }

    /**
     * Products that can be featured on the homepage
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeFeatured($query)
    {
        return $query->frontVisible()
                    ->where('featured', 1)
                    ->whereNotNull('image');
    }

    /**
     * Relationships
     */
    
    public function subcategory(){
        return $this->belongsTo('App\SubCategory');
    }

    public function items(){
        return $this->hasMany('App\Item');
    }

    public function memberships(){
        return $this->belongsToMany('App\Membership');
    }

    public function wishlists(){
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function recipe(){
        return $this->hasOne('App\Recipe');
    }


    /**
     * Methods
     */
    
    public function new(){
        if ($this->created_at > Carbon\Carbon::now()->subDays(180)->toDateString()){
            return true;    
        }
        
        return false;
    }

    public function available()
    {
        $count = 0;

        if ($this->items->count()) {
            //If product has items and items are visible
            $count = $this->items->filter(function ($item){
                            return $item->available();
                        })->count();   

        }

        if ($count > 0 && $this->is_visible && $this->in_stock){
            return true;
        }
        
        return false;
        
    }

    /**
     * Method for determing similar products 
     * or other products the user might like
     * @return [type] [description]
     */
    public function related_products(){
        return $this->subcategory->products->where('id', '!=', $this->id)->shuffle()->take(6);
    }

    public function classification(){
        $recipe = $this->recipe;

        $classifications = [];

        if ($recipe) 
        {
            
            $recipe->vegan() ? array_push($classifications, 'Vegan') : '';
        
            $recipe->gluten_free() ?array_push($classifications, 'Gluten Free') :'';
    
            $recipe->nut_free() ? array_push($classifications, 'Nut Free') : '';
    
            $recipe->soy_free() ? array_push($classifications, 'Soy Free') :'';
    
            $recipe->dairy_free() ? array_push($classifications, 'Dairy Free') : '';    
    
            return $classifications;            
        }

        else
        {
            return false;
        }
    }

    public function ingredient_list(){
        foreach ($this->recipe->ingredients as $ingredient) {
            if ($ingredient->pivot->organic) {
                $ingredient->name .= "*";
            }
        }
        return ucwords($this->recipe->ingredients->implode('name', ', '));
    }

    public function contains_organic_ingredient(){
        foreach ($this->recipe->ingredients as $ingredient) {
            if ($ingredient->pivot->organic) {
                return true;
            }
        }
        return false;
    }
}

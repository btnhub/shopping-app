<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class RawMaterial extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }


	/**
	 * Estimate cost of producing a product.  Only accounts for Ingredient and packaging cost
	 * Input: $ingredients (model collection), 
	 * @return [type] [description]
	 */
	
	public function productPriceCalculator(RawMaterial $raw_materials, Packaging $packaging)
    {
        
    }    

}

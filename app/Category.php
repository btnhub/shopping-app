<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function subcategories(){
        return $this->hasMany('App\SubCategory');
    }

    /**
     * Products visible on the front end
     * Must be set to visible and have subcategories that are visible
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeFrontVisible($query)
    {
        return $query->where('is_visible', 1)
                        ->whereHas('subcategories', function($subcategory){
                            $subcategory->frontVisible();
                        });
    }

}

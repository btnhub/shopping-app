<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
	use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function rawMaterials(){
        return $this->hasMany('App\RawMaterial');
    }

    public function purchases(){
        return $this->hasMany('App\Purchase');
    }
}

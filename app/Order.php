<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }
    
    /**
     * Relationships
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function payment(){
        return $this->hasOne('App\Payment');
    }

    public function refunds(){
        return $this->hasMany('App\Refund');
    }

    public function credits(){
        return $this->belongsToMany('App\Credit')
                    ->whereNull('credit_order.deleted_at')
                    ->withPivot('user_id', 'amount', 'notes', 'deleted_at')
                    ->withTimestamps();
    }

    public function scopePending($query){
        return $query->where('cancelled', 0)
                        ->where('shipped', 0)
                        ->whereHas('payment', function($q){
                                    return $q->where('paid', 1);}
                            );
    }   
    //Might Not Need Since Shippo Installed
    /*public function shipping_method(){
        return $this->belongsTo('App\ShippingMethod');
    }*/

    public function shipment(){
        return $this->hasOne('App\Shipment');
    }
    /**
     * Methods
     */
    
    /**
     * Return the shipping data for a payment. 
     * Source: https://dotdev.co/easily-handle-shipping-in-laravel-with-shippo-12055c903704#.z03s1yi2a
     * 
     * @return array
     */
    public function shippingAddress()
    {
        return [
            'name' => $this->full_name,
            /*'company' => $this->company,*/
            'street1' => $this->shipping_address,
            'street2' => $this->shipping_address_2,
            'city' => $this->city,
            'state' => $this->state,
            'zip' => $this->zip,
            'country' => $this->country,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }
}

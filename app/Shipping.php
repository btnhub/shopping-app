<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Shippo;
use App\Order;

use Shippo_Address;
use Shippo_Shipment;
use Shippo_Transaction;

class Shipping extends Model
{
    //Source: https://dotdev.co/easily-handle-shipping-in-laravel-with-shippo-12055c903704#.z03s1yi2a
    //https://github.com/dotdevco/laravel-shippo
    

    protected $fromAddress = [
        'name' => 'Mosaikz LLC',
        'company' => 'Mountain Myst',
        'street1' => '201 Columbine St #6883',
        'city' => 'Denver',
        'state' => 'CO',
        'zip' => '80206',
        'country' => 'US',
        'phone' => '765-543-7542',
        'email' => 'Contact@Mountain-Myst.com',
    ];

    public function __construct()
    {
        // Grab this private key from
        // .env and setup the Shippo api
        Shippo::setApiKey(config('services.shippo.key'));
    }

    /**
	 * Validate an address through Shippo service
	 *
	 * @param User $user
	 * @return Shippo_Adress
	 */
	public function validateAddress(Order $order)
	{
	    // Grab the shipping address from the User model
	    $toAddress = $order->shippingAddress();
	    // Pass a validate flag to Shippo
	    $toAddress['validate'] = true;
        /*$toAddress['test'] = true;*/
	    // Verify the address data
	    return Shippo_Address::create($toAddress);
	}

	/**
     * Create a Shippo shipping rates
     *
     * @param User $user
     * @param Product $product
     * @return Shippo_Shipment
     */
    public function rates(Order $order, Parcel $parcel)
    {
        // Grab the shipping address from the User model
        $toAddress = $order->shippingAddress();
        
        $parcel_dimensions = array(
        					'length' =>$parcel->length, 
        					'width' =>$parcel->width, 
        					'height' => $parcel->height, 
        					'distance_unit' => $parcel->distance_unit, 
        					'weight' => $parcel->weight, 
        					'mass_unit' => $parcel->mass_unit);
        
        // Get the shipment object
        return Shippo_Shipment::create([
                'address_from'=> $this->fromAddress,
                'address_to'=> $toAddress,
                'parcels'=> $parcel_dimensions,
                /*'extra' => 
                    ['insurance' => ['amount'=> '100',
                                    'currency'=> 'USD']],*/
                'async'=> false
        ]);
    }

    /**
     * Create the shipping label transaction
     *
     * @param $rateId -- object_id from rates_list
     * @return Shippo_Transaction
     */
    public function createLabel($rateId)
    {
        return Shippo_Transaction::create([
            'rate' => $rateId,
            'label_file_type' => "PDF",
            'async' => false
        ]);
    }

    /**
     * Find and return cost of FedEx Ground or USPS Priority Mail
     * Input = $rates array from Shippo
     */
    
    public function shipping_estimate($rates){
        $rates_collection = collect([]);

        foreach ($rates as $rate) {
            $rates_collection->push((object)['provider' => $rate->provider, 
                                    'servicelevel_name' => $rate->servicelevel->name,
                                    'duration_terms' => $rate->duration_terms,
                                    'amount' => $rate->amount]);
        }

        $shipping_fedex = $rates_collection->where('provider', 'FedEx')->where('servicelevel_name', 'Ground')->first();

        $shipping_usps = $rates_collection->where('provider', 'USPS')->where('servicelevel_name', 'Priority Mail')->first();

        $shipping_details = [];

        if ($shipping_fedex) {
            $shipping_details = (object)['provider' => "FedEx",
                                    'servicelevel_name' => "Ground",
                                    'amount' => $shipping_fedex->amount];
        }
        elseif ($shipping_usps) {
            $shipping_details = (object)['provider' => "USPS",
                                    'servicelevel_name' => "Priority Mail",
                                    'amount' => $shipping_usps->amount];
        }
        else{
            //charge 10 if rate not found
            $shipping_details = (object)['provider' => "",
                                    'servicelevel_name' => "",
                                    'amount' => 10];
        }

        return $shipping_details;
    }
}

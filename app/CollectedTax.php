<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\TaxState;
use App\TaxCity;

class CollectedTax extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function payment(){
        return $this->belongsTo('App\Payment');
    }

    public function tax_state(){
        return $this->belongsTo('App\TaxState');
    }

    public function tax_city(){
        return $this->belongsTo('App\TaxCity');
    }

    /**
     * Calculating Required Taxes
     * returns array
     */

    public static function calculate_taxes($taxable_sales, $shipping_state, $shipping_zipcode = null){
    	$taxable_sales = (float)str_replace(",", "", $taxable_sales);
        $shipping_state = strtoupper($shipping_state);
        $shipping_zipcode = substr($shipping_zipcode, 0, 5);  //return first 5 characters of zipcode (allows 9 digit zipcode 12345-9876)
    	
    	$combined_tax = 0;
    	$state_tax = 0;
    	$county_tax = 0;
		$city_tax = 0;
		$special_tax = 0;
        $tax_state_id = null;
        $tax_city_id = null;

        $tax_states = TaxState::active()->get();
        $nexus_states = $tax_states->pluck('state')->toArray();

        if (in_array($shipping_state, $nexus_states)) {
            $tax_state = $tax_states->where('state', $shipping_state)->first();
            $tax_state_id = $tax_state->id;

            $state_tax = $taxable_sales * ($tax_state->rate / 100);

            if ($shipping_zipcode) {
                $tax_city = TaxCity::active()->where('tax_state_id', $tax_state->id)
                                    ->where('zip_codes', 'like', "%$shipping_zipcode%")
                                    ->first();
                if($tax_city){
                    $tax_city_id = $tax_city->id;
                    $county_tax = $taxable_sales * ($tax_city->county_rate / 100);
                    $city_tax = $taxable_sales * ($tax_city->city_rate / 100);
                    $special_tax = $taxable_sales * ($tax_city->special_rate / 100);
                }
            }
            $combined_tax = $state_tax + $county_tax + $city_tax + $special_tax;
        }
    	
    	return ['combined_tax' => round($combined_tax, 2),
    			'state_tax' => round($state_tax, 2),
    			'county_tax' => round($county_tax, 2),
    			'city_tax' => round($city_tax, 2),
    			'special_tax' => round($special_tax, 2),
                'tax_state_id' => $tax_state_id,
                'tax_city_id' => $tax_city_id];
    }
}

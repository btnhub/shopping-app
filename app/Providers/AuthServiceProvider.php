<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        /**
         * Added: By Pass Gate checks if user is Super User
         */
        Gate::before(function($user, $ability)
            {
                if($user->hasRole('super-user'))
                    { 
                        return true;
                    }
            });

        /**
         * Admin / Super User access
         */
        Gate::define('admin-only', function($user)
        {
            if ($user->hasRole(['super-user', 'admin']) && !$user->hasRole('banned')) {
                return true;
            }
            
            \Session::flash('error', "You are not authorized to view this page");
            return redirect('/');
        });

        /**
         * User can access own data in the Users Table
         */
        Gate::define('self-access', function($user, $object)
        {
            if ($user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->id;
        });

        /**
         * Any non-banned users can access/edit this page
         */
        Gate::define('user-access', function($user, $object)
        {
            if ($user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->user_id;
        });
    }
}

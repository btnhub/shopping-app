<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Validator;

use App\ReCaptcha;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            $recaptcha = new Recaptcha;
            return $recaptcha->verify($value);
        });

        //View Composer to load values every time the topmenu layout is loaded
        view()->composer('layouts.organica.partials.topmenu', function($view){
                $view->with('categories', \App\Category::frontVisible()->wherehas('subcategories', function ($subcategory)
                                                        {
                                                            return $subcategory->has('products');
                                                        })
                                                        ->orderBy('website_order')
                                                        ->orderBy('id')
                                                        ->get());
        });
        view()->composer('layouts.organica.partials.topmenu_mobile', function($view){
                $view->with('categories', \App\Category::frontVisible()->wherehas('subcategories', function ($subcategory)
                                                        {
                                                            return $subcategory->has('products');
                                                        })
                                                        ->orderBy('website_order')
                                                        ->orderBy('id')
                                                        ->get());
        });

        view()->composer('layouts.organica.homepage', function($view){
                $view->with('products', \App\Product::featured()
                                                    ->inRandomOrder()
                                                    ->get()
                                                    /*->unique('subcategory_id')*/
                                                    ->take(10));
        });

        view()->composer('layouts.organica.homepage', function($view){
                $view->with('new_products', \App\Product::featured()
                                                    ->inRandomOrder()
                                                    ->get()
                                                    ->filter(function($product){
                                                        return $product->new();
                                                        })
                                                    /*->unique('subcategory_id')*/
                                                    ->take(10));
        });

        view()->composer('layouts.organica.homepage', function($view){
                $view->with('sale_products', \App\Product::featured()
                                                    ->where('sale', 1)
                                                    ->inRandomOrder()
                                                    ->get()
                                                    /*->unique('subcategory_id')*/
                                                    ->take(10));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //BugSnag
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }
}

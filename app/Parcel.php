<?php

namespace App;

use Cart;
use App\Item;
use App\Shipping;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parcel extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */

    public function order(){
        return $this->belongsTo('App\Order');
    }

    /**
     * Functions
     */
    
    public function shippingRates(Order $order, $cart_content = null){
    	// ** order must contain: name, email, phone, street, city, zip, country
    	
    	//Create Parcel For Shipping Rates 
    	//Parcel Size & Weight
       	$cart_items = $cart_content ?? Cart::content();
       	$parcel_length = 0;
       	$parcel_width = 0;
       	$parcel_height = 0;
       	$parcel_weight = 0;

       	$padding = 3; // estimation of bubble wrap dimensions (cm)
       	$parcel_padding = $padding * $cart_items->sum('qty');

       	foreach ($cart_items as $cart_item) {
       		$item = Item::find($cart_item->id);

       		$parcel_length = max($item->length, $parcel_length);
       		$parcel_width = max($item->width, $parcel_width);
       		$parcel_height = max($item->height, $parcel_height);
       		$parcel_weight += $item->weight;

       		$distance_unit = $item->distance_unit;
       		$mass_unit = $item->mass_unit;
       	}

       	$parcel = new Parcel([
            'length'=> $parcel_length + $parcel_padding,
            'width'=> $parcel_width + $parcel_padding,
            'height'=> $parcel_height + $parcel_padding,
            'distance_unit'=> $distance_unit,
            'weight'=> $parcel_weight,
            'mass_unit'=> $mass_unit,
        ]);

        // get a list of shipping providers and pricing
        $shipping = new Shipping;
        
        $rates = $shipping->rates($order, $parcel);
    	
    	return $rates;
    }
}

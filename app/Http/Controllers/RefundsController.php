<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Refund;
use App\Payment;

use DB;
use Carbon\Carbon;
use Faker\Factory;

class RefundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $refunds = Refund::all()->sortByDesc('id');

        return view('admin.refunds.index', compact('refunds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //REFUND ENTIRE PAYMENT
        unset($request['_token']);
        
        $payment = Payment::find($request->payment_id);

        $payment_method = DB::table('payment_methods')->select('method')->where('id', '=', $request->refund_method)->first()->method;

        //Refund Shipping
        if ($request->refund_shipping) {
            $refunded_amount = $payment->total_amount;
        }
        else{
            $refunded_amount = $payment->total_amount - $payment->shipping_fee;
        } 
            
        unset($request['refund_shipping']);
        

        //Create Reference ID
        $faker = Factory::create();
        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        
        $refund = Refund::create(['ref_id' => $ref_id]);
        $refund->update($request->all());

        if ($payment_method == 'stripe') {
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
            try {

                $stripe_refund = \Stripe\Refund::create([
                    "charge" => $payment->transaction_id,
                    'amount' => number_format($refunded_amount * 100, 0, '.', ''), /*converting to cents*/
                    ]);

                $refund->stripe_refund_id = $stripe_refund->id;
                $refund->save();

            } catch (\Stripe\Error\ApiConnection $e) {
               // Network problem, perhaps try again.
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();
                //return view('btn.stripe.error')->with('error', $error);

            } catch (\Stripe\Error\InvalidRequest $e) {
                // You screwed up in your programming. Shouldn't happen!
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();
                //return view('btn.stripe.error')->with('error', $error);

            } catch (\Stripe\Error\Api $e) {
                // Stripe's servers are down!
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();
                //return view('btn.stripe.error')->with('error', $error);

            } catch (\Stripe\Error\Card $e) {
                // Card was declined.
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();
                //return view('btn.stripe.error')->with('error', $error);
            }
        }

        $refund->refund_method = $payment_method; //convert payment id from form to payment method name
        $refund->refunded_amount = $refunded_amount;
        $refund->refund_issued = 1;
        $refund->processed_date = Carbon::now();
        $refund->save();

        \Session::flash('success', "Refund of \${$refund->refunded_amount} Issued, ID: $refund->id / Ref ID: $refund->ref_id");

        return redirect()->back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

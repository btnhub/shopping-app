<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\StoreCartSession;
use App\Jobs\StoreCollectedTaxes;
use App\Jobs\ApplyAccountCredit;
use App\Jobs\SendOrdersMail;

use Cart;
use App\Payment;
use App\Order;
use App\User;
use App\Item;
use App\Parcel;
use App\Customer;
use App\Credit;
use App\Shipping;
use App\Shipment;
use App\CollectedTax;
use App\RecurringOrder;
use App\PickupLocation;

use Auth;
use DB;
use Carbon;
use Faker\Factory;
use Shippo_Rate;

class CheckoutController extends Controller
{
 	public function __construct(Shipping $shipping, Parcel $parcel)
    {
    	$this->shipping = $shipping;
        $this->parcel = $parcel;

		$this->middleware(function ($request, $next) {
         	//If customer has recurring order items, make customer create an account
         	$cart_items = Cart::content();
			
	    	$reorder_items = $cart_items->where('options.reorder_frequency','!=', null);
	    	
	    	if ($reorder_items->count() && !$request->user()) {
		    	
		        \Session::flash('info', "You've selected to have items automatically reordered.  Please create an account before placing your order.");
		        
		        return redirect()->guest(url('/login'));
	    	}

	    	return $next($request);	

    	})->only('getCheckoutPage');
    }

    public function shippingMethod($shipping_selection)
    {
    	if ($shipping_selection == 'free_shipping') {
    		$shipping_method = (object)[
    								'id' => null,
    								'provider' => 'Free Shipping',
    								'servicelevel' => (object)['name' => ''],
    								'days' => config('other_constants.free_shipping_days'),
    								'amount' => 0,
    								'currency' => 'USD',
    								'duration_terms' => null,
    								];
    	}
    	elseif ($shipping_selection == 'free_pickup') {
    		$pickup_location = PickupLocation::find(session('pickup_location_id'));

    		$shipping_method = (object)[
    								'id' => null,
    								'provider' => 'Free Pickup @ ' . $pickup_location->event,
    								'servicelevel' => (object)['name' => $pickup_location->instructions],
    								'days' => '',
    								'amount' => 0,
    								'currency' => 'USD',
    								'duration_terms' => null,
    								];
    	}
    	else
    	{
    		$shipping_method = Shippo_Rate::retrieve($shipping_selection);	
    	}

    	return $shipping_method;

    }

    public function getCheckoutPage(){
    	$hide_cart = 1;

		return view('cart.checkout', compact('hide_cart'));

    }

    public function getShippingRates(Request $request){

    	$this->validate($request, [
    		'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|email|max:255',
    		"phone" => 'required|max:20',
    		"shipping_address" => 'required',
    		"city" => 'required',
    		"state" => 'required',
    		"zip" => 'required',
    		"country" => 'required'
    		]);
    	$hide_cart = 1;
    	
    	$cart_items = Cart::content();

		session(["first_name" => $request->first_name,
				"last_name" => $request->last_name,
				"email" => $request->email,
				"phone" => $request->phone,
				"shipping_address" => $request->shipping_address,
				"shipping_address_2" => $request->shipping_address_2,
				"city" => $request->city,
				"state" => $request->state,
				"zip" => $request->zip,
				"country" => $request->country,
				"save_address" => $request->save_address ?? null]);

		//Update shoppingcart Table With Customer Info (name, email, user_id)
		dispatch(new StoreCartSession(session('cart_identifier')));

    	//Validate Shipping
		// Try and validate the address
		$order = new Order([
				"first_name" => $request->first_name,
				"last_name" => $request->last_name,
				"email" => $request->email,
				"phone" => $request->phone,
				"shipping_address" => $request->shipping_address,
				"shipping_address_2" => $request->shipping_address_2,
				"city" => $request->city,
				"state" => $request->state,
				"zip" => $request->zip,
				"country" => $request->country,
			]);

        $validate = $this->shipping->validateAddress($order);
        
        // Make sure it's not an invalid address 
        if ($validate->validation_results && !$validate->validation_results->is_valid) {
        	$error_message = null;
        	foreach ($validate->validation_results->messages as $message) {
        		$error_message .= $message['text']."<br>";
        	}
            \Session::flash('error', $error_message);
            return redirect()->back();
        }

        // Get Shipping Rates
        $rates = $this->parcel->shippingRates($order);
       	
        //In case parcel is too large
        if (empty($rates->rates)) {
        	$error_message = "<b>We cannot ship your order because:</b>";

        	foreach ($rates->messages as $message) {
        		$error_message .= "<li>".$message->text;
        	}

        	\Session::flash('error', $error_message);
        	return redirect()->back();
        }
        
        //Find & Save shipping rate for recurring order (shipping rate if order subtotal drops below cutoff)
        if (Cart::content()->where('options.reorder_frequency','!=', null)->count()) {
        	$shipping = new Shipping;
        	$shipping_details = $shipping->shipping_estimate($rates->rates);

        	session(['reorder_shipping_provider' => $shipping_details->provider,
        				'reorder_servicelevel_name' => $shipping_details->servicelevel_name,
        				'reorder_shipping_rate' => $shipping_details->amount]);

        }

        //Pickup Locations
        $pickup_locations = PickupLocation::current()->get();

        return view('cart.partials.shipping_rates', ['rates' => $rates->rates], compact('hide_cart', 'pickup_locations'));

    }

    public function getPaymentForm(Request $request)
    {
    	$this->validate($request, ['rate' => 'required',]);
    	
    	$hide_cart = 1;

    	$credit_balance = 0;
		$user = auth()->user() ?? null;
		$customer = null;

		if ($user) {
			$credit_balance = $user->credit_balance();
			$customer = $user->customer;
		}

		elseif (session('credit_code'))
		{
			$credits = Credit::whereIn('credit_code', session('credit_code'))->get();
			foreach ($credits as $credit) {
				$credit_balance += $credit->balance();
			}
			$credit_balance = round($credit_balance, 2);
		}

    	session(['shipping_selection' => $request->rate]);

    	if ($request->rate == 'free_pickup') {
    		session(['pickup_location_id' => $request->pickup_location]);
    	}

    	$shipping_method = $this->shippingMethod($request->rate);

    	$cart_items = Cart::content();
		$cart_count = Cart::count();
		$cart_subtotal = (float) str_replace(",", "", Cart::subtotal()) ;
		
		$cart_shipping = round($shipping_method->amount, 2);
		
		$taxable_sales = $cart_subtotal /*+ $cart_shipping*/;
		
		if ($request->rate == 'free_pickup') {
			$pickup_location = PickupLocation::find(session('pickup_location_id'));

			$cart_tax = CollectedTax::calculate_taxes($taxable_sales, $pickup_location->state, $pickup_location->zip)['combined_tax'];
		}
		else{
			$cart_tax = CollectedTax::calculate_taxes($taxable_sales, session('state'), session('zip'))['combined_tax'];
		}
		

		$grand_total = round($cart_subtotal + $cart_shipping + $cart_tax, 2);

    	return view('cart.payment_page', compact('user', 'customer', 'credit_balance', 'cart_items', 'cart_count', 'cart_subtotal', 'cart_tax', 'cart_shipping', 'grand_total', 'shipping_method', 'hide_cart'));
    }
    
    public function submitPayment(Request $request){  	
    	//Update Order & Store Items In Cart (In case payment fails, the cart can be recreated)
		\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

		$faker = Factory::create();
		$ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);

		$payment_ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
		$payment_transaction_id = rand(10000000, 99999999);

		$user = Auth::user() ?? null;
		$user_id = Auth::user()->id ?? null;
		$first_name = session('first_name');
		$last_name = session('last_name');
		$email = session('email');
		$phone = session('phone');
		$shipping_address = session('shipping_address');
		$shipping_address_2 = session('shipping_address_2') ?? null;
		$city = session('city');
		$state = session('state');
		$zip = session('zip');
		$country = session('country');

		$cart_items = Cart::content();
		$cart_count = Cart::count();
		$cart_subtotal = (float)str_replace(",", "", Cart::subtotal());
		$credit_applied = null;

		$reorder_items = $cart_items->where('options.reorder_frequency','!=', null);

    	$order = Order::create ([
    			"ref_id" => $ref_id,
    			"user_id" => $user_id,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"email" => $email,
				"phone" => $phone,
				"shipping_address" => $shipping_address,
				"shipping_address_2" => $shipping_address_2,
				"city" => $city,
				"state" => $state,
				"zip" => $zip,
				"country" => $country,
				"details" => serialize($cart_items),
				"skip_grace_period" => $request->skip_grace_period ?? 0,
				"order_status" => "unpaid"
			]);

    	//Shippo rate & Create (do not save) Shipment Object
    	$shipping_method = $this->shippingMethod(session('shipping_selection'));

       	$cart_shipping = round($shipping_method->amount, 2);

		$taxable_sales = $cart_subtotal /*+ $cart_shipping*/;
		$tax_model = new CollectedTax();
		
		if (session('shipping_selection') == 'free_pickup') {
			$pickup_location = PickupLocation::find(session('pickup_location_id'));

			$cart_tax = $tax_model->calculate_taxes($taxable_sales, $pickup_location->state, $pickup_location->zip)['combined_tax'];
		}
		else{
			$cart_tax = $tax_model->calculate_taxes($taxable_sales, session('state'), session('zip'))['combined_tax'];
		}

		$order_balance = $grand_total = round($cart_subtotal + $cart_shipping + $cart_tax, 2);

		$payment = new Payment([
						"ref_id" => $payment_ref_id,
						"order_id" => $order->id,
						"user_id" => $user_id,
						"transaction_id" => $payment_transaction_id,
						"amount" => $cart_subtotal,
						"sales_tax" => $cart_tax,
						'shipping_fee' => $cart_shipping,
						"captured" => 0,
					    "paid" => 0,
					]);
		
    	$shipment = new Shipment ([
    				'provider' => $shipping_method->provider,
    				'servicelevel_name' => $shipping_method->servicelevel->name,
    				'days' => $shipping_method->days,
    				'duration_terms' => $shipping_method->duration_terms,
    				'shippo_rate_id' => $shipping_method->id,
    			]);

		if ($request->apply_credit_balance) {
			//Apply Credit and update credit_order table.
			$credit_job = dispatch(new ApplyAccountCredit($order, $order_balance, $taxable_sales));
			
			$credit_applied = $credit_job['credit_applied'];
			$order_balance = $credit_job['order_balance'];
			$taxable_sales = $credit_job['taxable_sales'];

			//If Credit covers the entire balance
			if ($order_balance == 0) {
				$order->order_status = "paid";
		  		$order->save();

				$payment_method_id = DB::table('payment_methods')->where('method', 'credit')->first()->id;
			
				$payment->credit_applied = $credit_applied;
				$payment->total_amount = $order_balance;
				$payment->payment_method_id = $payment_method_id;
				$payment->completed_at = Carbon\Carbon::now();
				$payment->captured = 1;
				$payment->paid = 1;
				$payment->save();

				//Create Shipment
				$shipment->order_id = $order->id;
				$shipment->save();
			}
		}

		try {
			//Pay Order Balance Using Stripe (Balance not covered by account credits)
			$amount_stripe = round($order_balance, 2) * 100; //amount in cents
			$charge = null;
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'] ?? null;

			//If user has recurring items, create Customer for future charges
			if ($reorder_items->count() && ($user && !$user->customer)) {
				//Create Stripe Customer
				$customer = \Stripe\Customer::create([
					"source" => $token,
				  	"description" => $user->full_name." - ".$user->id." - ".$user->email,
				  	"email" => $user->email
				  ]
				);

				//Save Customer Details To Database
			  	Customer::create([
					"user_id" => $user_id,
					"stripe_customer_id" => $customer->id,
					"card_brand" => $customer->sources->data[0]->brand,
					"card_last_four" => $customer->sources->data[0]->last4,
					"exp_month" => $customer->sources->data[0]->exp_month,
					"exp_year" => $customer->sources->data[0]->exp_year,
				]);
			}

			if ($amount_stripe > 0) {
				$description = "Mountain Myst Purchase (Order: $order->ref_id)";    
				$payment_method_id = DB::table('payment_methods')->where('method', 'stripe')->first()->id;
				
				$payment->credit_applied = $credit_applied;
				$payment->total_amount = $order_balance;
				$payment->payment_method_id = $payment_method_id;
				$payment->save();

				//Charge Stored Card
				if ($request->charge_stored_card || $reorder_items->count()) {
					//Find Customer
					$app_customer = Customer::where('user_id', $user->id)->first();

					$charge = \Stripe\Charge::create(array(
							  "amount" => $amount_stripe, // amount in cents, again
							  "currency" => "usd",
							  "customer" => $app_customer->stripe_customer_id,
							  "description" => $description
					  	));
				}
				
				//Charge Supplied Card Options
				else {

					//Save Customer & Charge Card
					if ($request->input("save_card") && !$user->customer) {

						//Create Stripe Customer
						$customer = \Stripe\Customer::create([
							"source" => $token,
						  	"description" => $user->full_name." - ".$user->id." - ".$user->email,
						  	"email" => $user->email
						  ]
						);

						//Save Customer Details To Database
					  	Customer::create([
							"user_id" => $user_id,
							"stripe_customer_id" => $customer->id,
							"card_brand" => $customer->sources->data[0]->brand,
							"card_last_four" => $customer->sources->data[0]->last4,
							"exp_month" => $customer->sources->data[0]->exp_month,
							"exp_year" => $customer->sources->data[0]->exp_year,
						]);

						$customer_id = $customer->id;

						// Charge the Customer instead of the card
						$charge = \Stripe\Charge::create(array(
							  "amount" => $amount_stripe, // amount in cents, again
							  "currency" => "usd",
							  "customer" => $customer_id,
							  "description" => $description
					  	));
					}

					elseif ($request->update_card) {
						//Update Customer
						$customer = $user->customer;
			            $stripe_customer = \Stripe\Customer::retrieve($customer->stripe_customer_id); 
			            $stripe_customer->source = $token;
			            $stripe_customer->save();

			            $customer->update([
			                    "stripe_customer_id" => $stripe_customer->id,
			                    "card_brand" => $stripe_customer->sources->data[0]->brand,
			                    "card_last_four" => $stripe_customer->sources->data[0]->last4,
			                    "exp_month" => $stripe_customer->sources->data[0]->exp_month,
			                    "exp_year" => $stripe_customer->sources->data[0]->exp_year,
			                ]);

						$customer_id = $stripe_customer->id;
						
						//Charge Customer
						$charge = \Stripe\Charge::create(array(
							  "amount" => $amount_stripe, // amount in cents, again
							  "currency" => "usd",
							  "customer" => $customer_id,
							  "description" => $description
					  	));
					}
					//Or just charge card
					else
					{	
					  $charge = \Stripe\Charge::create(array(
						    "amount" => $amount_stripe, // amount in cents
						    "currency" => "usd",
						    "source" => $token,
						    "description" => $description,
						    "receipt_email" => $order->email,
					    ));
					}
				}	
			}
					

	  		if ($charge) {
				$payment->transaction_id = $charge->id;
				$payment->stripe_customer_id = $customer_id ?? null;
				$payment->captured = $charge->captured;
				$payment->paid = $charge->paid;
				$payment->completed_at = date("Y-m-d H:i:s", $charge->created); 
				$payment->save();	

				$order->order_status = "paid";
	  			$order->save();

	  			//Create Shipment
				$shipment->order_id = $order->id;
				$shipment->save();
			}
  			
  			Cart::destroy();

  			//Update shoppingcart Table With Order ID
  			DB::table('shoppingcart')
                    ->where('identifier', session('cart_identifier'))
                    ->update([
                    	'order_id' => $order->id,
                        'updated_at' => Carbon\Carbon::now()
                        ]);             
  			$request->session()->forget('cart_identifier');

  			//Email Customer Confirmation
  			$subject= "Mountain Myst Confirmation - Order #$order->ref_id";
  			$email_view = 'emails.payment_confirmation';
			
			dispatch(new SendOrdersMail($order, $cart_items, $order->email, $subject, $email_view));

			//Notify Admin
  			$email_admin = config('mail.from.address');
  			$subject_admin= "New Order: $order->ref_id";
  			$email_view_admin = 'emails.order_created';
			
			dispatch(new SendOrdersMail($order, $cart_items, $email_admin, $subject_admin, $email_view_admin));

			//Insert Into Collected Taxes Table
			if ($cart_tax > 0) {
				if (session('shipping_selection') == 'free_pickup') {
					$order->state = $pickup_location->state;
					$order->zip = $pickup_location->zip;

					dispatch(new StoreCollectedTaxes($order, $taxable_sales));
				}
				else{
					dispatch(new StoreCollectedTaxes($order, $taxable_sales));	
				}
			}
			
			//Update Recurring Order & Items
			if ($reorder_items->count()) {
				$reorder_frequencies = $reorder_items->unique('options.reorder_frequency')->pluck('options.reorder_frequency');

				foreach ($reorder_frequencies as $key => $reorder_frequency) {
					$recur_ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);

					//create
					$recurring_order = new RecurringOrder();
					$recurring_order->ref_id = $recur_ref_id;
					$recurring_order->user_id = $user_id;
					$recurring_order->days = $reorder_frequency;
					$recurring_order->start_date = Carbon\Carbon::now();
					$recurring_order->last_charge = Carbon\Carbon::now();
					$recurring_order->next_charge = Carbon\Carbon::now()->addDays($reorder_frequency);
					$recurring_order->shipping_method = session('reorder_shipping_provider'). " ". session('reorder_servicelevel_name');
					$recurring_order->shipping_cost = session('reorder_shipping_rate');
					$recurring_order->save();

					//Populate Item_Recurring_order pivot table
					foreach ($reorder_items as $reorder_item) {
						
						//Add items to recurring order with same reorder frequency
						if ($reorder_item->options->reorder_frequency == $reorder_frequency) {

							$item = Item::find($reorder_item->id);

							$organic = $reorder_item->options->organic ?? 0;
							$glass = $reorder_item->options->glass ?? 0;

							$glass_cost = $glass ? config('other_constants.glass_cost') : 0; //additional cost to package in glass

        					$price = ($organic ? $item->organic_price : $item->price) + $glass_cost;
														
							$recurring_order->items()->save($item, [
										'organic' => $organic,
										'glass' => $glass,
										'price' => $price,
										'quantity' => $reorder_item->qty,
										'details' => serialize($reorder_item)
										]);
						}	
					}
				}
				
				$request->session()->forget('reorder_shipping_provider');
				$request->session()->forget('reorder_servicelevel_name');
				$request->session()->forget('reorder_shipping_rate');					
			}
			
			//Save Shipping Address Of Customer
			if ($user && (session('save_address') || $reorder_items->count())) {
				$user->phone = $phone;
				$user->shipping_address = $shipping_address;
				$user->shipping_address_2 = $shipping_address_2;
				$user->city = $city;
				$user->state = $state;
				$user->zip = $zip;
				$user->country = $country;
				$user->save();					
			}

		  	//Empty Cart & remove shipping selection from session
		  	$request->session()->forget('shipping_selection');
		  	$request->session()->forget('save_address');

		  	return view('cart.payment_success', compact('payment', 'cart_items', 'cart_count', 'cart_subtotal', /*'cart_total',*/ 'cart_tax', 'cart_shipping', 'grand_total', 'shipping_method'));		    

		} catch (\Stripe\Error\ApiConnection $e) {
 		   // Network problem, perhaps try again.
			$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			\Session::flash('error', $error['message']);
			return view('cart.payment_failed')->with('error', $error);

		} catch (\Stripe\Error\InvalidRequest $e) {
    		// You screwed up in your programming. Shouldn't happen!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			\Session::flash('error', $error['message']);
			return view('cart.payment_failed')->with('error', $error);

		} catch (\Stripe\Error\Api $e) {
    		// Stripe's servers are down!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			\Session::flash('error', $error['message']);
			return view('cart.payment_failed')->with('error', $error);

		} catch (\Stripe\Error\Card $e) {
    		// Card was declined.
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			\Session::flash('error', $error['message']);
			return view('cart.payment_failed')->with('error', $error);
		}
    }
}

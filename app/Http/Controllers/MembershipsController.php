<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\SendMembershipEmail;
use App\Jobs\SendAdminEmail;

use App\Role;
use App\User;
use App\Customer;
use App\Membership;
use App\MembershipPayment;

use DB;
use Carbon;
use Faker\Factory;

class MembershipsController extends Controller
{
    public function membershipPricing()
    {
        $memberships = Membership::where('website_compare', 1)->orderBy('price', 'ASC')->orderBy('discount', 'DESC')->get();

        //find price where savings begin
        foreach ($memberships->where('free_shipping', 1) as $membership) {

            $product_cost = 10; //start price
            $shipping_cost = 5; //estimated shipping cost
            $no_membership_cost = $product_cost + $shipping_cost;
            $membership_cost = $product_cost * (1-$membership->discount) + $membership->price;

            while ($no_membership_cost < $membership_cost) {
                $product_cost += 5; //add $5 to product cost
                $shipping_cost =  ($product_cost < config('other_constants.free_shipping_amount')) ? 5 : 0; //estimated shipping cost
                $no_membership_cost = $product_cost + $shipping_cost;
                $membership_cost = $product_cost * (1-$membership->discount) + $membership->price;
            }

            $savings["$membership->name"] = $product_cost; //cutoff where savings begin
        }

        return view('membership.pricing', compact('memberships', 'savings'));    
    }

    public function selectMembership()
    {
        
        $plans = Membership::where('website_compare', 1)->orderBy('price', 'ASC')->orderBy('discount', 'DESC')->get();

        $plans_array = ['' => 'Select A Plan'];

        foreach ($plans as $plan) {
            $plans_array[$plan->id] = ucwords($plan->name)." (".($plan->discount * 100)."% Off)". " - $$plan->price " . ucwords($plan->frequency);
        }

        return view('membership.purchase_membership', compact('plans_array'));
    }

    public function purchaseMembership(Request $request)
    {
        $this->validate($request, [
                'membership_plan' => 'required',
            ]);

        $membership = Membership::find($request->membership_plan);

        $user = auth()->user();

        //Stripe
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Get the credit card details submitted by the form
        $description = "Mountain Myst Membership Purchase - ".ucwords($membership->name)." Plan";
        $amount = number_format($membership->price, 2);
        $amount_stripe = $amount * 100; //amount in cents

        try {
            
            if (count($user->customer)) {
                //Find Existing Customer
                $customer = \Stripe\Customer::retrieve($user->customer->stripe_customer_id);
            }
            else
            {
                $token = $_POST['stripeToken'];
                // Create & Save New Customer
                $customer = \Stripe\Customer::create([
                    "source" => $token,
                    "description" => $user->full_name." - ".$user->id." - ".$user->email,
                    "email" => $user->email
                  ]);

                if ($customer) {
                    $new_customer = Customer::create([
                        "user_id" => $user->id,
                        "stripe_customer_id" => $customer->id,
                        "card_brand" => $customer->sources->data[0]->brand,
                        "card_last_four" => $customer->sources->data[0]->last4,
                        "exp_month" => $customer->sources->data[0]->exp_month,
                        "exp_year" => $customer->sources->data[0]->exp_year,
                    ]);
                }

                if (!$new_customer) {
                    abort(500, 'Could not save card details.  Please try again.');
                }    
            }

    
            // Add Customer to Plan
            
            $subscription = \Stripe\Subscription::create(array(
                              "customer" => $customer->id,
                              "plan" => $membership->stripe_plan
                            ));

            if ($subscription) {
                
                //Add to role_user table and membership_user tables
                $role_id = Role::where('role', 'member')->first()->id;
                $user->roles()->syncWithoutDetaching($role_id);
                $user->memberships()->syncWithoutDetaching([$membership->id => [
                                        'active' => 1,
                                        'price' => $membership->price,
                                        'frequency' => $membership->days,
                                        'start_date' => Carbon\Carbon::now(),
                                        'current_period_start' => date("Y-m-d H:i:s", $subscription->current_period_start),
                                        'current_period_end' => date("Y-m-d H:i:s", $subscription->current_period_end),
                                        'stripe_subscription_id' => $subscription->id
                                        ] ]);
                
                //Save MembershipPayment DELETE ON LIVE SITE (Webhooks create membership payment when an invoice is created)!!!
                /*$faker = Factory::create();
                $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);

                $membership_payment = new MembershipPayment();
                $membership_payment->ref_id = $ref_id;
                $membership_payment->user_id = $user->id;
                $membership_payment->membership_id = $membership->id;
                $membership_payment->amount = $amount;
                $membership_payment->paid = 1;
                $membership_payment->completed_at = Carbon\Carbon::now();
                $membership_payment->save(); */  
                
                //Email Payment Confirmation
                $subject = 'Welcome To Mountain Myst Membership';
                $subject_admin = "New Membership Was Purchased ($membership->name)";
                $email_view = 'emails.membership_new_purchase_confirmation';
                $email_view_admin = "emails.notify_admin";
                $admin_message = "New membership ($membership->name) purchased by $user->full_name ($user->email)";

                //Email Customer
                dispatch(new SendMembershipEmail($membership, $user, $subject, $email_view));
                
                //Notify Admin
                dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $admin_message));

                return view('membership.purchase_membership_confirmation', compact('membership'));
            }           

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('select.membership');

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('select.membership');

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('select.membership');

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('select.membership');
        }
        
    }

    public function changeMembership($stripe_subscription_id)
    {
        //https://stripe.com/docs/subscriptions/upgrading-downgrading
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $user = auth()->user();
        $current_membership = $user->current_membership($stripe_subscription_id);
        
        $other_memberships = Membership::where('website_compare', 1)
                        ->where('id', '!=', $current_membership->id)
                        ->orderBy('price', 'ASC')
                        ->orderBy('discount', 'DESC')
                        ->get();    
        

        // Set proration date to this moment:
        $proration_date = time();
        $available_memberships = ['' => 'Select A Membership'];

        foreach ($other_memberships as $new_membership) {

            $available_memberships[$new_membership->id] = ucwords($new_membership->name);

            if ($new_membership->price > $current_membership->price) {
                // See what the next invoice would look like with a plan switch and proration set:
                $cost = $new_membership->price;

                if ($current_membership->stripe_plan) {
                    try {

                        $invoice = \Stripe\Invoice::upcoming(array(
                              "customer" => $user->customer->stripe_customer_id,
                              "subscription" => $current_membership->pivot->stripe_subscription_id,
                              "subscription_plan" => $new_membership->stripe_plan, # Switch to new plan
                              "subscription_proration_date" => $proration_date
                            ));

                    } catch (\Stripe\Error\ApiConnection $e) {
                       // Network problem, perhaps try again.
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();

                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // You screwed up in your programming. Shouldn't happen!
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();

                    } catch (\Stripe\Error\Api $e) {
                        // Stripe's servers are down!
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();

                    } catch (\Stripe\Error\Card $e) {
                        // Card was declined.
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();
                    }

                    // Calculate the proration cost:
                    $cost = 0;
                    $current_prorations = array();
                    foreach ($invoice->lines->data as $line) {
                      if ($line->period->start == $proration_date) {
                        array_push($current_prorations, $line);
                        $cost += $line->amount;
                      }
                    }  

                    $cost = number_format($cost / 100, 2); //convert to dollars from cents
                }
  
                $available_memberships[$new_membership->id] .= " ($$cost To Upgrade Membership)";  
            }           
        } 

        return view('membership.update_membership', compact('available_memberships', 'current_membership', 'other_memberships', 'stripe_subscription_id'));       
    }

    public function updateMembership(Request $request)
    {
        $this->validate($request, ['membership_plan' => 'required']);

        $user = auth()->user();
        $stripe_subscription_id = $request->subid;

        $old_membership = $user->current_membership($stripe_subscription_id);

        $new_membership = Membership::find($request->membership_plan);
        
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));        

        try{
            $subscription = \Stripe\Subscription::retrieve($stripe_subscription_id);

            if ($old_membership->price > $new_membership->price) {
                //No Prorating current or future months if downgrading membership.  At next billing period, full amount of new membership is charged
                $subscription->prorate = false;
            }
            
            $subscription->plan = $new_membership->stripe_plan;
            $subscription->save();
            
            //Update Database
            $active = $subscription->status == 'active' ? 1 : 0;

            //Add row for new membership if DOWNGRADE to allow user to use old membership until end of period
            if ($old_membership->price > $new_membership->price) {    
                //Create new row for new membership (with more benefits), to allow user to keep using those benefits until the next pay period or if payment fails
                $user->memberships()->syncWithoutDetaching([$new_membership->id => [
                        'active' => $active,
                        'price' => $new_membership->price,
                        'frequency' => $new_membership->days,
                        'start_date' => date("Y-m-d H:i:s", $subscription->current_period_end),
                        'current_period_start' => date("Y-m-d H:i:s", $subscription->current_period_end),
                        'current_period_end' => date("Y-m-d H:i:s", $subscription->current_period_end),
                        'stripe_subscription_id' => $subscription->id
                        ] ]);

                //Change End Date for Old (Better) Membership
                $user->memberships()->syncWithoutDetaching([$old_membership->id => [
                        'end_date' => date("Y-m-d H:i:s", $subscription->current_period_end),
                        ] ]);             
            }

            //Charge For Proration
            if ($new_membership->price > $old_membership->price) {
                $invoice = \Stripe\Invoice::create([
                    "customer" => $user->customer->stripe_customer_id,
                    "description" => "Mountain Myst Membership Upgrade",
                    "subscription" => $subscription->id
                    ]);

                $completed_invoice = $invoice->pay();
                
                //Save MembershipPayment 
                if ($completed_invoice) {
                    //Create Membership Payment
                    $faker = Factory::create();
                    $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);

                    $membership_payment = new MembershipPayment();
                    $membership_payment->ref_id = $ref_id;
                    $membership_payment->user_id = $user->id;
                    $membership_payment->membership_id = $new_membership->id;
                    $membership_payment->amount = number_format($completed_invoice->amount_due / 100, 2);
                    $membership_payment->transaction_id = $completed_invoice->id;
                    $membership_payment->paid = $completed_invoice->paid;
                    $membership_payment->completed_at = Carbon\Carbon::now();
                    $membership_payment->save();

                    //Change End Date for Old (Worse) Membership
                    DB::table('membership_user')
                          ->where('stripe_subscription_id', '=', $subscription->id)
                          ->where('membership_id', $old_membership->id)
                          ->update([
                                'membership_id' => $new_membership->id,
                                'price' => number_format($subscription->plan->amount / 100, 2),
                                'active' => $active,
                                'updated_at' => Carbon\Carbon::now()
                                 ]);
                }
            }

            //EMAIL CUSTOMER
            $subject = "Mountain Myst Membership Updated";
            $email_view = "emails.membership_update_confirmation";

            //Email Customer
            dispatch(new SendMembershipEmail($new_membership, $user, $subject, $email_view));

            \Session::flash('success', "Your Membership Has Been Updated.");

            return redirect()->route('user.memberships', ['user_ref_id' => $user->ref_id]);
            
        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();
        }
    }

    public function cancelMembership(Request $request, $subscription_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $user = auth()->user();

        try {
            $subscription = \Stripe\Subscription::retrieve($subscription_id);
            $subscription->cancel(array('at_period_end' => true));

            $membership = Membership::where('stripe_plan', $subscription->plan->id)->first();

            DB::table('membership_user')
                ->where('stripe_subscription_id', $subscription->id)
                ->where('membership_id', $membership->id)
                ->update([
                    'end_date' => date("Y-m-d H:i:s", $subscription->current_period_end),
                    'cancelled' => 1
                    ]);

            $subject = "Mountain Myst Membership Cancelled";
            $email_view = "emails.membership_cancelled_confirmation";

            //Email Customer
            dispatch(new SendMembershipEmail($membership, $user, $subject, $email_view));

            \Session::flash('success', "We are cancelling your membership, this may take a few minutes to update. You will still be able to use your membership benefits until the listed end date.");

            return redirect()->route('user.memberships', ['user_ref_id' => $user->ref_id]);

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();
        }
        
    }
    
    public function reactivateMembership(Request $request, $subscription_id)
    {   
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        
        $user = auth()->user();

        try {
            $subscription = \Stripe\Subscription::retrieve($subscription_id);

            $membership = Membership::where('stripe_plan', $subscription->plan->id)->first();

            $subscription->plan = $membership->stripe_plan;
            $subscription->save();

            DB::table('membership_user')
                    ->where('stripe_subscription_id', $subscription->id)
                    ->where('membership_id', $membership->id)
                    ->update([
                        'end_date' => null,
                        'cancelled' => 0
                        ]);
            
            $subject = "Mountain Myst Membership Reactivated";
            $email_view = "emails.membership_update_confirmation";

            //Email Customer
            dispatch(new SendMembershipEmail($membership, $user, $subject, $email_view));

            \Session::flash('success', "Welcome Back!  We are reactivating your membership, this may take a few minutes to update.");

            return redirect()->route('user.memberships', ['user_ref_id' => $user->ref_id]);

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();
        }        
    }

    public function index()
    {
        $memberships = Membership::all();
        return view('admin.memberships.index', compact('memberships'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $user = User::find($id);

        return view('admin.users.show', compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

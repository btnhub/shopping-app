<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Purchase;
use App\Supplier;

class PurchasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchase::all();

        return view('admin.purchases.index', compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::orderBy('name')->get()->pluck('name', 'id');
        return view('admin.purchases.create', compact('suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        unset($request['_token']);
        
        $total = $request->amount + $request->shipping_fee + $request->sales_tax;

        $request->request->add(['total' => $total]);

        if ($request->use_tax_required == 1) {
            $taxable_sales = $request->amount + $request->shipping_fee;

            $state_use_tax = number_format($taxable_sales * config('taxes.state_tax_rate'), 2);
            $county_use_tax = number_format($taxable_sales * config('taxes.county_tax_rate'), 2);
            $city_use_tax = number_format($taxable_sales * config('taxes.city_tax_rate'), 2);
            $special_use_tax = number_format($taxable_sales * config('taxes.special_tax_rate'), 2);
            $request->request->add([
                'state_use_tax' => $state_use_tax,
                'county_use_tax' => $county_use_tax, 
                'city_use_tax' => $city_use_tax,
                 'special_use_tax' => $special_use_tax]);
        }
        
        Purchase::create($request->all());

        \Session::flash('success', 'Purchase Added!');

        return redirect()->route('purchases.index');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = Purchase::find($id);
        dd($purchase);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchase = Purchase::find($id);
        $suppliers = Supplier::orderBy('name')->get()->pluck('name', 'id');

        return view('admin.purchases.edit', compact('purchase', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $purchase = Purchase::find($id);
        
        unset($request['_token']);

        if ($request->recalc_taxes == 1) {
            $taxable_sales = $request->amount + $request->shipping_fee;

            $state_use_tax = number_format($taxable_sales * config('taxes.state_tax_rate'), 2);
            $county_use_tax = number_format($taxable_sales * config('taxes.county_tax_rate'), 2);
            $city_use_tax = number_format($taxable_sales * config('taxes.city_tax_rate'), 2);
            $special_use_tax = number_format($taxable_sales * config('taxes.special_tax_rate'), 2);

            $request->request->add([
                'state_use_tax' => $state_use_tax,
                'county_use_tax' => $county_use_tax, 
                'city_use_tax' => $city_use_tax,
                 'special_use_tax' => $special_use_tax]);
        }
        
        unset($request['recalc_taxes']);

        if ($request->use_tax_required == 0) {
            $request->state_use_tax = $request->county_use_tax = $request->city_use_tax = $request->special_use_tax = 0;
            $request->request->add([
                    'state_use_tax' => 0,
                    'county_use_tax' => 0,
                    'city_use_tax' => 0,
                    'special_use_tax' => 0]);
        }

        $purchase->update($request->all());

        \Session::flash('success', 'Purchase Updated!');

        return redirect()->route('purchases.index');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Purchase::destroy($id);

        \Session::flash('success', 'Purchase Removed!');

        return redirect()->route('purchases.index');
    }
}

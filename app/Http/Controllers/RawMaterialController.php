<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RawMaterial;
use App\Supplier;

class RawMaterialController extends Controller
{
    public function __construct()
    {
        $this->volume_units = [
            'fl_oz' => 'fl. oz',
            'pint' => 'pint',
            'qt' => 'quart',
            'gal' => 'gallon',
            'ml' => 'ml',
            'L' => 'L',
        ];
        
        $this->mass_units = [
            'oz' => 'oz',
            'lb' => 'lb',
            'g' => 'g',
            'kg' => 'kg'
        ];

        $this->length_units = [
            'in' => 'in',
            'ft' => 'ft',
            'cm' => 'cm',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $raw_materials = RawMaterial::all();

        return view('admin.raw_materials.index', compact('raw_materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::all()->pluck('name', 'id')->toArray();
        $suppliers = array_merge(['' => "Select A Supplier"], $suppliers);

        $mass_units = $this->mass_units;
        $length_units = $this->length_units;
        $volume_units = $this->volume_units;

        return view('admin.raw_materials.create', compact('suppliers', 'mass_units', 'length_units', 'volume_units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'price' => 'numeric']);
        unset($request['_token']);
        
        RawMaterial::create($request->all());

        \Session::flash('success', 'Raw Material Added!');

        return redirect()->route('raw_materials.index');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $raw_material = RawMaterial::find($id);
        
        $suppliers = Supplier::all()->pluck('name', 'id')->toArray();
        $suppliers = array_merge(['' => "Select A Supplier"], $suppliers);

        $mass_units = $this->mass_units;
        $length_units = $this->length_units;
        $volume_units = $this->volume_units;

        return view('admin.raw_materials.edit', compact('raw_material', 'suppliers', 'mass_units', 'length_units', 'volume_units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $raw_material = RawMaterial::find($id);
        
        unset($request['_token']);

        $raw_material->update($request->all());

        \Session::flash('success', 'Raw Material Updated!');

        return redirect()->route('raw_materials.index');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RawMaterial::destroy($id);

        \Session::flash('success', 'Raw Material Removed!');

        return redirect()->route('raw_materials.index');   
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\SendOrdersMail;

use App\Order;
use App\Refund;

use DB;
use PDF;
use Carbon;
use Faker\Factory;

class OrdersController extends Controller
{
    public function cancelOrder($order_ref_id){
        //Cancel Entire Order & Issue Refund 
        //Initiated by Customer within 24 hours of purchase
        
        $order = Order::where('ref_id', $order_ref_id)->first();
        
        $this->authorize('user-access', $order);
        
        if ($order->created_at->diffInHours(Carbon\Carbon::now()) > 24 && !auth()->user()->isAdmin()) {
            \Session::flash('error', 'Cannot Cancel Order Created Over 24 Hours Ago.');
            return redirect()->back();
        }

        $stripe_refund_id = null;
        $cancelled_by = auth()->user() ? auth()->user()->full_name : $order->full_name;

        $faker = Factory::create();
        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        
        $payment = $order->payment;
        $payment_method = $payment->payment_method();
        $account_credit = DB::table('payment_methods')->where('method', "credit")->first();
        $stripe_method = DB::table('payment_methods')->where('method', "stripe")->first();
        
        //HANDLE REFUNDING CREDITS
        if ($order->credits->count()) {
            //delete row entry in order_credit table
            foreach ($order->credits as $credit) {
                $credit->pivot->deleted_at = Carbon\Carbon::now();
                $credit->pivot->save();
            }
        }
        
        //Refund Stripe Charges
        if ($payment->payment_method_id == $stripe_method->id) {
         
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
         
            try {
                $re = \Stripe\Refund::create([
                      "charge" => $payment->transaction_id
                    ]);
                if ($re) {
                    $stripe_refund_id = $re->id;
                }

            } catch (\Stripe\Error\ApiConnection $e) {
               // Network problem, perhaps try again.
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back(); 

            } catch (\Stripe\Error\InvalidRequest $e) {
                // You screwed up in your programming. Shouldn't happen!
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back(); 

            } catch (\Stripe\Error\Api $e) {
                // Stripe's servers are down!
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back(); 

            } catch (\Stripe\Error\Card $e) {
                // Card was declined.
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back(); 
            }
        }
        
        $refund = Refund::create([
                    'ref_id' => $ref_id,
                    'user_id' => $order->user_id,
                    'order_id' => $order->id,
                    'payment_id' => $payment->id,
                    'stripe_refund_id' => $stripe_refund_id,
                    'refund_method' => $payment->payment_method(),
                    'requested_amount' => $payment->total,
                    'refund_reason' => "Cancelled By $cancelled_by",
                    'refunded_sales' => $payment->total_amount - $payment->sales_tax,
                    'refunded_tax' => $payment->sales_tax,
                    'refund_issued' => 1,
                    'processed_date' => Carbon\Carbon::now()
                        ]);

        $payment->refunded = 1;
        $payment->save();

        
        //Cancel Order
        $order->order_status = "canceled";
        $order->cancelled = 1;
        $order->save();
        
        //Delete Payment, Taxes & Order
        if ($payment->collected_tax) {
            $payment->collected_tax->delete();
        }

        //$payment->delete();
        //$order->delete();

        //Email Confirmation To User & Admin
        $subject = $subject_admin = "Order Cancelled - #{$order->ref_id}";
        $email_view = 'emails.order_cancelled';

        $cart_items = unserialize($order->details);
        
        dispatch(new SendOrdersMail($order, $cart_items, $order->email, $subject, $email_view));

        //Notify Admin
        $email_admin = config('mail.from.address');

        dispatch(new SendOrdersMail($order, $cart_items, $email_admin, $subject, $email_view));

        \Session::flash('success', 'Order Cancelled & A Refund Was Issued');

        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::has('payment')->orderByDesc('id')->get(); //Only load orders that have a payment associated with them
        $pending_orders = Order::pending()->get();

        $filled_orders = $orders->where('shipped', 1);

        $unpaid_orders = $orders->where('cancelled', 0)
                                ->filter(function($order){
                                    return $order->payment->paid == 0;});
        
        $cancelled_orders = $orders->where('cancelled', 1);

        $incomplete_orders = Order::doesntHave('payment')->get();

        return view('admin.orders.index', compact('filled_orders', 'pending_orders', 'unpaid_orders', 'cancelled_orders', 'incomplete_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::withTrashed()->where('id', $id)->first();
        
        $order->reviewed = 1;
        $order->save();

        $cart_items = unserialize($order->details);

        return view('admin.orders.show', compact('order', 'cart_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::destroy($id);

        \Session::flash('success', 'Order Deleted!');

        return redirect()->route('orders.index');
    }

    /**
     * Mark order as unreviweed
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function markAsUnreviewed($id, Request $request)
    {
        $order = Order::find($id);
        $order->reviewed = 0;
        $order->save();

        \Session::flash('success', 'Order Marked As Unreviewed');
        return redirect()->route('orders.index');
    }
       
}

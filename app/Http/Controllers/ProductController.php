<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Recipe;
use App\Item;
use App\Ingredient;
use App\SubCategory;
use App\Supplier;
use App\Membership;
use Faker\Factory;

use Carbon;

use Illuminate\Support\Facades\Response;


class ProductController extends Controller
{
    public function facebookDataFeedCSV(){
      $products = Product::frontVisible()->get();
      $filename = "mountain_myst-fb_data_feed-".date('Y-m-d').".csv";
      
      $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=$filename",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
      );

      $columns = array("id","title","description","availability","condition","price","link","image_link","brand","additional_image_link","age_group","color","gender","item_group_id","google_product_category","material","pattern","product_type","sale_price","sale_price_effective_date","shipping","shipping_weight","size","custom_label_0","custom_label_1","custom_label_2","custom_label_3","custom_label_4", "rich_text_description", "inventory", "visibility", "additional_variant_attribute");

      $callback = function() use ($products, $columns)
      {
          $file = fopen('php://output', 'w');
          fputcsv($file, $columns);

          foreach($products as $product) {
              $product_availablility = $product->available() ? "in stock" : "available for order"; //Supported values: in stock, available for order, preorder, out of stock, discontinued
              
              // $price = $product->organic_option ? $product->items->min('organic_price') : $product->items->min('price');
              // $price .= " USD";

              $product_url = route("product.details",['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id]);
              $product_image = asset("images/products/$product->image");
              $brand = "Mountain Myst";
              
              $additional_images = null; 
              $images = [];
              //When Items have pictures
              if ($product->items->where('image', '!=', NULL)) {
                foreach ($product->items->where('image', '!=', NULL) as $item) {
                  array_push($images, asset("images/products/items/$item->image"));
                }

                $additional_images = implode(",", $images);
              }

              $product_type = $product->subcategory->name;
              $description = $rich_text_description = $product->website_description . "<br><br>" . $product->detailed_description;
              
              $item_group_id = $product->ref_id;
              $google_product_category = "Health & Beauty > Personal Care > Cosmetics > Skin Care"; //https://www.google.com/basepages/producttype/taxonomy.en-US.txt

              foreach ($product->items as $item) {
                if ($item->available()) {
                  $age_group = $color = $gender = $material = $pattern = $sale_price = $sale_price_effective_date = $shipping = $shipping_weight = $size = $custom_label_0 = $custom_label_1 = $custom_label_2 = $custom_label_3 = $custom_label_4 = null;
                  
                  $size = $item->size;
                  $color = $item->color;
                  $additional_variant_attribute = $custom_label_0 = !$item->color ? $item->details() : null;

                  $inventory = 6;
                  $visibility = 'published';

                  $price = $product->organic_option ? $item->organic_price : $item->price;
                  $price .= " USD";

                  if ($product->sale || $item->sale) {
                    //$sale_price = $product->items->min('sale_price') . " USD";
                    $sale_price = $item->sale_price . " USD";
                    $sale_price_effective_date = Carbon\Carbon::now()->subDay()->format('Y-m-d') . "T0:00-23:59/" . Carbon\Carbon::now()->addDays(8)->format('Y-m-d') ."T0:00-23:59";
                  }

                  fputcsv($file, array(
                    $item->ref_id, $product->name, $description, $product_availablility, "new", $price, $product_url, $product_image, $brand, $additional_images, $age_group, $color, $gender, $item_group_id, $google_product_category, $material, $pattern, $product_type, $sale_price, $sale_price_effective_date, $shipping, $shipping_weight, $size, $custom_label_0, $custom_label_1, $custom_label_2, $custom_label_3, $custom_label_4, $rich_text_description, $inventory, $visibility, $additional_variant_attribute));
                  }
              }
          }
          fclose($file);
      };
      return Response::stream($callback, 200, $headers);
    }

    public function searchProducts(Request $request){
        $query = $request['query'];
        $page = $request['page'] ?? 1;
        
        $perPage = 15; // # of results per page
        //Subcategory Filter:  only return products that have a visible subcategory
        $subcategories = SubCategory::where('is_visible', 1)->pluck('name')->toArray();
        
        $subcat_filter = ' AND (subcategory:"';
        $subcat_filter .= implode('" OR subcategory:"', $subcategories);
        $subcat_filter .= '")';

        $products = Product::search($query,  function ($algolia, $query, $options) use($perPage, $page, $subcat_filter){
                                return $algolia->search($query, [
                                                  'filters' =>'available=1'. $subcat_filter,
                                                  'hitsPerPage' => $perPage,
                                                  'page' => $page-1]);
                              })->paginate($perPage);
        
        return view('frontend.product_search_results', compact('products', 'query'));
    }

    public function productDetails($cat_slug = null, $sub_slug = null, $product_id, $prod_slug = null)
    {
        $product = Product::find($product_id);
        
        $items = $product->items()->frontVisible()->orderBy('price', 'ASC')->get();

        $min_price = $product->organic_option ? $items->min('organic_price') : $items->min('price');
        
        $min_sale_price = $product->sale ?  $items->where('sale_price', '>', 0)->min('sale_price') : NULL;

        $max_discount = Membership::where('website_compare',1)->max('discount'); 
        
        $member_discount = 0;
        if (auth()->user() && auth()->user()->best_membership()) {
              $member_discount = auth()->user()->best_membership()->discount;
        }

        $size_array = [];
        
        if ($items->unique('size')->count() > 1) {
            $size_array[""] = "Choose A Size";
        }

        foreach ($items->unique('size')->sortBy('size') as $item) {
            $size_array[$item->size] = "$item->size";
        }

        return view('frontend.product_details', compact('product', 'items', 'member_discount', 'min_price', 'min_sale_price', 'max_discount', 'size_array'));
    }

    public function createRecipe($product_id)
    {
        $product = Product::find($product_id);

        $ingredients = Ingredient::select('id', 'name', 'type')->orderBy('name')->get();
        
        $ingredient_types = Ingredient::orderBy('type')->groupBy('type')->pluck('type');

        return view('admin.products.recipe_create', compact('product', 'ingredients', 'ingredient_types'));
    }

    public function storeRecipe(Request $request, $product_id)
    {
        $this->validate($request, ['name' => 'required']);

        $product = Product::find($product_id);
        $recipe = Recipe::firstOrCreate(['product_id' => $product_id, 'name' => $request->name]);

        foreach ($request->ingredient_ids as $ingredient_id) {
            $ingredient = Ingredient::find($ingredient_id);
            
            $recipe->ingredients()->syncWithoutDetaching([$ingredient->id => [
                                        'amount' => $request["amount($ingredient_id)"],
                                        'unit' => $request["unit($ingredient_id)"],
                                        'organic' => $request["organic($ingredient_id)"]
                                    ]]);
        }

        \Session::flash('success', "$recipe->name Added");

        return redirect()->route('products.index');
    }

    public function editRecipe($product_id)
    {
        $product = Product::find($product_id);

        $ingredients = Ingredient::select('id', 'name', 'type')->orderBy('name')->get();
        
        $ingredient_types = Ingredient::orderBy('type')->groupBy('type')->pluck('type');

        $recipe_model = (object)["name" => $product->recipe->name];
        $ingredient_ids = [];

        foreach ($product->recipe->ingredients as $ingredient) {
            $recipe_ingredient = [              
                            "organic($ingredient->id)" => $ingredient->pivot->organic,
                            "amount($ingredient->id)" => $ingredient->pivot->amount,
                            "unit($ingredient->id)" => $ingredient->pivot->unit];
            
            foreach ($recipe_ingredient as $key => $value) {
                $recipe_model->$key = $value;
            }

            array_push($ingredient_ids, $ingredient->id);
        }

        return view('admin.products.recipe_edit', compact('product', 'ingredients', 'ingredient_types', 'recipe_model', 'ingredient_ids'));
    }

    public function updateRecipe(Request $request, $product_id)
    {
       //dd('update');
       $this->validate($request, ['name' => 'required']);

        $product = Product::find($product_id);
        $product->recipe->name = $request->name;
        $product->recipe->save();

        foreach ($request->ingredient_ids as $ingredient_id) {
            $ingredient = Ingredient::find($ingredient_id);
            
            $product->recipe->ingredients()->updateExistingPivot($ingredient->id, [
                                        'amount' => $request["amount($ingredient_id)"],
                                        'unit' => $request["unit($ingredient_id)"],
                                        'organic' => $request["organic($ingredient_id)"]
                                    ]);
        }

        //Remove unselected ingredient
        $product->recipe->ingredients()->sync($request->ingredient_ids);

        \Session::flash('success', "{$product->recipe->name} Updated");

        return redirect()->back(); 
    }

    //Copy unique items from one product to a newly created product
    public function copyItems(Request $request, $new_product_id)
    {
      $existing_product = Product::find($request->product_id);
      $items = $existing_product->items;
      $faker = Factory::create();

      $count = 0;

      foreach ($items as $item) {
        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);

        $check = Item::where([
                  'product_id' => $new_product_id,
                  'size' => $item->size ?: null,
                  'color' => $item->color ?: null,
                  'scent' => $item->scent ?: null,
                  'flavor' => $item->flavor ?: null,
                  'additional_feature' => $item->additional_feature ?: null])->count();
        
        //Create item if one does not already exist
        if ($check == 0) {
          Item::create([
              'ref_id' => $ref_id,
              'product_id' => $new_product_id,
              'size' => $item->size,
              'color' => $item->color,
              'scent' => $item->scent,
              'flavor' => $item->flavor,
              'additional_feature' => $item->additional_feature,
              'price' => $item->price,
              'organic_price' => $item->organic_price,
              'length' => $item->length,
              'width' => $item->width,
              'height' => $item->height,
              'distance_unit' => $item->distance_unit,
              'weight' => $item->weight,
              'mass_unit' => $item->mass_unit
              ]);
          $count++;
        }
        
      }

      \Session::flash('success', "$count Items Copied From $existing_product->name");
      
      return redirect()->route('products.index');   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $subcategories = SubCategory::all();
        return view('admin.products.index', compact('products', 'subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategories = SubCategory::all()->pluck('name', 'id');
        $suppliers = Supplier::all()->pluck('name', 'id');
        return view('admin.products.create', compact('subcategories', 'suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['image_file' => 'image']);
        
        unset($request['_token']);
        
        $faker = Factory::create();

        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        
        $request->request->add(['ref_id' => $ref_id]);
        $product = Product::create($request->except('image_file'));

        if ($request->hasFile('image_file')) {
            $new_filename = sprintf("%03s", $product->id).'-'.$request->file('image_file')->getClientOriginalName();
            $file_move = $request->file('image_file')->move(public_path('images/products'), $new_filename);
            //$request->merge(['image' => $new_filename]);
            $product->image = $new_filename;
            $product->save();
        }

        

        \Session::flash('success', 'Product Added!');

        return redirect()->route('products.index');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ref_id)
    {
        $product = Product::where('ref_id', $ref_id)->first();
        $items = $product->items()->orderBy('price', 'ASC')->get();
        //dd($items);
        return redirect()->route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $subcategories = SubCategory::all()->pluck('name', 'id');
        $suppliers = Supplier::all()->pluck('name', 'id');

        $related_products = $product->subcategory->products->where('id', '!=', $product->id)->pluck('name', 'id')->toArray();

        foreach ($related_products as $product_id => $product_name) {
          $items_count = Product::find($product_id)->items->count();
          
          if ($items_count > 0) {
            $related_products[$product_id] = $product_name . " - $items_count Items";
          }
          else
          {
            //remove from array if product has no items
            unset($related_products[$product_id]);
          }
          
        }

        return view('admin.products.edit', compact('product', 'subcategories', 'suppliers', 'related_products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['image_file' => 'image']);

        $product = Product::find($id);
        
        if ($request->hasFile('image_file')) {
            $new_filename = sprintf("%03s", $product->id).'-'.$request->file('image_file')->getClientOriginalName();
            $file_move = $request->file('image_file')->move(public_path('images/products'), $new_filename);
            $request->merge(['image' => $new_filename]);
        }  

        unset($request['_token']);       
        
        $product->update($request->except('image_file'));

        \Session::flash('success', 'Product Updated!');

        return redirect()->back();   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        \Session::flash('success', 'Product Removed!');

        return redirect()->route('products.index');   
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\SendMembershipEmail;
use App\Jobs\SendAdminEmail;

use App\Role;
use App\User;
use App\Customer;
use App\Membership;
use App\MembershipPayment;

use DB;
use Carbon;
use Faker\Factory;

class StripeWebhooksController extends Controller
{

    public function processWebHook(){
        
        //REF: https://stripe.com/docs/recipes/sending-emails-for-failed-payments
        //Stripe
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);

        //$event_json = config('stripe_webhooks.customer_subscription_deleted'); //FOR LOCAL TESTING
        
        //Verify the event (if livemode is true, i.e. not testing webhooks)
        // Check against Stripe to confirm that the ID is valid
        $event = $event_json->livemode ? \Stripe\Event::retrieve($event_json->id) : $event_json; 
    

        if (isset($event)) {
            //Resource related to event (e.g. invoice, subscription) is accessed by            
            switch ($event->type) {
                case 'invoice.created':
                    $invoice = $event->data->object ?? $event->object;
                    $membership_payment_exists = MembershipPayment::where('transaction_id', $invoice->id)->count();

                    if (!$membership_payment_exists) {
                    	//Create Entry For Membership Payment if it does not already exist in the database
                    	$faker = Factory::create();

	                    $customer = Customer::where('stripe_customer_id', $invoice->customer)->first();

	                    $invoice_items = $invoice->lines->data;

			            foreach ($invoice_items as $invoice_item) {
			            	$ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);

			            	$subscription = null;

			            	$subscription_id = strpos($invoice_item->id, 'sub_') !== false ? $invoice_item->id : (strpos($invoice_item->subscription, 'sub_') !== false ? $invoice_item->subscription : null);

				        	if ($subscription_id) {
				        		$amount = $invoice_item->amount / 100;

		                        $membership_id = Membership::where('stripe_plan', $invoice_item->plan->id)->first()->id;

					            $membership_payment = MembershipPayment::create([
					                                    'ref_id' => $ref_id,
					                                    'user_id' => $customer->user_id,
					                                    'membership_id' => $membership_id,
					                                    'amount' => $amount,
					                                    'transaction_id' => $invoice->id,
					                                    'paid' => $invoice->paid,
					                                    'notes' => $invoice_item->description
					                                    ]);	
				        	}
				        	else {
				        		$subject_admin = "Stripe $event->type Error - Subscription $subscription_id Not Found";
				        		$email_view_admin = "emails.notify_admin";
				        		$message_admin = "Subscription $subscription_id Not Found";

				        		dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $message_admin));
				        	}
			            }	
                    }
                    
                    break;

                case 'invoice.payment_succeeded':
                    $invoice = $event->data->object ?? $event->object;

                    $membership_payments = MembershipPayment::where('transaction_id', $invoice->id)->get();

		            foreach ($membership_payments as $membership_payment) {
		            	$membership_payment->paid = $invoice->paid;
			            $membership_payment->completed_at = Carbon\Carbon::now();
			            $membership_payment->save();
		            }
		            
		            //UPDATE current period dates for subscription(s)
			        $invoice_items = $invoice->lines->data;

		            foreach ($invoice_items as $invoice_item) {
			        	$subscription = null;

			        	$subscription_id = strpos($invoice_item->id, 'sub_') !== false ? $invoice_item->id : (strpos($invoice_item->subscription, 'sub_') !== false ? $invoice_item->subscription : null);

			            
			            if ($subscription_id) {
			            	$subscription = \Stripe\Subscription::retrieve($subscription_id);

			            	$membership = Membership::where('stripe_plan', $invoice_item->plan->id)->first();

				            DB::table('membership_user')
				                  ->where('stripe_subscription_id', $subscription->id)
				                  ->where('membership_id', $membership->id)
				                  ->update([
				                        'active' => 1,
				                        'current_period_start' => date('Y-m-d H:i:s', $subscription->current_period_start), 
				                        'current_period_end' => date('Y-m-d H:i:s', $subscription->current_period_end),
				                        'updated_at' => Carbon\Carbon::now()
				                        ]);	
			            }
			            else
			            {
			            	$subject_admin = "Stripe $event->type Error - Subscription $subscription_id Not Found";
			        		$email_view_admin = "emails.notify_admin";
			        		$message_admin = "Subscription $subscription_id Not Found";
			        		
			        		dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $message_admin));
			            }
			        }
                    
                    if ($subscription) {
                    	//EMAIL CUSTOMER
	                    $subject = "Membership Payment Confirmation";
	                    $email_view = "emails.membership_payment_confirmation";

	                    $user = User::find($membership_payment->user_id);

	                    //Email Payment Confirmation
	                   dispatch(new SendMembershipEmail($membership, $user, $subject, $email_view));
                    }

                    break;

                case 'invoice.payment_failed':
		            $invoice = $event->data->object ?? $event->object;

		            //Cancel Membership
		            $membership_payments = MembershipPayment::where('transaction_id', $invoice->id)->get();

		            foreach ($membership_payments as $membership_payment) {
		            	$membership_payment->paid = $invoice->paid;
			            $membership_payment->save();
		            }

		            //Update Subscription(s)
		            $invoice_items = $invoice->lines->data;

		            foreach ($invoice_items as $invoice_item) {
		            	$subscription = null;

		            	$subscription_id = strpos($invoice_item->id, 'sub_') !== false ? $invoice_item->id : (strpos($invoice_item->subscription, 'sub_') !== false ? $invoice_item->subscription : null);

		            	if ($subscription_id) {
		            		$subscription = \Stripe\Subscription::retrieve($subscription_id);	

		            		$membership = Membership::where('stripe_plan', $invoice_item->plan->id)->first();

			            	DB::table('membership_user')
			                  ->where('stripe_subscription_id', $subscription->id)
			                  ->where('membership_id', $membership->id)
			                  ->update([
			                        'active' => 0
			                        ]);
		            	}
			        	
			        	else {
			        		$subject_admin = "Stripe $event->type Error- Subscription $subscription_id Not Found";
			        		$email_view_admin = "emails.notify_admin";
			        		$message_admin = "Subscription $subscription_id Not Found";
			        		
			        		dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $message_admin));
					    }			            	
		            }
		            
		            if ($subscription) {
		            	//EMAIL CUSTOMER

	                    $subject = "Mountain Myst Membership Payment Declined";
	                    $email_view = "emails.membership_payment_failed";

	                    $user = User::find($membership_payment->user_id);

	                    //Email Payment Confirmation
	                    dispatch(new SendMembershipEmail($membership, $user, $subject, $email_view));
		            }

                    break;

               	case 'customer.subscription.updated':
                    $subscription = $event->data->object;
                    
                    $subscription_id = strpos($subscription->id, 'sub_') !== false ? $subscription->id : null;

                    if ($subscription_id) {
                    	$subscription = \Stripe\Subscription::retrieve($subscription_id);

	                    $new_plan = $subscription->plan;
	                    $new_membership = Membership::where('stripe_plan',$new_plan->id)->first();
	                    
	                    $active = $subscription->status == 'active' ? 1 : 0;

	                    //Check if new membership is already in table
	                    $membership_exists = DB::table('membership_user')
	                          					->where('stripe_subscription_id', '=', $subscription->id)
	                          					->where('membership_id', '=', $new_membership->id)
	                          					->count();

	                    if ($membership_exists) {
	                    	DB::table('membership_user')
	                          ->where('stripe_subscription_id', '=', $subscription->id)
	                          ->where('membership_id', '=', $new_membership->id)
	                          ->update([
	                                'active' => $active,
	                                'updated_at' => Carbon\Carbon::now()
	                                 ]);	
	                    }

	                    else
	                    {
	                    	DB::table('membership_user')
	                          ->where('stripe_subscription_id', '=', $subscription->id)
	                          ->update([
	                                'membership_id' => $new_membership->id,
	                                'price' => number_format($new_plan->amount / 100, 2),
	                                'active' => $active,
	                                'updated_at' => Carbon\Carbon::now()
	                                 ]);	
	                    }
	                                        
                    }
                    else
                    {
                    	$subject_admin = "Stripe $event->type Error- Subscription $subscription_id Not Found";
		        		$email_view_admin = "emails.notify_admin";
		        		$message_admin = "Subscription $subscription_id Not Found";
		        		
		        		dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $message_admin));
                    }
                    
                    break;

                case 'customer.subscription.deleted':
                    $subscription = $event->data->object;

		            //UPDATE current period dates
		            $subscription_id = strpos($subscription->id, 'sub_') !== false ? $subscription->id : null;

		            if ($subscription_id) {
		            	$subscription = \Stripe\Subscription::retrieve($subscription_id);
						$membership = Membership::where('stripe_plan', $subscription->plan->id)->first();
			            
			            DB::table('membership_user')
			                  ->where('stripe_subscription_id', $subscription->id)
			                  ->where('membership_id', $membership->id)
			                  ->update([
			                  		'cancelled' => 1,
			                        'end_date' => date('Y-m-d H:i:s', $subscription->current_period_end),
			                        'current_period_end' => date('Y-m-d H:i:s', $subscription->current_period_end),
			                        'updated_at' => Carbon\Carbon::now()
			                        ]);
			            
	                    //EMAIL CUSTOMER
	                    $member_row = DB::table('membership_user')
	                                    ->where('stripe_subscription_id', $subscription->id)
	                                    ->where('membership_id', $membership->id)
	                                    ->first();

	                    $user_id = $member_row->user_id;

	                    $subject = "Mountain Myst Membership Cancelled";
	                    $email_view = "emails.membership_cancelled_confirmation";
	                    $email_view_admin = "emails.membership_cancelled_notify_admin";

	                    $user = User::find($user_id);

	                    //Email Payment Confirmation
	                    dispatch(new SendMembershipEmail($membership, $user, $subject, $email_view));
		            }
		            
		            else
		            {
		            	$subject_admin = "Stripe $event->type Error- Subscription $subscription_id Not Found";
		        		$email_view_admin = "emails.notify_admin";
		        		$message_admin = "Subscription $subscription_id Not Found";
		        		
		        		dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $message_admin));
		            }

                    break;
            }

            /*
		    if (!$event->livemode) {
		    	//Notify Admin In TEST MODE
		    	$subject_admin = $message_admin = "WEBHOOK TEST: $event->type";
		        $email_view_admin = "emails.notify_admin";

		        dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $message_admin));
		    }*/
		    
            return response('Webhook Processed');
        }
    }
}

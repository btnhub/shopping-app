<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Order;
use App\Customer;
use App\Membership;

class UsersController extends Controller
{

    public function myAccount($user_ref_id){
        
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        return view('customers.my_account', compact('user'));
    }

    public function editAccount($user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        return view('customers.edit_account', compact('user'));
    }

    public function updateAccount(Request $request, $user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        if (trim($request['password']) == '') 
        {
            //remove password if not entered to prevent error with validation rule of min 6 characters
            unset($request['password']);
        }

        $this->validate($request, [
                    'first_name' => 'required|min:2|max:255',
                    'last_name' => 'required|min:2|max:255',
                    'email' => 'required|email|unique:users,email,'.$user->id,
                    'password' => 'sometimes|min:6|confirmed',
                    'phone' => 'sometimes|max:20',
                ]);   
        
        $user->update($request->except('_token', 'password', 'password_confirmation'));

        if (trim($request['password']) != '') {
            $user->password = bcrypt($request['password']); 
            $user->save();
        }

        \Session::flash('sucess', 'Account Updated!');

        return redirect()->route('user.account', ['user_ref_id' => $user->ref_id]);
    }

    public function myMemberships($user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        return view('customers.my_memberships', compact('user'));
    }

    public function storeCreditCard(Request $request, $user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Get the credit card details submitted by the form
        $token = $_POST['stripeToken'];

        // Create a Customer
        $customer = \Stripe\Customer::create([
            "source" => $token,
            "description" => $user->full_name." - ".$user->id." - ".$user->email,
            "email" => $user->email
          ]
        );

        if ($customer) {
            $new_customer =  Customer::create([
                "user_id" => $user->id,
                "stripe_customer_id" => $customer->id,
                "card_brand" => $customer->sources->data[0]->brand,
                "card_last_four" => $customer->sources->data[0]->last4,
                "exp_month" => $customer->sources->data[0]->exp_month,
                "exp_year" => $customer->sources->data[0]->exp_year,
            ]);
        }

        if (!$new_customer) {
           abort(500, 'Could not save card details.  Please try again.');
        }

        \Session::flash('sucess', 'Card Added');

        return redirect()->route('user.account', ['user_ref_id' => $user_ref_id]);
    }

    public function editCreditCard($user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();

        $this->authorize('self-access', $user);

        return view('customers.edit_card', compact('user'));
    }

    public function updateCreditCard(Request $request, $user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        $customer = $user->customer;

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Get the credit card details submitted by the form
        $token = $_POST['stripeToken'];

        try {
            $cu = \Stripe\Customer::retrieve($customer->stripe_customer_id); // stored in your application
            $cu->source = $token; // obtained with Checkout
            $cu->save();

            $customer->update([
                    "stripe_customer_id" => $cu->id,
                    "card_brand" => $cu->sources->data[0]->brand,
                    "card_last_four" => $cu->sources->data[0]->last4,
                    "exp_month" => $cu->sources->data[0]->exp_month,
                    "exp_year" => $cu->sources->data[0]->exp_year,
                ]);

            \Session::flash('sucess', 'Card Updated!');

            return redirect()->route('user.account', ['user_ref_id' => $user_ref_id]);

          } catch(\Stripe\Error\Card $e) {

            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('edit.card', ['user_ref_id' => $user_ref_id]);

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('edit.card', ['user_ref_id' => $user_ref_id]);

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('edit.card', ['user_ref_id' => $user_ref_id]);

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('edit.card', ['user_ref_id' => $user_ref_id]);

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->route('edit.card', ['user_ref_id' => $user_ref_id]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        
        $user_ids = $users->pluck('id');

        $no_account_orders = Order::whereNull('user_id')->get();
        return view('admin.users.index', compact('users', 'no_account_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $user = User::find($id);
        
        $memberships = [];
        $hidden_memberships = Membership::where('website_compare', 0)
                                            ->whereNotIn('id', $user->memberships->pluck('id'))
                                            ->get();

        if ($hidden_memberships->count()) {
            $memberships = ['' => "Select A Special Membership"];
            foreach ($hidden_memberships as $membership) {
                $memberships[$membership->id] = ucwords($membership->name). " - " . $membership->description;
            }
        }
        return view('admin.users.show', compact('user', 'memberships'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

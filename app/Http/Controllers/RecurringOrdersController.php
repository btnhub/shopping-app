<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\StoreCollectedTaxes;
use App\Jobs\ApplyAccountCredit;
use App\Jobs\SendOrdersMail;

use App\RecurringOrder;
use App\Item;
use App\User;
use App\Order;
use App\Parcel;
use App\Payment;
use App\Customer;
use App\Credit;
use App\Shipping;
use App\Shipment;
use App\CollectedTax;

use DB;
use Cart;
use Carbon;
use Faker\Factory;
use Shippo_Rate;

class RecurringOrdersController extends Controller
{
    /**
     * Display all recurring orders for customer
     * @param  [type] $user_ref_id [description]
     * @return [type]              [description]
     */
    public function subscriptions($user_ref_id)
    {
        // List all recurring orders for customer
        $user = User::where('ref_id', $user_ref_id)->first();        

        $this->authorize('self-access', $user);
        
        $recurring_orders = RecurringOrder::where('user_id', $user->id)->get();

        return view('customers.recurring_orders', compact('recurring_orders'));
    }

    /**
     * Edit  selected Subscription
     * @param  [type] $user_ref_id [description]
     * @return [type]              [description]
     */
    public function editSubscription($ref_id)
    {
        $user = User::where('ref_id', $user_ref_id)->first();        

        $this->authorize('self-access', $user);
        
        $recurring_order = RecurringOrder::where('ref_id', $ref_id)->first();
    }

    /**
     * Update  Selected Subscription (Store changes made)
     * @param  [type] $user_ref_id [description]
     * @return [type]              [description]
     */
    public function updateSubscription(Request $request, $ref_id)
    {
        
    }

    /**
     * Remove Individual Item From Subscription
     */
    public function removeItem($recurring_order_ref_id, $item_ref_id)
    {
        $this->authorize('self-access', auth()->user());

        $recurring_order = RecurringOrder::where('ref_id', $recurring_order_ref_id)->first(); 
        
        $item = Item::where('ref_id', $item_ref_id)->first();

        $recurring_order->items()->updateExistingPivot($item->id, [
                                            'deleted_at' => Carbon\Carbon::now()
                                            ]);
        \Session::flash('success', 'Item Removed From Subscription');

        return redirect()->back();   
    }

    /**
     * Delete  Subscription
     */
    public function destroySubscription($ref_id)
    {
        $this->authorize('self-access', auth()->user());

        $recurring_order = RecurringOrder::where('ref_id', $ref_id)->first(); 
        $recurring_order->end_date = Carbon\Carbon::now();
        $recurring_order->save();

        RecurringOrder::destroy($recurring_order->id);

        \Session::flash('success', 'Subscription Cancelled');

        return redirect()->back();   
    }

    public function fillRecurringOrder($recurring_order_id)
    {
        $faker = Factory::create();
        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        
        $payment_ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        $payment_transaction_id = rand(10000000, 99999999);

        $recurring_order = RecurringOrder::find($recurring_order_id);
        $user = $recurring_order->user;
        
        $cart_items = collect([]);
        $cart_subtotal = $recurring_order->subtotal_cost();
        
        $credit_applied = 0;

        //build collection of items for order
        foreach ($recurring_order->items as $item) {
                $cart_items->push(unserialize($item->pivot->details));
        }

        $order = Order::create ([
                "ref_id" => $ref_id,
                "user_id" => $user->id,
                "recurring_order_id" => $recurring_order->id,
                "first_name" => $user->first_name,
                "last_name" => $user->last_name,
                "email" => $user->email,
                "phone" => $user->phone,
                "shipping_address" => $user->shipping_address,
                "shipping_address_2" => $user->shipping_address_2,
                "city" => $user->city,
                "state" => $user->state,
                "zip" => $user->zip,
                "country" => $user->country,
                "details" => serialize($cart_items),
                "skip_grace_period" => 0,
                "order_status" => "unpaid"
            ]);

        $description = "Mountain Myst Purchase (Order: $order->ref_id)";    

        //Shipping Details & Cost
        if ($recurring_order->shipping_price() > 0) {
            $cart_shipping = $recurring_order->shipping_price();

            $shipping_provider = explode(" ", $recurring_order->shipping_method)[0];
            
            if ($shipping_provider == "FedEx") {
                $shipment = new Shipment ([
                    'provider' => "FedEx",
                    'servicelevel_name' => "Ground",
                    'days' => null,
                    'duration_terms' => null,
                    'shippo_rate_id' => null,
                ]);

                $shipping_servicelevel = "Ground";
            }

            else {
                $shipment = new Shipment ([
                    'provider' => "USPS",
                    'servicelevel_name' => "Priority Mail",
                    'days' => null,
                    'duration_terms' => null,
                    'shippo_rate_id' => null,
                ]); 

                $shipping_servicelevel = "Priority Mail";
            }
        }

        else{
            //FREE SHIPPING
            $cart_shipping = 0;

            $shipment = new Shipment ([
                    'provider' => config('other_constants.free_shipping_provider'),
                    'servicelevel_name' => config('other_constants.free_shipping_servicelevel'),
                    'days' => null,
                    'duration_terms' => null,
                    'shippo_rate_id' => null,
                ]);

            $shipping_servicelevel = config('other_constants.free_shipping_servicelevel');
            $shipping_provider  = config('other_constants.free_shipping_servicelevel');
        }
        
        $shipping_method = (object)[
                            'id' => null,
                            'provider' => $shipping_provider,
                            'servicelevel' => (object)['name' => $shipping_servicelevel],
                            'days' => null,
                            'amount' => $cart_shipping,
                            'currency' => 'USD',
                            'duration_terms' => null];
        
        //Taxes
        $taxable_sales = $cart_subtotal + $cart_shipping;
        $tax_model = new CollectedTax();
        $cart_tax = $tax_model->calculate_taxes($taxable_sales, session('state'), session('zip'))['combined_tax'];

        $order_balance = $grand_total = number_format($cart_subtotal + $cart_shipping + $cart_tax, 2);

        $payment = new Payment([
                        "ref_id" => $payment_ref_id,
                        "order_id" => $order->id,
                        "user_id" => $user->id,
                        "transaction_id" => $payment_transaction_id,
                        "amount" => $cart_subtotal,
                        "sales_tax" => $cart_tax,
                        'shipping_fee' => $cart_shipping,
                        "captured" => 0,
                        "paid" => 0,
                        "notes" => "Recurring Order Payment (Recurring Order: $recurring_order->id)"
                    ]);

        //Apply Account Credits
        if ($user->credit_balance() > 0) {
            //Apply Credit and update credit_order table.
            $credit_job = dispatch(new ApplyAccountCredit($order, $order_balance, $taxable_sales));
            
            $credit_applied = $credit_job['credit_applied'];
            $order_balance = $credit_job['order_balance'];
            $taxable_sales = $credit_job['taxable_sales'];

            //If Credit covers the entire balance
            if ($order_balance == 0) {
                $order->order_status = "paid";
                $order->save();

                $payment_method_id = DB::table('payment_methods')->where('method', 'credit')->first()->id;
            
                $payment->credit_applied = $credit_applied;
                $payment->total_amount = $order_balance;
                $payment->payment_method_id = $payment_method_id;
                $payment->completed_at = Carbon\Carbon::now();
                $payment->captured = 1;
                $payment->paid = 1;
                $payment->save();
            }
        }
        
        //Charge Customer
        try {
            //Pay Order Balance Using Stripe (Balance not covered by account credits)
            $amount_stripe = number_format($order_balance, 2) * 100; //amount in cents
            $charge = null;
            $customer_id = $user->customer->stripe_customer_id;

            if ($amount_stripe > 0) {

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                $payment_method_id = DB::table('payment_methods')->where('method', 'stripe')->first()->id;
                
                $payment->credit_applied = $credit_applied;
                $payment->total_amount = $order_balance;
                $payment->payment_method_id = $payment_method_id;
                $payment->save();

                // Charge the Customer
                $charge = \Stripe\Charge::create(array(
                      "amount" => $amount_stripe, // amount in cents, again
                      "currency" => "usd",
                      "customer" => $customer_id,
                      "description" => $description
                ));
            }
                    
            if ($charge) {
                $order->order_status = "paid";
                $order->save();

                $payment->transaction_id = $charge->id;
                $payment->stripe_customer_id = $customer_id ?? null;
                $payment->captured = $charge->captured;
                $payment->paid = $charge->paid;
                $payment->completed_at = date("Y-m-d H:i:s", $charge->created); 
                $payment->save();       
            }

            //Insert Into Collected Taxes Table
            if ($cart_tax > 0) {
                dispatch(new StoreCollectedTaxes($order, $taxable_sales));
            }
            
            //Create Shipment
            $shipment->order_id = $order->id;
            $shipment->save();
            
            //Update Date For Next Payment
            $recurring_order->last_charge = Carbon\Carbon::now();
            $recurring_order->next_charge = Carbon\Carbon::now()->addDays($recurring_order->days);
            $recurring_order->save();

            //Email Customer Confirmation
            $subject = "Mountain Myst Confirmation - Order #{$order->ref_id}";
            $email_view = 'emails.payment_confirmation';
            
            dispatch(new SendOrdersMail($order, $cart_items, $order->email, $subject, $email_view));
            
            //Notify Admin
            /* 
            $email_admin = config('mail.from.address');
            $subject_admin= "New Recurring Order: {$order->ref_id}";
            $email_view_admin = 'emails.order_created';
            
            dispatch(new SendOrdersMail($order, $cart_items, $email_admin, $subject_admin, $email_view_admin));*/

            \Session::flash('success', "New Order #$order->id (#$order->ref_id) Created!");

            return redirect()->back();           

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect()->back();
        }
    }
        


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recurring_orders = RecurringOrder::all()->sortBy('next_charge');
        //dd($recurring_orders);

        return view('admin.recurring_orders.index', compact('recurring_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($recurring_order_id)
    {
        $recurring_order = RecurringOrder::where('id', $recurring_order_id)->first();

        return view('admin.recurring_orders.show', compact('recurring_order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

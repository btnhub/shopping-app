<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SubCategory;
use App\Category;
use Faker\Factory;

class SubCategoryController extends Controller
{
    public function listProducts($cat_slug = null, $subcategory_id, $sub_slug = null)
    {
        //For Front End
        $subcategory = SubCategory::find($subcategory_id);

        return view('frontend.subcategory_products', compact('subcategory'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $subcategories = SubCategory::all();

        return view('admin.subcategories.index', compact('subcategories', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->pluck('name', 'id');
        return view('admin.subcategories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['image_file' => 'image']);

        unset($request['_token']);
        
        $faker = Factory::create();

        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        
        $request->request->add(['ref_id' => $ref_id]);
        $subcategory = SubCategory::create($request->except('image_file'));

        if ($request->hasFile('image_file')) {
            $new_filename = $subcategory->id.'-'.$ref_id.'-'.$request->file('image_file')->getClientOriginalName();
            $file_move = $request->file('image_file')->move(public_path('images/categories/subcategories'), $new_filename);
            //$request->merge(['image' => $new_filename]);
            $subcategory->image = $new_filename;
            $subcategory->save();
        }

        \Session::flash('success', 'Subcategory Added!');

        return redirect()->route('subcategories.index');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = SubCategory::find($id);
        $categories = Category::all()->pluck('name', 'id');

        return view('admin.subcategories.edit', compact('subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['image_file' => 'image']);

        $subcategory = SubCategory::find($id);

        if ($request->hasFile('image_file')) {
            $new_filename = $subcategory->id.'-'.$subcategory->ref_id.'-'.$request->file('image_file')->getClientOriginalName();
            $file_move = $request->file('image_file')->move(public_path('images/categories/subcategories'), $new_filename);
            $request->merge(['image' => $new_filename]);
        }
        
        unset($request['_token']);       
        
        $subcategory->update($request->except('image_file'));

        \Session::flash('success', 'Subcategory Updated!');

        return redirect()->route('subcategories.index');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubCategory::destroy($id);

        \Session::flash('success', 'Subcategory Removed!');

        return redirect()->route('subcategories.index');   
    }
}

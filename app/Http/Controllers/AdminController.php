<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Membership;
use App\Payment;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
    	$sales_today = Payment::where('created_at', '>', Carbon::now()->subHours(24))
    				->where('paid',1)
    				->get()
    				->sum('amount');
    	$sales_week = Payment::where('created_at', '>', Carbon::now()->subDays(7))
    				->where('paid',1)
    				->get()
    				->sum('amount');
    	$sales_month = Payment::where('created_at', '>', Carbon::now()->subWeeks(4))
    				->where('paid',1)
    				->get()
    				->sum('amount');

        $registered_users = User::count();
        $registered_last_week = User::where('created_at', '>', Carbon::now()->subWeek())->count();
        $registered_last_month = User::where('created_at', '>', Carbon::now()->subWeeks(4))->count();

        $members = User::has('memberships')->count();
        $subscribers = User::has('recurring_orders')->count();

    	return view('admin.index', compact('sales_today', 'sales_week', 'sales_month', 'registered_users', 'registered_last_week', 'registered_last_month', 'members', 'subscribers'));
    }

    public function cloneUser($user_id)
    {
        auth()->loginUsingId($user_id);
        $user = auth()->user();
        \Session::flash('success', "Logged In As $user->full_name");
        
        return redirect()->route('user.account', [$user->ref_id]);
    }

    /**
     * Add user to hidden memberships
     * @param [type] $user_id       [description]
     * @param [type] $membership_id [description]
     */
    public function addUserToMembership(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $membership = Membership::find($request->membership_id);

        $user->memberships()->save($membership, [
                                        'active' => 1, 
                                        'price' => 0,
                                        'start_date' => Carbon::now(),
                                        'end_date' => Carbon::now()->addYear(),
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        ]);

        \Session::flash('sucess', "$user->full_name Added To $membership->name");
        
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Product;

class WishlistController extends Controller
{
    public function getWishlist($user_ref_id){
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);
        
        return view('customers.wishlist', compact('user'));
    }

    public function addProduct($product_ref_id){
    	$user = auth()->user();
    	
        $this->authorize('self-access', $user);

        $product = Product::where('ref_id', $product_ref_id)->first();

    	$user->wishlist()->syncWithoutDetaching([$product->id]);

    	\Session::flash('success', "{$product->name} Added To Wishlist");
    	
    	return redirect()->back();
    }

    public function removeProduct($product_ref_id){
    	$user = auth()->user();
    	
        $this->authorize('self-access', $user);

        $product = Product::where('ref_id', $product_ref_id)->first();
    	$user->wishlist()->detach($product->id);

    	\Session::flash('success', "{$product->name} Removed From Wishlist");
    	
    	return redirect()->back();
    }


}

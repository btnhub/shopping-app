<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;
use App\Product;
use Faker\Factory;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();

        return view('admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd('items create');
        $products = Product::orderBy('id', 'DESC')->get()->pluck('name', 'id');
        return view('admin.items.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                        'image_file' => 'image', 
                        'price' => 'numeric',
                        'organic_price' => 'numeric',
                        'length' => 'numeric',
                        'width' => 'numeric',
                        'height' => 'numeric']);

        unset($request['_token']);
        
        $faker = Factory::create();

        $ref_id = implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3);
        
        $request->request->add(['ref_id' => $ref_id]);
        $item = Item::create($request->except('image_file'));

        if ($request->hasFile('image_file')) {
            $new_filename = sprintf("%03s", $item->id).'-'.$request->file('image_file')->getClientOriginalName();
            $file_move = $request->file('image_file')->move(public_path('images/products/items'), $new_filename);
            //$request->merge(['image' => $new_filename]);
            $item->image = $new_filename;
            $item->save();
        }

        \Session::flash('success', 'Item Added!');

        return redirect()->back();
        //return redirect()->route('items.index');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ref_id)
    {
        dd('items show');
        /*$product = Product::where('ref_id', $ref_id)->first();
        $items = $product->items()->orderBy('price', 'ASC')->get();

        return view('frontend.item_details', compact('product', 'items'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $products = Product::orderBy('id', 'DESC')->get()->pluck('name', 'id');

        return view('admin.items.edit', compact('item', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                        'image_file' => 'image', 
                        'price' => 'numeric',
                        'organic_price' => 'numeric',
                        'length' => 'numeric',
                        'width' => 'numeric',
                        'height' => 'numeric']);

        $item = Item::find($id);
        
        if ($request->hasFile('image_file')) {
            $new_filename = sprintf("%03s", $item->id).'-'.$request->file('image_file')->getClientOriginalName();
            $file_move = $request->file('image_file')->move(public_path('images/products/items'), $new_filename);
            $request->merge(['image' => $new_filename]);
        }
        
        unset($request['_token']);       
        
        $item->update($request->except('image_file'));

        \Session::flash('success', 'Item Updated!');

        return redirect()->back();

        // return redirect()->route('items.index');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::destroy($id);

        \Session::flash('success', 'Item Removed!');

        return redirect()->back();
        
        // return redirect()->route('items.index');  
    }
}

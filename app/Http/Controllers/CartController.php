<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\StoreCartSession;

use Cart;
use Auth;

use App\Item;
use App\Payment;
use App\Order;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart_items = Cart::content();
        
        return view('cart.index', compact('cart_items'));        
    }

    /**
     * Add item(s) to cart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                            'item_detail' => 'required',
                            'item_size' => 'required'
                        ]);
        $item = Item::where('ref_id', $request->item_ref_id)->first();
        $quantity = $request->quantity ?? 1;

        $reorder_frequency = $request->recur ? $request->reorder_frequency : null;
        $organic = $request->organic ?? 1; //default to organic product
        $glass = $request->glass ?? null;
        
        $glass_cost = $glass ? config('other_constants.glass_cost') : 0; //additional cost to package in glass

        //$price = ($organic ? $item->organic_price : $item->price) + $glass_cost;
        
        if ($item->sale || $item->product->sale) {
            $price = $item->sale_price + $glass_cost;
        }
        else{
            $price = ($organic ? $item->organic_price : $item->price) + $glass_cost;
        }
        
        if (auth()->user() && auth()->user()->best_membership()) {
            //apply membership discount to price
            $discount = auth()->user()->best_membership()->discount;
            $price *= (1-$discount);

        }

        Cart::add($item->id, $item->product->name, $quantity, $price, [
                    'organic' => $organic,
                    'glass' => $glass,
                    'reorder_frequency' => $reorder_frequency,])
            ->associate('App\Item');

        if (!session('cart_identifier')) {
            session(['cart_identifier' => mt_rand()]);
        }

        dispatch(new StoreCartSession(session('cart_identifier')));

        \Session::flash('success', "{$item->product->name} Added To Shopping Cart");
        
        return redirect()->back();
    }

    /**
     * Quick Add Item To Shopping Cart -- NOT USED
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ref_id, $quantity = 1)
    {
        /*$item = Item::where('ref_id', $ref_id)->first();


        Cart::add($item->id, $item->product->name, $quantity, $item->price, [])
            ->associate('App\Item');

        \Session::flash('success', "{$item->product->name} Added To Shopping Cart");

        return redirect()->back();   */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $row_id)
    {
        Cart::update($row_id, $request->qty);
        dispatch(new StoreCartSession(session('cart_identifier')));

        \Session::flash('success', "Shopping Cart Updated");

        return redirect()->back();   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($row_id)
    {
        Cart::remove($row_id);
        dispatch(new StoreCartSession(session('cart_identifier')));

        \Session::flash('success', "Shopping Cart Updated");

        return redirect()->back();            
    }

    public function emptyCart()
    {
        Cart::destroy();
        dispatch(new StoreCartSession(session('cart_identifier')));

        \Session::flash('success', "Shopping Cart Updated");

        return redirect()->back();            
    }

    public function reorderItems($order_ref_id){
        $order = Order::where('ref_id', $order_ref_id)->first();
        $restored_cart = unserialize($order->details);
        
        $organic = 0;
        $glass = null;

        $session_type = "success";
        $session_message = "All Items Added To Shopping Cart";

        foreach ($restored_cart as $restored_item) {  
            $organic = $restored_item->options->organic;
            $glass = $restored_item->options->glass;

            $item = $restored_item->model;

            if ($item->available() && $item->product->available()) {
                $glass_cost = $glass ? config('other_constants.glass_cost') : 0;

                $price = ($organic ? $item->organic_price : $item->price) + $glass_cost;

                if (auth()->user() && auth()->user()->best_membership()) {
                    //apply membership discount to price
                    $discount = auth()->user()->best_membership()->discount;
                    $price *= (1-$discount);

                }

                Cart::add($item->id, $item->product->name, $restored_item->qty, $price, [
                        'organic' => $organic,
                        'glass' => $glass])
                        ->associate('App\Item');  
            }

            else
            {
                $session_type = "warning";
                $session_message = "Items Added To Cart.  Please Note That Some Items Are Not Available For Purchase.";  
            }
        }

        if (!session('cart_identifier')) {
            session(['cart_identifier' => mt_rand()]);
        }

        dispatch(new StoreCartSession(session('cart_identifier')));
        
        \Session::flash($session_type, $session_message);

        return redirect()->back();   
        
    }
}

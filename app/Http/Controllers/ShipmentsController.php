<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\SendOrdersMail;

use App\Shipment;
use App\Order;

use DB;
use PDF;

class ShipmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shipments = Shipment::has('order')->with('order')->get()->sortByDesc('id');

        $pending_shipments = $shipments->filter(function($shipment){
                                    return $shipment->order->shipped == 0;});
        
        $completed_shipments = $shipments->filter(function($shipment){
                                    return $shipment->order->shipped == 1;});
    
        return view('admin.shipments.index', compact('pending_shipments', 'completed_shipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Add tracking number
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addTrackingNumber($id, Request $request)
    {
        $this->validate($request, [
                    'shipping_details' => 'required',
                    'tracking_no' => 'required']);

        $order = Order::find($id);
        $order->tracking_no = $request->tracking_no;
        $order->shipped = 1;

        $order->order_status = "shipped";
        $order->save();

        if ($order->shipment) {
            $order->shipment->status = 'shipped';
            
            $shipping_details = trim($request->shipping_details);
            $order->shipment->provider = substr($shipping_details, 0, strpos($shipping_details, ' '));
            $order->shipment->servicelevel_name = substr($shipping_details, strpos($shipping_details, ' '));

            $order->shipment->save();
        }
        
        $cart_items = unserialize($order->details);

        $subject= "Your order has shipped!";
        $email_view = 'emails.order_shipped';
        
        dispatch(new SendOrdersMail($order, $cart_items, $order->email, $subject, $email_view));

        \Session::flash('success', 'Tracking Number Added, Customer Notified Of Shipment');
        
        return redirect()->back();
    }

    public function createPackingSlip($order_id) 
    {
        $order = Order::find($order_id);
        $cart_items = unserialize($order->details);
        $company_name_id = DB::table('settings')->where('field', 'company_name')->first()->id;
        $company_email_id = DB::table('settings')->where('field', 'company_email')->first()->id;
        $company_address_id = DB::table('settings')->where('field', 'company_address')->first()->id;
        $company_logo_id = DB::table('settings')->where('field', 'company_logo')->first()->id;

        $company_name = DB::table('settings_values')->where('setting_id', $company_name_id)->first()->value;
        $company_email = DB::table('settings_values')->where('setting_id', $company_email_id)->first()->value;
        $company_address = DB::table('settings_values')->where('setting_id', $company_address_id)->first()->value;
        $company_logo = DB::table('settings_values')->where('setting_id', $company_logo_id)->first()->value;

        $pdf = PDF::loadView('admin.packing_slip', compact('order', 'cart_items', 'company_name', 'company_email', 'company_address', 'company_logo'));
        return $pdf->stream();
    }

}

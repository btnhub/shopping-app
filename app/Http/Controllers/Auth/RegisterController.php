<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Jobs\SendGeneralMail;

use DB;
use Faker\Factory;
use Carbon;
use App\Credit;
use App\ReCaptcha;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/my-account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function redirectTo()
    {
        $redirectPath = '/my-account';
        if (session('url.intended')) {
            //get path (e.g. '/subscriptions') from url
            $redirectPath = parse_url(session('url.intended'))['path'];
        }
        return $redirectPath;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|email|max:255|unique:users',
            /*'phone' => 'required|max:20',*/
            'password' => 'required|min:6|confirmed',
            'marketing' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $faker = Factory::create();

        $user = User::create([
            'ref_id' => implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3),
            'first_name' => ucwords(trim($data['first_name'])),
            'last_name' => ucwords(trim($data['last_name'])),
            'email' => trim($data['email']),
            /*'phone' => trim($data['phone']),*/
            'password' => bcrypt($data['password']),
            'last_login_date' => Carbon\Carbon::now(),
        ]);

        $subject = 'Welcome To Mountain Myst!';
        $email_view = 'emails.registration_confirmation';

        foreach ($data['marketing'] as $key => $value) {
            DB::table('marketing')->insert([
                    'user_id' => $user->id,
                    'method' => $value,
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now()
                ]);
        }

        dispatch(new SendGeneralMail($user, $subject, $email_view));

        //Promotion: add $5 credit to new user accounts
        $expiration_date = Carbon\Carbon::create(2017,12,31,23,59,59,'America/Denver');
        
        $credit_amount = 5;

        if (Carbon\Carbon::now() < $expiration_date) {
            $faker = Factory::create();
            $credit_code = "P".implode('', $faker->randomElements(range('A', 'Z'), 2))."-".implode('', $faker->randomElements(range('A', 'Z'), 3))."-".implode('', $faker->randomElements(range('A', 'Z'), 4));
            
            $credit = new Credit();
            $credit->recipient_id = $user->id;
            $credit->recipient_name = $user->full_name;
            $credit->recipient_email = $user->email;
            $credit->provider_id = 1;
            $credit->provider_name = "Mountain Myst";
            $credit->amount = $credit_amount;
            $credit->type = 'promotion';
            $credit->taxable = 0;
            $credit->credit_code = $credit_code;
            $credit->expiration_date = $expiration_date;
            $credit->details = "Mountain Myst Promotion (Registration)";
            $credit->approved = 1;
            $credit->approval_date = Carbon\Carbon::now();
            $credit->save();
        }

        

        return $user;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\SendCreditsMail;
use App\Jobs\SendAdminEmail;

use App\User;
use App\Credit;
use App\Customer;

use Faker\Factory;
use Carbon;

class CreditsController extends Controller
{

    public function myCredits($user_ref_id)
    {
        $user = User::where('ref_id', $user_ref_id)->first();
        
        $this->authorize('self-access', $user);

        $credits = $user->credits->filter(function($credit){
                                return $credit->isValid();
                            });

        return view('customers.account_credits', compact('credits'));
    }

    public function purchaseGiftCard(Request $request){
        $this->validate($request, [
            'recipient_name' => 'required',
            'recipient_email' => 'required|email',
            'provider_name' => 'required',
            'provider_email' => 'required|email',
            'amount' => 'required|numeric|min:10|max:250'
            ]);
        
        $recipient_name = ucwords($request->recipient_name);
        $recipient_email = $request->recipient_email;
        $provider_name = ucwords($request->provider_name);
        $provider_email = $request->provider_email;
        $amount = number_format($request->amount, 2);
        $message = $request->message;
        $credit = new Credit();

        session([
                'provider_name' => $provider_name,
                'provider_email' => $provider_email,
                ]);

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Get the credit card details submitted by the form
        $description = "Gift Card Purchase For $recipient_name";
        $amount_stripe = $amount * 100; //amount in cents

        try {
            
            //Use Stored Card (NOTE: Gift Card Form does not allow this option YET)
            if ($request->input("charge_stored_card")) {
                $user = auth()->user();
                $customer_id = $user->customer->stripe_customer_id;

                // Charge the Customer instead of the card
                $charge = \Stripe\Charge::create(array(
                      "amount" => $amount_stripe, // amount in cents, again
                      "currency" => "usd",
                      "customer" => $customer_id,
                      "description" => $description
                ));
            }

            //Or just charge card
            else
            {   
              $token = $_POST['stripeToken'];
              $charge = \Stripe\Charge::create(array(
                    "amount" => $amount_stripe, // amount in cents
                    "currency" => "usd",
                    "source" => $token,
                    "description" => $description,
                    "receipt_email" => $recipient_email,
                ));
            }

            if ($charge) {
                // Create Gift Card Code
                $faker = Factory::create();
                $credit_code = "G".implode('', $faker->randomElements(range('A', 'Z'), 2))."-".implode('', $faker->randomElements(range('A', 'Z'), 3))."-".implode('', $faker->randomElements(range('A', 'Z'), 4));

                $credit->recipient_name = $recipient_name;
                $credit->recipient_email = $recipient_email;
                $credit->provider_name = $provider_name;
                $credit->provider_email = $provider_email;
                $credit->amount = $amount;
                $credit->type = 'gift_card';
                $credit->credit_code = $credit_code;
                $credit->provider_id = $user->id ?? null;
                $credit->expiration_date = Carbon\Carbon::now()->addYear();
                $credit->details = "Gift Card from $provider_name to $recipient_name";
                $credit->message = $message;
                $credit->transaction_id = $charge->id;
                $credit->approved = 1;
                $credit->approval_date = Carbon\Carbon::now();
                $credit->save();
                
                //Update Recurring Order & Items
                
                //Email Sender Payment Confirmation
                $subject_sender= "Gift Card Purchase Confirmation";
                $email_view_sender = 'emails.gift_card_purchase_confirmation';

                dispatch(new SendCreditsMail($credit, $provider_email, $subject_sender, $email_view_sender));
                
                //Email Recipient
                $subject_recipient= "A Gift Card From $provider_name";
                $email_view_recipient = 'emails.gift_card_notify_recipient';

                dispatch(new SendCreditsMail($credit, $recipient_email, $subject_recipient, $email_view_recipient));

                //Notify Admin
                $subject_admin = "Gift Card Purchased ($$amount)";
                $email_view_admin = "emails.notify_admin";
                $admin_message = "Gift Card Purchased for $$amount";

                dispatch(new SendAdminEmail($subject_admin, $email_view_admin, $admin_message));

                return view('gift_cards.purchase_confirmation', compact('recipient_name', 'recipient_email', 'amount'));
            }           

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect('gift-cards');

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect('gift-cards');

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect('gift-cards');

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            
            \Session::flash('error', $error['message']);
            return redirect('gift-cards');
        }
    }

    /**
     * Redeem and apply gift card or referral code without user account
     * @param  request
     */
    public function redeemCreditCode(Request $request){
      //$this->validate($request, ['credit_code' => 'required']);
      $credit_code = $request->credit_code;

      $credit = Credit::where('credit_code', $credit_code)->first();     
      
      if (!$credit || !$credit->isValid()) {
        //If card code isn't found or isn't approved
        \Session::flash('error', "Not Found. Please check the code and try again. Be sure to include the hyphen (-).");
        return redirect()->back();
      }

      if (!is_null($credit->recipient_id)) {
        //Code already associated with different account
        \Session::flash('error', "Code Has Already Been Claimed.");
        return redirect()->back();
      }

      $credit_balance = $credit->balance();

      if ($credit_balance <= 0) {
        \Session::flash('error', "This Code Has $0 Balance");
        return redirect()->back();
      }

      if (!auth()->user()) {
        //If user is not logged in
        if (!session('credit_code')) {
          session(['credit_code' => [$credit_code]]);
        } 

        else {
          $request->session()->push('credit_code', $credit_code);
        }
      }
      else {
        //If user is logged in, associate the gift card with the user
        $credit->recipient_id = auth()->user()->id;
        $credit->save();
      }

      \Session::flash('success', "Account Credit $credit->credit_code Applied!  Balance: $$credit_balance.");

      return redirect()->back();
    }

    public function approveCreditCode(Request $request, $credit_id){
        $credit = Credit::find($credit_id);

        $credit->approved = 1;
        $credit->approved_by = auth()->user()->id;
        $credit->approval_date = Carbon\Carbon::now();
        $credit->save();

        \Session::flash('success', 'Credit Code Approved!');

        return redirect()->back();
    }

    public function index(){
        //Admin Only
        $credits = Credit::all();

        $gift_credits = $credits->where('type', 'gift')
                                    ->filter(function($credit){
                                        return $credit->balance() && $credit->expiration_date->isFuture();
                                    });

        $promotion_credits = $credits->where('type', 'promotion')
                                    ->filter(function($credit){
                                        return $credit->balance() && $credit->expiration_date->isFuture();
                                    });
        $referral_credits = $credits->where('type', 'referral')
                                    ->filter(function($credit){
                                        return $credit->balance() && $credit->expiration_date->isFuture();
                                    });

        $account_credits = $credits->where('type', 'account_credit')
                                    ->filter(function($credit){
                                        return $credit->balance() && $credit->expiration_date->isFuture();
                                    });

        $used_up_credits = $credits->filter(function ($credit){
                                        return $credit->balance() <= 0;
                                    });

        $expired_credits = $credits->filter(function ($credit){
                                        return $credit->expiration_date && $credit->balance() && $credit->expiration_date->isPast();
                                    });

        return view('admin.credits.index', compact('credits', 'gift_credits', 'promotion_credits', 'referral_credits', 'account_credits','used_up_credits','expired_credits'));    
    }    

    public function create(){
        $credit_types = ['' => "Select A Credit Type",
                            'account_credit' => "Account Credit",
                            'referral' => "Referral Credit",
                            'gift' => "Gift Card",
                            'promotion' => "Promotion"];
        $users = User::orderBy('first_name')->get()->pluck('full_name', 'id')->toArray();
        $users = ['' => 'Select User'] + $users;

        return view('admin.credits.create', compact('credit_types', 'users'));
    }

    public function store(Request $request){
        if ($request->recipient_id) {
            $user = User::find($request->recipient_id);
            $request->request->add(['recipient_name' => $user->full_name, 'recipient_email' => $user->email]);
        }

        //dd($request->except('_token', 'send_email', 'expiration_date'));

        $faker = Factory::create();
        $credit_code = "M".implode('', $faker->randomElements(range('A', 'Z'), 2))."-".implode('', $faker->randomElements(range('A', 'Z'), 3))."-".implode('', $faker->randomElements(range('A', 'Z'), 4));
        $expiration_date = Carbon\Carbon::parse($request->expiration_date . " MST")->addHours(23)->addMinutes(59)->addSeconds(59);

        $request->request->add(['credit_code' => $credit_code, 'expiration_date' => $expiration_date]);

        $credit = Credit::create($request->except('_token', 'send_email'));

        $credit->provider_name = "Mountain Myst";
        $credit->provider_id = auth()->user()->id;
        $credit->credit_code = $credit_code;
        $credit->approved = 1;
        $credit->approved_by = auth()->user()->id;
        $credit->approval_date = Carbon\Carbon::now();
        $credit->save();

        if ($request->send_email) {
            $subject_recipient= "$$credit->amount Gift Card From Mountain Myst";
            $email_view_recipient = 'emails.credit_notify_recipient';

            dispatch(new SendCreditsMail($credit, $credit->recipient_email, $subject_recipient, $email_view_recipient));
        }
        else{
            $credit->message = NULL;
            $credit->save();
        }
        

        \Session::flash('success', "Credit Created!  ID: $credit->id, Code: $credit->credit_code");

        return redirect()->route('credits.index');   
    }

    public function show($id){
        $credit = Credit::find($id);
        return view('admin.credits.show', compact('credit'));
    }

    public function edit($id){
        //TEMP in case file contains 
        //return redirect()->route('credits.show', $id);
    }

    public function update(Request $request, $id){
        $credit = Credit::find($id);
        
        return redirect()->back();
    }

    public function destroy($id)
    {
        Credit::destroy($id);

        \Session::flash('success', 'Credit Deleted!');

        return redirect()->route('credits.index');   
    }
}

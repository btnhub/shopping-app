<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Relationships
     */
    
    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function ingredients(){
        return $this->belongsToMany('App\Ingredient')
        				->orderBy('ingredient_recipe.amount', 'DESC')
                        ->withPivot('amount', 'unit', 'organic', 'notes');
    }

    public function vegan(){
        //count non-vegan items
        return $this->ingredients->filter(function ($ingredient){
                       return !$ingredient->vegan;
                    })->count() ? false : true;
    }

    public function gluten_free(){
        //count non-gluten-free items
        return $this->ingredients->filter(function ($ingredient){
                       return !$ingredient->gluten_free;
                    })->count() ? false : true;
    }

    public function dairy_free(){
        //count non-dairy-free items
        return $this->ingredients->filter(function ($ingredient){
                       return !$ingredient->dairy_free;
                    })->count() ? false : true;
    }

    public function soy_free(){
        //count non-soy-free items
        return $this->ingredients->filter(function ($ingredient){
                       return !$ingredient->soy_free;
                    })->count() ? false : true;
    }

    public function nut_free(){
        //count non-nut-free items
        return $this->ingredients->filter(function ($ingredient){
                       return !$ingredient->nut_free;
                    })->count() ? false : true;
    }
}


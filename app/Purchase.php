<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;

   	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }


}

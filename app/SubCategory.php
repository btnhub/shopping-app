<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
	
	protected $table = 'subcategories';

    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];


    /**
     * Relationships
     */
    
    public function category(){
        return $this->belongsTo('App\Category');
    }

    /**
     * For frontend.  Only show products that are visible
     * @return [type] [description]
     */
    public function products(){
        return $this->hasMany('App\Product', 'subcategory_id')
                        ->frontVisible();
    }
    
    /**
     * For backend to show all products
     * @return [type] [description]
     */
    public function all_products(){
        return $this->hasMany('App\Product', 'subcategory_id');
    }

    /**
     * Subcategory visible on the front end
     * Must be set to visible and have products that are visible
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeFrontVisible($query)
    {
        return $query->where('is_visible', 1)
                        ->whereHas('products', function($product){
                            $product->frontVisible();
                        });
    }
}

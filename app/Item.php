<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at'];

    /**
     * Mutators
     */

    public function getSizeAttribute($value)
    {
        return ucfirst($value);
    }

    public function getColorAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Items visible on the front end
     * Must be set to visible and have items that are visible
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeFrontVisible($query)
    {
        return $query->where('is_visible', 1)
                        ->where('discontinued', 0);
    }

    /**
     * Relationships
     */
    

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function recurring_orders(){
        return $this->belongsToMany('App\RecurringOrder')
                    ->whereNull('item_recurring_order.deleted_at')
                    ->withPivot('price', 'quantity', 'details', 'organic')
                    ->withTimestamps();
    }

    public function available()
    {
        if ($this->is_visible && $this->in_stock && !$this->discontinued){
            return true;
        }
        
        return false;
        
    }

    public function details()
    {
        $details = null;

        if ($this->color) {
            $details .= "Color: $this->color <br>";
        }

        if ($this->scent) {
            $details .= "Scent: $this->scent <br>";
        }

        if ($this->flavor) {
            $details .= "Flavor: $this->flavor <br>";
        }

        if ($this->additional_feature) {
            $details .= "Addtl Feature: $this->additional_feature <br>";
        }

        return  rtrim($details, "<br>");

    }
}

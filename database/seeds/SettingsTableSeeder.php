<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        

        DB::table('settings')->insert([
        	'field' => 'company_name',
        	'title' => 'Company Name',
        	'description' => 'Name of business'
        	]);

        DB::table('settings')->insert([
        	'field' => 'company_phone',
        	'title' => 'Company Phone',
        	'description' => 'Phone number of business'
        	]);

        DB::table('settings')->insert([
        	'field' => 'company_email',
        	'title' => 'Company Email',
        	'description' => 'Email address of business'
        	]);
        
        DB::table('settings')->insert([
        	'field' => 'company_address',
        	'title' => 'Company Address',
        	'description' => 'Business Address'
        	]);

        DB::table('settings')->insert([
            'field' => 'company_logo',
            'title' => 'Company Logo',
            'description' => 'Image For Business Logo'
            ]);

        DB::table('settings')->insert([
            'field' => 'company_logo',
            'title' => 'Admin Area Company Logo',
            'description' => 'Image For Business Logo In The Admin Area'
            ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        /*$this->call(UsersTableSeeder::class);*/
        $this->call(PaymentMethodsSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SettingsValuesTableSeeder::class);
        /*$this->call(MembershipsTableSeeder::class);*/
        $this->call(SuppliersTableSeeder::class);
        /*$this->call(CreditsTableSeeder::class);*/
    }
}

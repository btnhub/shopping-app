<?php

use Illuminate\Database\Seeder;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('role_user')->truncate();
        DB::table('customers')->truncate();

        $faker = Faker\Factory::create();
        $customer_cards = collect([
        		(object)["stripe_customer_id" => "cus_ApmWyZQoIGg5GD",
        					"card_brand" => "MasterCard",
        					"card_last_four" => 4444,
        					"exp_month" => "5",
        					"exp_year" => "2019"],

        		(object)["stripe_customer_id" => "cus_ArECmeQrVaG7br",
        					"card_brand" => "Discover",
        					"card_last_four" => 9424,
        					"exp_month" => "6",
        					"exp_year" => "2019"],

        		(object)["stripe_customer_id" => "cus_ArWMlzhJxdMTZp",
        					"card_brand" => "American Express",
        					"card_last_four" => 5,
        					"exp_month" => "1",
        					"exp_year" => "2019"],

        		(object)["stripe_customer_id" => "cus_ArdSg7XAnU5fhL",
        					"card_brand" => "Visa",
        					"card_last_four" => 5556,
        					"exp_month" => "12",
        					"exp_year" => "2019"],

        		(object)["stripe_customer_id" => "cus_AszJsPO9f4ywZ4",
        					"card_brand" => "Diners Club",
        					"card_last_four" => 3237,
        					"exp_month" => "3",
        					"exp_year" => "2018"]
        		]);

        // create admin, customer, staff accounts
        DB::table('users')->insert(
	            [
			        'ref_id' => implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3),
			        'first_name' => $faker->firstName,
			        'last_name' => $faker->lastName,
			        'email' => 'admin@example.com',
			        /*'phone' => $faker->phoneNumber,*/
			        'password' => bcrypt('secret'),
			        'remember_token' => str_random(10),
			        'last_login_date' => $faker->dateTimeThisYear,
			        'created_at' => $faker->dateTimeThisMonth,
			        'updated_at' => $faker->dateTimeThisMonth,
	            ]);
        
        DB::table('role_user')->insert(
	            [
	            	'user_id' => 1,
	            	'role_id' => Role::where('role', 'admin')->first()->id
	            ]);

        
        //Customers
   		$customer_number = 0;
        for ($j=2; $j < 11; $j++) { 
        	$customer_number++;

	        DB::table('users')->insert(
		            [
				        'ref_id' => implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3),
				        'first_name' => $faker->firstName,
				        'last_name' => $faker->lastName,
				        'email' => "customer$customer_number@example.com",
				        'phone' => $faker->phoneNumber,
				        'school' => $faker->randomElement([null, "CU-Boulder", "CU-Denver", "Denver University", "Metro State", "CSU", "CU-Colorado Springs"])
				        'password' => bcrypt('secret'),
				        'remember_token' => str_random(10),
				        'last_login_date' => $faker->dateTimeThisYear,
				        'created_at' => $faker->dateTimeThisMonth,
				        'updated_at' => $faker->dateTimeThisMonth,
		            ]);
	        
	        if ($faker->randomElement([null, 1])) {
	        	$random_card = $customer_cards->random();
	        	DB::table('customers')->insert(
	            	['user_id' => $j,
	            	'stripe_customer_id' => $random_card->stripe_customer_id,
	            	'card_brand' => $random_card->card_brand,
	            	'card_last_four' => $random_card->card_last_four,
	            	'exp_month' => $random_card->exp_month,
	            	'exp_year' => $random_card->exp_year
	            	]);
	        }
    	}

        //Create more random accounts
        for ($i=11; $i < 51; $i++) {     		
       		DB::table('users')->insert(
	            [
			        'ref_id' => implode('', $faker->randomElements(range('A', 'Z'), 3)).$faker->randomNumber(4).implode('', $faker->randomElements(range('A', 'Z'), 2)).$faker->randomNumber(3),
			        'first_name' => $faker->firstName,
			        'last_name' => $faker->lastName,
			        'email' => $faker->safeEmail,
			        'phone' => $faker->phoneNumber,
			        'school' => $faker->randomElement([null, "CU-Boulder", "CU-Denver", "Denver University", "Metro State", "CSU", "CU-Colorado Springs"])
			        'password' => bcrypt('secret'),
			        'remember_token' => str_random(10),
			        'last_login_date' => $faker->dateTimeThisYear,
			        'created_at' => $faker->dateTimeThisMonth,
			        'updated_at' => $faker->dateTimeThisMonth,
			        'deleted_at' => $faker->randomElement([null, null, null, null, $faker->dateTimeThisMonth]),
	            ]);
	        
	        if ($faker->randomElement([null, null, null, null, 1])) {
		        DB::table('role_user')->insert(
			            [
			            	'user_id' => $i,
			            	'role_id' => $faker->numberBetween(3,5)
			            ]);
	    	}
	    }
    }
}

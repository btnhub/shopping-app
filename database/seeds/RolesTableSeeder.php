<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();

        DB::table('roles')->insert(
    		['role' => 'super-user', 
    		'label' => 'Super User'
    		]);

       	DB::table('roles')->insert(
    		['role' => 'admin', 
    		'label' => 'Administrator'
    		]);

       	DB::table('roles')->insert(
            ['role' => 'staff', 
            'label' => 'Staff'
            ]);

       	DB::table('roles')->insert(
    		['role' => 'member', 
    		'label' => 'Member'
    		]);

       	DB::table('roles')->insert(
    		['role' => 'banned', 
    		'label' => 'Banned'
    		]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MembershipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('memberships')->truncate();

        DB::table('memberships')->insert([
        	'name' => 'exclusive',
        	'stripe_plan' => null,
        	'description' => 'Inner Circle Discount',
        	'frequency' => null,
        	'days' => null,
        	'price' => 0,
        	'discount' => 0.3,
        	'free_shipping' => 0,
        	'website_compare' => 0,
        	'created_at' => Carbon\Carbon::now(),
        	'updated_at' => Carbon\Carbon::now()
        	]);

        DB::table('memberships')->insert([
        	'name' => 'balanced',
        	'stripe_plan' => "balanced",
        	'description' => 'Balanced Plan 10% Off, No Free Shipping',
        	'frequency' => "monthly",
        	'days' => 30,
        	'price' => 10,
        	'discount' => 0.1,
        	'free_shipping' => 0,
        	'website_compare' => 1,
        	'created_at' => Carbon\Carbon::now(),
        	'updated_at' => Carbon\Carbon::now()
        	]);

        DB::table('memberships')->insert([
        	'name' => 'holistic',
        	'stripe_plan' => "holistic",
        	'description' => 'Holistic Plan 15% off',
        	'frequency' => "monthly",
        	'days' => 30,
        	'price' => 15,
        	'discount' => 0.15,
        	'free_shipping' => 1,
        	'website_compare' => 1,
        	'created_at' => Carbon\Carbon::now(),
        	'updated_at' => Carbon\Carbon::now()
        	]);

        DB::table('memberships')->insert([
        	'name' => 'enlightened',
        	'stripe_plan' => "enlightened",
        	'description' => 'Enlightnened Plan 25% off',
        	'frequency' => "monthly",
        	'days' => 30,
        	'price' => 25,
        	'discount' => 0.25,
        	'free_shipping' => 1,
        	'website_compare' => 1,
        	'created_at' => Carbon\Carbon::now(),
        	'updated_at' => Carbon\Carbon::now()
        	]);
    }
}

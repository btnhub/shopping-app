<?php

use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->truncate();

        DB::table('payment_methods')->insert(
    		['method' => 'stripe', 
    		'title' => 'Stripe',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
    		]);

        DB::table('payment_methods')->insert(
    		['method' => 'paypal', 
    		'title' => 'PayPal',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
    		]);

        DB::table('payment_methods')->insert(
    		['method' => 'square', 
    		'title' => 'Square',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
    		]);

        DB::table('payment_methods')->insert(
    		['method' => 'cash', 
    		'title' => 'Cash',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
    		]);

        DB::table('payment_methods')->insert(
    		['method' => 'check', 
    		'title' => 'Check',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
    		]);

        DB::table('payment_methods')->insert(
            ['method' => 'credit', 
            'title' => 'Account Credit',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
            ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class SettingsValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings_values')->truncate();

        DB::table('settings_values')->insert([
        	'setting_id' => 1,
        	'value' => 'Mountain Myst'
        	]);

        DB::table('settings_values')->insert([
        	'setting_id' => 2,
        	'value' => '765-543-7542'
        	]);

        DB::table('settings_values')->insert([
        	'setting_id' => 3,
        	'value' => 'Contact@Mountain-Myst.com'
        	]);
        
        DB::table('settings_values')->insert([
        	'setting_id' => 4,
        	'value' => '1905 15th St. # 1932, Boulder, CO 80306'
        	]);

        DB::table('settings_values')->insert([
            'setting_id' => 5,
            'value' => 'mountain_myst_logo.png'
            ]);

        DB::table('settings_values')->insert([
            'setting_id' => 6,
            'value' => 'mountain_myst_logo_inverted.png'
            ]);
    }
}

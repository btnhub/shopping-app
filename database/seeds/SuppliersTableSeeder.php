<?php

use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->truncate();

        DB::table('suppliers')->insert(
    		['name' => 'Handmade', 
    		'website' => null,
    		'created_at' => Carbon\Carbon::now(),
    		'updated_at' => Carbon\Carbon::now(),
    		]);

        DB::table('suppliers')->insert(
    		['name' => 'Bulk Apothecary', 
    		'website' => "http://www.bulkapothecary.com/",
    		'created_at' => Carbon\Carbon::now(),
    		'updated_at' => Carbon\Carbon::now(),
    		]);

        DB::table('suppliers')->insert(
    		['name' => 'Brambleberry', 
    		'website' => "https://www.brambleberry.com/",
    		'created_at' => Carbon\Carbon::now(),
    		'updated_at' => Carbon\Carbon::now(),
    		]);

        DB::table('suppliers')->insert(
    		['name' => 'Mountain Rose Herbs', 
    		'website' => "https://www.mountainroseherbs.com/",
    		'created_at' => Carbon\Carbon::now(),
    		'updated_at' => Carbon\Carbon::now(),
    		]);

        DB::table('suppliers')->insert(
    		['name' => 'Natural Grocers', 
    		'website' => "https://www.naturalgrocers.com/",
    		'created_at' => Carbon\Carbon::now(),
    		'updated_at' => Carbon\Carbon::now(),
    		]);

        DB::table('suppliers')->insert(
    		['name' => 'Sprouts', 
    		'website' => null,
    		'created_at' => Carbon\Carbon::now(),
    		'updated_at' => Carbon\Carbon::now(),
    		]);
    }
}

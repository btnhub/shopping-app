<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();
        DB::table('items')->truncate();

        $faker = Faker\Factory::create();

        $product1 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 1,
			        'name' => 'Moisturizing Body Cream Scented With Essential Oils',
			        'description' => 'Selection of body creams scented with Essential Oils',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'image' => 'Sub01-Lotion_mint.jpg',
			        'notes' => 'something about the product that only admin can read',
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '2 oz',
		        /*'image' => 'SS-Calendula-Lotion-444235048.jpg',*/
		        'scent' => 'Vanilla',
		        'price' => 20,
		        'length' => '5',
		        'width' => '5',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '100',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '4 oz',
		        'scent' => 'Vanilla',
		        'price' => 35,
		        'length' => '6',
		        'width' => '6',
		        'height' => '4',
		        'distance_unit' => 'cm',
		        'weight' => '200',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '2 oz',
		        /*'image' => 'SS-Calendula-Lotion-444235048.jpg',*/
		        'scent' => 'Mint',
		        'price' => 20,
		        'length' => '5',
		        'width' => '5',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '100',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '4 oz',
		        'scent' => 'Mint',
		        'price' => 35,
		        'length' => '6',
		        'width' => '6',
		        'height' => '4',
		        'distance_unit' => 'cm',
		        'weight' => '200',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '2 oz',
		        /*'image' => 'SS-Calendula-Lotion-444235048.jpg',*/
		        'scent' => 'Lavender',
		        'price' => 20,
		        'length' => '5',
		        'width' => '5',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '100',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '4 oz',
		        'scent' => 'Lavender',
		        'price' => 35,
		        'length' => '6',
		        'width' => '6',
		        'height' => '4',
		        'distance_unit' => 'cm',
		        'weight' => '200',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '2 oz',
		        /*'image' => 'SS-Calendula-Lotion-444235048.jpg',*/
		        'scent' => 'Grapefruit',
		        'price' => 20,
		        'length' => '5',
		        'width' => '5',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '100',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 1,
		        'size' => '4 oz',
		        'scent' => 'Grapefruit',
		        'price' => 35,
		        'length' => '6',
		        'width' => '6',
		        'height' => '4',
		        'distance_unit' => 'cm',
		        'weight' => '200',
		        'mass_unit' => 'g',
		        /*'notes' => 'Who can see me?',*/
		        'created_at' => $faker->dateTimeThisMonth()
            ]);


        $product2 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 2,
			        'name' => 'Chamomile Infused Body Cream',
			        'description' => 'Body cream made with oils infused with chamomile',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'image' => 'Sub02-Chamomile_Body_Cream.jpg',
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);
        
        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 2,
		        'size' => '2 oz',
		        'scent' => 'Vanilla',
		        'price' => 20,
		        'length' => '5',
		        'width' => '5',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '100',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 2,
		        'size' => '4 oz',
		        'scent' => 'Vanilla',
		        'price' => 35,
		        'length' => '6',
		        'width' => '6',
		        'height' => '4',
		        'distance_unit' => 'cm',
		        'weight' => '200',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);


		$product3 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 2,
			        'name' => 'Calendula Infused Body Cream',
			        'description' => 'Body cream made with oils infused with calendula',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'image' => 'Sub02-Calendula_Body_Cream.jpg',
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);
        
        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 3,
		        'size' => '2 oz',
		        'scent' => 'Vanilla',
		        'price' => 20,
		        'length' => '5',
		        'width' => '5',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '100',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 3,
		        'size' => '4 oz',
		        'scent' => 'Vanilla',
		        'price' => 35,
		        'length' => '6',
		        'width' => '6',
		        'height' => '4',
		        'distance_unit' => 'cm',
		        'weight' => '200',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        $product4 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 4,
			        'name' => 'Hand & Body Bar Soap',
			        'description' => 'Handmade hand and body bar soap',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'image' => '',
			        'notes' => 'blah blah blah',
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 4,
		        'size' => '3 oz',
		        'scent' => 'Lemongrass',
		        'price' => 6,
		        'length' => '9',
		        'width' => '3',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '120',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 4,
		        'size' => '3 oz',
		        'scent' => 'Grapefruit',
		        'price' => 6,
		        'length' => '9',
		        'width' => '3',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '120',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);
       
        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 4,
		        'size' => '3 oz',
		        'scent' => 'Mojito',
		        'price' => 6,
		        'length' => '9',
		        'width' => '3',
		        'height' => '5',
		        'distance_unit' => 'cm',
		        'weight' => '120',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        $product5 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 6,
			        'name' => 'Honey Facial Wash',
			        'description' => 'Facial wash made with honey for a moisturizing clean',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 5,
		        'size' => '4 fl. oz',
		        'price' => 10,
		        'length' => '4',
		        'width' => '4',
		        'height' => '13',
		        'distance_unit' => 'cm',
		        'weight' => '180',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

		
		$product6 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 6,
			        'name' => 'Oil Cleansers',
			        'description' => 'Blend of oils for the oil cleansing method',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 6,
		        'size' => '4 fl. oz',
		        'price' => 15,
		        'length' => '4',
		        'width' => '4',
		        'height' => '13',
		        'distance_unit' => 'cm',
		        'weight' => '130',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        $product7 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 7,
			        'name' => 'Natural Deodorant',
			        'description' => 'Natural deodorant with baking soda',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'notes' => 'blah blah blah',
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 7,
		        'size' => '2.6 oz',
		        'scent' => 'Lavender Mint',
		        'price' => 10,
		        'length' => '6',
		        'width' => '3',
		        'height' => '12',
		        'distance_unit' => 'cm',
		        'weight' => '75',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 7,
		        'size' => '2.6 oz',
		        'scent' => 'Rosemary Mint',
		        'price' => 10,
		        'length' => '6',
		        'width' => '3',
		        'height' => '12',
		        'distance_unit' => 'cm',
		        'weight' => '75',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 7,
		        'size' => '2.6 oz',
		        'scent' => 'Grapefruit',
		        'price' => 10,
		        'length' => '6',
		        'width' => '3',
		        'height' => '12',
		        'distance_unit' => 'cm',
		        'weight' => '75',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);
        
        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 7,
		        'size' => '2.6 oz',
		        'scent' => 'Rose Geranium',
		        'price' => 10,
		        'length' => '6',
		        'width' => '3',
		        'height' => '12',
		        'distance_unit' => 'cm',
		        'weight' => '75',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        $product8 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 8,
			        'name' => 'Lip Balms',
			        'description' => 'Natural lip balm',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 8,
		        'size' => '0.25 oz',
		        'scent' => 'Lavender Mint',
		        'price' => 3,
		        'length' => '1.5',
		        'width' => '1.5',
		        'height' => '7',
		        'distance_unit' => 'cm',
		        'weight' => '20',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 8,
		        'size' => '0.25 oz',
		        'scent' => 'Rosemary Mint',
		        'price' => 3,
		        'length' => '1.5',
		        'width' => '1.5',
		        'height' => '7',
		        'distance_unit' => 'cm',
		        'weight' => '20',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 8,
		        'size' => '0.25 oz',
		        'scent' => 'Chocolate Mint',
		        'price' => 3,
		        'length' => '1.5',
		        'width' => '1.5',
		        'height' => '7',
		        'distance_unit' => 'cm',
		        'weight' => '20',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 8,
		        'size' => '0.25 oz',
		        'scent' => 'Mojito',
		        'price' => 3,
		        'length' => '1.5',
		        'width' => '1.5',
		        'height' => '7',
		        'distance_unit' => 'cm',
		        'weight' => '20',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 8,
		        'size' => '0.25 oz',
		        'scent' => 'Hibiscus',
		        'price' => 3,
		        'length' => '1.5',
		        'width' => '1.5',
		        'height' => '7',
		        'distance_unit' => 'cm',
		        'weight' => '20',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);

        $product9 = DB::table('products')->insert(
	            [
			        'ref_id' => $faker->randomNumber(8),
			        'subcategory_id' => 9,
			        'name' => 'Muscle Salve',
			        'description' => 'Muscle salve for sore muscles',
			        'website_description' => $faker->paragraph(3),
			        'detailed_description' => $faker->paragraph(11),
			        'created_at' => $faker->dateTimeThisMonth()
	            ]);

        DB::table('items')->insert(
            [
		        'ref_id' => $faker->randomNumber(8),
		        'product_id' => 9,
		        'size' => '2 oz',
		        'scent' => 'Marjoram',
		        'price' => 10,
		        'length' => '6.5',
		        'width' => '6.5',
		        'height' => '2',
		        'distance_unit' => 'cm',
		        'weight' => '70',
		        'mass_unit' => 'g',
		        'created_at' => $faker->dateTimeThisMonth()
            ]);
    }
}

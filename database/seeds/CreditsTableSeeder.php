<?php

use Illuminate\Database\Seeder;

use App\User;

class CreditsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credits')->truncate();
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) { 
        	$recipient = User::all()->random();
        	$provider = User::all()->random();

        	$type = $faker->randomElement(['gift', 'gift', 'referral']);

        	if ($type == 'gift') {
        		$credit_code = "G".implode('', $faker->randomElements(range('A', 'Z'), 2))."-".implode('', $faker->randomElements(range('A', 'Z'), 3))."-".implode('', $faker->randomElements(range('A', 'Z'), 4));
        		
        		$taxable = 1;
        		
        		$amount = $faker->randomElement([10,25,50,125]);
        		$recipient_id = $faker->randomElement([null, null, $recipient->id]);
        		$recipient_name = "Recipient Name";
        		$recipient_email = "recipient@example.com";

        		if ($recipient_id) {
        			$recipient_name = $recipient->full_name;
        			$recipient_email = $recipient->email;
        		}

        		$provider_id = $faker->randomElement([null, $provider->id]);
        		$provider_name = "Provider Name";
        		$provider_email = "provider@example.com";

        		if ($provider_id) {
        			$provider_name = $provider->full_name;
        			$provider_email = $provider->email;
        		}

        		$details = "Gift Card From $provider_name";
        		$message = $faker->paragraph(3);
        		$transaction_id = rand(10000000, 99999999);
        		$approved = $approved_by = 1;
        		$approval_date = $faker->dateTimeThisYear;
        	}

        	else
        	{
        		$credit_code = "R".implode('', $faker->randomElements(range('A', 'Z'), 2))."-".implode('', $faker->randomElements(range('A', 'Z'), 3))."-".implode('', $faker->randomElements(range('A', 'Z'), 4));

        		$taxable = 0;
        		$amount = 10;
        		$recipient_id = $recipient->id;
        		$recipient_name = $recipient->full_name;
        		$recipient_email = $recipient->email;
        		$provider_id = 1;
        		$provider_name = "Admin Referral Credit";
        		$provider_email = null;
        		$details = "Referral Credit For Referring {email of referred customer}";
        		$message = $transaction_id = $approved_by = $approval_date = null;
        		
        		$approved = $faker->randomElement([1,0]);
        		$approved_by = $approval_date = null;

        		if ($approved) {
        			$approved_by = 1;
        			$approval_date = $faker->dateTimeThisYear;
        		}

        	}

        	DB::table('credits')->insert([
        		'type' => $type,
        		'taxable' => $taxable,
        		'credit_code' => $credit_code,
        		'amount' => $amount,
        		'recipient_id' => $recipient_id,
        		'recipient_name' => $recipient_name,
        		'recipient_email' => $recipient_email,
        		'provider_id' => $provider_id,
        		'provider_name' => $provider_name,
        		'provider_email' => $provider_email,
        		'expiration_date' => $faker->dateTimeThisYear,
        		'details' => $details,
        		'message' => $message,
        		'transaction_id' => $transaction_id,
        		'approved' => $approved,
        		'approved_by'=> $approved_by,
        		'approval_date' => $approval_date,
        		'created_at' => $faker->dateTimeThisYear,
        		'updated_at' => $faker->dateTimeThisYear,
        		]);
        }     
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id')
                    ->foreign('supplier_id')
                    ->references('id')
                    ->on('suppliers')
                    ->onDelete('cascade');
            $table->string('transaction_id')->nullable();
            $table->date('purchase_date')->nullable();
            $table->decimal('amount', 10,2)->nullable()
                        ->comment('negative means refunded amount');;
            $table->decimal('shipping_fee', 10,2)->nullable();
            $table->decimal('sales_tax', 10,2)->nullable();
            $table->decimal('total', 10,2)->nullable();
            $table->date('date_received')->nullable();
            $table->boolean('use_tax_required')->nullable();
            $table->decimal('state_use_tax', 10, 2)->nullable(); 
            $table->decimal('county_use_tax', 10, 2)->nullable(); 
            $table->decimal('city_use_tax', 10, 2)->nullable();   
            $table->decimal('special_use_tax', 10, 2)->nullable(); 
            $table->boolean('use_tax_paid')->default(0)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->boolean('reviewed')->default(0);
            $table->integer('user_id')
                    ->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->nullable();
            $table->integer('recurring_order_id')
                    ->foreign('recurring_order_id')
                    ->references('id')
                    ->on('recurring_orders')
                    ->onDelete('cascade')
                    ->unsigned()
                    ->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_address_2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->text('details')->nullable();
            $table->string('order_status')->nullable();
            $table->boolean('skip_grace_period')
                    ->default(0)
                    ->comment("skip 24 hour waiting period to cancel order");
            $table->boolean('cancelled')->default(0);
            $table->boolean('shipped')->default(0);
            $table->date('shipping_date')->nullable();
            $table->string('tracking_no')->nullable();
            $table->string('tracking_link')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

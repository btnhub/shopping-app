<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawMaterialPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_material_purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('raw_material_id')
                    ->foreign('raw_material_id')
                    ->references('id')
                    ->on('raw_materials')
                    ->onDelete('cascade');
            $table->integer('purchase_id')
                    ->foreign('purchase_id')
                    ->references('id')
                    ->on('purchases')
                    ->onDelete('cascade');
            $table->integer('quantity_ordered')->nullable();
            $table->integer('quantity_defective')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->decimal('discount', 10, 2)->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_material_purchase');
    }
}

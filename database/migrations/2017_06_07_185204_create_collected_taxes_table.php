<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectedTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collected_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')
                    ->foreign('payment_id')
                    ->references('id')
                    ->on('payments')
                    ->onDelete('cascade')
                    ->nullable();
            $table->decimal('taxable_sales', 10, 2)
                    ->comment('negative means refunded amount');
            $table->decimal('state_tax', 10, 2)->nullable(); 
            $table->decimal('county_tax', 10, 2)->nullable(); 
            $table->decimal('city_tax', 10, 2)->nullable();   
            $table->decimal('special_tax', 10, 2)->nullable(); 
            $table->boolean('reported')->nullable(); 
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collected_taxes');
    }
}

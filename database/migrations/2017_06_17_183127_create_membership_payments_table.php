<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('user_id');
            $table->integer('membership_id');
            $table->decimal('amount', 10,2)->nullable();
            $table->string('transaction_id')->nullable();
            $table->boolean('paid')->default(0);
            $table->datetime('completed_at')->nullable();
            $table->boolean('refunded')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_payments');
    }
}

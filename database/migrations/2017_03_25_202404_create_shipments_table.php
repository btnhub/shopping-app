<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')
                    ->foreign('order_id')
                    ->references('id')
                    ->on('orders')
                    ->onDelete('cascade')
                    ->nullable();
            $table->string('status')->nullable();            
            $table->string('provider')->nullable();
            $table->string('servicelevel_name')->nullable();
            $table->string('days')->nullable();
            $table->string('duration_terms')->nullable();
            $table->string('shippo_rate_id')->nullable();
            $table->string('shippo_shipment_id')->nullable();
            $table->string('shippo_parcel_id')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}

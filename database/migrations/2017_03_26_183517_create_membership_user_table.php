<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('membership_id')
                    ->foreign('membership_id')
                    ->references('id')
                    ->on('memberships')
                    ->onDelete('cascade');
            $table->integer('user_id')
                    ->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');            
            $table->string('stripe_subscription_id')->nullable();
            $table->boolean('active')->default(0);
            $table->boolean('cancelled')->default(0);
            $table->decimal('price', 10,2)->nullable();
            $table->integer('frequency')->nullable();
            $table->datetime('start_date');
            $table->datetime('end_date')->nullable();
            $table->datetime('current_period_start')->nullable();
            $table->datetime('current_period_end')->nullable();
            $table->datetime('pause_start_date')->nullable();
            $table->datetime('pause_end_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_user');
    }
}

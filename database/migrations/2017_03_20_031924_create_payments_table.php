<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('order_id')
                    ->foreign('order_id')
                    ->references('id')
                    ->on('orders')
                    ->onDelete('cascade')
                    ->nullable();
            $table->integer('user_id')
                    ->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->nullable();
            $table->string('stripe_customer_id')->nullable();
            $table->string('transaction_id')->unique();
            $table->string('billing_address')->nullable();
            $table->string('billing_address_2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->default('USA')->nullable();
            $table->decimal('amount', 10, 2)->unsigned();
            $table->decimal('discount_amount', 10,2)->unsigned()->nullable();
            $table->decimal('shipping_fee', 10,2)->unsigned()->nullable();
            $table->decimal('sales_tax', 10,2)->unsigned()->nullable();
            $table->decimal('credit_applied', 10,2)->unsigned()->nullable();
            $table->decimal('total_amount', 10, 2)->unsigned();
            $table->string('coupon_id')->nullable();
            $table->string('invoice_id')->nullable();
            $table->tinyInteger('payment_method_id')->unsigned()
                ->foreign('payment_method')
                ->references('id')
                ->on('payment_method')
                ->onDelete('cascade');
            $table->boolean('captured')->default(0);
            $table->boolean('paid')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->boolean('refunded')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

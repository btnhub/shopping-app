<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('credit_id');
            $table->integer('order_id');
            $table->integer('user_id')->nullable();
            $table->decimal('amount', 10,2)->comment('credit applied to order, negative means refunded');
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_order');
    }
}

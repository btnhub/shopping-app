<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('tax_state_id')->unsigned();
            $table->string('city', 100)->nullable();
            $table->text('zip_codes')->nullable();
            $table->decimal('county_rate', 6, 4)->nullable();
            $table->decimal('city_rate', 6, 4)->nullable();
            $table->decimal('special_rate', 6, 4)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('account_number')->nullable();
            $table->string('filing_frequency', 50)->nullable();
            $table->string('website')->nullable();
            $table->text('credentials')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_cities');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_recipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id')
                    ->foreign('recipe_id')
                    ->references('id')
                    ->on('recipes')
                    ->onDelete('cascade');
            $table->integer('ingredient_id')
                    ->foreign('ingredient_id')
                    ->references('id')
                    ->on('ingredients')
                    ->onDelete('cascade');
            $table->decimal('amount', 8,2)->comment('of ingredient in recipe')->nullable();
            $table->string('unit')->nullable();
            $table->boolean('organic')->comment('ingredient')->default(0);
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_recipe');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaxCityFiledToCollectedTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collected_taxes', function (Blueprint $table) {
            $table->string('tax_state_id')
                    ->nullable()
                    ->after('payment_id');
            $table->string('tax_city_id')
                    ->nullable()
                    ->after('tax_state_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collected_taxes', function (Blueprint $table) {
            $table->dropColumn('tax_state_id');            
            $table->dropColumn('tax_city_id');
        });
    }
}

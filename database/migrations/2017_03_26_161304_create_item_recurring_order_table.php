<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemRecurringOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_recurring_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')
                ->foreign('item_id')
                ->references('id')
                ->on('items')
                ->onDelete('cascade');
            $table->integer('recurring_order_id')
                ->foreign('recurring_order')
                ->references('id')
                ->on('recurring_orders')
                ->onDelete('cascade');
            $table->boolean('organic')->default(0);
            $table->boolean('glass')->default(0);
            $table->decimal('price', 10,2)->unsigned();
            $table->integer('quantity')->unsigned();
            $table->text('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_recurring_order');
    }
}

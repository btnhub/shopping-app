<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('scientific_name')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->text('disclaimer')->nullable();
            $table->string('type')->nullable();
            $table->boolean('vegan')->default(1);    
            $table->boolean('gluten_free')->default(1);
            $table->boolean('dairy_free')->default(1);
            $table->boolean('soy_free')->default(1);      
            $table->boolean('nut_free')->default(1);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}

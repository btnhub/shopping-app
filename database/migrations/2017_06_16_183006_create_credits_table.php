<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->boolean('taxable')->default(1);
            $table->string('credit_code')->unique();
            $table->decimal('amount', 10,2)->unsigned();
            $table->integer('recipient_id')->nullable();
            $table->string('recipient_name')->nullable();
            $table->string('recipient_email')->nullable();
            $table->integer('provider_id')->nullable();
            $table->string('provider_name')->nullable();
            $table->string('provider_email')->nullable();
            $table->datetime('expiration_date');
            $table->text('details')->comment('description seen by user')->nullable();
            $table->text('message')->comment('message with gift card')->nullable();
            $table->string('transaction_id')->comment('stripe id for gift cards purchased')->nullable();
            $table->boolean('approved')->default(0);
            $table->integer('approved_by')->nullable();
            $table->datetime('approval_date')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}

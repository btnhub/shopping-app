<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id');
            $table->integer('user_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('payment_id')->nullable();
            $table->string('stripe_refund_id')->nullable();
            $table->string('refund_reason')->nullable();
            $table->text('reason_details')->nullable();
            $table->string('refund_method')->nullable();
            $table->decimal('requested_amount', 8,2)->nullable();
            $table->decimal('deductions', 8, 2)->nullable();
            $table->decimal('refunded_sales', 8,2)->nullable();
            $table->decimal('refunded_tax', 8,2)->nullable();
            $table->boolean('refund_issued')->default(0);
            $table->timestamp('processed_date')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refunds');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('stripe_plan')->nullable();
            $table->text('description')->nullable();
            $table->string('frequency')->nullable();
            $table->integer('days')->nullable()->comment('frequncy in days');
            $table->decimal('price', 10, 2)->nullable();
            $table->float('discount', 3,2)->nullable();
            $table->boolean('free_shipping')->default(0);
            $table->boolean('website_compare')->comment('whether plan show up for selection')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}

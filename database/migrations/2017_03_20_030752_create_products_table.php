<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('subcategory_id')
                    ->foreign('subcategory_id')
                    ->references('id')
                    ->on('subcategories')
                    ->onDelete('cascade')
                    ->unsigned();
            $table->integer('supplier_id')
                    ->foreign('supplier_id')
                    ->references('id')
                    ->on('suppliers')
                    ->onDelete('cascade')
                    ->unsigned()
                    ->nullable();        
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('website_description')->nullable();
            $table->text('detailed_description')->nullable();
            $table->text('instructions')->nullable()->comment('how to use product');
            $table->text('ingredients')->nullable();
            $table->text('disclaimer')->nullable();
            $table->string('image')->nullable();
            $table->boolean('glass')->nullable()->comment('able to be packaged in glass');
            $table->boolean('organic_option')->default(1)->nullable()->comment('whether organic option is available');
            $table->boolean('is_visible')->default(1)->nullable();
            $table->boolean('featured')->default(1)->comment('whether image can be featured on homepage');
            $table->boolean('sale')->default(0);
            $table->boolean('surplus')->default(0);
            $table->boolean('in_stock')->default(1);
            $table->integer('website_order')->default(99);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('product_id')
                    ->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade')
                    ->unsigned()
                    ->nullable();  
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->string('scent')->nullable();
            $table->string('flavor')->nullable();
            $table->string('additional_feature')->nullable();
            $table->string('image')->nullable();
            $table->decimal('price', 10,2)->unsigned()->nullable();
            $table->decimal('organic_price', 10,2)->unsigned()->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('distance_unit')->nullable();
            $table->string('weight')->nullable();
            $table->string('mass_unit')->nullable();
            $table->boolean('recurring_option')->default(1)->comment('whether item available for recurring orders');
            $table->boolean('in_stock')->default(1);
            $table->boolean('is_visible')->default(1)->nullable();
            $table->boolean('sale')->default(0);
            $table->decimal('sale_price', 10,2)->unsigned()->nullable();
            $table->boolean('surplus')->default(0);
            $table->decimal('surplus_price', 10,2)->unsigned()->nullable();
            $table->smallinteger('qty_limit')
                    ->unsigned()
                    ->nullable()
                    ->comment('max quantity that can be ordered at once');
            $table->boolean('discontinued')->default(0);
            $table->integer('website_order')->default(99);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}

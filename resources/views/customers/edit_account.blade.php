@extends('layouts.organica.template')
@section('page_title', "Edit Account")
@section('content')

	{!! Form::model($user, ['method' => 'POST', 'route' => ['update.account', $user->ref_id], 'class' => 'form-horizontal']) !!}
	
		<div class="row">
			<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('first_name', 'First Name') !!}
		        {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
		        <small class="text-danger">{{ $errors->first('first_name') }}</small>
		    </div>

		    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('last_name', 'Last Name') !!}
		        {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
		        <small class="text-danger">{{ $errors->first('last_name') }}</small>
		    </div>
		</div>
	    
	    <div class="row">
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('email', 'Email address') !!}
		        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com']) !!}
		        <small class="text-danger">{{ $errors->first('email') }}</small>
		    </div>

		    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('phone', 'Phone Number') !!}
		        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('phone') }}</small>
		    </div>
	    </div>

		<div class="row">
			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('password', 'Password') !!}
		        {!! Form::password('password', ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('password') }}</small>
		    </div>

		    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('password_confirmation', 'Confirm Password') !!}
		        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
		    </div>
		</div>	

	    <div class="row">
		    <div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('shipping_address', 'Shipping Address') !!}
		        {!! Form::text('shipping_address', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('shipping_address') }}</small>
		    </div>
	    </div>

	    <div class="row">
	    	<div class="form-group{{ $errors->has('shipping_address_2') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('shipping_address_2', 'Shipping Address 2') !!}
		        {!! Form::text('shipping_address_2', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('shipping_address_2') }}</small>
		    </div>	
	    </div>

	    <div class="row">
	    	<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('city', 'City') !!}
		        {!! Form::text('city', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('city') }}</small>
		    </div>

		    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('state', 'State') !!}
		        {!! Form::select('state',array_merge(['' => 'Select A State/Province'], config('codes_states')), null, ['id' => 'state', 'class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('state') }}</small>
		    </div>
	    </div>

	    
	    <div class="row">
	    <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('zip', 'Zip Code') !!}
		        {!! Form::text('zip', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('zip') }}</small>
		    </div>	

		    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('country', 'Country') !!}
		        {!! Form::select('country',array_merge(['' => 'Select A Country'], config('codes_country')), null, ['id' => 'country', 'class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('country') }}</small>
		    </div>
	    </div>
	    
	
	    <div class="col-md-4">
	        {!! Form::reset("Reset", ['class' => 'btn btn-warning pull-right']) !!}
	        {!! Form::submit("Update", ['class' => 'btn btn-primary pull-left']) !!}
	    </div>
	
	{!! Form::close() !!}
@endsection
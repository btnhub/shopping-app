@extends('layouts.organica.template')

@section('page_title', "Payment Settings")

@section('content')
	
	@if ($user->customer)
		<div class="col-md-2 col-md-offset-3">
			<h4>Card Details</h4>
			<i class="fa fa-credit-card" aria-hidden="true"></i>
			<b>{{$user->customer->card_brand}}</b> xxxx-{{$user->customer->card_last_four}} <br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Exp: </b>{{$user->customer->exp_month}} / {{$user->customer->exp_year}}
		</div>

		<div class="col-md-4">
			<h4>Update Card</h4>
			{!! Form::open(['method' => 'POST', 'route' => ['update.card', $user->ref_id], 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}
			  
			  @include('layouts.stripe_form')

			  <div class="login-button mt-20 mb-30">
			    <button>Update Card</button>
			  </div>
			{!! Form::close() !!}
		</div>
		
	@else
		<div class="col-md-4 col-md-offset-2">
			<h4>Add A Card</h4>
			To easily reorder your favorite products on a regular basis (e.g. every 15 days, every 30 days), please add a card to your account.
		</div>
		<div class="col-md-4">
			<h4>Card Details</h4>
			{!! Form::open(['method' => 'POST', 'route' => ['add.card', $user->ref_id], 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}
			  
			  @include('layouts.stripe_form')	

			  <div class="login-button mt-20 mb-30">
			    <button>Add Card</button>
			  </div>
			{!! Form::close() !!}
		</div>
	@endif
@endsection
@extends('layouts.organica.template')
@section('page_title', "Credits & Gift Cards")
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h4>My Gift Cards & Credits</h4>
			Listed below are referral credits earned and account credits for returns. {{--  Type in the Credit ID when checking out to apply the credit to your order. --}}
			<br><br>
			A Mountain Myst Gift Card are a great present for friends and family!  <a href="{{url('/gift-cards')}}"><b>Send A Gift Card</b></a>
			<br><br>

			<table class="table">
				<thead>
					<tr>
						<th>Code</th>
						<th>Amount</th>
						<th>Status</th>
						<th>Balance</th>
						<th>Expires On</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($credits as $credit)
						@if($credit->balance() > 0)
							<tr>
								<td>{{$credit->credit_code}}<br>
								{{-- {{ucfirst($credit->type)}} --}}
								</td>
								<td>${{$credit->amount}}</td>
								<td>
									@if ($credit->approved)
										Approved On {{$credit->approval_date->format('m/d/Y')}}
									@else
										<span style="color:darkred">Approval Pending
										</span>
									@endif
								</td>
								<td>${{$credit->balance()}}</td>
								<td>{{$credit->expiration_date->format('m/d/Y')}}</td>
							</tr>
						@endif
					@empty
						<tr>
							<td>No Account Credits</td>
						</tr>
						
					@endforelse
					
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					@include('cart.partials.gift_card_form')
				</div>
			</div>
		</div>
	</div>
	<hr>

	
	<br><Br>
	
</div>
@endsection
@extends('layouts.organica.template')
@section('page_title', "Wish List")

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="section-title mb-30">
		<h2>My Wish List</h2>
	</div>
	<div class="cart-table mb-50 bg-fff">
		<div class="table-content table-responsive">
			<table>
				{{--
				<thead>
					<tr>
						
						<th class="product-remove"></th>
						<th class="product-thumbnail"></th>
						<th class="product-name">Product Name</th>
						<th class="product-price">Unit Price </th>
						<th class="product-quantity">Stock Status</th>
						<th class="product-subtotal"></th>
					</tr>
				</thead>
				--}}
				<tbody>
					@forelse ($user->wishlist as $product)
						<tr class="cart-item">
							<td class="product-remove">
								{!! Form::open(['method' => 'DELETE', 'route' => ['wishlist.remove.product', 'product_ref_id' => $product->ref_id], 'class' => 'delete']) !!}	
									{{--<a href="#" class="remove" title="Remove this item">x</a>--}}
									<center>
										<div class="btn-group">
									    	{!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
									    </div>
								    </center>
								{!! Form::close() !!}
								
							</td>
							<td class="product-thumbnail product-thumbnail-img hidden-sm hidden-xs">
								@if ($product->image)
                                    <a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">
                                        <img src='{{asset("images/products/{$product->image}")}}' alt="" />
                                    </a>
                                @else
                                    <a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">
                                        <img src='{{asset("images/placeholder.png")}}' alt="" />
                                    </a>
                                @endif
							</td>
							<td class="product-name">
								<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">{{$product->name}} </a>
							</td>
							<td class="product-price">
								<span class="amounte">${{$product->items()->orderBy('price', 'asc')->first()->price}} </span>
								{{--<span class="amounte">$300.00 <del>$350.000</del></span>--}}
							</td>
							{{--<td class="product-quantity">
								<span>In Stock</span>
							</td>
							<td class="product-subtotal">
								<div class="coupon text-center">
									<input class="button" name="apply_coupon" value="Add To Cart" type="submit">
								</div>
							</td>--}}
						</tr>	
					@empty
						<tr>
							<td>
								No Items On Wish List.
							</td>
						</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
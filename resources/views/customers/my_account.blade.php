@extends('layouts.organica.template')
@section('page_title', "My Account")
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h4>My Info</h4>
				{{$user->full_name}}<br>
				{{$user->email}}<br>
				{{$user->phone}}<br>
				<br>
				<a href="{{route('edit.account', $user->ref_id)}}">Edit Contact Information</a><br>
			</div>
			<div class="col-md-4">
				<h4>Shipping Address</h4>
				@if ($user->shipping_address)
					{{$user->shipping_address}}<br>
					@if ($user->shipping_address_2)
						{{$user->shipping_address_2}}<br>
					@endif
					{{$user->city}}, {{$user->state}}<br>
					{{$user->country}}
				@else
					None Provided
				@endif
			</div>
			<div class="col-md-4">
				<h4>My Details</h4>
				<a href="{{route('user.wishlist', $user->ref_id)}}">
					<i class="fa fa-heart" aria-hidden="true"></i>
					My Wishlist 
					@if ($user->wishlist->count())
						({{$user->wishlist->count()}})
					@endif
					
				</a><br>
				<a href="{{route('user.subscriptions', $user->ref_id)}}">
					<i class="fa fa-calendar" aria-hidden="true"></i>
					My Subscriptions
					@if ($user->recurring_orders->count())
						({{$user->recurring_orders->count()}})
					@endif
				</a><br>
				<a href="{{route('user.memberships', $user->ref_id)}}">
					<i class="fa fa-users" aria-hidden="true"></i>
					My Membership
					@if ($user->best_membership())
						({{ucwords($user->best_membership()->name)}})
					@endif
				</a><br>
				<a href="{{route('edit.card', $user->ref_id)}}">
					<i class="fa fa-credit-card" aria-hidden="true"></i>
					Payment Settings
				</a><br>
				<a href="{{route('user.credits', $user->ref_id)}}">
					<i class="fa fa-dollar" aria-hidden="true"></i>
					Credits & Gift Cards 
					@if ($user->credit_balance() > 0)
						(Balance: ${{$user->credit_balance()}})
					@endif
				</a><br>
			</div>
		</div>
		<hr>

		<div class="row">
			<?php $items_available = 0; ?>
			<div class="col-md-12">
				<h3>Order History</h3>
				<div class="col-md-offset-1">
					@forelse ($user->recentOrders() as $order)
						<?php $cart_items = unserialize($order->details); ?>
						@if ($cart_items->count() > 0)
							<div class="row">
								<h4>Order #: {{$order->ref_id}}</h4>
								<div class="col-md-5">
									<table class="table table-hover">
										<tbody>
											@foreach ($cart_items as $cart_item)
												<tr>
													<td>
														@if ($cart_item->model->available() && $cart_item->model->product->available() )
															<?php $items_available++ ;?>
															<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}" target="_blank">
																{{$cart_item->name}} 
															</a> 
														@else
															{{$cart_item->name}} 
														@endif
														
														@if ($cart_item->options->organic)
															(Organic)
														@endif
															x {{$cart_item->qty}}
															<br>
														<small>{{$cart_item->model->size ?? ''}} {{$cart_item->options->glass ? 'Glass Container' : ''}} @ ${{number_format($cart_item->price, 2)}}<br>
														{{$cart_item->model->details() ?? ''}} </small>
													</td>
												</tr>
											@endforeach			
										</tbody>
									</table>
								</div>

								<div class="col-md-4">
									Order Date: {{$order->created_at->format('m/d/Y')}}<br>
									Order Status: {{ucwords($order->order_status)}}<br>
									@if (!$order->shipped)
										{{$order->shipment ? "{$order->shipment->provider} {$order->shipment->servicelevel_name}" : ''}}<br>
										 {{$order->tracking_no}}
									@endif
									
									@if ($items_available > 0)
										{!! Form::open(['method' => 'POST', 'route' => ['reorder.items', $order->ref_id], 'class' => 'form-horizontal']) !!}
										    <div class="btn-group">
										        {!! Form::submit("Reorder Items", ['class' => 'btn btn-primary']) !!}
										    </div>
										{!! Form::close() !!}
									@endif
									
									<br>

									{{--Customer Has 24 Hours To Cancel Order--}}
									
									@if ($order->created_at->diffInHours(Carbon\Carbon::now())  < 24)
										@if ($order->skip_grace_period)
											<div class="btn-group">
										        {!! Form::button("Cancel Order", ['class' => 'btn btn-sm btn-danger disabled']) !!}
										    </div>	
										    <br>
										    <small>
										    	24 Hour Grace Period Was Skipped.  <br>
											    Order Cannot Be Canceled Or Refunded
										    </small>
										@elseif (!$order->cancelled)
											{!! Form::open(['method' => 'DELETE', 'route' => ['cancel.order', $order->ref_id], 'class' => 'delete']) !!}
										
											    <div class="btn-group">
											        {!! Form::submit("Cancel Order", ['class' => 'btn btn-sm btn-danger']) !!}
											    </div>
												<br>
												<small>
													You Have {{Carbon\Carbon::now()->diffInHours($order->created_at->addDays(1))}} Hours Left To Cancel This Order <br>
													And Receive A Full Refund.
												</small>
											{!! Form::close() !!}	
										@endif
										
									@endif
									
								</div>
								

							</div>
							<hr>
						@endif
					@empty
						<center><em>No recent orders</em></center>
					@endforelse
				</div>
			</div>
		</div>
	</div>
@endsection
@extends('layouts.organica.template')
@section('page_title', "Subscriptions")

@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="section-title mb-30">
		<h2>My Subscriptions</h2>
	</div>
	<div class="cart-table mb-50 bg-fff">
		@if ($recurring_orders->first() && $recurring_orders->first()->user->best_membership())
			<b>Your Membership Discount Of {{$recurring_orders->first()->user->best_membership()->discount * 100}}% 
			@if ($recurring_orders->first()->user->best_membership()->free_shipping)
				And Free Shipping
			@endif

			Has Been Applied.</b>
			<br><br>
		@endif
		<div class="table-content table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Order</th>
						<th>Cost</th>
						<th>Frequency</th>
						<th>Charge Dates</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@forelse ($recurring_orders as $recurring_order)
						<tr>
							<td>
								<b>Subscription ID: {{$recurring_order->ref_id}}</b><br>
								@foreach ($recurring_order->items as $recurring_item)
									<a href="{{route('product.details', ['cat_slug' => str_slug($recurring_item->product->subcategory->category->name), 'sub_slug' => str_slug($recurring_item->product->subcategory->name), 'prod_slug' => str_slug($recurring_item->product->name), 'product_id' => $recurring_item->product->id])}}">{{$recurring_item->product->name}}</a>  
									{{$recurring_item->size}} 
									{{$recurring_item->pivot->glass ? 'Glass Container': ''}}

									@if ($recurring_item->pivot->organic)
										(Organic)
									@endif

									@if ($recurring_order->items->count() > 1 && Carbon\Carbon::now()->diffInHours($recurring_order->next_charge, false) > 24)
										{!! Form::open(['method' => 'DELETE', 'route' => ['remove.subscription.item', 'recurring_order_ref_id' => $recurring_order->ref_id, 'item_ref_id' => $recurring_item->ref_id], 'class' => 'delete']) !!}
											<div class="btn-group pull-right">
										        {!! Form::submit("X", ['class' => 'btn btn-danger btn-xs']) !!}
										    </div>
										{!! Form::close() !!}	
									@else
										<br>
									@endif
									
									<small>({{$recurring_item->pivot->quantity}} x 
									@if ($recurring_order->user->best_membership())
										<del>${{$recurring_item->pivot->price}}</del> 
										${{number_format($recurring_item->pivot->price  * (1-$recurring_order->user->best_membership()->discount), 2)}})
									@else
										${{$recurring_item->pivot->price}})
									@endif
									</small>

									<br>

									<small>{!!$recurring_item->details()!!}</small><br>
									<br>
								@endforeach
							</td>
							<td>
								<b>Total: ${{$recurring_order->total_cost()}}</b>
								<hr>
								Subtotal: ${{$recurring_order->subtotal_cost()}}<br>
								Shipping: ${{number_format($recurring_order->shipping_price(), 2)}}<br>
								Tax: ${{$recurring_order->tax()}}
								
								
							</td>
							<td>
								Reorder Every {{$recurring_order->days}} Days
							</td>
							<td>
								<b>Last Charge:</b> {{$recurring_order->last_charge->format('m/d/Y')}}<br><br>
								<b>Next Charge: </b>{{$recurring_order->next_charge->format('m/d/Y')}} <br>
							</td>
							<td>
								@if(Carbon\Carbon::now()->diffInHours($recurring_order->next_charge, false) > 24)
									{!! Form::open(['method' => 'DELETE', 'route' => ['subscription.destroy', 'ref_id' => $recurring_order->ref_id], 'class' => 'delete']) !!}	
										<div class="btn-group pull-right">
									        {!! Form::submit("Delete Subscription", ['class' => 'btn btn-danger']) !!}
									    </div>
									{!! Form::close() !!}
								@else
									<button type="button" class="btn btn-danger pull-right" disabled>Delete Subscription**</button>
								@endif
								
							</td>
						</tr>
					@empty
						<tr><td colspan="7">
							<center>No Subscriptions</center>
						</td></tr>
					@endforelse
					
				</tbody>
			</table>
			<small>Orders with a subtotal over ${{config('other_constants.free_shipping_amount')}} (after all discounts are applied) are shipped for free within the Continental USA (48 states).</small>
			<br>
			<small>**Delete an item or subscription at least 1 day prior to the next charge date.</small>		
		</div>
	</div>
</div>
@endsection
@extends('layouts.organica.template')
@section('page_title', "My Membership")
@section('content')

<div class="container">
	<div class="row">
		@if ($user->memberships->count() > 1)
			The highlighted row is the membership currently being applied to your account.
		@endif
		<br>
		<table class="table">
			<thead>
				<tr>
					<th>Membership</th>
					<th>Price</th>
					<th>Start / End Date</th>
					<th>Status</th>
					<th>Next Payment</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse ($user->memberships->sortByDesc('pivot.created_at') as $membership)
					<tr 
						@if ($user->memberships->count() > 1 && $membership == $user->best_membership())
							style="font-weight:bold"
						@endif
						>
						<td>
							{{ucwords($membership->name)}} <br>
							{{$membership->discount * 100}}% off 
							@if ($membership->free_shipping)
								+ Free Shipping
							@endif

							@if ($membership->isStudentMembership() && !$user->hasRole('student'))
			                    <br>
			                  <small> Enrollment verification is required<br>
			                     to activate membership benefits<br>
			                    @if (!$membership->pivot->cancelled)
			                    	If you do not qualify, you <b>must Change This Plan <br> or Cancel it immediately to avoid future charges.</b></small>
			                    @endif
			                @endif
						</td>
						<td>${{$membership->pivot->price}}</td>
						<td>
							Start Date: {{Carbon\Carbon::parse($membership->pivot->start_date)->format('m/d/Y')}}<br>
							End Date: {{$membership->pivot->end_date ? Carbon\Carbon::parse($membership->pivot->end_date)->format('m/d/Y') : "N/A"}}
						</td>
						<td>Active: {!!$membership->active() ? "<span style='color:darkgreen'>Yes</span>" : "<span style='color:darkred'>No</span>"!!}<br>
							{!!$membership->pivot->cancelled ? "<span style='color:darkred'>Cancelled</span>" : ""!!}
						</td>
						<td>
							@if ($membership->pivot->current_period_end != $membership->pivot->end_date && $membership->pivot->current_period_end > Carbon\Carbon::now())
								{{Carbon\Carbon::parse($membership->pivot->current_period_end)->format('m/d/Y')}}
							@else
								<center>-</center>
							@endif
							
						</td>
						<td>
							@if ($membership == $user->current_membership($membership->pivot->stripe_subscription_id))
								@if (!$membership->pivot->cancelled && $membership->pivot->stripe_subscription_id)
									<a href="{{route('change.membership', $membership->pivot->stripe_subscription_id)}}"><button type="button" class="btn btn-primary">Change Plan</button></a>

									{!! Form::open(['method' => 'DELETE', 'route' => ['cancel.membership', 'subscription_id' => $membership->pivot->stripe_subscription_id], 'class' => 'delete']) !!}

									    <div class="btn-group pull-right">
									        
									        {!! Form::submit("Cancel", ['class' => 'btn btn-danger']) !!}
									    </div>
									
									{!! Form::close() !!}
								@elseif ($membership->pivot->cancelled && !is_null($membership->pivot->end_date) &&Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $membership->pivot->end_date)->isFuture())
									{!! Form::open(['method' => 'POST', 'route' => ['reactivate.membership', 'subscription_id' => $membership->pivot->stripe_subscription_id], 'class' => 'form-horizontal']) !!}
									
									    <div class="btn-group pull-right">
									        {!! Form::submit("Reactivate Membership", ['class' => 'btn btn-success']) !!}
									    </div>
										
									{!! Form::close() !!}
								@endif	
							@endif
							
							
						</td>
					</tr>	
				@empty	
					<tr><td colspan="5">Sign up for a Mountain Myst Membership to save on products and shipping!  <a href="{{url('membership')}}">Learn More</a></td></tr>
				@endforelse
				
			</tbody>
		</table>
	</div>
	<div class="row">
		<h2>Recent Payments</h2>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Date</th>
					<th>Membership</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
				@forelse ($user->membership_payments->sortByDesc('created_at') as $payment)	
					<tr>
						<td>{{$payment->ref_id}}</td>
						<td>{{$payment->created_at->format('m/d/Y')}}</td>
						<td>{{ucwords($payment->membership->name)}} Plan</td>
						<td>{{$payment->amount < 0 ? "-$".abs($payment->amount) : "\$$payment->amount"}}</td>
						<td>{!!$payment->paid ? "<span style='color:green'>Paid</span>" : "<span style='color:red'>Payment Failed</span>"!!}</td>
						<td>{{$payment->notes}}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6">No Payments</td>
					</tr>
				@endforelse
				
			</tbody>
		</table>
	</div>
</div>

@endsection
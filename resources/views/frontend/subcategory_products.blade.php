<?php 
    $membership_active_field = \DB::table('settings')->where('field', 'membership_active')->first()->id;
    $membership_active = \DB::table('settings_values')->where('setting_id', $membership_active_field)->first()->value;
?>

@extends('layouts.organica.template')

@section('page_title', $subcategory->name)
@section('subtitle', $subcategory->description)

@section('content')
<div class="container bg-1" style="padding:30px">
	<div class="row">
		<!-- product-vew area start -->
		{{-- ORIGINAL <div class="col-lg-9  col-md-8 col-sm-12 col-xs-12"> --}}
		<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
			{{--Banner For Subcategories--}}
			{{--
			<div class="slider mb-20">
				<img src='{{asset("images/categories/subcategories/$subcategory->image")}}' alt="" />
			</div>
			--}}
			<div class="tab-area shop-tab-area">
				{{--<div class="shop-taitle mb-20">
					<h2>Shop</h2>
				</div>--}}
				
				<div class="tab-menu-area border-bottom mb-30">
					<div class="row">
						<h4>
							{{-- {{$subcategory->products->count()}} {{$subcategory->name}} {{str_plural('Product', $subcategory->products->count())}} --}}
							{{$subcategory->products->count()}} {{str_plural($subcategory->name, $subcategory->products->count())}}
						</h4>
					</div>
					<br>
					<div class="row">
						<div class="col-md-7 col-sm-6 col-xs-12">
							<div class="shop-tab-menu">
								<ul>
									<li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-th"></i></a></li>
									{{-- <li><a href="#tab2" data-toggle="tab"><i class="fa fa-th-list"></i></a></li> --}}
								</ul>
								{{--<span> Items 1-9 of 13</span>--}}
							</div>
						</div>
						{{--
						<div class=" col-md-5 col-sm-6 hidden-xs">
							<div class="woocommerce-ordering text-right">
								<strong>Sort By </strong>
								<select name="orderby">
									<option value="menu_order" selected="selected">Position</option>
									<option value="popularity">Product Name</option>
									<option value="rating">Price</option>
								</select>
								<a href="#"><i class="fa fa-arrow-up"></i></a>
							</div>
						</div>
						--}}
					</div>
				</div>
				
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tab1">
						<div class="row">
							@foreach ($subcategory->products->sortBy('website_order') as $product)
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									@include('layouts.organica.partials.products')
								</div>	
							@endforeach
						</div>
					</div>
					{{-- <div role="tabpanel" class="tab-pane fade" id="tab2">
						<div class="row">
							@foreach ($subcategory->products->sortBy('website_order') as $product)
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="product-wrapper clear border-bottom mb-30">
										<div class="product-img shop-product-img">
											<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">
												@if ($product->image)
													<img src='{{asset("images/products/{$product->image}") }}' alt=""/>
												@else
													<img src='{{asset("images/placeholder.png") }}' alt=""/>
												@endif
											</a>
											@if ($product->new())
									            <span class="new">new</span>
									        @elseif ($product->sale)
            									<span class="sale">Sale</span>
									        @endif
										</div>
										<div class="product-item-details shop-product-item-details">
											<div class="product-name-review">
												<div class="product-name ">
													<strong><a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">{{$product->name}}</a></strong>
												</div>
												<div class="product-review">
								                    from
								                
								                    <span class="special-price">${{$product->items()->orderBy('price', 'asc')->first()->price}}</span>
													<!--
													<span class="old-price">
														<del>$ 46.00</del>
													</span>
													-->
													<p>{!!$product->website_description!!}</p>
													<div class="readmore-btn">
														<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'porduct_id' => $product->id])}}">Learn More<i class="fa fa-long-arrow-right"></i></a>
													</div>
												</div>
											</div>
											<div class="add-to-cart text-uppercase ptb-35">
												
                    							<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}"><button>View Options</button></a>
								                
								                <!--
												<ul>
													<li><button>add to cart</button></li>
													<li>
														<a href="#"><i class="fa fa-adjust"></i></a>
													</li>
													<li>
														<a href="#"><i class="fa fa-heart-o"></i></a>
													</li>
												</ul>
												-->
											</div>
										</div>
									</div>
								</div>								
							@endforeach
						</div>
					</div> --}}
				</div>
			</div>
			<!-- woocommerce-pagination-area -->
			{{--
			<div class="row">
				<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
					<div class="woocommerce-pagination-area pb-40 mb-100 border-bottom fix">
						<div class="woocommerce-pagination pull-left">
							<ul>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
							</ul>
						</div>
						<div class="woocommerce-ordering pull-right">
							<strong>Show</strong>
							<select name="orderby">
								<option value="menu_order" selected="selected">1</option>
								<option value="popularity">2</option>
								<option value="rating">3</option>
							</select>
							<span> per page</span>
						</div>
					</div>
				</div>
			</div>
			--}}
		</div>
	</div>
</div>
@endsection
<?php 
    $membership_active_field = \DB::table('settings')->where('field', 'membership_active')->first()->id;
    $membership_active = \DB::table('settings_values')->where('setting_id', $membership_active_field)->first()->value;
?>

@extends('layouts.organica.template')

@section('page_title', "$product->name")
@section('breadcrumbs', "{$product->subcategory->category->name} > {$product->subcategory->name}")
@section('content')
    
    <!-- simple product area start -->
    <div class="simple-product-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                    <div class="symple-product mb-35">
                        <div class="single-product-tab border">
                            <div class="tab-content">
                                {{--Product Image--}}
                                <div role="tabpanel" class="tab-pane active" id="one">
                                    @if ($product->image)
                                        <a class="popup" href='{{asset("images/products/{$product->image}")}}'>
                                            <img src='{{asset("images/products/{$product->image}")}}' alt="Handmade {{$product->name}}"/>
                                        </a>
                                    @else
                                        <a class="popup" href='{{asset("images/placeholder.png")}}'>
                                            <img src='{{asset("images/placeholder.png")}}' alt="" />
                                        </a>
                                    @endif
                                </div>
                                {{--Additional Images Of Items--}}
                                @foreach ($product->items as $single_item)
                                    @if ($single_item->image && $single_item->image != $product->image)
                                        <div role="tabpanel" class="tab-pane" id="{{$single_item->ref_id}}">
                                        <a class="popup" href='{{asset("images/products/items/{$single_item->image}")}}'>
                                            <img src='{{asset("images/products/items/{$single_item->image}")}}' alt="Handmade {{$product->name}}" />
                                        </a>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        {{-- <div class="single-product-menu mb-30 border">
                            <div class="single-product-active clear next-prev-style">
                                <!--Product Image-->
                                <div class="single-img floatleft">
                                    @if ($product->image)
                                        <a href="#one" data-toggle="tab">
                                            <img src='{{asset("images/products/{$product->image}")}}' alt="Handmade {{$product->name}}" />
                                        </a>
                                    @endif
                                </div>
                                <!--Additional Images For Items-->
                                @foreach ($product->items as $single_item)
                                    @if ($single_item->image && $single_item->image != $product->image)
                                    <div class="single-img floatleft">
                                        <a href="#{{$single_item->ref_id}}" data-toggle="tab">
                                            <img src='{{asset("images/products/items/{$single_item->image}")}}' alt="Handmade {{$product->name}}" />
                                        </a>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="symple-product p-20 mb-30 bg-1">
                        <h3>{{$product->name}}</h3>
                        <div class="product-review simple-product-review">
                            {{--
                            <ul>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            <a href="#">(3 customer reviews)</a><br/>
                            --}}
                            <p>{!!$product->website_description ?? '' !!}</p>
                            
                            @if ($membership_active || $product->sale)
                                <small>starting at </small>
                                @if($product->sale)
                                    <span class="old-price">
                                        <del>${{$min_price}}</del>
                                    </span>
                                    <span class="special-price">${{$min_sale_price}}</span>
                                @else
                                    <span class="special-price">${{$min_price}}</span>
                                @endif
                            @endif
                            @if ($membership_active)
                                <small><span style="color:darkred">Members save {{$max_discount * 100}}%</span></small> <a href="{{url('/membership')}}">learn more</a>
                            @endif
                            
                            
                        </div>

                        <div class="select-area mtb-20 clear">
                            <div class="select-option floatleft">
                                {!! Form::open(['method' => 'POST', 'route' => 'cart.store', 'class' => 'form-horizontal']) !!}
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <div class="radio{{ $errors->has('item_detail') ? ' has-error' : '' }}">
                                                <?php $unique_items = $items->unique(function ($item) {
                                                        return $item['color'].$item['scent'].$item['flavor'].$item['additional_feature'];
                                                            }); 
                                                        $first_item = $unique_items->first();
                                                ?>
                                                

                                                <div class="radio{{ $errors->has('item_detail') ? ' has-error' : '' }}">
                                                    @foreach ($unique_items as $item)
                                                        <?php 
                                                            //$radio_checked = $unique_items->count() == 1 ? "checked" : '';
                                                            $radio_checked = $first_item == $item ? "checked" : '';
                                                        ?>

                                                        <label>
                                                            {!! Form::radio('item_detail', "$item->color/^/$item->scent/^/$item->flavor/^/$item->additional_feature",  null, ['id' => 'radio_id', "onchange" =>"product_price()", $radio_checked]) !!}

                                                            {{strlen(implode(array_filter(array($item->color, $item->scent,$item->flavor, $item->additional_feature)), " - ")) ? implode(array_filter(array($item->color, $item->scent,$item->flavor, $item->additional_feature)), " - ") : $item->product->name}}
                                                            @if (!$product->sale && $item->sale)
                                                                - SALE
                                                            @endif
                                                        </label>
                                                        <br>
                                                    @endforeach
                                                    <small class="text-danger">{{ $errors->first('item_detail') }}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="select-area mb-10 clear">
                                                <div class="select-option">
                                                    <div class="row">
                                                        <div class="form-group{{ $errors->has('item_size') ? ' has-error' : '' }} col-md-12 col-xs-6" style="padding-top: 10px">
                                                            {!! Form::select('item_size', $size_array, null, ['id' => 'item_size', "onchange" => "product_price()"]) !!}
                                                            <small class="text-danger">{{ $errors->first('item_size') }}</small>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }} col-md-12 col-xs-6">
                                                            {!!Form::label('quantity', 'Quantity')!!}
                                                            {!! Form::selectRange('quantity', 1, 12, []) !!}
                                                            <small class="text-danger">{{ $errors->first('quantity') }}</small>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <br>

                                            <center>
                                                <h2 id="item_price" style="color:orange"></h2>
                                                <h4 id="member_price"></h4>
                                                <h4 id="original_price"></h4>
                                            </center>
                                        </div>
                                    </div>

                                    <div class="simple-product-form mtb-20 add-to-cart">  
                                        <div class="radio{{ $errors->has('recur') ? ' has-error' : '' }}">
                                            <label>
                                                {!! Form::radio('recur',0,  1, ['required']) !!} <b>One Time Purchase</b>
                                            </label>
                                            <br>
                                                <label>
                                                    {!! Form::radio('recur',1,  null, ["id" => "recur_selected"]) !!} <b>Sign up For Subscription</b>: Select to automatically reorder this item.
                                                </label>
                                            <small class="text-danger">{{ $errors->first('recur') }}</small>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-offset-1 col-md-4">
                                                <div class="form-group{{ $errors->has('reorder_frequency') ? ' has-error' : '' }}">
                                                    Reorder Every
                                                    {!! Form::select('reorder_frequency',config('other_constants.reorder_frequency'), null, ['id' => 'reorder_frequency', "onfocus" => "document.getElementById('recur_selected').checked = true;"]) !!}
                                                    <small class="text-danger">{{ $errors->first('reorder_frequency') }}</small>
                                                    <a href="{{url('subscription')}}" target="_blank">Learn More</a>
                                                </div>    
                                            </div>
                                            
                                        </div>
                                        
                                        <input type="hidden" name="item_ref_id" id='item_ref_id' class="login-button">
                                        {!! Form::button("<i class='fa fa-shopping-basket'></i>  Add To Cart", ['type' => 'submit', 'id' => 'submit_button']) !!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <div >
                                {!! Form::open(['method' => 'POST', 'route' => ['wishlist.add.product', 'product_ref_id' => $product->ref_id], 'class' => 'form-horizontal']) !!}
                                
                                        {!! Form::submit("Add To Wishlist", ['class' => 'btn btn-info']) !!}
                                
                                {!! Form::close() !!}
                            </div>
                        </div>                        

                        <div class="product_meta">
                            <div class="category mb-10">
                                @if ($product->classification())
                                    @foreach ($product->classification() as $value)
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> {{$value}} &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endforeach
                                @endif
                                
                                {{-- <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> No Palm Oil &nbsp;&nbsp;&nbsp;&nbsp; --}}

                                {{-- <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> No Animal Testing &nbsp;&nbsp;&nbsp;&nbsp; --}}
                                {{-- <b>Categories:</b>
                                <a href="#"> {{$product->subcategory->category->name}},</a>
                                <a href="#">{{$product->subcategory->name}}</a> --}}
                            </div>
                            {{--<div class="single-blog-tag category bb pb-10">
                                <b>Tags:</b>
                                <a href="#">fashion,</a>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="simple-product-tab bg-2">
                        <div class="simple-product-tab-menu clear">
                            <ul>
                                @if ($product->detailed_description)
                                    <li class="active"><a href="#description" data-toggle="tab">Description</a></li>
                                @endif

                               @if ($product->recipe)
                                
                                   <li @if (!$product->detailed_description)
                                       class="active"
                                   @endif>
                                        <a href="#ingredients" data-toggle="tab">Ingredients</a>
                                    </li>
                               @endif
                                
                            </ul>
                        </div>
                        <div class="tab-content bg-1">
                            @if ($product->detailed_description)
                                <div class="tab-pane active" id="description">
                                    <div class="product-description p-20 bt">
                                        <p>{!!$product->detailed_description!!}</p>
                                    </div>
                                </div>
                            @endif
                            @if ($product->recipe)
                                <div class="tab-pane @if (!$product->detailed_description)
                                    active
                                @endif" id="ingredients">
                                    <div class="product-description p-20 bt">
                                        <p><em>{{$product->ingredient_list()}}</em></p>

                                        @if ($product->contains_organic_ingredient())
                                            <p><em>*Organic Ingredient</em></p>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- featureproduct-slider-area start-->
    
    <div class="featureproduct-slider-area mt-80 mb-65 tab-area">
        @if ($product->related_products()->count() > 0)
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title home5-section-title text-center mb-40">
                            <h2>Related Products</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="featureproduct-slider-active next-prev-style">
                        @foreach ($product->related_products() as $related_product)
                            <div class="col-md-12">
                                @if ($related_product->items->count())
                                    @include('layouts.organica.partials.products', ['product' => $related_product])
                                @endif
                            </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        @endif
    </div>
    
    
    <!-- featureproduct-slider-area end-->
    <!-- simple product area end -->
@endsection

@section('scripts')
<script type="text/javascript">
    window.onload = product_price();

    var qty_select = document.getElementById('quantity');
    qty_select.addEventListener('change', function(){
                                    product_price();
                                }, false);

    function product_price()
    {
        var found = 0;
        var item_detail_selected = 0;
        var quantity = document.getElementById('quantity').value;
        
        var e = document.getElementById("item_size");
        var selected_size = e.options[e.selectedIndex].value;

        var item_details = document.getElementsByName('item_detail');

        //get checked radio value
        for (var i = 0, length = item_details.length; i < length; i++) {
            if (item_details[i].checked) {
                var feature = item_details[i].value.split("/^/");
                var color = feature[0];
                var scent = feature[1];
                var flavor = feature[2];
                var additional_feature = feature[3];
                item_detail_selected = 1;
                break;
            }
        }

        @foreach ($items as $item)
            if (selected_size == "{{$item->size}}" && color == "{{$item->color}}" && scent == "{{$item->scent}}" && flavor == "{{$item->flavor}}" && additional_feature == "{{$item->additional_feature}}") {
                
                found = 1; //item found
                
                var organic_price = {{$product->sale || $item->sale ? $item->sale_price : $item->organic_price}} * quantity;
                organic_price = roundCurrency(organic_price);

                var organic_price_member = roundCurrency(organic_price * (1-{{$member_discount}}));
                var organic_price_lowest = roundCurrency(organic_price * (1-{{$max_discount}}));

                if ({{$member_discount}}) {
                    document.getElementById('item_price').innerHTML = "$" + organic_price_member;
                    document.getElementById('original_price').innerHTML = "<small>Original: $"+ organic_price + "</small>";
                }
                else
                {
                    document.getElementById('item_price').innerHTML = "$"+organic_price;
                    
                    if({{$membership_active}}){
                        document.getElementById('member_price').innerHTML = "Members: $" + organic_price_lowest;
                    }
                }
                
                document.getElementById('item_ref_id').value = "{{$item->ref_id}}";
                document.getElementById('submit_button').style.visibility = "visible";
            }
        @endforeach

            if (!found && !selected_size) {
                document.getElementById('item_price').innerHTML = "--";
                document.getElementById('member_price').innerHTML = "";
                document.getElementById('original_price').innerHTML = "";
                document.getElementById('item_ref_id').value = null;        
                document.getElementById('submit_button').style.visibility = "visible";
            }
    
            if (!found && selected_size && item_detail_selected) {
                document.getElementById('item_price').innerHTML = "Not Available";
                document.getElementById('member_price').innerHTML = "";
                document.getElementById('original_price').innerHTML = "";
                document.getElementById('item_ref_id').value = null;        
                document.getElementById('submit_button').style.visibility = "hidden";
            }        
    }

    function roundCurrency(price){
        return price.toFixed(2); 
        
        //OLD: integer (no decimal) or 2 decimal price
        price_cents = Math.round(price * 100);
        //if remainder when divide cents by 100, convert to 2 decimal places
        
        return price_cents % 100 ? (price_cents / 100).toFixed(2): price_cents / 100; 
    }
</script>
@endsection
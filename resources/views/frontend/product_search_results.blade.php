@extends('layouts.organica.template')

@section('page_title', "Search Results")

@section('content')

	<div class="container bg-1" style="padding:30px">
	<div class="row">
		<!-- product-vew area start -->
		{{-- ORIGINAL <div class="col-lg-9  col-md-8 col-sm-12 col-xs-12"> --}}
		<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
			<div class="tab-area shop-tab-area">
				<div class="tab-menu-area border-bottom mb-30">
					<div class="row">
						<h4>
							{{$products->total()}} {{str_plural('Result', $products->total())}}
						</h4>
					</div>
					<br>
					<div class="row">

						<div class="col-md-7 col-sm-6 col-xs-12">
							<div class="shop-tab-menu">
								<ul>
									<li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-th"></i></a></li>
									<li><a href="#tab2" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
								</ul>
								{{--<span> Items 1-9 of 13</span>--}}
							</div>
						</div>
						{{--
						<div class=" col-md-5 col-sm-6 hidden-xs">
							<div class="woocommerce-ordering text-right">
								<strong>Sort By </strong>
								<select name="orderby">
									<option value="menu_order" selected="selected">Position</option>
									<option value="popularity">Product Name</option>
									<option value="rating">Price</option>
								</select>
								<a href="#"><i class="fa fa-arrow-up"></i></a>
							</div>
						</div>
						--}}
					</div>
				</div>
				
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tab1">
						<div class="row">
							@forelse ($products as $product)
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									@include('layouts.organica.partials.products')
								</div>	
							@empty
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<em>No Results</em>
								</div>	
							@endforelse
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="tab2">
						<div class="row">
							@forelse ($products as $product)
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="product-wrapper clear border-bottom mb-30">
										<div class="product-img shop-product-img">
											<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">
												@if ($product->image)
													<img src='{{asset("images/products/{$product->image}") }}' alt=""/>
												@else
													<img src='{{asset("images/placeholder.png") }}' alt=""/>
												@endif
											</a>
											@if ($product->new())
									            <span class="new">new</span>
									        @elseif ($product->sale)
            									<span class="sale">Sale</span>
									        @endif
										</div>
										<div class="product-item-details shop-product-item-details">
											<div class="product-name-review">
												<div class="product-name ">
													<strong><a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">{{$product->name}}</a></strong>
												</div>
												<div class="product-review">
													{{--
													<ul>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													<small>1 Review Add Your Review</small><br />
													--}}
								                    from
								                
								                    {{-- <span class="special-price">${{$product->items()->orderBy('price', 'asc')->first()->price}}</span> --}}
													{{--
													<span class="old-price">
														<del>$ 46.00</del>
													</span>
													--}}
													<p>{!!$product->website_description!!}</p>
													<div class="readmore-btn">
														<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'porduct_id' => $product->id])}}">Learn More<i class="fa fa-long-arrow-right"></i></a>
													</div>
												</div>
											</div>
											<div class="add-to-cart text-uppercase ptb-35">
												
                    							<a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}"><button>View Options</button></a>
								                
								                {{--
												<ul>
													<li><button>add to cart</button></li>
													<li>
														<a href="#"><i class="fa fa-adjust"></i></a>
													</li>
													<li>
														<a href="#"><i class="fa fa-heart-o"></i></a>
													</li>
												</ul>
												--}}
											</div>
										</div>
									</div>
								</div>								
							@empty
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<em>No Results</em>
								</div>
							@endforelse
						</div>
					</div>
				</div>
			</div>

			<center>{{ $products->appends(['query' => $query])->links() }}</center>
		</div>
	</div>
</div>

	

@endsection
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Admin Area - @yield('page_title', 'Dashboard')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link href="{{asset('admin_josh/css/app.css')}}" rel="stylesheet" type="text/css" />
    <!-- end of global css -->
    <!--page level css -->
        @yield('page_level_css')
    <!--end of page level css-->
</head>

<body class="skin-josh">
    <header class="header">
        @include('layouts.admin_josh.header')
    </header>

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                @include('layouts.admin_josh.sidenav')
            </aside>
        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side" style="overflow-x: scroll;">
            @include('layouts.alerts')
            {{--
            <div class="alert alert-success alert-dismissable margin5">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Success:</strong> You have successfully logged in.
            </div>
            --}}

            <!-- Main content -->
            <section class="content-header">
                <h1>@yield('page_title','Dashboard')</h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <a href="#">
                            <i class="livicon" data-name="home" data-size="14" data-color="#333" data-hovercolor="#333"></i> Dashboard
                        </a>
                    </li>
                </ol>
            </section>
            <section class="content">
                @yield('content')
            </section>
                
        </aside>
        <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{asset('admin_josh/js/app.js')}}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
        @yield('page_level_js')
    <!-- end of page level js -->
    <script>
        $(".delete").on("submit", function(){
            return confirm("Are you sure you want to delete?");
        });
    </script>
</body>

</html>

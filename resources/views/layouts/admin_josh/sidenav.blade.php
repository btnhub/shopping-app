<section class="sidebar ">
    <div class="page-sidebar  sidebar-nav">
        
        <div class="nav_icons"> 
            <ul class="sidebar_threeicons">
                <li>
                    <a href="{{route('users.index')}}">
                        <i class="livicon" data-name="users" title="Users List" data-size="25" data-c="#01bc8c" data-hc="#01bc8c" data-loop="true"></i>
                    </a>
                </li>
                <li>
                    <a href="{{route('products.index')}}">
                        <i class="livicon" data-name="leaf" title="Products" data-c="#F89A14" data-hc="#F89A14" data-size="25" data-loop="true"></i>
                    </a>                
                </li>
                <li>
                    <a href="{{route('payments.index')}}">
                        <i class="livicon" data-name="piggybank" title="Payments" data-c="#418BCA" data-hc="#418BCA" data-size="25" data-loop="true"></i>
                    </a>
                </li>
                <li>
                    <a href="{{route('orders.index')}}">
                        <i class="livicon" data-c="#EF6F6C" title="Orders" data-hc="#EF6F6C" data-name="shopping-cart" data-size="25" data-loop="true"></i>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="clearfix"></div>
        <!-- BEGIN SIDEBAR MENU -->
        <ul id="menu" class="page-sidebar-menu">
            <li class="active">
                <a href="{{route('admin.index')}}">
                    <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li>
                <a href="{{route('orders.index')}}">
                    <i class="livicon" data-name="shopping-cart" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C" data-loop="true"></i>
                    <span class="title">New Orders</span>
                    <span class="badge badge-danger">{{App\Order::pending()->count()}}</span>
                </a>
            </li>
            <li>
                <a href="{{route('recurring_orders.index')}}">
                    <i class="livicon" data-name="shopping-cart" data-c="#5bc0de" data-hc="#5bc0de" data-size="18" data-loop="true"></i>
                    <span class="title">Recurring Orders</span>
                    {{-- <span class="fa arrow"></span> --}}
                </a>
            </li>
            <li>
                <a href="{{route('users.index')}}">
                    <i class="livicon" data-name="users" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Users</span>
                    <?php $new_users = App\User::where('created_at', '>', Carbon\Carbon::now()->subWeek())->count();?>
                    @if(    $new_users)
                        <span class="badge badge-danger">{{$new_users}}</span>
                    @endif
                    
                </a>
            </li>
            <li>
                <a href="{{route('memberships.index')}}">
                    <i class="livicon" data-name="users" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Memberships</span>
                </a>
            </li>
            <li>
                <a href="{{route('credits.index')}}">
                    <i class="livicon" data-name="money" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Credits & Gift Cards</span>
                </a>
            </li>
            <li>
                <a href="{{route('products.index')}}">
                    <i class="livicon" data-name="leaf" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                    <span class="title">Products</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('products.index')}}">
                            <i class="fa fa-angle-double-right"></i> Products List
                        </a>
                    </li>
                    <li>
                        <a href="{{route('items.index')}}">
                            <i class="fa fa-angle-double-right"></i> Items List
                        </a>
                    </li>
                </ul>
            </li>
            {{-- <li>
                <a href="#">
                    <i class="livicon" data-name="list" data-c="#00bc8c" data-hc="#00bc8c" data-size="18" data-loop="true"></i>
                    <span class="title">Items</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('items.index')}}">
                            <i class="fa fa-angle-double-right"></i> Items List
                        </a>
                    </li>
                    <li>
                        <a href="{{route('items.create')}}">
                            <i class="fa fa-angle-double-right"></i> Add An Item
                        </a>
                    </li>
                </ul>
            </li> --}}
            <li>
                <a href="#">
                    <i class="livicon" data-name="list" data-c="#F89A14" data-hc="#F89A14" data-size="18" data-loop="true"></i>
                    <span class="title">Categories</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('categories.index')}}"> <i class="fa fa-angle-double-right"></i> All Categories </a>
                    </li>
                    <li>
                        <a href="{{route('categories.create')}}"> <i class="fa fa-angle-double-right"></i> Add A New Category </a>
                    </li>
                    <li>
                        <a href="{{route('subcategories.index')}}"> <i class="fa fa-angle-double-right"></i> All Subcategories </a>
                    </li>
                    <li>
                        <a href="{{route('subcategories.create')}}"> <i class="fa fa-angle-double-right"></i> Add A New Subcategory </a>
                    </li>
                </ul>
            </li>
            {{-- <li>
                <a href="#">
                    <i class="livicon" data-name="list" data-c="#5bc0de" data-hc="#5bc0de" data-size="18" data-loop="true"></i>
                    <span class="title">Subcategories</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('subcategories.index')}}"> <i class="fa fa-angle-double-right"></i> All Subcategories </a>
                    </li>
                    <li>
                        <a href="{{route('subcategories.create')}}"> <i class="fa fa-angle-double-right"></i> Add A New Subcategory </a>
                    </li>
                </ul>
            </li> --}}
            <li>
                <a href="#">
                    <i class="livicon" data-name="piggybank" data-c="#5bc0de" data-hc="#5bc0de" data-size="18" data-loop="true"></i>
                    <span class="title">Payments & Refunds</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('payments.index')}}">
                            <i class="fa fa-angle-double-right"></i> Payments
                        </a>
                    </li>
                    <li>
                        <a href="{{route('refunds.index')}}">
                            <i class="fa fa-angle-double-right"></i> Refunds
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-double-right"></i> Incomplete Payments (TO DO)
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="livicon" data-name="truck" data-c="#F89A14" data-hc="#F89A14" data-size="18" data-loop="true"></i>
                    <span class="title">Shipments</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('shipments.index')}}">
                            <i class="fa fa-angle-double-right"></i> All Shipments
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-double-right"></i> Pending Shipments (To DO)
                        </a>
                    </li>
                </ul>
            </li>
            {{-- <li>
                <a href="#">
                    <i class="livicon" data-name="shopping-cart-out" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Refunds</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('refunds.index')}}">
                            <i class="fa fa-angle-double-right"></i> All Refunds
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-double-right"></i> Pending Refunds
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-double-right"></i> Completed Refunds
                        </a>
                    </li>
                </ul>
            </li> --}}
            
            <li>
                <a href="#">
                    <i class="livicon" data-name="list" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                    <span class="title">Suppliers & Materials</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('suppliers.index')}}">
                            <i class="fa fa-angle-double-right"></i> Suppliers
                        </a>
                    </li>
                    <li>
                        <a href="{{route('raw_materials.index')}}">
                            <i class="fa fa-angle-double-right"></i> Raw Materials
                        </a>
                    </li>
                </ul>
            </li>
            {{-- <li>
                <a href="#">
                    <i class="livicon" data-name="apple" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Raw Materials</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('raw_materials.index')}}">
                            <i class="fa fa-angle-double-right"></i> List
                        </a>
                    </li>
                    <li>
                        <a href="{{route('raw_materials.create')}}">
                            <i class="fa fa-angle-double-right"></i> Add Raw Material 
                        </a>
                    </li>
                </ul>
            </li> --}}

            <li>
                <a href="#">
                    <i class="livicon" data-name="credit-card" data-size="18" data-c="#5bc0de" data-hc="#5bc0de" data-loop="true"></i>
                    <span class="title">Purchases & Expenses</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('purchases.index')}}">
                            <i class="fa fa-angle-double-right"></i> Purchases List
                        </a>
                    </li>
                    {{-- <li>
                        <a href="{{route('purchases.create')}}">
                            <i class="fa fa-angle-double-right"></i> Add A Purchase
                        </a>
                    </li> --}}
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-double-right"></i> Expenses List (To Do)
                        </a>
                    </li>
                </ul>
            </li>
            
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</section>
<!-- /.sidebar -->

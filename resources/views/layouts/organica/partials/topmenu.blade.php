<ul>
	{{--<li><a href="#">About</a></li>--}}
	{{-- Mega Menu For When More Menu Items Are Present--}}
	{{--
	<li><a href="#">Shop <i class="fa fa-chevron-down"></i></a>
		<div class="megamenu">
		<!-- Or <div class="megamenu home3-megamenu-left"> For Index 3-->
			@foreach ($categories as $category)
				@if ($category->subcategories->count() > 0)
					<span>
						<a class="mega-title" href="#">{{$category->name}}</a>
						@foreach ($category->subcategories as $subcategory)
							@if ($subcategory->products->count() > 0)
								<a href="{{route('list.products', ['cat_slug' => str_slug($subcategory->category->name), 'subcategory_id' => $subcategory->id, 'sub_slug' => str_slug($subcategory->name)])}}">{{$subcategory->name}}</a>
							@endif
						@endforeach
					</span>
				@endif
			@endforeach
		</div>
	</li>	

	--}}	
	@foreach ($categories as $category)
		@if ($category->subcategories->count() > 0)
			<li><a href="#">{{$category->name}} <i class="fa fa-chevron-down"></i></a>
				<ul>
					@foreach ($category->subcategories->sortBy('website_order') as $subcategory)
						<?php $product_count = $subcategory->products
															->filter(function ($product) {
    															return $product->items->count() > 0;
															})->count();
						?>
						@if ($product_count > 0 && $subcategory->is_visible)
							<li><a href="{{route('list.products', ['cat_slug' => str_slug($subcategory->category->name), 'subcategory_id' => $subcategory->id, 'sub_slug' => str_slug($subcategory->name)])}}">{{$subcategory->name}}</a></li>
						@endif
					@endforeach
				</ul>
			</li>
		@endif
	@endforeach
	
	@include('layouts.organica.partials.toplinks')
</ul>

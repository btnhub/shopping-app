<?php 
    $max_discount = /**isset($membership_active) && **/ $membership_active ? App\Membership::where('website_compare',1)->max('discount') : 0;
    $starting_price = number_format(($product->organic_option ? $product->items()->frontVisible()->min('organic_price') : $product->items()->min('price')) * (1-$max_discount), 2);
    $starting_sale_price = $product->sale || $product->items()->frontVisible()->where('sale', 1)->count() ? number_format($product->items()->frontVisible()/*->where('sale', 1)*/->where('sale_price', '>', 0)->min('sale_price') * (1-$max_discount), 2) : false;
?>

<div class="single-product">
    <div class="product-img">
        <a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">
            @if ($product->image)
                <img src='{{asset("images/products/{$product->image}") }}' alt=""/>
            @else
                <img src='{{asset("images/placeholder.png") }}' alt=""/>
            @endif
        </a>
        @if ($starting_sale_price)
            <span class="sale">Sale</span>
        @elseif ($product->new())
            <span class="new">new</span>
        @elseif ($product->items->count() == 0)
            <span class="sale">coming soon!</span>
        @endif
        
    </div>
    <div class="product-item-details text-center">
        <div class="product-name-review tab-product-name-review">
            <div class="product-name mt-30 ">
                <span>{{$product->subcategory->category ? "{$product->subcategory->category->name} - {$product->subcategory->name}" : ''}}</span>
                <strong><a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}">{{$product->name}}</a></strong>
            </div>
            <div class="product-review">
                {{--
                <ul>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
                --}}

                @if ($starting_sale_price)
                    from 
                    <span class="old-price">
                        <del>${{$starting_price}}</del>
                    </span>
                    <span class="special-price">${{$starting_sale_price}}</span>
                @else
                    from
                    <span class="special-price">${{$starting_price}}</span>
                @endif
            </div>
        </div>
        <div class="add-to-cart-area clear pt-35 mb-50">
            <div class="add-to-cart text-uppercase">
                <a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}"><button>View Options</button></a>
            </div>
            {{--
            <div class="add-to-links">
                <ul>
                    <li class="left">
                        <a href="#"><i class="fa fa-adjust"></i></a>
                    </li>
                    <li class="right">
                        <a href="#"><i class="fa fa-heart-o"></i></a>
                    </li>
                </ul>
            </div>
            --}}
        </div>
    </div>
</div>
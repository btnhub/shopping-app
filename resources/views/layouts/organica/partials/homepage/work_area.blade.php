<div class="work-area" style="background:url({{asset('images/frontend/mountains-banner.jpg')}}) no-repeat scroll 50% 100%/cover;">
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="work-wrap mtb-30">
				<div class="single-work">
					<div class="work-info text-center">
						<h3>Subscription</h3>
						<p>
							<b>Never go without your favorite products! </b><br><br>
							Add an item to your cart and specify how often you'd like to reorder it. <br><br>
							<b>No subscription fee!</b>
						</p>
						<div class="readmore-btn mt-40 work-btn">
							<a href="{{url('subscription')}}">Learn More {{-- <i class="fa fa-long-arrow-right"></i> --}}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php 
		$artisan_setting_id = DB::table('settings')->where('field', 'artisan_shows')->first()->id;
		$artisan_dates =  DB::table('settings_values')->where('setting_id', $artisan_setting_id)->first()->value;?>

		<div class="col-md-4 col-sm-6  col-xs-12">
			<div class="work-wrap mtb-30">
				<div class="single-work">
					<div class="work-info text-center">
						<h3>Artisan Shows & Festivals</h3>
						<p><b>We'll be at various artisan markets and festivals.  Stop by and say Hi!</b></p>
						<br>
						<p><b>Events: </b></p>
						<p>{!!$artisan_dates!!}</p>
						<br>

						<p>Follow us on <a href="https://www.facebook.com/MountainMystCO" target="_blank" style="color:lightblue">Facebook</a> & <a href="https://www.instagram.com/MountainMystCO" target="_blank" style="color:lightblue">Instagram</a> for announcements and more details.</p>
						{{-- <h3>Mountain Myst Membership</h3>
						<p><b>Discounts and Free Shipping!</b></p><br>

						<p>With our monthly membership, save 25% plus free shipping! </p>
						<div class="readmore-btn mt-40 work-btn">
							<a href="{{url('/membership')}}">Learn More</a>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12  col-xs-12">
			<div class="work-wrap mtb-30">
				<div class="single-work">
					<div class="work-info text-center">
						<h3>Contact Information</h3>
						<p>Handmade In Denver, Colorado</p>
						{{-- <strong>Phone: 888.444.6455 - Fax: 608.625.2600</strong> --}}
						<br>
						<strong><a href="mailto:Contact@Mountain-Myst.com" style="color:white">Contact@Mountain-Myst.com</a></strong>
						<br><br>
						<strong><a href="https://www.facebook.com/MountainMystCO" style="color:white" target="_blank">
						<img src="{{asset('images/frontend/facebook_icon.png')}}" width="8%">
						fb.me/MountainMystCO</a></strong>
						<br><br>
						<strong><a href="https://www.instagram.com/mountainmystco/" style="color:white" target="_blank">
						<img src="{{asset('images/frontend/instagram_icon.png')}}" width="8%">
						@MountainMystCO</a></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

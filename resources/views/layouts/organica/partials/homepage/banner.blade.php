<div class="banner-area fix">
	{{-- <div class="single-banner pull-left">
		<a href="#"><img src="" alt="" /></a>
	</div> --}}
	
	<div class="single-banner pull-left">
		<a href="#" style="background:url({{asset('images/frontend/stock-photo-natural-facial-cream-with-beauty-japanese-cherry-blossoms-on-wood-137981567_CROPPED.jpg')}}) no-repeat; background-size: cover; height:210px;">
		<br><br>
		<div class="pull-right">
			
			<strong><span style="color:black; font-size:36px; font-family:Times New Roman;">
				Subscription
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span></strong>
			<br>

			<span style="color:black; font-size:24px">
				<b>Your favorite products sent
				<br>
				to you on a regular basis!</b>
			</span>
		</div>
			
		</a>
	</div>

	<div class="single-banner pull-right">
		<a href="{{url('/membership')}}" style="background:url({{asset('images/frontend/istock-524705312_CROPPED.jpg')}}); background-size: cover; height:210px;">
		<br><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<strong><span style="color:white; font-size:36px; font-family:Lobster Two,cursive;">Save With Membership</span></strong>
			<br>
			<span style="color:black; font-size:24px">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>Members Save Up To 25% Off
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					Plus Free Shipping!
				 </strong>
			</span>
		</a>
	</div>
</div>
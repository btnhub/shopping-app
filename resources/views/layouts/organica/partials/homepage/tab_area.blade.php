<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="tab-menu text-center mb-30">
				<ul>
					<li class="active"><a href="#featured" data-toggle="tab">Featured</a></li>
					@if ($new_products->count())
						<li><a href="#new-products" data-toggle="tab">New Products</a></li>			
					@endif
					@if ($sale_products->count())
						<li><a href="#onsale" data-toggle="tab"> OnSale</a></li>
					@endif
				</ul>
			</div>
		</div>
	</div>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="featured">
			<div class="tab-active next-prev-style">
				@foreach ($products as $product)
					@if ($product->image)
						<div class="col-md-12">
							@include('layouts.organica.partials.products')
						</div>
					@endif
				@endforeach
			</div>				
		</div>
		<div role="tabpanel" class="tab-pane fade" id="new-products">
			<div class="tab-active next-prev-style">
				@forelse ($new_products as $product)
					@if ($product->image)
						<div class="col-md-12">
							@include('layouts.organica.partials.products')
						</div>
					@endif
				@empty
				@endforelse
			</div>								
		</div>
		<div role="tabpanel" class="tab-pane fade" id="onsale">
			<div class="tab-active next-prev-style">
				@forelse ($sale_products as $product)
					@if ($product->image)
						<div class="col-md-12">
							@include('layouts.organica.partials.products')
						</div>
					@endif
				@empty
				@endforelse
			</div>
		</div>
	</div>
</div>

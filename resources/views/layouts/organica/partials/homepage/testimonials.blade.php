<div class="testimonial-main-area clear">
	<div class="testimonial bg-img-2 clear pull-left">
		<div class="testimo-slider pull-right">
			<div class="test-active next-prev-style">
				<div class="single-test text-center pt-90">
					<div class="test-img">
						<a href="#">
							<img src="{{asset('organica/img/test/1.png')}}" alt="" />
						</a>
					</div>
					<div class="test-content mtb-35 plr-55">
						<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis</p>
						<div class="test-info mt-30">
							<a href="#">victor</a>
							<span>Webdesigner</span>
							<span class="email">hasib.me1995@gmail.com</span>
						</div>
					</div>
				</div>
				<div class="single-test text-center pt-90">
					<div class="test-img">
						<a href="#">
							<img src="{{asset('organica/img/test/2.png')}}" alt="" />
						</a>
					</div>
					<div class="test-content mtb-35 plr-55">
						<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis</p>
						<div class="test-info mt-30">
							<a href="#">Aussiemines</a>
							<span>Webdesigner</span>
							<span class="email">hasib.me1995@gmail.com</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Newsletter-area pull-right bg-img-3 ptl-100">
		<div class="news-taitle">
			<h3>Sign up for</h3>
			<h1>send</h1>
			<h2>Newsletter</h2>
		</div>
		<div class="newsletter">
			<form action="#">
				<input type="email" placeholder="Enter your email address"/><br />
				<button>Subscribe <i class="fa fa-long-arrow-right"></i></button>
			</form>
		</div>
	</div>
</div>
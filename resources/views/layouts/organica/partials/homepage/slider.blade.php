<div id="slider-active">
	<img src="{{asset('images/frontend/123rf-12853972-Jar-of-moisturizing-facial-cream-Stock-Photo-cosmetic.jpg')}}" alt="" title="#active1" height="750"/>

	<img src="{{asset('images/frontend/stock-photo-homemade-coconut-products-on-white-wooden-table-background-oil-scrub-milk-lotion-mint-and-soap-553049746_CROPPED.jpg')}}" alt="" title="#active2" height="750"/>

	{{-- <img src="{{asset('images/frontend/depositphotos_146902369-stock-photo-homemade-skin-care-and-body.jpg')}}" alt="" title="#active2"/> --}}
</div>
<div id="active1" class="nivo-html-caption">
	<div class="slider-progress"></div>	
	<div class="container">
		<div class="row">
			<div class="col-md-offset-1 col-md-11 col-xs-12 col-sm-12">
				<div class="slide1-text text-left">
					<div class="middle-text">
						<div class="cap-dec wow fadeInDown" data-wow-duration="1.2s" data-wow-delay=".5s">
							<h1>Mountain Myst</h1>
						</div>	
						<div class="cap-title wow fadeInLeft text-uppercase" data-wow-duration="1.2s" data-wow-delay="0.2s">
							<h3>Handmade Body Products</h3>
						</div>
						<div class="cap-title wow flipInX hidden-xs" data-wow-duration="1.5s" data-wow-delay="0.2s">
							<p>The natural body care products you love <br>
							without the harmful artificial additives.</p>						
						</div>
						{{-- <div class="cap-readmore wow flipInX organic-btn hidden-xs" data-wow-duration="1.6s" data-wow-delay=".5s">
							<a href="#">shop now</a>
						</div>	 --}}
					</div>	
				</div>				
			</div>
		</div>
	</div>
</div>
<div id="active2" class="nivo-html-caption">
	<div class="slider-progress"></div>	
	<div class="container">
		<div class="row">
			<div class="col-md-offset-1 col-md-11 col-xs-12 col-sm-12">
				<div class="slide1-text text-left">
					<div class="middle-text">
						<div class="cap-dec wow fadeInDown" data-wow-duration="1.2s" data-wow-delay=".5s">
							<h3>Made To Order</h3>
						</div>	
						<div class="cap-title wow fadeInLeft text-uppercase" data-wow-duration="1.2s" data-wow-delay="0.2s">
							<h1>Get The Freshest Batch </h1>
						</div>
						<div class="cap-title wow flipInX hidden-xs" data-wow-duration="1.5s" data-wow-delay="0.2s">
							<p>
								<b>Most of our products are made-to-order!</b><br>

								Simply place an order and we'll whip up a fresh batch.
							</p>
						</div>
						{{--
						<div class="cap-readmore wow flipInX organic-btn hidden-xs" data-wow-duration="1.6s" data-wow-delay=".5s">
							<a href="#">shop now</a>
						</div>	
						--}}
					</div>	
				</div>				
			</div>
		</div>
	</div>
</div>

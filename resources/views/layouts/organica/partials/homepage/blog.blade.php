<div class="blog-area mtb-60">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title text-center mt-25 mb-10">
					<h2>Blog posts</h2>
				</div>
			</div>
		</div>
		<div class="row blog-active next-prev-style">
			<div class="col-md-12">
				<div class="blog-wrap text-center">
					<div class="blog-img fix">
						<a href="blog-details.html">
							<img src="{{asset('organica/img/blog/1.jpg')}}" alt="" />
						</a>
					</div>
					<div class="blog-content">
						<div class="blog-meta">
							<span>08</span>
							<span>/</span>
							<span>September</span>
							<span>/</span>
							<span>2016</span>
						</div>
						<div class="blog-info">
							<h3>
								<a href="blog-details.html">Pellentesque massa erat, blandit eget tincidunt porta, eleifend nec .</a>
							</h3>
							<p>We're releasing 2 premium Magento templates each month. Every template is designed with different styles and targeted for each business trend</p>
							<div class="readmore-btn">
								<a href="#">Read more <i class="fa fa-long-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="blog-wrap text-center">
					<div class="blog-img fix">
						<a href="blog-details.html">
							<img src="{{asset('organica/img/blog/2.jpg')}}" alt="" />
						</a>
					</div>
					<div class="blog-content">
						<div class="blog-meta">
							<span>08</span>
							<span>/</span>
							<span>September</span>
							<span>/</span>
							<span>2016</span>
						</div>
						<div class="blog-info">
							<h3>
								<a href="blog-details.html">Pellentesque massa erat, blandit eget tincidunt porta, eleifend nec .</a>
							</h3>
							<p>We're releasing 2 premium Magento templates each month. Every template is designed with different styles and targeted for each business trend</p>
							<div class="readmore-btn">
								<a href="#">Read more <i class="fa fa-long-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="blog-wrap text-center">
					<div class="blog-img fix">
						<a href="blog-details.html">
							<img src="{{asset('organica/img/blog/3.jpg')}}" alt="" />
						</a>
					</div>
					<div class="blog-content">
						<div class="blog-meta">
							<span>08</span>
							<span>/</span>
							<span>September</span>
							<span>/</span>
							<span>2016</span>
						</div>
						<div class="blog-info">
							<h3>
								<a href="blog-details.html">Pellentesque massa erat, blandit eget tincidunt porta, eleifend nec .</a>
							</h3>
							<p>We're releasing 2 premium Magento templates each month. Every template is designed with different styles and targeted for each business trend</p>
							<div class="readmore-btn">
								<a href="#">Read more <i class="fa fa-long-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="blog-wrap text-center">
					<div class="blog-img fix">
						<a href="blog-details.html">
							<img src="{{asset('organica/img/blog/2.jpg')}}" alt="" />
						</a>
					</div>
					<div class="blog-content">
						<div class="blog-meta">
							<span>08</span>
							<span>/</span>
							<span>September</span>
							<span>/</span>
							<span>2016</span>
						</div>
						<div class="blog-info">
							<h3>
								<a href="blog-details.html">Pellentesque massa erat, blandit eget tincidunt porta, eleifend nec .</a>
							</h3>
							<p>We're releasing 2 premium Magento templates each month. Every template is designed with different styles and targeted for each business trend</p>
							<div class="readmore-btn">
								<a href="#">Read more <i class="fa fa-long-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="blog-wrap text-center">
					<div class="blog-img fix">
						<a href="blog-details.html">
							<img src="{{asset('organica/img/blog/1.jpg')}}" alt="" />
						</a>
					</div>
					<div class="blog-content">
						<div class="blog-meta">
							<span>08</span>
							<span>/</span>
							<span>September</span>
							<span>/</span>
							<span>2016</span>
						</div>
						<div class="blog-info">
							<h3>
								<a href="blog-details.html">Pellentesque massa erat, blandit eget tincidunt porta, eleifend nec .</a>
							</h3>
							<p>We're releasing 2 premium Magento templates each month. Every template is designed with different styles and targeted for each business trend</p>
							<div class="readmore-btn">
								<a href="#">Read more <i class="fa fa-long-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="our-history-area" style="background:url({{asset('images/frontend/coconut-oil-2535374_900_594.jpg')}}) no-repeat scroll 5% /*0*//*/cover*/;">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-6 col-md-6 col-sm-offset-4 col-sm-8 col-xs-12">
				<div class="our-history" style="background-color: rgba(255, 255, 255, 0.8);">
					<h3>About Us</h3>
					<h4>We are dedicated to creating nourishing skin & body products from natural, organic and ethically sourced ingredients.  <b>Our products are all handmade in Denver, Colorado.</b></h4>
					<p></p>
					<table class="table">
						<thead>
							<tr>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<span style="font-size: 20px">
										<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span> No Artificial Preservatives<br>
										<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span> No Artificial Colors<br>
										<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span> No Artificial Fragrance Oils or Parafum<br>
										<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span>  No Artificial Flavors<br>
										<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span>  No Palm Oil*<br>
										{{-- <span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span>  No Mica<br> --}}
										<span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:darkred"></span>  No Animal Testing<br>
									</span>
								</td>
							</tr>
						</tbody>
					</table>
					{{-- <p>Most of our products are made to order; simply submit an order and we'll whip up a fresh batch just for you! </p>
					<p>Sign up for <a href="{{url('/membership')}}">Membership</a> and receive up to 25% off + Free Shipping!</p> --}}
					<p>* While we do not use palm oil, a few of our products contain palm-derived ingredients, such as vegetable glycerin & emulsifying wax. We are still searching for palm-free versions of these ingredients.</p>
					<br><br><br><br>
					{{-- <div class="readmore-btn mt-40">
						<a href="#">Read More <i class="fa fa-long-arrow-right"></i></a>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tab-area mtb-100">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="tab-menu text-center mb-30">
					<ul>
						<li class="active"><a href="#new-products" data-toggle="tab">New Products</a></li>
						<li><a href="#onsale" data-toggle="tab"> OnSale</a></li>
						<li><a href="#featured" data-toggle="tab">Featured</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="new-products">
				<div class="tab-active next-prev-style">
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/11.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Chaz Kangeroo Hoodie1</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 50.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/10.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Fusion Backpack</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 55.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/9.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Joust Duffle Bag1</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 40.00</span>
										<span class="old-price">
											<del>$ 44.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/7.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">MH01-Black</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 30.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/8.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Rival Field Messenger</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
										<span class="old-price">
											<del>$ 46.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/6.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Strive Shoulder Pack</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/8.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Wayfarer Messenger Bag</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
										<span class="old-price">
											<del>$ 46.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>								
			</div>
			<div role="tabpanel" class="tab-pane fade" id="onsale">
				<div class="tab-active next-prev-style">
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/8.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Chaz Kangeroo Hoodie1</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 50.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/11.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Fusion Backpack</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 55.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/9.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Joust Duffle Bag1</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 40.00</span>
										<span class="old-price">
											<del>$ 44.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/8.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">MH01-Black</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 30.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/6.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Rival Field Messenger</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
										<span class="old-price">
											<del>$ 46.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/7.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Strive Shoulder Pack</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/10.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Wayfarer Messenger Bag</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
										<span class="old-price">
											<del>$ 46.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="featured">
				<div class="tab-active next-prev-style">
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/10.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Chaz Kangeroo Hoodie1</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 50.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/10.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Fusion Backpack</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 55.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/9.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Joust Duffle Bag1</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 40.00</span>
										<span class="old-price">
											<del>$ 44.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/8.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">MH01-Black</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 30.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/7.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Rival Field Messenger</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
										<span class="old-price">
											<del>$ 46.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/6.jpg')}}" alt="" /></a>
								<span class="new">new</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Strive Shoulder Pack</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="single-product">
							<div class="product-img">
								<a href="shop-single-product.html"><img src="{{asset('organica/img/product/11.jpg')}}" alt="" /></a>
								<span class="sale">Sale</span>
							</div>
							<div class="product-item-details text-center">
								<div class="product-name-review tab-product-name-review">
									<div class="product-name mt-30 ">
										<span>Sample Category</span>
										<strong><a href="shop-single-product.html">Wayfarer Messenger Bag</a></strong>
									</div>
									<div class="product-review">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span class="special-price">$ 45.00</span>
										<span class="old-price">
											<del>$ 46.00</del>
										</span>
									</div>
								</div>
								<div class="add-to-cart-area clear pt-35">
									<div class="add-to-cart text-uppercase">
										<button>add to cart</button>
									</div>
									<div class="add-to-links">
										<ul>
											<li class="left">
												<a href="#"><i class="fa fa-adjust"></i></a>
											</li>
											<li class="right">
												<a href="#"><i class="fa fa-heart-o"></i></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
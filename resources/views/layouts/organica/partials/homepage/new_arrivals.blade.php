<?php $setting_id = DB::table('settings')->where('field', 'homepage_new_arrivals_image')->first()->id;
	$new_arrival_image = DB::table('settings_values')->where('setting_id', $setting_id)->first()->value;
?>
<div class="new-arrivals-area" style="background:url({{asset('images/frontend/'.$new_arrival_image)}})no-repeat scroll 50% 50%/cover;">
	<!--<div class="new-arrivals-area bg-img-11">-->
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				{{-- <div class="col-md-offset-6 col-md-6"> --}}
				<div class="new-arrivals-content text-center">
					<div style="background-color: rgba(255, 255, 255, 0.8);padding:20px;">
					<h3>Made To Order</h3>
					<h1>Body Cream</h1>
					<p>Our body cream and lotions are made to order.  We make them only when you place an order and ship it immediately.</p>
					{{-- <h2> up to 30% off </h2> --}}
					<p>
						<b>No Artificial Preservatives, Fragrances, Colors</b><br>
						<b>Palm Free, Cruelty Free</b>
					</p>
					<p>Have an allergy? Make it vegan? We can customize our lotions (and other items) to your needs. Contact us today!</p>
					{{-- <div class="organic-btn new-arrivals-btn mt-35">
						<a href="#">Shop Now<i class="fa fa-long-arrow-right"></i></a> --}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
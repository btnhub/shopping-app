<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="section-title text-center mb-50">
				<h2>Featured Products</h2>
			</div>
		</div>
	</div>
    
	<div class="row">
		<div class="product-active next-prev-style">
			@foreach ($products as $product)
				@if ($product->image)
					<div class="col-md-12">
						@include('layouts.organica.partials.products')
					</div>
				@endif
				
			@endforeach
			
		</div>
	</div>
</div>
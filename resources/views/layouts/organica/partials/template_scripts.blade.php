<!-- jquery latest version -->
<script src="{{asset('organica/js/vendor/jquery-1.12.4.min.js')}}"></script>
	<!-- bootstrap js -->
<script src="{{asset('organica/js/bootstrap.min.js')}}"></script>
	<!-- owl.carousel.2.0.0-beta.2.4 css -->
<script src="{{asset('organica/js/owl.carousel.min.js')}}"></script>
	<!-- meanmenu js -->
<script src="{{asset('organica/js/jquery.meanmenu.js')}}"></script>
	<!-- jquery-ui js -->
<script src="{{asset('organica/js/jquery-ui.min.js')}}"></script>
	<!-- nivo-slider js -->
<script src="{{asset('organica/js/jquery.nivo.slider.pack.js')}}"></script>
	<!-- jquery countdown js -->
<script src="{{asset('organica/js/jquery.countdown.min.js')}}"></script>
	<!-- wow js -->
<script src="{{asset('organica/js/wow.min.js')}}"></script>
	<!-- plugins js -->
<script src="{{asset('organica/js/plugins.js')}}"></script>
	<!-- main js -->
<script src="{{asset('organica/js/main.js')}}"></script>
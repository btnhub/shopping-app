<nav>
	<ul>
		<li><a href="#">Shop</a>
			<ul>
				@foreach ($categories as $category)
					<li><a href="#">{{$category->name}}</a>
						<ul>
							@foreach ($category->subcategories->sortBy('website_order') as $subcategory)
								<?php $product_count = $subcategory->products
															->filter(function ($product) {
    															return $product->items->count() > 0;
															})->count();
								?>
								@if ($product_count > 0 && $subcategory->is_visible)	
									<li><a href="{{route('list.products', ['cat_slug' => str_slug($subcategory->category->name), 'subcategory_id' => $subcategory->id, 'sub_slug' => str_slug($subcategory->name)])}}">{{$subcategory->name}}</a></li>
								@endif
							@endforeach
						</ul>
					</li>
				@endforeach
			</ul>
		</li>
		
		@include('layouts.organica.partials.toplinks')
		
	</ul>
</nav>
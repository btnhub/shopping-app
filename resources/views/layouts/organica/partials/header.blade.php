<header class="header-area ptb-30">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div class="logo">
					<a href="{{url('/')}}">
						<img src="{{asset('images/logo/mountain_myst_logo.png')}}" alt="" />
					</a>
				</div>
			</div>
			<div class="col-md-8 col-sm-10 col-xs-10">
				<div class="mainmenu-area">
					<div class="top-bar clear">
						<div class="top-bar-left pull-left hidden-xs">
							<div class="hotline pull-left hidden-sm">
								
								<p><a href="{{url('/shipping')}}"><b><i class="fa fa-truck"></i> Free Shipping On Orders Over ${{config('other_constants.free_shipping_amount')}}!</b></a></p>
								
							</div>
						</div>
						{{--Search Box--}}
						
						<div class="top-bar-right">
							<div class="search-box">
								{!! Form::open(['method' => 'GET', 'url' => 'search', 'class' => 'form-horizontal']) !!}
								
								    <input type="search" name="query" id="query" placeholder="Search here ....."/>
									<button type="submit"><i class="fa fa-search"></i></button>
								
								{!! Form::close() !!}
							</div>
						</div>
						
						<div class="top-bar-right pull-right">
				           	{{-- <a href="mailto:Contact@Mountain-Myst.com">Contact@Mountain-Myst.com</a> |  --}}
							@if (isset($membership_active) && $membership_active)
									<a href="{{url('/membership')}}">Membership</a> | 
								@endif
							<a href="{{url('/gift-cards')}}">Gift Cards</a>	|

				            @if (Auth::user())
				                    <a href="{{route('user.account', Auth::user()->ref_id)}}">
				                    My Account
				                    </a> |    
				                    <a href="{{route('user.wishlist', Auth::user()->ref_id)}}">
				                    Wish List <b>({{Auth::user()->wishlist->count()}})</b>
				                    </a> |    
				                    <a href="{{ route('logout') }}"
				                        onclick="event.preventDefault();
				                                 document.getElementById('logout-form').submit();">
				                        Logout
				                    </a>

				                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				                        {{ csrf_field() }}
				                    </form>
				                
				            @else
				                <a href="{{ url('/login') }}">Login</a>    
				            @endif
				            
				            {{-- Facebook Link & Like --}}
				            @include('layouts.organica.partials.facebook_like_button')
						</div>
					</div>
					<div class="mainmenu dropdown hidden-xs hidden-sm">
						<nav class="pull-right">
							@include('layouts.organica.partials.topmenu')
						</nav>
					</div>
				</div>
			</div>
			{{--Cart--}}
			<div class="col-md-1 col-sm-2 col-xs-2">
				<div class="cart-area">
					@include('layouts.organica.partials.cart_menu')
				</div>
			</div>
		</div>
	</div>
	<!-- mobail-menu -->
	<div class="mobail-menu-area">
		<div class="container">
			<div class="row">
				<div class=" hidden-lg hidden-md col-sm-12">
					<div class="mobail-menu-active">
						@include('layouts.organica.partials.topmenu_mobile')
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
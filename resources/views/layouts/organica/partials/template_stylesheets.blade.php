<!-- bootstrap v3.3.7 css -->
<link rel="stylesheet" href="{{asset('organica/css/bootstrap.min.css')}} ">
	<!-- animate css -->
<link rel="stylesheet" href="{{asset('organica/css/animate.css')}}">
	<!-- jquery-ui.min css -->
<link rel="stylesheet" href="{{asset('organica/css/jquery-ui.min.css')}}">
	<!-- meanmenu css -->
<link rel="stylesheet" href="{{asset('organica/css/meanmenu.css')}}">
	<!-- nivo-slider css -->
<link rel="stylesheet" href="{{asset('organica/css/nivo-slider.css')}}">
	<!-- owl.carousel.2.0.0-beta.2.4 css -->
<link rel="stylesheet" href="{{asset('organica/css/owl.carousel.css')}}">
<!-- magnific-popup -->
<link rel="stylesheet" href="{{asset('organica/css/magnific-popup.css')}}">
	<!-- font-awesome v4.6.3 css -->
<link rel="stylesheet" href="{{asset('organica/css/font-awesome.min.css')}}">
	<!-- style css -->
	<link rel="stylesheet" href="{{asset('organica/style.css')}}">
	<!-- responsive css -->
<link rel="stylesheet" href="{{asset('organica/css/responsive.css')}}">
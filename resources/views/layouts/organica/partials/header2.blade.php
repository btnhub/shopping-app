<header class="header-area">
	<div class="home3-header-top-area ptb-15 border-bottom h3-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="top-sale text-uppercase">
						{{-- <b><a href="mailto:Contact@Mountain-Myst.com">Contact@Mountain-Myst.com</a></b> --}}
						<p><a href="{{url('/shipping')}}"><b><i class="fa fa-truck"></i> Free Shipping On Orders Over ${{config('other_constants.free_shipping_amount')}}!</b></a></p>
						{{--
						<p>MID-SEASON SALE UP TO 70% OFF. USE CODE"SALE70%".</p>
						<a href="shop.html">Shop now</a>
						--}}
						|
						@if (isset($membership_active) && $membership_active)
							<a href="{{url('/membership')}}">Membership</a> |
						@endif
						<a href="{{url('/gift-cards')}}">Gift Cards</a> |

				           	@if (Auth::user())
				                    <a href="{{route('user.account', Auth::user()->ref_id)}}">
				                    My Account
				                    </a> |    
				                    <a href="{{route('user.wishlist', Auth::user()->ref_id)}}">
				                    Wish List <b>({{Auth::user()->wishlist->count()}})</b>
				                    </a> | 
				                    <a href="{{ route('logout') }}"
				                        onclick="event.preventDefault();
				                                 document.getElementById('logout-form').submit();">
				                        Logout
				                    </a>

				                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				                        {{ csrf_field() }}
				                    </form>
				                
				            @else
				                <a href="{{ url('/login') }}">Login</a>    
				            @endif

				            {{-- Facebook Link & Like --}}
				            @include('layouts.organica.partials.facebook_like_button')

				    </div>
					<div class="cart-area home3-cart-area">
						@include('layouts.organica.partials.cart_menu')

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="home3-header-meddil-area pt-20 pb-20 border-bottom hm-3-res">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="hotline mt-15">
						{{--
						<p>Order online or call us : +0123 456 789</p>
						--}}
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="logo">
						<a href="{{url('/')}}">
							<img src="{{asset('images/logo/mountain_myst_logo.png')}}" alt="" />
						</a>
					</div>
				</div>
				<div class="col-md-4 clear mt-15  col-sm-12 col-xs-12">
					{{--
					<div class="language-USD dropdown pull-left home3-language-USD hidden-sm hidden-xs">
						<ul>
							<li><span>$<i class="fa fa-caret-down"> </i></span>
								<ul>
									<li><a href="#">US Dollar</a></li>
									<li><a href="#">US Dollar</a></li>
									<li><a href="#">US Dollar</a></li>
								</ul>
							</li>
							<li><span><img src="img/lg/1.png" alt="" /><i class="fa fa-caret-down"></i></span>
								<ul>
									<li><a href="#">Bangla</a></li>
									<li><a href="#">Bangla</a></li>
									<li><a href="#">Bangla</a></li>
								</ul>
							</li>
						</ul>
					</div>
					--}}
					{{--Search Box--}}
						
					<div class="top-bar-right pull-right">
						<div class="search-box">
							{!! Form::open(['method' => 'GET', 'url' => 'search', 'class' => 'form-horizontal']) !!}
								
								    <input type="search" name="query" id="query" placeholder="Search here ....."/>
									<button type="submit"><i class="fa fa-search"></i></button>
								
								{!! Form::close() !!}
						</div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="mainmenu dropdown text-center mt-40 home3-mainmenu hidden-xs hidden-sm">
						<nav>
							@include('layouts.organica.partials.topmenu')
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- mobail-menu -->
		<div class="mobail-menu-area">
			<div class="container">
				<div class="row">
					<div class=" hidden-lg hidden-md col-sm-12">
						<div class="mobail-menu-active">
							@include('layouts.organica.partials.topmenu_mobile')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</header>
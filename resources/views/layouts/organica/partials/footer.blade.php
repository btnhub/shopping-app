<!-- newsletter area start -->
{{--
<div class="home2-newsletter-area mt-60" style="background:url({{asset('images/footer_background.png')}}) no-repeat scroll 0 0;">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="newsletter home2-newsletter">
					
					<span>
						<a href="mailto:Contact@Mountain-Myst.com"><i class="fa fa-envelope-o "></i>  Contact@Mountain-Myst.com</a>
					</span>
				</div>
				
				<div class="socialicon ptb-50">
					<ul>
						<li>
							<a href="#"><i class="fa fa-envelope-o "></i></a>

						</li>
					</ul>
				</div>
				
			</div>
			
			<div class="col-md-8 col-sm-8  col-xs-12">
				<div class="newsletter home2-newsletter">
					<span>Sign up for send newsletter</span>
					<form action="#">
						<input type="email" placeholder="Enter your email address"/>
						<button>Subscribe <i class="fa fa-long-arrow-right"></i></button>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>
--}}
<!-- newsletter area end -->
<footer>
	{{-- 
	<div class="top-footer ptb-20 bg-2">
		<div class="container">
			<div class="row">
				<div class="col-md-6  col-sm-6  col-xs-12">
					<div class="contact clear">
						<div class="icon">
							<i class="fa fa-envelope-o"></i>
							<span>Do you have </span>
							<span> a question?</span>
						</div>
						<div class="text">
							<p><a href="mailto:Contact@Mountain-Myst.com">
							Contact@Mountain-Myst.com </a></p>
						</div>
					</div>
				</div> 
				<div class="col-md-4  col-sm-6 col-xs-12">
					<div class="contact clear">
						<div class="icon">
							<i class="fa fa-phone"></i>
							<span>Do you have </span>
							<span> a question?</span>
						</div>
						<div class="text">
							+0123 456 789
						</div>
					</div>
				</div>
				
				<div class="col-md-4 hidden-sm  col-xs-12">
					<div class="contact clear">
						<div class="icon">
							<i class="fa fa-life-saver"></i>
							<span> Support  </span>
							<span>question?</span>
						</div>
						<div class="text">
							<p>support@organica.com </p>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	 --}}
	<!-- contuct area end -->
	<!-- footer menu area start -->
	<div class="footer-medil-area bg-2">
		<div class="container">
			<div class="row">
				<div class="col-md-3 border-right col-sm-3  col-xs-12">
					<div class="footer-wrap pb-50">
						<div class="footer-static-title  text-uppercase  mt-50 mb-30">
							<h3>Information</h3>
						</div>
						<div class="footer-menu  text-uppercase">
							<ul>
								@if (isset($membership_active) && $membership_active)
									<li><a href="{{url('/membership')}}">Membership</a></li>
								@endif
								<li><a href="{{url('/subscription')}}">Subscription</a></li>
								<li><a href="{{url('/gift-cards')}}">Gift Cards</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 border-right col-sm-3  col-xs-12">
					<div class="footer-wrap pb-50">
						<div class="footer-static-title  text-uppercase  mt-50 mb-30">
							<h3>My Account</h3>
						</div>
						<div class="footer-menu  text-uppercase">
							<ul>
								<li><a href="{{route('my.account')}}">My Account</a></li>
								<li><a href="{{route('my.wishlist')}}">My Wish List</a></li>
								<li><a href="{{route('my.subscriptions')}}">My Subscriptions</a></li>				
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3  border-right col-sm-3  col-xs-12">
					<div class="footer-wrap pb-50">
						<div class="footer-static-title  text-uppercase  mt-50 mb-30">
							<h3>Policies</h3>
						</div>
						<div class="footer-menu  text-uppercase">
							<ul>
								<li><a href="{{url('/shipping')}}">Shipping Policy</a></li>
								<li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
								<li><a href="{{url('/terms')}}">Terms Of Use</a></li>
								<li><a href="{{url('/refunds')}}">Refunds & Returns</a></li>
								{{-- <li><a href="{{url('/faqs')}}">FAQs</a></li> --}}
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3  col-sm-3  col-xs-12">
					<div class="footer-wrap pb-50">
						<div class="footer-static-title  text-uppercase  mt-50 mb-30">
							<h3>Extras</h3>
						</div>
						<div class="footer-menu  text-uppercase">
							<ul>
								<li><a href="https://www.ewg.org/skindeep/search.php" target="_blank">EWG Skin Deep Cosmetics Database</a></li>
								<li><a href="{{url('/giving-back')}}">Giving Back</a></li>
								<li><a href="{{url('/contact-us')}}">Contact Us</a></li>
								{{-- <li><a href="#">Referral Policy  (ADD LINK)</a></li> --}}
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer menu area end -->
	<!-- copy right area start -->
	<div class="footer-bottom ptb-20 bg-4">
		<div class="container">
			<div class="row">
				<div {{-- class="col-lg-6 col-md-6 col-sm-6 col-xs-12" --}}>
					<div class="copyright">
						
						<small><em>The content on this website is for educational purposes only. This information has not been evaluated by the Food and Drug Administration. This information is not intended to diagnose, treat, cure, or prevent any disease. </em>
						</small>
					</div>
				</div>	
			</div>
			<br>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="copyright">
						<span>Copyright &copy; Mountain Myst, {{date('Y')}}</span>
					</div>
				</div>	
			</div>
			

			
		</div>
	</div>
	<!-- copy right area end -->
</footer>
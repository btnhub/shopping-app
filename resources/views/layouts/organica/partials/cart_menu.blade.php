@if (!isset($hide_cart))
	<div class="maincart-wrap">
		<a href="javascript:void(0);"><i class="fa fa-shopping-basket"></i>
			<span>{{Cart::content()->count()}}</span>
		</a>
	</div>
	<div class="cart">
		<div class="total-items pb-20 border-bottom mb-15">
			<div class="sub-total clear">
				<strong>{{Cart::count() ?? 0}}</strong>
				<span>{{str_plural('Item', Cart::count())}}</span>
				<span class="pull-right total">
					<span>Cart Subtotal :</span>
					<strong>${{Cart::subtotal()}}</strong>
				</span>
			</div>
			@if (Cart::count() > 0)
				<div class="cart-btn mt-15">
					<a href="{{route('cart.index')}}"><button>View Cart</button></a>
				</div>
			@endif

		</div>
		<div class="cart-items clear mb-15">
			@forelse (Cart::content() as $cart_item)				
				<div class="cart-item ptb-20 border-bottom">
					<div class="cart-img pull-left">
						@if ($cart_item->model->product->image)
							<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}">
								<img src='{{ asset("images/products/{$cart_item->model->product->image}") }}' alt="" />
							</a>
						@endif
					</div>
					<div class="cart-item-details clear">
						<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}">{{$cart_item->name}}</a>
						<br>
						<small>{{$cart_item->model->size ?? ''}}</small>
						<span class="price">${{number_format($cart_item->price, 2)}}</span>
						
						<div class="remove-edit">
							<ul>
								{{--<li><a href="#"><i class="fa fa-cog"></i></a></li>--}}
								{!! Form::open(['method' => 'PATCH', 'route' => ['cart.update', $cart_item->rowId], 'class' => 'form-horizontal']) !!}
									<div class="details-qty pull-left">
										<span>Qty :</span>
										<input type="number" name="qty" min="0" max="12" value="{{$cart_item->qty}}">			
									</div>
							        

								    <div class="btn-group pull-left">
								        {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
								    </div>
								{!! Form::close() !!}
								&nbsp;&nbsp;&nbsp;
								<li>
									{!! Form::open(['method' => 'DELETE', 'route' => ['cart.destroy', 'row_id' => $cart_item->rowId], 'class' => 'delete']) !!}			
									    <div class="btn-group pull-right">
									        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) !!}
									    </div>
									{!! Form::close() !!}
							</ul>
						</div>
					</div>
				</div>
			@empty
				<div>
					<center>No Items In Cart</center>
				</div>
			@endforelse
		</div>
		@if (Cart::count() > 0)
			<div class="organic-btn pt-20 text-center border-top">
				<a href="{{route('get.checkout')}}">Go to Checkout</a>
			</div>
		@endif
	</div> 
@endif
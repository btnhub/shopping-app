@extends('layouts.organica.template')

@section('header')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection
@section('page_title', 'Login')
@section('content')
    <?php 
        $marketing_options = ['friend' => 'Friend', 'internet' => 'Internet Search', 'facebook' => 'Facebook', 'google' => 'Google Ad', 'other' => 'Other'];    
    ?>

    <div class="main-container mb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{--
                    <div class="account-title mb-40 text-center">
                        <h1>My Account</h1>
                    </div>
                    --}}
                    
                    {{--NOTE!  Be sure to change expiration date on RegisterController --}}
                    <?php $expiration_date = Carbon\Carbon::create(2017,12,31,23,59,59,'America/Denver');?>

                    @if (Carbon\Carbon::now() < $expiration_date) 
                        <div class="row">
                            <div class="container">
                                <div class="row">
                            
                                    <div class="col-md-12">
                                        <h3>$5 Account Credit</h3>
                                        <p>Sign up and instantly receive a $5 credit!  No code needed, just create an account.  </p>

                                        <small>For new customers only.  Account credit cannot be redeemed for cash and cannot be used to purchase Mountain Myst membership or gift cards.  Offer & credit expire on {{$expiration_date->format('m/d/Y')}}.</small>

                                        <br><br>
                                    </div>
                            
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-6">
                    <div class="account-heading mb-20">
                        <h2>Login</h2>
                    </div>
                    
                    <div class="account  bg-1  p-20  mb-30">
                        <div class="account-form">
                            <form role="form" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <b>E-mail address <span>*</span></b>
                                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <b>Password <span>*</span></b>
                                    <input id="password" type="password" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="login-button">
                                    <button type="submit">
                                        Login
                                    </button>
                                    
                                    {{--
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                    --}}
                                    <a href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="account-heading mb-20">
                        <h2>Register</h2>
                    </div>
                    <div class="account">
                        <div class="account-form p-20 bg-1">
                            <form role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <b>First Name  <span>*</span></b>
                                        <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>    
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <b>Last Name  <span>*</span></b>
                                        <input id="last_name" type="text"  name="last_name" value="{{ old('last_name') }}" required autofocus>

                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>    
                                </div>
                                

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <b>E-mail Address  <span>*</span></b>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <b>Password  <span>*</span></b>
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <b>Confirm Password  <span>*</span></b>
                                        <input id="password-confirm" type="password" name="password_confirmation" required>
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('marketing') ? ' has-error' : '' }} col-md-5 col-sm-5">
                                    {!! Form::label('marketing[]', 'How You Heard About Us *') !!}
                                    {!! Form::select('marketing[]', $marketing_options, null, ['id' => 'marketing', 'class' => 'form-control', 'required' => 'required', 'multiple', 'size' => '6']) !!}
                                    <small class="text-danger">{{ $errors->first('marketing') }}</small>
                                </div>
                                
                                By creating an account, you agree to <a href="{{url('/terms')}}" style="text-decoration:underline">our terms</a>.<br><br>
                                <div class="login-button">
                                    <div class="g-recaptcha" data-sitekey="{{config('services.google_recaptcha.key')}}"></div>
                                    <small class="text-danger">{{ $errors->first('g-recaptcha-response') }}</small>

                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </form>
                            <br><Br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<?php 
	$membership_active_field = \DB::table('settings')->where('field', 'membership_active')->first()->id;
    $membership_active = \DB::table('settings_values')->where('setting_id', $membership_active_field)->first()->value;
?>

<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>@yield('page_title') - Mountain Myst</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
        
		<!-- all css here -->
		@include('layouts.organica.partials.template_stylesheets')
		<!-- modernizr css -->
        <script src="{{asset('organica/js/vendor/modernizr-2.8.3.min.js')}}"></script>

        @yield('header')
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- header start -->
		@include('layouts.organica.partials.header')
        <!-- header end -->
		<!-- about-area start -->
		@if (isset($subcategory) && $subcategory->image)
			{{--Breadcrumbs with Banner--}}
			<div class="breadcrumbs-area {{-- bg-4 mb-20 --}}" style='background:url({{"/images/categories/subcategories/$subcategory->image"}}) no-repeat center; background-size: 100%'>
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="woocommerce-breadcrumb mtb-150">
								<h2>@yield('page_title', '')</h2>
								<em>@yield('subtitle', "")</em>

								{{-- <div class="menu">
									<ul>
										<li class="active"><a href="javascript:void(0);">@yield('page_title', 'Page Title')</a></li>
									</ul>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		@else
		<div class="breadcrumbs-area bg-4 mb-20">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="woocommerce-breadcrumb mtb-15">
								<div class="menu">
									<ul>
										<li class="active">{{-- <a href="javascript:void(0);"> --}}@yield('breadcrumbs', ''){{-- </a> --}}</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		@endif
		

		<div class="about-main-area">
			@if (!isset($subcategory))
				{{--For regular pages, e.g. Membership--}}
				<div class="about-title-area pb-20">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="about-heading">
									<h1>@yield('page_title', '')</h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			@elseif (!$subcategory->image)
				<div class="about-title-area pb-20">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="cap-title">
									<h1>@yield('page_title', '')</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="about-heading">
									<h3>@yield('subtitle', '')</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif

			{{--Display Alerts & Error Flash Messages--}}
        	@include('layouts.alerts')

			<div class="about-area">
				<div class="container">
					<div class="row">
						@yield('content')
					</div>					
				</div>
			</div>
			
		
		<!-- footer area start -->
		<div class="mt-50">
			@include('layouts.organica.partials.footer')
		</div>
		<!-- footer area end -->
		<!-- all js here -->
		@include('layouts.organica.partials.template_scripts')

		@yield('scripts')

		<script>
	        $(".delete").on("submit", function(){
	            return confirm("Are you sure you want to delete?");
	        });
	    </script>
    </body>
</html>
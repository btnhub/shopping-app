<?php 
	$membership_active_field = \DB::table('settings')->where('field', 'membership_active')->first()->id;
    $membership_active = \DB::table('settings_values')->where('setting_id', $membership_active_field)->first()->value;
?>

<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Mountain Myst - Handmade, Natural Body Care</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
        
		<!-- all css here -->
		@include('layouts.organica.partials.template_stylesheets')
		<!-- modernizr css -->
        <script src="{{asset('organica/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- header start -->
		@include('layouts.organica.partials.header2')
        <!-- header end -->
        
        {{--Display Alerts & Error Flash Messages--}}
        @include('layouts.alerts')
        

        <!-- static-home3-top-area start -->
        <?php 
		$donation_setting_id = DB::table('settings')->where('field', 'donation_percent')->first()->id;
		$donation_percent =  DB::table('settings_values')->where('setting_id', $donation_setting_id)->first()->value * 100; 
		?>

		{{--Banners: Special Info & Deals--}}
		{{-- <div class="static-home3-top border-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="static-content text-uppercase border-left">
							<i class="fa fa-dollar" style="color:green"></i>
							<p><a href="{{url('/login')}}"><b>Create An Account & Receive A $5 Credit Instantly!</b><small> (Exp 12/31/17)</small></a></p>
						</div>
					</div>
					<div class="col-md-6  col-sm-6  col-xs-12">
						<div class="static-content text-uppercase border-left border-right">
							<i class="fa fa-heart" style="color:pink"></i>
							<p><a href="{{url('/giving-back')}}">Giving Back & Supporting Local Organizations</a></p>
						</div>
					</div>
				</div>
			</div>
		</div> --}}

		<!-- slider area start -->
		<div class="slider-area">
			<img src="{{asset('images/frontend/homepage_photo04.png')}}" alt="Mountain Myst" height="750"/>
			{{-- @include('layouts.organica.partials.homepage.slider') --}}
		</div>
		<!-- slider area end -->
		<!-- banner area  start-->
			{{-- @include('layouts.organica.partials.homepage.banner') --}}
		<!-- banner area  end-->
		
		<!-- Our History area start -->
		@include('layouts.organica.partials.homepage.our_mission')
		<!-- Our History area end -->

		<!-- product area start -->
		{{-- <div class="product-area bg-1 ptb-50">
			@include('layouts.organica.partials.homepage.product_area')
		</div> --}}
		<!-- product area end -->
		
		<!-- tab area start  -->
		<div class="tab-area {{-- mtb-100 --}} bg-1 ptb-50">
			@include('layouts.organica.partials.homepage.tab_area')	
		</div>
		<!-- tab area end  -->
		<!-- New arrivals area start -->
		{{-- @include('layouts.organica.partials.homepage.new_arrivals') --}}
		<!-- New arrivals area end -->
		
		<!-- work and store area start -->
		@include('layouts.organica.partials.homepage.work_area')
		<!-- work and store area start -->
		
		<!-- testmonial area start -->
			{{--@include('layouts.organica.partials.homepage.testimonials')--}}
		<!-- testmonial area end -->
		<!-- blog area start -->
			{{--@include('layouts.organica.partials.homepage.blog')--}}
		<!-- blog area end -->
		
		<!-- footer area start -->
		@include('layouts.organica.partials.footer')
		<!-- footer area end -->
		
		<!-- all js here -->
		@include('layouts.organica.partials.template_scripts')

		@yield('scripts')

		<script>
	        $(".delete").on("submit", function(){
	            return confirm("Are you sure you want to delete?");
	        });
	    </script>
	    
    </body>
</html>

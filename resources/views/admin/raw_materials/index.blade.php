@extends('layouts.admin_josh.template')
@section('page_title', 'Raw Materials')
@section('content')
    <div class="row">

        <div class="col-md-12">
            <h3>Raw Materials</h3>
             
            <a href="{{route('raw_materials.create')}}" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add A Raw Material</a></button>

            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Material</th>
                        <th>Supplier</th>
                        <th>Description</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Organic</th>
                        <th>Source</th>
                        <th>Notes</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($raw_materials as $raw_material)
                    <tr>
                        <td>{{$raw_material->id}}</td>
                        <td>{{$raw_material->material}}</td>
                        <td>{{$raw_material->supplier->name ?? ''}}</td>
                        <td>{{$raw_material->description}}</td>
                        <td>{{$raw_material->size}} {{$raw_material->unit}}</td>
                        <td>{{$raw_material->price}}</td>
                        <td>{{$raw_material->organic ? 'Yes' : 'No'}}</td>
                        <td>{{$raw_material->source_link}}</td>
                        <td>{{$raw_material->notes}}</td>
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['raw_materials.destroy', 'id' => $raw_material->id], 'class' => 'delete']) !!}
                                <a href="{{route('raw_materials.edit', ['id'=> $raw_material->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></button>

                                <div class="btn-group pull-right">
                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                </div>
                            
                            {!! Form::close() !!}

                            </td>
                    </tr>
                    @empty
                        <tr><td>No Materials.</td></tr>

                    @endforelse
                </tbody>
            </table>            
        </div>

    </div>

@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Add A Raw Material')

@section('content')
    <div class="container">
    	<div class="row">
    
    		<div class="col-md-12">
    			<h3>Add A Raw Material</h3>
			    {!! Form::open(['method' => 'POST', 'route' => 'raw_materials.store', 'class' => 'form-horizontal']) !!}
			    
			        @include('admin.raw_materials.form_partial')
			    	
			        <div class="btn-group pull-right">
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
			            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
			        </div>
			    
			    {!! Form::close() !!}			
    		</div>
    
    	</div>
    </div>
    

@endsection
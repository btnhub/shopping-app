@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Raw Material')

@section('content')

	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<h3>Edit Raw Material</h3>
			    {!! Form::model($raw_material, ['method' => 'PATCH', 'route' => ['raw_materials.update', 'id' => $raw_material->id], 'class' => 'form-horizontal']) !!}
			    
			        @include('admin.raw_materials.form_partial')

			        <div class="btn-group pull-right">
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
			            {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
			        </div>
			    
			    {!! Form::close() !!}			
			</div>
		</div>
	</div>
    
@endsection
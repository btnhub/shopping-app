<div class="form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }}">
    {!! Form::label('supplier_id', 'Select A Supplier') !!}
    {!! Form::select('supplier_id', $suppliers, null, ['id' => 'supplier_id', 'class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
</div>

<div class="form-group{{ $errors->has('material') ? ' has-error' : '' }}">
    {!! Form::label('material', 'Raw Material') !!}
    {!! Form::text('material', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('material') }}</small>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('size') ? ' has-error' : '' }} col-md-3">
        {!! Form::label('size', 'Size') !!}
        {!! Form::text('size', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('size') }}</small>
    </div>

    <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }} col-md-3">
        {!! Form::label('unit', 'Unit') !!}
        {!! Form::select('unit',array_merge_recursive(['' => "Select Unit"], ['vol' => 'Volume Units'], $volume_units, ['weight' => 'Weight Units'], $mass_units, ['length' => 'Length Units'], $length_units), null, ['id' => 'unit', 'class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('unit') }}</small>
    </div>

    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} col-md-3">
        {!! Form::label('price', 'Price ($)') !!}
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('price') }}</small>
    </div>

    <div class="radio{{ $errors->has('organic') ? ' has-error' : '' }} col-md-3">
        <b>Is the material Organic?</b><br>
        <label>
            {!! Form::radio('organic',1,  null, ['id' => 'radio_id']) !!} Yes
        </label>
        <label>
            {!! Form::radio('organic',0,  null, ['id' => 'radio_id']) !!} No
        </label>
        <small class="text-danger">{{ $errors->first('organic') }}</small>
    </div>

</div>

<div class="form-group{{ $errors->has('source_link') ? ' has-error' : '' }}">
    {!! Form::label('source_link', 'Website Link To Material') !!}
    {!! Form::text('source_link', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('source_link') }}</small>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Additional Notes') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control', 'size' => '4x4']) !!}
    <small class="text-danger">{{ $errors->first('notes') }}</small>
</div>
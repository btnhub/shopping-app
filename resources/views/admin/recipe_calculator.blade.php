<?php
	$ingredients = App\RawMaterial::all();

	$default_mass_unit = "ounce";
	$default_volume_unit = "us_ounce";

	$mass_units = [
        'gram',
        'kilogram',
        'metric_ton',
        'ounce',
        'pound'];

	$volume_units = ['millilitre',
        'litre',
        'gallon',
        'quart',
        'pint',
        'tablespoon',
        'teaspoon',
        'us_gallon',
        'us_quart',
        'us_pint',
        'us_cup',
        'us_ounce',
        'us_tablespoon',
        'us_teaspoon'];

    foreach ($ingredients as $ingredient) {
    	if (in_array($ingredient->unit, $mass_units)) {
    		//convert mass
    		$ingredient->size = Conversion::convert($ingredient->size,$ingredient->unit)->to($default_mass_unit)->format(2,'.',',');
    		$ingredient->unit = $default_mass_unit;
    	}
    	elseif (in_array($ingredient->unit, $volume_units)) {
    		//convert volume
    		$ingredient->size = Conversion::convert($ingredient->size,$ingredient->unit)->to($default_volume_unit)->format(2, '.', ',');
    		$ingredient->unit = $default_volume_unit;
    	}

    	else
    	{
    		//remove from collection if not measured in mass or volume
    		$ingredients->forget($ingredient->id);
    	}
    }
    
?>
Lotion Recipe ~ 16 oz<br>
Unit trial: <br>
<table class="table">
	<thead>
		<tr>
			<th>Ingredient</th>
			<th>Unit Price</th>
			<th>Amount</th>
			<th>Price</th>
		</tr>
	</thead>
	<tbody>
		@for ($i = 1; $i <= 6; $i++)
			<tr><td>
					<div class="form-group{{ $errors->has('ingredient') ? ' has-error' : '' }}">
					    {!! Form::label('ingredient', 'Ingredient '. $i) !!}
					    {!! Form::select('ingredient', $ingredients, null, ['id' => 'ingredient'.$i, 'class' => 'form-control', 'onChange' => "totalPrice(this.options[this.selectedIndex].text, $i)"]) !!}
					    <small class="text-danger">{{ $errors->first('ingredient') }}</small>
					</div>
				</td>
				<td>
					<p id="unit_price{{$i}}"></p>
				</td>
				<td>
					<input type="number" name="ingredient_amount" id="ingredient_amount{{$i}}" value="0">
					<input type="text" id="ingredient_unit{{$i}}">
				</td>
				<td>
					<input type="number" name="ingredient_cost" id="ingredient_cost{{$i}}">
				</td>
			</tr>
		@endfor
		<tr>
			<td></td>
			<td><b>Total</b></td>
			<td>
				<p id="total_amount"></p>
			</td>
			<td>
				<b><p id="total_cost"></p></b>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><b>Shipping & Tax (Estimated 25%)</b></td>
			<td>
			</td>
			<td>
				<b><p id="shipping_cost"></p></b>
			</td>
		</tr>
		<tr>
			<td></td>
			<td># 8 oz</td>
			<td>
				<p id="units_8"></p>
			</td>
			<td>
				<b><p id="shipping_cost"></p></b>
			</td>
		</tr>
	</tbody>
</table>

<script type='text/javascript'>	
/*
	function convertUnit(amount, start_unit, target_unit, target_element, ingredient){
		alert('blah');
		document.getElementById(target_element).value = new_amount;

		totalVolume(); // update total volume
		ingredientPrice(ingredient);
	}*/


	function totalPrice(object, row_id){
		var total_amount = 0;
		var total_cost = 0;

		var price = JSON.parse(object).price;
		var size = JSON.parse(object).size;
		var unit = JSON.parse(object).unit;

		var unit_price = (price / size).toFixed(2);
		var ingredient_amount = document.getElementById('ingredient_amount' + row_id).value
		
		document.getElementById("unit_price" + row_id).innerHTML = unit_price;
		document.getElementById("ingredient_unit" + row_id).value = unit;
		document.getElementById("ingredient_cost" + row_id).value = (ingredient_amount * unit_price).toFixed(2);

		var ingredients_amount = document.getElementsByName('ingredient_amount');
		var ingredients_cost = document.getElementsByName('ingredient_cost');
		
		for(var i=0;i<ingredients_amount.length;i++){
	        if(parseFloat(ingredients_amount[i].value))
	            total_amount += parseFloat(ingredients_amount[i].value);
	    }

	    for(var i=0;i<ingredients_cost.length;i++){
	        if(parseFloat(ingredients_cost[i].value))
	            total_cost += parseFloat(ingredients_cost[i].value);
	    }

		document.getElementById("total_amount").innerHTML = total_amount + " " + unit;
	    document.getElementById("total_cost").innerHTML = "$" +total_cost.toFixed(2);
	}
</script>
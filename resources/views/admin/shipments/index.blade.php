@extends('layouts.admin_josh.template')
@section('page_title', 'Shipments')
@section('content')
    <div class="row">

        <div class="col-md-12">
            <h3>Shipments</h3>
            <h4>Pending Shipments</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Order & Shipment IDs</th>
                        <th>Order Status</th>
                        <th>Customer</th>
                        <th>Shipping Method</th>
                        <th>Duration</th>
                        <th>Insurance</th>
                        <th>Shippo IDs </th>
                        <th>Notes</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pending_shipments as $pending_shipment)
                        <tr>
                            <td>{{Carbon\Carbon::parse($pending_shipment->created_at)->format('m/d/Y')}}</td>
                            <td>
                                Order ID: 
                                @if ($pending_shipment->order)
                                    <a href="{{route('orders.show', $pending_shipment->id)}}">{{$pending_shipment->order->id}} / {{$pending_shipment->order->ref_id}}</a>
                                @else
                                    Could not Find Order
                                @endif
                                
                                <br>
                                Shipment ID: {{$pending_shipment->id}}
                            </td>
                            <td>
                                <b>Shippo Status:</b> {{$pending_shipment->status}}
                            </td>

                            <td>{{$pending_shipment->order ? $pending_shipment->order->full_name : 'Could Not Find Order'}}</td>
                            <td>{{$pending_shipment->provider}} - {{$pending_shipment->servicelevel_name
                            }}</td>
                            <td>{{$pending_shipment->days}} days -> {{$pending_shipment->duration_terms}}</td>
                            <td>${{$pending_shipment->insurance_amount ?? 0 }}</td>
                            <td>
                                Shippo Rate ID: {{$pending_shipment->shippo_rate_id}}<br>
                                Shippo Shipment ID: {{$pending_shipment->shippo_shipment_id}}<br>
                                Shippo Parcel ID: {{$pending_shipment->shippo_parcel_id}}<br>
                            </td>
                            <td>{{$pending_shipment->notes}}</td>
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['shipments.destroy', 'id' => $pending_shipment->id], 'class' => 'delete']) !!}
                                    <a href="{{route('shipments.edit', ['id'=> $pending_shipment->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></button>
                                    
                                    <div class="btn-group pull-right">
                                        {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                    </div>
                                
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>

            <h4>Completed Shipments</h4>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Order & Shipment IDs</th>
                        <th>Order Status</th>
                        <th>Customer</th>
                        <th>Shipping Method</th>
                        <th>Duration</th>
                        <th>Insurance</th>
                        <th>Shippo IDs </th>
                        <th>Notes</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($completed_shipments as $completed_shipment)
                        <tr>
                            <td>{{Carbon\Carbon::parse($completed_shipment->created_at)->format('m/d/Y')}}</td>
                            <td>
                                Order ID: 
                                @if ($completed_shipment->order)
                                    <a href="{{route('orders.show', $completed_shipment->id)}}">{{$completed_shipment->order->id}} / {{$completed_shipment->order->ref_id}}</a>
                                @else
                                    Could not Find Order
                                @endif
                                
                                <br>
                                Shipment ID: {{$completed_shipment->id}}
                            </td>
                            <td>
                                <b>Shippo Status:</b> {{$completed_shipment->status}}
                            </td>

                            <td>{{$completed_shipment->order ? $completed_shipment->order->full_name : 'Could Not Find Order'}}</td>
                            <td>{{$completed_shipment->provider}} - {{$completed_shipment->servicelevel_name
                            }}</td>
                            <td>{{$completed_shipment->days}} days -> {{$completed_shipment->duration_terms}}</td>
                            <td>${{$completed_shipment->insurance_amount ?? 0 }}</td>
                            <td>
                                Shippo Rate ID: {{$completed_shipment->shippo_rate_id}}<br>
                                Shippo Shipment ID: {{$completed_shipment->shippo_shipment_id}}<br>
                                Shippo Parcel ID: {{$completed_shipment->shippo_parcel_id}}<br>
                            </td>
                            <td>{{$completed_shipment->notes}}</td>
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['shipments.destroy', 'id' => $completed_shipment->id], 'class' => 'delete']) !!}
                                    <a href="{{route('shipments.edit', ['id'=> $completed_shipment->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></button>
                                    
                                    <div class="btn-group pull-right">
                                        {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                    </div>
                                
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>        
        </div>

    </div>
    
@endsection

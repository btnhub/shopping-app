@extends('layouts.admin_josh.template')
@section('page_title', 'Recurring Orders')
@section('content')
    <div class="container">
        <div class="row">
	        <div class="col-md-12">
	    		<table class="table">
		        	<thead>
		        		<tr>
		        			<th>ID</th>
		        			<th>User</th>
		        			<th>Items</th>
		        			<th>Frequency</th>
		        			<th>Dates</th>
		        			<th>Charges</th>
		        			<th>Notes</th>
		        		</tr>
		        	</thead>
		        	<tbody>
		        		@forelse ($recurring_orders as $recurring_order)
		        			<tr>
			        			<td>
			        				<a href="{{route('recurring_orders.show', $recurring_order->id)}}">{{$recurring_order->id}} / {{$recurring_order->ref_id}}</a>
			        			</td>
			        			<td>{{$recurring_order->user->full_name}}</td>
			        			<td>
			        				<ol>
			        					@foreach ($recurring_order->items as $item)
				        					<li>
				        						{{$item->product->name}} ({{$item->size}}) x {{$item->pivot->quantity}}
				        					</li>
				        				@endforeach	
			        				</ol>
			        			</td>
			        			<td>{{$recurring_order->frequency}} Every {{$recurring_order->days}} Days</td>
			        			<td>
			        				<b>Start Date:</b> {{$recurring_order->start_date ? $recurring_order->start_date->format('m/d/Y'): "-"}}<br>
			        				<b>End Date:</b> {{$recurring_order->end_date ? $recurring_order->end_date->format('m/d/Y') : "-"}}
			        			</td>
			        			<td>
			        				<b>Previous Payment:</b> {{$recurring_order->last_charge ? $recurring_order->last_charge->format('m/d/Y') : "-"}}<br>
			        				<b>Next Payment:</b> {{$recurring_order->next_charge ? $recurring_order->next_charge->format('m/d/Y') : "-"}}
			        			</td>
			        			<td>{{$recurring_order->notes}}</td>
			        		</tr>
		        		@empty

		        		@endforelse
		        		
		        	</tbody>
		        </table>    	
	        </div>
        
		</div>
	</div>

@endsection
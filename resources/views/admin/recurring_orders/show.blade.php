@extends('layouts.admin_josh.template')
@section('page_title', 'Recurring Orders Details')
@section('content')
	
<div class="row">

	<div class="col-md-12">
		<a href="{{route('recurring_orders.index')}}"><-- Back to all recurring orders</a>
		<br>
		<h3>Recurring Order: {{$recurring_order->id}}</h3>

		<div class="col-md-4">
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>User: </td>
						<td>
							<a href="{{route('users.show', $recurring_order->user->id)}}" target="_blank">{{$recurring_order->user->full_name}} </a>
						</td>
					</tr>
					<tr>
						<td>Recurring Order ID: </td>
						<td>{{$recurring_order->id}} / {{$recurring_order->ref_id}}</td>
					</tr>
					<tr>
						<td>Frequency</td>
						<td>Every {{$recurring_order->days}} Days</td>
					</tr>
					<tr>
						<td>Dates</td>
						<td>
		    				<b>Start Date:</b> {{$recurring_order->start_date ? $recurring_order->start_date->format('m/d/Y'): "-"}}<br>
		    				<b>End Date:</b> {{$recurring_order->end_date ? $recurring_order->end_date->format('m/d/Y') : "-"}}
		    			</td>
					</tr>
					<tr>
						<td>Payments</td>
						<td>
		    				<b>Previous Payment:</b> {{$recurring_order->last_charge ? $recurring_order->last_charge->format('m/d/Y') : "-"}}<br>
		    				<b>Next Payment:</b> {{$recurring_order->next_charge ? $recurring_order->next_charge->format('m/d/Y') : "-"}}
		    			</td>
					</tr>
					<tr style="font-weight: bold">
						<td>Order Total</td>
						<td>
							${{$recurring_order->total_cost()}}
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-md-4">
		@if (Carbon\Carbon::now()->diffInDays($recurring_order->next_charge, false) < 1)
			{!! Form::open(['method' => 'POST', 'route' => ['fill.recurring.order', $recurring_order->id], "onsubmit" => "return confirm('Are You Sure? Please Double Check Before Submitting');"]) !!}
			    <center>
			        {!! Form::submit("Charge & Create New Order", ['class' => 'btn btn-success']) !!}
			    </center>
			
			{!! Form::close() !!}
		@else
			<center><button type="button" class="btn btn-success" disabled>Charge & Create New Order</button></center>
			<center><small>Will activate within 24 hours of the next charge</small></center>
		@endif
			
		</div>
		
		<div class="col-md-4">
			{!! Form::open(['method' => 'DELETE', 'route' => ['subscription.destroy', 'ref_id' => $recurring_order->ref_id], 'class' => 'delete']) !!}	
				<div class="btn-group">
			        {!! Form::submit("Delete Subscription", ['class' => 'btn btn-danger']) !!}
			        <br><br>
			        <small>FOLLOW UP WITH EMAIL TO CUSTOMER</small>
			    </div>
			{!! Form::close() !!}
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Image</th>
					<th>Product</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($recurring_order->items as $item)
					<tr>
						<td>
							@if ($item->product->image)
                                <a href="{{route('product.details', ['cat_slug' => str_slug($item->product->subcategory->category->name), 'sub_slug' => str_slug($item->product->subcategory->name), 'prod_slug' => str_slug($item->product->name), 'product_id' => $item->product->id])}}" target="_blank"><img src='{{ asset("images/products/{$item->product->image}") }}' height='50'></a><br>
                            @endif
						</td>
						<td>
							<a href="{{route('product.details', ['cat_slug' => str_slug($item->product->subcategory->category->name), 'sub_slug' => str_slug($item->product->subcategory->name), 'prod_slug' => str_slug($item->product->name), 'product_id' => $item->product->id])}}" target="_blank">{{$item->product->name}} 
								@if (unserialize($item->pivot->details)->options->organic)
									(Organic)
								@endif
							</a><br>
							{{$item->details()}} - {{$item->size}} {{$item->pivot->glass ? 'Glass Container': ''}}
						</td>
						<td>${{$item->pivot->price}}</td>
						<td>{{$item->pivot->quantity}}</td>
						<td>
							${{number_format($item->pivot->price * $item->pivot->quantity, 2)}}
						</td>
						<td>	
							@if ($recurring_order->items->count() > 1)
								{!! Form::open(['method' => 'DELETE', 'route' => ['remove.subscription.item', 'recurring_order_ref_id' => $recurring_order->ref_id, 'item_ref_id' => $item->ref_id], 'class' => 'delete']) !!}
									<div class="btn-group pull-right">
								        {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
								    </div>
								{!! Form::close() !!}	
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>

@endsection
@foreach ($ingredient_types as $type)
	<div class="row">
		<h3>{{ucwords(str_replace("_", " ", $type))}}</h3>
		@foreach ($ingredients->where('type', $type) as $ingredient)
			<div class="form-group">
			    <div class="checkbox{{ $errors->has('ingredient_id') ? ' has-error' : '' }} col-md-3">
			        <label>
			            {!! Form::checkbox('ingredient_id', $ingredient->id, in_array($ingredient->id, $ingredient_ids ?? []), ['id' => "ingredient_id_$ingredient->id", 'name' => 'ingredient_ids[]', 'onclick' => "show_amounts($ingredient->id)"]) !!} {{$ingredient->name}}
			        </label>

			        <div id="details_{{$ingredient->id}}" @if (!in_array($ingredient->id, $ingredient_ids ?? []))
			        		style="display:none"
			        	@endif
			        >
			        	<div class='radio{{ $errors->has("organic($ingredient->id)") ? ' has-error' : '' }}'>
				            Organic:
				            <label>
			                	{!! Form::radio("organic($ingredient->id)",1,  null, []) !!} Yes &nbsp;&nbsp;&nbsp;
			               	</label>
			               	<label>
			                	{!! Form::radio("organic($ingredient->id)",0,  1, []) !!} No
				            </label>
				            <small class="text-danger">{{ $errors->first("organic($ingredient->id)") }}</small>
				        </div>
				        <div class='form-group{{ $errors->has("amount($ingredient->id)") ? " has-error" : "" }} col-md-7'>
				            {!! Form::text("amount($ingredient->id)", null, ['class' => 'form-control', 'min' => 0]) !!}
				            <small class="text-danger">{{ $errors->first("amount($ingredient->id)") }}</small>
				        </div>
				        <div class="form-group{{ $errors->has("unit($ingredient->id)") ? ' has-error' : '' }} col-md-5">
				            {!! Form::select("unit($ingredient->id)", config('other_constants.recipe_units'), null, ['class' =>'form-control']) !!}
				            <small class="text-danger">{{ $errors->first("unit($ingredient->id)") }}</small>
				        </div>
			        </div>
			    </div>
			    <small class="text-danger">{{ $errors->first('ingredient_id') }}</small>
			</div>
		@endforeach	
	</div>
@endforeach    

<script type="text/javascript">
	function show_amounts(ingredient_id)
	{
		var div_id = "details_" + ingredient_id;
		var ingredient_checkbox = "ingredient_id_" + ingredient_id;

		if (document.getElementById(ingredient_checkbox).checked == true) {
			document.getElementById(div_id).style.display = 'block';
		}
		else
		{
			document.getElementById(div_id).style.display = 'none';
		}	
	}
</script>

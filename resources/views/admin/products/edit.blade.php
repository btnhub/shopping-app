@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Product')

@section('content')
<div class="container">
   <div class="row">

       <div class="col-md-12">
          <a href="{{route('products.index')}}"><-- Back To All Products</a>
          <h3>Edit {{$product->name}} Product</h3>
          <div class="row">
            <div class="col-md-4">
              @if ($product->image)
                  <a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}" target="_blank"><img src='{{asset("images/products/$product->image")}}' height="220"></a>
              @else
                  ADD IMAGE
              @endif
            </div> 
            <div class="col-md-8" >
              
              <a href="{{route('items.create')}}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add An Item</a>
              <br>

              <hr>
              <div class="col-md-6 pull-right" >
              OR Copy Items From Similar Product
              {!! Form::open(['method' => 'POST', 'route' => ['copy.items', 'new_product_id' => $product->id], 'class' => 'form-horizontal']) !!}
              
                  <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                      {!! Form::label('product_id', 'Product') !!}
                      {!! Form::select('product_id', array_replace(["" => "Select A Product"], $related_products), null, ['id' => 'product_id', 'class' => 'form-control', 'required' => 'required']) !!}
                      <small class="text-danger">{{ $errors->first('product_id') }}</small>
                  </div>
              
                  <div class="btn-group pull-right">
                      {!! Form::submit("Copy Items", ['class' => 'btn btn-success']) !!}
                  </div>
              
              {!! Form::close() !!}
              <br>
              </div>
              
              {{$product->items->count()}} Items
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th></th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Details</th>
                    <th>Dimensions (LxWxH)</th>
                    <th>Shipping Weight</th>
                    <th>Notes</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($product->items as $item)
                    <tr>
                      <td>
                        @if ($item->image)
                          <img src='{{asset("images/products/items/$item->image")}}' height="85">
                        @endif
                      </td>
                      <td>{{$item->size}}</td>
                      <td>${{$item->organic_price}}
                          @if ($item->sale)
                            <br>
                            <span style="color:red">${{$item->sale}} <small>Sale</small></span>
                          @elseif ($item->surplus)
                            <br>
                            <span style="color:green">${{$item->surplus}} <small>Surplus</small></span>
                          @endif
                      </td>
                      <td>
                          <a href="{{route('items.edit', ['id'=> $item->id])}}" target="_blank">
                            {{$item->color ? "Color: ". $item->color : ""}} {{$item->scent ? "Scent: ". $item->scent : ""}} {{$item->flavor ? "Flavor: ". $item->flavor : ""}} {{$item->additional_feature ? "Addtl Feature: ". $item->additional_feature : ""}}
                          </a>
                      </td>
                      <td>{{$item->length}} x {{$item->width}} x {{$item->height}} {{$item->distance_unit}}</td>
                      <td>{{$item->weight}} {{$item->mass_unit}}</td>
                      <td>
                          {!! $item->recurring_option ? '' : "Not Available For Recurring Orders<br>" !!}
                          {!! $item->discontinued ? "<span style='color:red'>Discontinued</span><br>" : ''!!}
                          
                          @if ($item->notes)
                            <b>Notes:</b><br>
                            {{$item->notes}}
                          @endif
                      </td>
                      <td>
                          {!! Form::open(['method' => 'DELETE', 'route' => ['items.destroy', 'id' => $item->id], 'class' => 'delete']) !!}
                                <a href="{{route('items.edit', ['id'=> $item->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

                                <div class="btn-group pull-right">
                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                </div>
                            
                            {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-6">

              {!! Form::model($product, ['method' => 'PATCH', 'route' => ['products.update', 'id' => $product->id], 'class' => 'form-horizontal', 'files' => true]) !!}
              
                  @include('admin.products.form_partial')

                  <br><br>
                  <div>
                      {!! Form::reset("Reset", ['class' => 'btn btn-warning pull-right']) !!}
                      {!! Form::submit("Update", ['class' => 'btn btn-success pull-left']) !!}
                  </div>
              
              {!! Form::close() !!}           
            </div>
          </div>
       </div>

   </div>
</div>
@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Add A Product')

@section('content')
<div class="container">
	<div class="row">

		<div class="col-md-12">
		    <h3>Add A Product</h3>
		    {!! Form::open(['method' => 'POST', 'route' => 'products.store', 'class' => 'form-horizontal', 'files' => true]) !!}
		    
		        @include('admin.products.form_partial')
		    	<br><br>
		        <div>
		            {!! Form::reset("Reset", ['class' => 'btn btn-warning pull-right']) !!}
		            {!! Form::submit("Add", ['class' => 'btn btn-success pull-left']) !!}
		        </div>
		    
		    {!! Form::close() !!}
			
		</div>

	</div>
</div>

@endsection
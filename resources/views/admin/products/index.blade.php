@extends('layouts.admin_josh.template')
@section('page_title', 'Products')
@section('content')
    <div class="row">

        <div class="col-md-12">
            <h3>Products</h3>
            <a href="{{route('products.create')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add A Product</a>
            <br><br>
            {{$products->count()}} Products Total.  Click image to view product on the front end.             
        </div>

    </div>
    
    

    @foreach ($subcategories as $subcategory)
        @if ($subcategory->all_products->count())
            <div class="row">
                <div class="col-md-12">
                    <h2>{{$subcategory->name}}</h2>
                    {{$subcategory->all_products->count()}} Products
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Items Count</th>
                                <th>Website Order</th>
                                <th>Available</th>
                                <th>Category</th>
                                <th>Description</th>
                                {{-- <th>Notes</th> --}}
                                {{-- <th>Date Added</th> --}}
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($subcategory->all_products->sortBy('website_order') as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>
                                    @if ($product->image)
                                        @if ($product->items()->count() > 0)
                                            <a href="{{route('product.details', ['cat_slug' => str_slug($product->subcategory->category->name), 'sub_slug' => str_slug($product->subcategory->name), 'prod_slug' => str_slug($product->name), 'product_id' => $product->id])}}" target="_blank"><img src='{{ asset("images/products/$product->image") }}' height='50'></a><br>
                                            <small>{{$product->image}}</small><br>
                                        @else
                                            <img src='{{ asset("images/products/$product->image") }}' height='50'><br>
                                            <small>{{$product->image}}</small><br>
                                        @endif
                                        
                                    @endif
                                    <br>
                                    
                                    @if ($product->recipe)
                                        <a href="{{route('edit.recipe', ['product_id' => $product->id])}}">Edit Recipe</a>
                                    @else
                                        <a href="{{route('create.recipe', ['product_id' => $product->id])}}">ADD RECIPE!</a>
                                    @endif
                                    
                                    @if ($product->sale)
                                        <span style="color:red">On Sale</span>
                                    @endif

                                    @if ($product->surplus)
                                        <span style="color:red">Surplus</span>
                                    @endif
                                    
                                </td>
                                <td>
                                    <a href="{{route('products.edit', ['id'=> $product->id])}}">{{$product->name}}</a>
                                    <br><br>
                                    @if ($product->items->count())
                                        <?php 
                                            $cheapest = $product->items->count() ? $product->items->sortby('price')->first() : null; 
                                            $expensive = $product->items->count() ? $product->items->sortbyDesc('price')->first() : null;
                                        ?>

                                        {{$cheapest->size}} - ${{$cheapest->organic_price}}<br>
                                        @if ($expensive->size != $cheapest & $expensive->organic_price != $cheapest->organic_price)
                                            {{$expensive->size}} - ${{$expensive->organic_price}}
                                        @endif    
                                    @endif
                                    
                                    
                                </td>
                                <td>
                                    @if ($product->items->count() > 0)
                                        {{$product->items->count()}}
                                    @else
                                        <a href="{{route('items.create')}}">Add Items</a>
                                    @endif
                                </td>
                                <td>{{$product->website_order}}</td>
                                <td>{!!$product->available() ? "Yes" : "<span style='color:red'>No</span>"!!}</td>
                                <td>{{$product->subcategory->name}}</td>
                                <td>{!!substr($product->detailed_description,0,200)!!}
                                    @if (strlen($product->detailed_description) > 200)
                                        ...
                                    @endif
                                @if (!$product->detailed_description)
                                    <span style="color:darkred">Add Detailed Description</span>
                                @endif
                                </td>
                                {{-- <td>{{$product->notes}}</td> --}}
                                {{-- <td>{{Carbon\Carbon::parse($product->created_at)->format('m/d/Y')}}</td> --}}
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy', 'id' => $product->id], 'class' => 'delete']) !!}
                                        <a href="{{route('products.edit', ['id'=> $product->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

                                        <div class="btn-group pull-right">
                                            {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                        </div>
                                    
                                    {!! Form::close() !!}

                                    </td>
                            </tr>
                            @empty
                                <tr><td>No products.</td></tr>

                            @endforelse
                        </tbody>
                    </table>               
                </div>
            </div>
        @endif
    @endforeach
@endsection
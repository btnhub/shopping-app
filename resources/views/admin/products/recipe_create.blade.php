@extends('layouts.admin_josh.template')
@section('page_title', 'Create Recipe')

@section('content')
<div class="container">
   <div class="row">

       <div class="col-md-12">
          	<a href="{{route('products.index')}}"><-- Back To All Products</a>
		   	<h2>Store {{$product->name}} Recipe</h2>
		   	Type in amounts in recipe in the SAME UNIT (e.g. all in oz or all in g).  The proportions of each ingredient (not the actual amount) will be calculated and saved.

		   	<br><br>
		   	{!! Form::open(['method' => 'POST', 'route' => ['store.recipe', 'product_id' => $product->id]]) !!}
		   		
		   		<div class="row">
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-4">
					    {!! Form::label('name', 'Recipe Name') !!}
					    {!! Form::text('name', $product->name, ['class' => 'form-control']) !!}
					    <small class="text-danger">{{ $errors->first('name') }}</small>
					</div>
				</div>	
				@include('admin.products.recipe_form_partial')

				<br>
		   	    <div>
		   	        {!! Form::reset("Reset", ['class' => 'btn btn-warning pull-right']) !!}
		   	        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
		   	    </div>
		   	
		   	{!! Form::close() !!}
		   	
		</div>
   </div>
</div>

@endsection
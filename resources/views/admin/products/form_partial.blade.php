<div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
    {!! Form::label('subcategory_id', 'Select A Subcategory') !!}
    {!! Form::select('subcategory_id', $subcategories, null, ['id' => 'subcategory_id', 'class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('subcategory_id') }}</small>
</div>

<div class="form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }}">
    {!! Form::label('supplier_id', 'Select A Supplier') !!}
    {!! Form::select('supplier_id', $suppliers, null, ['id' => 'supplier_id', 'class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
</div>

<div class="form-group{{ $errors->has('website_order') ? ' has-error' : '' }}">
    {!! Form::label('website_order', 'Website Order') !!}
    {!! Form::number('website_order', null, ['class' => 'form-control', 'required' => 'required', "min" => 1]) !!}
    <small class="text-danger">{{ $errors->first('website_order') }}</small>
</div>

<div class="radio{{ $errors->has('is_visible') ? ' has-error' : '' }}">
    Product ready to be viewed on front end: &nbsp;&nbsp;&nbsp;&nbsp;
    <label>
        {!! Form::radio('is_visible',1,  null, ['id' => 'radio_id']) !!} Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </label>
    <label>
        {!! Form::radio('is_visible',0,  null, []) !!} No
    </label>
    <small class="text-danger">{{ $errors->first('is_visible') }}</small>
</div>
<br>

<div class="form-group">
    <div class="checkbox{{ $errors->has('organic_option') ? ' has-error' : '' }}">
        <label for="organic_option">
            {!! Form::checkbox('organic_option', '1', 1, ['id' => 'organic_option']) !!} Organic Option (Whether product has an organic option)
        </label>
    </div>
    <small class="text-danger">{{ $errors->first('organic_option') }}</small>
</div>
 
<div class="form-group">
    <div class="checkbox{{ $errors->has('glass') ? ' has-error' : '' }}">
        <label for="glass">
            {!! Form::checkbox('glass', '1', null, ['id' => 'glass']) !!} Product Can Be Packaged In Glass
        </label>
    </div>
    <small class="text-danger">{{ $errors->first('glass') }}</small>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Product Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>	

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {!! Form::label('description', 'Product Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>

<div class="form-group{{ $errors->has('website_description') ? ' has-error' : '' }}">
    {!! Form::label('website_description', 'Description Seen On Website') !!}
    {!! Form::text('website_description', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('website_description') }}</small>
</div>

<div class="form-group{{ $errors->has('detailed_description') ? ' has-error' : '' }}">
    {!! Form::label('detailed_description', 'Detailed description of product group for website') !!}
    {!! Form::textarea('detailed_description', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('detailed_description') }}</small>
</div>

<div class="form-group{{ $errors->has('image_file') ? ' has-error' : '' }}">
    {!! Form::label('image_file', 'Image') !!}
    {!! Form::file('image_file') !!}
    <p class="help-block">Please upload a photo (professional) for all items in this set</p>
    <small class="text-danger">{{ $errors->first('image_file') }}</small>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Admin Notes (not seen on website)') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('notes') }}</small>
</div>

<hr>

<div class="radio{{ $errors->has('featured') ? ' has-error' : '' }}">
    Should Product Be Featured On The Homepage?
    <label>
        {!! Form::radio('featured',1, 1, []) !!} Yes
    </label>
    <label>
        {!! Form::radio('featured',0, []) !!} No
    </label>    
    <small class="text-danger">{{ $errors->first('featured') }}</small>
</div>

<div class="radio{{ $errors->has('sale') ? ' has-error' : '' }}">
    Is the ENTIRE Product Line On Sale?
    <label>
        {!! Form::radio('sale',1,  null, []) !!} Yes
    </label>
    <label>
        {!! Form::radio('sale',0,  null, []) !!} No
    </label>
    <small class="text-danger">{{ $errors->first('sale') }}</small>
</div>

<div class="radio{{ $errors->has('surplus') ? ' has-error' : '' }}">
    Does The ENTIRE Product Line Have A Surplus Sale?
    <label>
        {!! Form::radio('surplus',1,  null, []) !!} Yes
    </label>
    <label>
        {!! Form::radio('surplus',0,  null, []) !!} No
    </label>
    <small class="text-danger">{{ $errors->first('surplus') }}</small>
</div>
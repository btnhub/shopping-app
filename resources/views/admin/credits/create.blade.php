@extends('layouts.admin_josh.template')
@section('page_title', 'Create A Credit')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <h3>Create A Credit</h3>
            <br><br>

            {!! Form::open(['method' => 'POST', 'route' => 'credits.store']) !!}
            	<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                      {!! Form::label('amount', 'Credit Amount') !!}
                      {!! Form::number('amount', null, ['class' => 'form-control', 'required' => 'required']) !!}
                      <small class="text-danger">{{ $errors->first('amount') }}</small>
                  </div>
                  <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
            	    {!! Form::label('type', 'Credit Type') !!}
            	    {!! Form::select('type',$credit_types, null, ['id' => 'type', 'class' => 'form-control', 'required' => 'required']) !!}
            	    <small class="text-danger">{{ $errors->first('type') }}</small>
            	</div>
            	<hr>
            	<h3>User Details</h3>
            	<div class="form-group{{ $errors->has('recipient_id') ? ' has-error' : '' }}">
            	    {!! Form::label('recipient_id', 'User') !!}
            	    {!! Form::select('recipient_id',$users, null, ['id' => 'recipient_id', 'class' => 'form-control']) !!}
            	    <small class="text-danger">{{ $errors->first('recipient_id') }}</small>
            	</div>
            	<center><b>OR</b></center>
            	<div class="form-group{{ $errors->has('recipient_name') ? ' has-error' : '' }}">
            	    {!! Form::label('recipient_name', 'Customer Full Name') !!}
            	    {!! Form::text('recipient_name', null, ['class' => 'form-control']) !!}
            	    <small class="text-danger">{{ $errors->first('recipient_name') }}</small>
            	</div>

            	<div class="form-group{{ $errors->has('recipient_email') ? ' has-error' : '' }}">
            	    {!! Form::label('recipient_email', 'Customer Email') !!}
            	    {!! Form::email('recipient_email', null, ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) !!}
            	    <small class="text-danger">{{ $errors->first('recipient_email') }}</small>
            	</div>
            	<hr>

            	<h3>Credit Details</h3>
            	<div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
            	    {!! Form::label('details', 'Short description of credit (seen by customer)') !!}
            	    {!! Form::text('details', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => "Mountain Myst Promotion (Instagram), Gift Card From MM, Referral Credit"]) !!}
            	    <small class="text-danger">{{ $errors->first('details') }}</small>
            	</div>
            	<div class="form-group{{ $errors->has('expiration_date') ? ' has-error' : '' }}">
            	    {!! Form::label('expiration_date', 'Expiration Date (default is 1 month from now') !!}
            	    {!! Form::date('expiration_date',Carbon\Carbon::now()->addMonth(), ['class' => 'form-control', 'required' => 'required']) !!}
            	    <small class="text-danger">{{ $errors->first('expiration_date') }}</small>
            	</div>
            	<div class="form-group">
            	    <div class="checkbox{{ $errors->has('send_email') ? ' has-error' : '' }}">
            	        <label for="send_email">
            	            {!! Form::checkbox('send_email', '1', 1, ['id' => 'send_email']) !!} Send Email Notification To Customer
            	        </label>
            	    </div>
            	    <small class="text-danger">{{ $errors->first('send_email') }}</small>
            	</div>
            	<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
            	    {!! Form::label('message', 'Message To Recipient (Required If Sending E-mail)') !!}
            	    {!! Form::textarea('message', "A gift just for you!  Enjoy this $ ____ GIFT CERTIFICATE.  ", ['class' => 'form-control']) !!}
            	    <small class="text-danger">{{ $errors->first('message') }}</small>
            	</div>
            	<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
            	    {!! Form::label('notes', 'Note To Self') !!}
            	    {!! Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => "Customer's IG account, reason for giving credit"]) !!}
            	    <small class="text-danger">{{ $errors->first('notes') }}</small>
            	</div>
			    <div class="btn-group pull-right">
			        {!! Form::submit("Create Credit Code", ['class' => 'btn btn-primary']) !!}
			    </div>

			{!! Form::close() !!}
        </div>
    </div>
@endsection
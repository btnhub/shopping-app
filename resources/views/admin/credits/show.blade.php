@extends('layouts.admin_josh.template')

@section('page_title', "Credit Details")

@section('content')
<div class="container">
	<div class="row">
		<h3>Credit Details (ID: {{$credit->id}})</h3>
		<div class="col-md-6">
			{!! Form::open(['method' => 'DELETE', 'route' => ['credits.destroy', 'id' => $credit->id], 'class' => 'delete']) !!}
	                    
	                    <div class="btn-group pull-right">
	                        {!! Form::submit("Delete Credit", ['class' => 'btn btn-danger']) !!}
	                    </div>
	                
	                {!! Form::close() !!}
	        <br><br>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td><b>Recipient:</b> </td>
						<td>
							@if($credit->recipient_id)
		                		<a href="{{route('users.show', $credit->recipient_id)}}" target="_blank">{{$credit->recipient_name}}</a>
		                	@else
		                		{{$credit->recipient_name}}<br>
		                		<small>{{$credit->recipient_email}}</small>
		                	@endif
						</td>
					</tr>
					<tr>
						<td><b>Provider: </b> </td>
						<td>
							@if($credit->provider_id)
		                		<a href="{{route('users.show', $credit->provider_id)}}" target="_blank">{{$credit->provider_name}}</a>
		                	@else
		                		{{$credit->provider_name}}
		                		<br>
		                		<small>{{$credit->recipient_email}}</small>
		                	@endif
						</td>
					</tr>
					<tr>
						<td><b>Amount:</b> </td>
						<td><b>${{$credit->amount}}</b></td>
					</tr>
					@if($credit->balance() != $credit->amount)
						<tr>
							<td><b>Balance:</b> </td>
							<td><b>${{$credit->balance()}}</b></td>
						</tr>
					@endif
					<tr>
						<td><b>Type:</b> </td>
						<td>{{ucwords($credit->type)}} - {{$credit->details}} 
							@if($credit->createdByAdmin())
								<br>
								{{$credit->message}}
							@endif
							@if(!$credit->approved)
								<br>
								<span style="color:red">NOT APPROVED</span>
								
								{!! Form::open(['method' => 'POST', 'route' => ['approve.credit.code', 'credit_id' => $credit->id]]) !!}
						              {!! Form::submit("Approve Credit", ['class' => 'btn btn-primary pull-right']) !!}
						        {!! Form::close() !!} 
							@endif
						</td>					
					</tr>
					<tr>
						<td><b>Code:</b> </td>
						<td>{{$credit->credit_code}} </td>
					</tr>
					<tr>
						<td><b>Created At</b></td>
						<td>{{$credit->created_at->format('m/d/Y')}}</td>
					</tr>
					<tr>
						<td><b>Expires On</b></td>
						<td>
							{{$credit->expiration_date->format('m/d/Y g:ia')}}
							@if($credit->expiration_date->isPast())
								<br>
								<span style="color:darkred">Expired</span>
							@endif
						</td>
					</tr>

					@if($credit->notes)
						<tr>
							<td><b>Note</b></td>
							<td>{{$credit->notes}}</td>
						</tr>
					@endif
					
					@if($credit->deleted_at)
						<tr style="color:darkred; font-weight: bold">
							<td><b>Deleted On</b></td>
							<td>{{$credit->deleted_at->format('m/d/Y g:ia')}}</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			
                
		</div>
	</div>
</div>
@endsection
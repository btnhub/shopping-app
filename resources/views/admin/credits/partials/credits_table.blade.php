<h4 style="font-weight: bold">Total: ${{$credits->sum('amount')}}</h4>
<?php 
	$remaining_sum = 0;
	foreach ($credits as $credit) {
		$remaining_sum += $credit->balance();
	}
?>

@if($credits->sum('amount') != $remaining_sum)
	<h4 style="font-weight: bold;color:darkgreen">Remaining: ${{$remaining_sum}}</h4>
@endif


<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Provider</th>
            <th>Recipient</th>
            <th>Amount</th>
            <th>Code</th>
            <th>Notes</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($credits as $credit)
            <tr>
                <td>{{$credit->id}}</td>
                <td>{{Carbon\Carbon::parse($credit->created_at)->format('m/d/Y')}}</td>
                <td>
                	@if($credit->provider_id)
                		<a href="{{route('users.show', $credit->provider_id)}}" target="_blank">{{$credit->provider_name}}</a>
                	@else
                		{{$credit->provider_name}}
                		<br>
                		<small>{{$credit->recipient_email}}</small>
                	@endif
                </td>
                <td>
                	@if($credit->recipient_id)
                		<a href="{{route('users.show', $credit->recipient_id)}}" target="_blank">{{$credit->recipient_name}}</a>
                	@else
                		{{$credit->recipient_name}}<br>
                		<small>{{$credit->recipient_email}}</small>
                	@endif
                </td>
                <td>${{$credit->amount}}
                	@if(!$credit->approved)
                		<br>
                		<span style="color:red">Not Approved</span>
                	@endif
                	
                	@if($credit->amount != $credit->balance())
                		<br>
                		<small>${{$credit->balance()}}</small>
                	@endif
                </td>
                <td>{{$credit->credit_code}}</td>
                <td>{{$credit->notes}}</td>
                <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['credits.destroy', 'id' => $credit->id], 'class' => 'delete']) !!}
                    <a href="{{route('credits.show', ['id'=> $credit->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

                    <div class="btn-group pull-right">
                        {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                    </div>
                
                {!! Form::close() !!}

                </td>

            </tr>
        @endforeach
    </tbody>
</table>
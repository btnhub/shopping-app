@extends('layouts.admin_josh.template')
@section('page_title', 'Credits')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Credits & Gift Cards</h3>
            <a href="{{route('credits.create')}}" class="btn btn-primary">Create Credit Code</a>
            <br><br>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#gifts">Gift Cards
                    @if ($gift_credits->count())
                        ({{$gift_credits->count()}})
                    @endif</a>
                </li>
                <li><a data-toggle="tab" href="#promotions">Promotions
                    @if ($promotion_credits->count())
                        ({{$promotion_credits->count()}})
                    @endif</a>
                </li>
                <li><a data-toggle="tab" href="#referrals">Referrals
                    @if ($referral_credits->count())
                        ({{$referral_credits->count()}})
                    @endif</a>
                </li>
                <li><a data-toggle="tab" href="#account">Account Credits
                    @if ($account_credits->count())
                        ({{$account_credits->count()}})
                    @endif</a>
                </li>
                <li><a data-toggle="tab" href="#used">Used Up Credits
                        @if ($used_up_credits->count())
                            ({{$used_up_credits->count()}})
                        @endif
                    </a>
                </li>
                <li><a data-toggle="tab" href="#expired">Expired Credits
                        @if ($expired_credits->count())
                            ({{$expired_credits->count()}})
                        @endif
                    </a>
                </li>
            </ul>    

            <div class="tab-content">
                <div id="gifts" class="tab-pane fade in active">
                    <p>Gift card credits purchased by customer</p>
                    @include('admin.credits.partials.credits_table', ['credits' => $gift_credits])
                </div>
                <div id="promotions" class="tab-pane fade">
                    <p>Promotional credits created and distributed by company</p>
                        @include('admin.credits.partials.credits_table', ['credits' => $promotion_credits])
                
                </div>   
                <div id="referrals" class="tab-pane fade">
                    <p>Referral credits earned by customer for referring another customer.</p>
                        @include('admin.credits.partials.credits_table', ['credits' => $referral_credits])
                </div>   
                <div id="account" class="tab-pane fade">
                    <p>Account credits, e.g. given to customer to resolve an issue.</p>
                        @include('admin.credits.partials.credits_table', ['credits' => $account_credits])
                
                </div>  
                <div id="used" class="tab-pane fade">
                    <p>Credits that have been completely used up.</p>
                        @include('admin.credits.partials.credits_table', ['credits' => $used_up_credits])
                </div> 
                <div id="expired" class="tab-pane fade">
                    <p>Credits that expired before being used up.</p>
                    @include('admin.credits.partials.credits_table', ['credits' => $expired_credits])
                </div>   
            </div>
        </div>
    </div>
    
@endsection

@extends('layouts.admin_josh.template')
@section('page_title', 'Add A Subcategory')
@section('content')
	<div class="container">
		<div class="row">
			<a href="{{route('subcategories.index')}}"><-- Back</a>
			<div class="col-md-12">
				<h3>Add A Subcategory</h3>
			    {!! Form::open(['method' => 'POST', 'route' => 'subcategories.store', 'class' => 'form-horizontal', 'files' => true]) !!}
			    
			        @include('admin.subcategories.form_partial')    	

			        <div>
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning pull-right']) !!}
			            {!! Form::submit("Add Subcategory", ['class' => 'btn btn-success pull-left']) !!}
			        </div>
			    
			    {!! Form::close() !!}			
			</div>
		</div>
	</div>
@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Subcategories')
@section('content')
    <div class="container">
        <h3>Subcategories</h3>
        <a href="{{route('subcategories.create')}}" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add A Subcategory</a></button>

        @foreach ($categories as $category)
            <div class="row">
                <div class="col-md-12">
                    <h3>{{$category->name}}</h3> 
                    <br><br>
                    {{$category->subcategories->count()}} Subcategories
                    <br>
                    <small>Click image to view front end</small>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th># Of Products</th>
                                <th>Description (Page Subtitle)</th>
                                <th>Website Order</th>
                                {{-- <th>Date Added</th> --}}
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($category->subcategories->sortBy('website_order') as $subcategory)
                            <tr>
                                <td>{{$subcategory->id}}</td>
                                <td>
                                    @if ($subcategory->image)
                                        <a href="{{route('list.products', ['cat_slug' => str_slug($subcategory->category->name), 'subcategory_id' => $subcategory->id, 'sub_slug' => str_slug($subcategory->name)])}}" target="_blank"><img src='{{ asset("images/categories/subcategories/$subcategory->image") }}' height='50'></a>
                                    @else
                                        <a href="{{route('list.products', ['cat_slug' => str_slug($subcategory->category->name), 'subcategory_id' => $subcategory->id, 'sub_slug' => str_slug($subcategory->name)])}}" target="_blank">View Front End</a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('subcategories.edit', ['id'=> $subcategory->id])}}">
                                    {{$subcategory->name}}</a>
                                </td>
                                <td>{{$subcategory->products->count()}}</td>
                                <td>{{$subcategory->description}}</td>
                                <td>{{$subcategory->website_order}}</td>
                                {{-- <td>{{Carbon\Carbon::parse($subcategory->created_at)->format('m/d/Y')}}</td> --}}
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['subcategories.destroy', 'id' => $subcategory->id], 'class' => 'delete']) !!}
                                        <a href="{{route('subcategories.edit', ['id'=> $subcategory->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                        
                                        <div class="btn-group pull-right">
                                            {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                        </div>
                                    
                                    {!! Form::close() !!}

                                    </td>
                            </tr>
                            @empty
                                <tr><td>No subcategories.</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
        
            </div>
        @endforeach
        
    </div>
    
@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Subcategory')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<a href="{{route('subcategories.index')}}"><-- Back</a>
			<h3>Edit Subcategory</h3>
		    <div class="row">
				<div class="col-md-4">
	              @if ($subcategory->image)
	                  <img src='{{asset("images/categories/subcategories/$subcategory->image")}}' height="220">
	              @else
	                  ADD IMAGE
	              @endif
	            </div> 
			</div>
		    {!! Form::model($subcategory, ['method' => 'PATCH', 'route' => ['subcategories.update', 'id' => $subcategory->id], 'class' => 'form-horizontal', 'files' => true]) !!}
		    
		        @include('admin.subcategories.form_partial')       

		        <div>
		            {!! Form::reset("Reset", ['class' => 'btn btn-warning pull-right']) !!}
		            {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
		        </div>
		    
		    {!! Form::close() !!}		
		</div>
	</div>
</div>
    
@endsection
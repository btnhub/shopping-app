<div class="row">
	<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }} col-md-6">
	    {!! Form::label('category_id', 'Select A Category') !!}
	    {!! Form::select('category_id', $categories, null, ['id' => 'category_id', 'class' => 'form-control', 'required' => 'required']) !!}
	    <small class="text-danger">{{ $errors->first('category_id') }}</small>
	</div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('image_file') ? ' has-error' : '' }} col-md-4">
        {!! Form::label('image_file', 'Image') !!}
        {!! Form::file('image_file') !!}
        <p class="help-block">Please upload a photo (professional) for the subcategory banner</p>
        <small class="text-danger">{{ $errors->first('image_file') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Subcategory Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>


<div class="form-group{{ $errors->has('website_order') ? ' has-error' : '' }}">
    {!! Form::label('website_order', 'Position On Website (smaller number = higher position)') !!}
    {!! Form::number('website_order', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('website_order') }}</small>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {!! Form::label('description', 'Description (for page subtitle)') !!}
    {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>

<div class="form-group{{ $errors->has('website_description') ? ' has-error' : '' }}">
    {!! Form::label('website_description', 'Website Description') !!}
    {!! Form::textarea('website_description', null, ['class' => 'form-control', 'size' => '20x4']) !!}
    <small class="text-danger">{{ $errors->first('website_description') }}</small>
</div>

<div class="form-group{{ $errors->has('detailed_description') ? ' has-error' : '' }}">
    {!! Form::label('detailed_description', 'Detailed Description') !!}
    {!! Form::textarea('detailed_description', null, ['class' => 'form-control', 'size' => '20x6']) !!}
    <small class="text-danger">{{ $errors->first('detailed_description') }}</small>
</div>			

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Admin Notes') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control', 'size' => '20x6']) !!}
    <small class="text-danger">{{ $errors->first('notes') }}</small>
</div>
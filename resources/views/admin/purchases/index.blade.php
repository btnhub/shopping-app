@extends('layouts.admin_josh.template')
@section('page_title', 'Purchases')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <h3>Purchases</h3>
             
            <a href="{{route('purchases.create')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add A Purchase</a>
            <br><br>

            <table class="table">
            	<thead>
            		<tr>
            			<th>ID</th>
            			<th>Supplier</th>
            			<th>Transaction ID</th>
            			<th>Cost</th>
            			<th>Dates</th>
            			<th>Use Tax</th>
            			<th></th>
            		</tr>
            	</thead>
            	<tbody>
            		@forelse ($purchases as $purchase)
            			<tr>
	            			<td>{{$purchase->id}}</td>
	            			<td>{{$purchase->supplier ? $purchase->supplier->name : ''}}</td>
	            			<td>{{$purchase->transaction_id}}</td>
	            			<td>
	            				Amount: ${{$purchase->amount}}<br>
	            				Shipping: ${{$purchase->shipping_fee}}<br>
	            				Sales Tax: ${{$purchase->sales_tax ?? 0}}<br>
	            				<b>Total: ${{$purchase->total}}</b>
	            			</td>
	            			<td>
	            				
	            				Purchased: {{$purchase->purchase_date}}<br>
	            				Received: {{$purchase->date_received}}
	            			</td>
	            			<td>
	            				State: ${{$purchase->state_use_tax}}<br>
	            				County: ${{$purchase->county_use_tax}}<br>
	            				City: ${{$purchase->city_use_tax}}<br>
	            				Special: ${{$purchase->special_use_tax}}
	            			</td>
							<td>
								{!! Form::open(['method' => 'DELETE', 'route' => ['purchases.destroy', 'id' => $purchase->id], 'class' => 'delete']) !!}
	                                <a href="{{route('purchases.edit', ['id'=> $purchase->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

	                                <div class="btn-group pull-right">
	                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
	                                </div>
	                            
	                            {!! Form::close() !!}

							</td>            		
	            		</tr>
            		@empty
            			<tr>
            				<td>
            					<center>No Purchases</center>
            				</td>
            			</tr>
            		@endforelse
            	</tbody>
            </table>
        </div>
    </div>
@endsection
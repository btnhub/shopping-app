@extends('layouts.admin_josh.template')
@section('page_title', 'Add A Purchase')

@section('content')
	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<a href="{{route('purchases.index')}}"> <-- Back To Purchases </a>
				<h3>Add A Purchase</h3>
			    {!! Form::open(['method' => 'POST', 'route' => 'purchases.store', 'class' => 'form-horizontal']) !!}
			    
			        @include('admin.purchases.form_partial')
			    	
			        <div class="btn-group pull-right">
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
			            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
			        </div>
			    
			    {!! Form::close() !!}
			</div>	
		</div>
	</div>
    
@endsection
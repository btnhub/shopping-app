@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Purchase')

@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-12">
          <a href="{{route('purchases.index')}}"><-- Back To All Purchases</a>
          <h3>Edit Purchase</h3>

          <div class="row">
            {!! Form::model($purchase, ['method' => 'PATCH', 'route' => ['purchases.update', 'id' => $purchase->id], 'class' => 'form-horizontal', 'files' => true]) !!}
              
                  @include('admin.purchases.form_partial')

                  <div class="row">

                    <div class="form-group{{ $errors->has('state_use_tax') ? ' has-error' : '' }} col-md-3">
                        {!! Form::label('state_use_tax', 'State Use Tax') !!}
                        {!! Form::number('state_use_tax', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('state_use_tax') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('county_use_tax') ? ' has-error' : '' }} col-md-3">
                        {!! Form::label('county_use_tax', 'County Use Tax') !!}
                        {!! Form::number('county_use_tax', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('county_use_tax') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('city_use_tax') ? ' has-error' : '' }} col-md-3">
                        {!! Form::label('city_use_tax', 'CIty Use Tax') !!}
                        {!! Form::number('city_use_tax', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('city_use_tax') }}</small>
                    </div>

                    <div class="form-group{{ $errors->has('special_use_tax') ? ' has-error' : '' }} col-md-3">
                        {!! Form::label('special_use_tax', 'Special Use Tax') !!}
                        {!! Form::number('special_use_tax', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('special_use_tax') }}</small>
                    </div>
                  </div>

                  <div class="radio{{ $errors->has('recalc_taxes') ? ' has-error' : '' }}">
                      <b>Recalculate Use Taxes? </b> (Based on changes to Amount & Shipping): <br>

                      <label>
                          {!! Form::radio('recalc_taxes', 1, 1, ['id' => 'radio_id']) !!} Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </label>
                      <label>
                          {!! Form::radio('recalc_taxes', 0, null, ['id' => 'radio_id']) !!} No
                      </label>
                      <small class="text-danger">{{ $errors->first('recalc_taxes') }}</small>
                  </div>
                  <br>
                  <div class="btn-group pull-left">
                      {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                      {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
                  </div>
              
              {!! Form::close() !!}           
          </div>
      </div>
    </div>
</div>
@endsection
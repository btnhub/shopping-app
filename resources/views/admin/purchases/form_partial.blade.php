<div class="row">
    <div class="form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }} col-md-6">
        {!! Form::label('supplier_id', 'Select A Supplier') !!}
        {!! Form::select('supplier_id', $suppliers, null, ['id' => 'supplier_id', 'class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('supplier_id') }}</small>
    </div>
</div>

<div class="row">
	<div class="form-group{{ $errors->has('transaction_id') ? ' has-error' : '' }} col-md-4">
	    {!! Form::label('transaction_id', 'Transaction ID / Receipt ID') !!}
	    {!! Form::text('transaction_id', null, ['class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('transaction_id') }}</small>
	</div>
	<div class="form-group{{ $errors->has('purchase_date') ? ' has-error' : '' }} col-md-4">
	    {!! Form::label('purchase_date', 'Purchase Date') !!}
	    {!! Form::date('purchase_date', null, ['class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('purchase_date') }}</small>
	</div>
	<div class="form-group{{ $errors->has('date_received') ? ' has-error' : '' }} col-md-4">
	    {!! Form::label('date_received', 'Date Received') !!}
	    {!! Form::date('date_received', null, ['class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('date_received') }}</small>
	</div>

</div>

<div class="row">
	<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }} col-md-4">
	    {!! Form::label('amount', 'Amount (Without Tax & Shipping)') !!}
	    {!! Form::number('amount', null, ['class' => 'form-control', 'required' => 'required']) !!}
	    <small class="text-danger">{{ $errors->first('amount') }}</small>
	</div>	

	<div class="form-group{{ $errors->has('shipping_fee') ? ' has-error' : '' }} col-md-4">
	    {!! Form::label('shipping_fee', 'Shipping Cost') !!}
	    {!! Form::number('shipping_fee', null, ['class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('shipping_fee') }}</small>
	</div>

	<div class="form-group{{ $errors->has('sales_tax') ? ' has-error' : '' }} col-md-4">
	    {!! Form::label('sales_tax', 'Sales Tax') !!}
	    {!! Form::number('sales_tax', null, ['class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('sales_tax') }}</small>
	</div>	
</div>

<div class="row">
	<div class="radio{{ $errors->has('use_tax_required') ? ' has-error' : '' }}">
	   	Use Tax Required &nbsp;&nbsp;&nbsp;&nbsp;
	    <label>
	        {!! Form::radio('use_tax_required', 1,  1) !!} Yes 
	    </label>
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    <label>
	        {!! Form::radio('use_tax_required', 0,  null) !!} No
	    </label>
	    <small class="text-danger">{{ $errors->first('use_tax_required') }}</small>
	</div>
</div>

<div class="row">
	<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }} col-md-8">
	    {!! Form::label('notes', 'Notes') !!}
	    {!! Form::textarea('notes', null, ['class' => 'form-control', 'size' => '20x4']) !!}
	    <small class="text-danger">{{ $errors->first('notes') }}</small>
	</div>
</div>
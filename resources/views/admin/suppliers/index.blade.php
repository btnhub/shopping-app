@extends('layouts.admin_josh.template')
@section('page_title', 'Suppliers')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('suppliers.create')}}" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add A Supplier</a></button>

            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Phone</th>
                        <th>Website</th>
                        <th>Address</th>
                        <th>Notes</th>
                        <th>Date Added</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($suppliers as $supplier)
                    <tr>
                        <td>{{$supplier->id}}</td>
                        <td>{{$supplier->name}}</td>
                        <td>{{$supplier->email}}</td>
                        <td>{{$supplier->phone}}</td>
                        <td>{{$supplier->website}}</td>
                        <td>{{$supplier->address}}</td>
                        <td>{{$supplier->notes}}</td>
                        <td>{{Carbon\Carbon::parse($supplier->created_at)->format('m/d/Y')}}</td>
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['suppliers.destroy', 'id' => $supplier->id], 'class' => 'delete']) !!}
                                <a href="{{route('suppliers.edit', ['id'=> $supplier->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></button>

                                <div class="btn-group pull-right">
                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                </div>
                            
                            {!! Form::close() !!}

                            </td>
                    </tr>
                    @empty
                        <tr><td>No Suppliers.</td></tr>

                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <h3>Suppliers</h3>
     
    
@endsection
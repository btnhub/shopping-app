<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Supplier Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>	

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'Email address') !!}
    {!! Form::email('email', null, ['class' => 'form-control',  'placeholder' => 'eg: foo@bar.com']) !!}
    <small class="text-danger">{{ $errors->first('email') }}</small>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    {!! Form::label('phone', 'Phone Number') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('phone') }}</small>
</div>

<div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
    {!! Form::label('website', 'Supplier Website') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('website') }}</small>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    {!! Form::label('address', 'Supplier Address') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('address') }}</small>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Additional Notes') !!}
    {!! Form::text('notes', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('notes') }}</small>
</div>
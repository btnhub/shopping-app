@extends('layouts.admin_josh.template')
@section('page_title', 'Add A Supplier')
@section('content')
	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<h3>Add A Supplier</h3>
			    {!! Form::open(['method' => 'POST', 'route' => 'suppliers.store', 'class' => 'form-horizontal']) !!}
			    
			        @include('admin.suppliers.form_partial')
			    	
			        <div class="btn-group pull-right">
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
			            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
			        </div>
			    
			    {!! Form::close() !!}
			</div>	
		</div>
	</div>
    
@endsection
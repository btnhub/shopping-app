<table class="table">
    <thead>
        <tr>
            <th>Order Date</th>
            <th>ID / Ref ID</th>
            <th>Customer</th>
            <th>Total</th>
            <th>Shipping Method</th>
            <th>Notes</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
            <tr>
                <td>{{Carbon\Carbon::parse($order->created_at)->format('m/d/Y')}}</td>
                <td><a href="{{route('orders.show', $order->id)}}">{{$order->id}} / {{$order->ref_id}}</a></td>
                <td>{{$order->full_name}}</td>
                <td>${{$order->payment->total_amount}}</td>
                <td>{{$order->shipment ? $order->shipment->servicelevel_name : ""}}</td>
                <td>{{$order->notes}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
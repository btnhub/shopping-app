@extends('layouts.admin_josh.template')
@section('page_title', 'Order Details')
@section('content')
	
<div class="row">

	<div class="col-md-12">
		<h3>Order No: {{$order->id}} / {{$order->ref_id}}
		@if($order->deleted_at)
			<span style="color:red;font-weight: bold">ORDER DELETED!</span>
		@endif

		</h3>
		<h3 style="color:darkred">Order Status: {{ucwords($order->order_status)}}</h3>
		
		<a href="{{route('orders.index')}}"><-- Back</a>
		<br><br>
		@if (!$order->shipped)
			<div class="row">
				<div class="col-md-6">
					{{--Option To Mark --}}
					{!! Form::open(['method' => 'POST', 'route' => ['mark.order.unreviewed', 'id' => $order->id], 'class' => 'form-horizontal']) !!}
			            <div class="pull-left">
			                {!! Form::submit("Mark As Unreviewed", ['class' => 'btn btn-primary']) !!}
			            </div>
			        
			        {!! Form::close() !!}
			        <br><br>
					{{-- <a href="{{route('orders.edit', $order->id)}}" class="btn btn-success">Edit Order (TO DO)</a>		 --}}
				</div>
				
				<div class="col-md-6">

					{!! Form::open(['method' => 'DELETE', 'route' => ['cancel.order', $order->ref_id], 'class' => 'delete']) !!}
					
					    @if ($order->cancelled)
					    	<div class="pull-right">
						        {!! Form::button("Order Cancelled", ['class' => "btn btn-warning disabled"]) !!}
						    </div>
					    @else
						    <div class="pull-right">
						        {!! Form::submit("Cancel Order & Issue Refund", ['class' => "btn btn-warning"]) !!}
						    </div>
					    @endif
					    
					
					{!! Form::close() !!}	
					<br><br>
					
					@if($order->deleted_at)
						<div class="pull-right">
						        {!! Form::button("Order Deleted", ['class' => "btn btn-danger disabled"]) !!}
						    </div>
					@else
						{!! Form::open(['method' => 'DELETE', 'route' => ['orders.destroy', 'id' => $order->id], 'class' => 'delete']) !!}                            
				            <div class="pull-right">
				                {!! Form::submit("Delete Order (No Refund)", ['class' => 'btn btn-danger']) !!}
				            </div>
				        
				        {!! Form::close() !!}	
			        @endif
				</div>
			</div>
		@endif
			
		<hr>
		<div class="row">
			<div class="col-md-4">
				<h4>Customer Info</h4>
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><b>Order Date: </b></td>
							<td>{{Carbon\Carbon::parse($order->created_at)->format('m/d/Y')}} </td>
						</tr>
						<tr>
							<td><b>Name: </b></td>
							<td>{{$order->full_name}} </td>
						</tr>
						<tr>
							<td><b>Email: </b></td>
							<td>{{$order->email}} </td>
						</tr>
						<tr>
							<td><b>Phone: </b></td>
							<td>{{$order->phone}} </td>
						</tr>
						<tr>
							<td><b>Address: </b></td>
							<td>
								{{$order->shipping_address}} <br>
								{{$order->shipping_address_2 ? $order->shipping_address_2.'<br>' : null}}
								{{$order->city}}, {{$order->state}} {{$order->zip}} <br>
								{{$order->country}}
							</td>
						</tr>
						<tr>
							<td><b>Notes: </b></td>
							<td>{{$order->notes}} </td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-4">
				<h4>Payment Info</h4>
				@if ($order->payment)
					<table class="table table-striped">
						<tbody>
							<tr>
								<td>
									<b>ID:</b>
								</td>
								<td>
									<a href="{{route('payments.edit', $order->payment->id)}}" target="_blank">{{$order->payment->id}} / {{$order->payment->ref_id}}</a>

								</td>
							</tr>
							<tr>
								<td><b>Method: </b></td>
								<td>{!! $order->payment->paid ? $order->payment->payment_method() : '<span style="color:red">Unpaid </span>'!!} </td>
							</tr>
							<tr>
								<td><b>Subtotal: </b></td>
								<td>${{$order->payment->amount}} </td>
							</tr>
							@if ($order->payment->discount_amount > 0)
								<tr>
									<td><b>Discount: </b></td>
									<td>${{$order->payment->discount_amount ?? 0}} </td>
								</tr>
							@endif
							<tr>
								<td><b>Sales Tax: </b></td>
								<td>${{$order->payment->sales_tax}}</td>
							</tr>
							<tr>
								<td><b>Shipping Fee: </b></td>
								<td>${{$order->payment->shipping_fee}} </td>
							</tr>
							@if ($order->payment->credit_applied > 0)
								<tr>
									<td><b>Credit Applied</b></td>
									<td>-${{$order->payment->credit_applied}}</td>
								</tr>
							@endif
							<tr>
								<td><b>Total: </b></td>
								<td><b>${{$order->payment->total_amount}}</b></td>
							</tr>
						</tbody>
					</table>
				@else
					No payment details
				@endif
				
			</div>

			<div class="col-md-4">
				<h4>Shipping Info</h4>
				@if ($order->shipment)
					<table class="table table-striped">
						<tbody>
							<tr>
								<td><b>Method:</b></td>
								<td>
								@if ($order->payment->shipping_fee > 0)
									{{$order->shipment->servicelevel_name}} (${{$order->payment->shipping_fee}})
								@else
									Free Shipping
								@endif
									
								</td>
							</tr>
							<tr>
								<td colspan="2"> 
									<a href="{{route('create.packing.slip', $order->id)}}" target="_blank"><center>Click For Packing Slip</center></a>

								</td>
							</tr>
							<tr>
								<td><b>Shipped:</b></td>
								<td>
									@if ($order->shipped)
										<span style="color:green">Yes</span><br>
										<b>Tracking No:</b> {{$order->shipment ? $order->shipment->provider : '' }} {{$order->tracking_no}}
									@else
										<span style="color:red">No</span><br>
										<a data-toggle="collapse" href="#order_{{$order->id}}" aria-expanded="false" aria-controls="order_{{$order->id}}"><b>Add Tracking #</b></a>
									

										<div id="order_{{$order->id}}" class="collapse">
											{!! Form::open(['method' => 'POST', 'route' =>[ 'add.tracking.no', 'id' => $order->id], 'class' => 'form-horizontal']) !!}
										
												<div class="form-group{{ $errors->has('shipping_details') ? ' has-error' : '' }}">
												    {!! Form::label('shipping_details', 'Shipping Details') !!}
												    {!! Form::text('shipping_details', $order->shipment->provider ." ". $order->shipment->servicelevel_name,  ['class' => 'form-control', 'required' => 'required']) !!}
												    <small class="text-danger">{{ $errors->first('shipping_details') }}</small>
												</div>
											    <div class="form-group{{ $errors->has('tracking_no') ? ' has-error' : '' }}">
											        {!! Form::label('tracking_no', 'Tracking Number') !!}
											        {!! Form::text('tracking_no', null, ['class' => 'form-control']) !!}
											        <small class="text-danger">{{ $errors->first('tracking_no') }}</small>
											    </div>
											
											    <div class="btn-group pull-right">
											        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
											    </div>
											
											{!! Form::close() !!}		
										</div>
									@endif

								</td>
							</tr>
							<tr>
								<td><b>Shippo IDs:</b></td>
								<td>
									<b>Rate ID:</b> {{$order->shipment->shippo_rate_id}}<br>
									<b>Shipment ID:</b> {{$order->shipment->shippo_shipment_id}}<br>
									<b>Parcel ID:</b> {{$order->shipment->shippo_parcel_id}}
								</td>
							</tr>
							<tr>
								<td><b>Notes:</b></td>
								<td>{{$order->shipment->notes}}</td>
							</tr>
						</tbody>
					</table>
				@else
					No Shipment Details
				@endif
				
			</div>
		</div>
		
		<hr>
		
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Image</th>
					<th>Name</th>
					<th>Size</th>
					<th>Details</th>
					<th>Quantity</th>
					<th>Unit Price</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>	
				@foreach ($cart_items as $cart_item)
					<tr>
						<td>
							@if ($cart_item->model->product->image)
								<a href="{{route('products.show', $cart_item->model->product->ref_id)}}" target="_blank"><img src='{{ asset("images/products/{$cart_item->model->product->image}") }}' height="75"></a>
							@endif
						</td>
						<td>
							<a href="{{route('products.edit', ['id'=> $cart_item->model->product->id])}}" target="_blank">
								{{$cart_item->name}}
								{{$cart_item->options->organic ? '(Organic)' : ''}}
							</a>
							@if ($cart_item->options->reorder_frequency)
								<br>
								<small><b><em>Reorder Every {{$cart_item->options->reorder_frequency}} Days </em></b></small>
							@endif
						</td>
						<td>{{$cart_item->model->size ?? ''}} {!!$cart_item->options->glass ? '<br>Glass Container' : ''!!}</td>
						<td>{!!$cart_item->model->details()!!}</td>
						<td>{{$cart_item->qty}}</td>
						<td>${{$cart_item->price}}</td>
						<td>${{number_format($cart_item->price * $cart_item->qty, 2)}}</td>
					</tr>
				@endforeach						

			</tbody>
		</table>
	</div>

</div>

@endsection
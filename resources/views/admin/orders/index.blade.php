@extends('layouts.admin_josh.template')

@section('page_title', 'Orders')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Orders</h3>

                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#pending">Pending Orders
                        @if ($pending_orders->count())
                            ({{$pending_orders->count()}})
                        @endif</a>
                    </li>
                  <li><a data-toggle="tab" href="#filled">Filled Orders
                        @if ($filled_orders->count())
                            ({{$filled_orders->count()}})
                        @endif</a>
                    </li>
                  <li><a data-toggle="tab" href="#unpaid">Unpaid Orders
                        @if ($unpaid_orders->count())
                            ({{$unpaid_orders->count()}})
                        @endif</a>
                    </li>
                  <li><a data-toggle="tab" href="#cancelled">Cancelled Orders 
                        @if ($cancelled_orders->count())
                            ({{$cancelled_orders->count()}})
                        @endif</a>
                    </li>
                  <li><a data-toggle="tab" href="#incomplete">Incomplete Orders
                        @if ($incomplete_orders->count())
                            ({{$incomplete_orders->count()}})
                        @endif</a>
                    </li>
                </ul>

                <div class="tab-content">
                  <div id="pending" class="tab-pane fade in active">
                    <h4>Pending Orders</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Order Date</th>
                                <th>ID / Ref ID</th>
                                <th>Customer</th>
                                <th>Total</th>
                                <th>Shipping Method</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pending_orders as $pending_order)
                                <tr @if (!$pending_order->reviewed)
                                    class="warning" style="font-weight:bold"
                                @endif >
                                    <td>{{Carbon\Carbon::parse($pending_order->created_at)->format('m/d/Y')}}</td>
                                    <td><a href="{{route('orders.show', $pending_order->id)}}">{{$pending_order->id}} / {{$pending_order->ref_id}}</a></td>
                                    <td>{{$pending_order->full_name}}</td>
                                    <td>${{$pending_order->payment->total_amount}}</td>
                                    <td>{{$pending_order->shipment ? $pending_order->shipment->servicelevel_name : ''}}</td>
                                    <td>{{$pending_order->notes}}</td>
                                    <td>
                                        @if ($pending_order->reviewed)
                                            {{--Option To Mark Order As Unreviwed--}}
                                            {!! Form::open(['method' => 'POST', 'route' => ['mark.order.unreviewed', 'id' => $pending_order->id], 'class' => 'form-horizontal']) !!}
                                                
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                                                </div>
                                            
                                            {!! Form::close() !!}

                                            {!! Form::open(['method' => 'DELETE', 'route' => ['orders.destroy', 'id' => $pending_order->id], 'class' => 'delete']) !!}                            
                                                <div class="btn-group col-md-offset-3 col-md-5">
                                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                                </div>
                                            
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                  </div>

                  <div id="filled" class="tab-pane fade">
                    <h4>Filled Orders</h4>
                        @include('admin.orders.partials.orders_table', ['orders' => $filled_orders])
                  </div>

                  <div id="unpaid" class="tab-pane fade">
                    <h4>Unpaid Orders</h4>
                        @include('admin.orders.partials.orders_table', ['orders' => $unpaid_orders])
                  </div>

                  <div id="cancelled" class="tab-pane fade">
                    <h4>Cancelled Orders</h4>
                        @include('admin.orders.partials.orders_table', ['orders' => $cancelled_orders])
                  </div>

                  <div id="incomplete" class="tab-pane fade">
                    <h4>Incomplete Orders</h4>
                    Orders which were initated but abandoned by the customer before a payment was created.<br>
                        @include('admin.orders.partials.orders_table', ['orders' => $incomplete_orders])
                  </div>
                </div>                
            </div>
        </div>
    </div>
    
@endsection
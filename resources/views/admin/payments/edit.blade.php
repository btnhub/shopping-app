@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Payment')
@section('content')
<div class="row">
	<div class="col-md-12">
		<a href="{{route('payments.index')}}"><-- Back To All Payments</a>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h3>Payment (#{{$payment->id}} / {{$payment->ref_id}}) From {{$payment->order->full_name}}</h3>
					<table class="table">
						<tbody>
							<tr>
								<td><b>Method</b></td>
								<td>{{$payment->payment_method()}}</td>
							</tr>			
							<tr>
								<td><b>Order ID</b></td>
								<td>

									<a href="{{route('orders.show', $payment->order->id)}}" target="_blank">{{$payment->order->id}} / {{$payment->order->ref_id}}</a>
								</td>
							</tr>	
							<tr>
								<td><b>Transaction ID</b></td>
								<td>{{$payment->transaction_id}}</td>
							</tr>
							<tr>
								<td><b>Subtotal</b></td>
								<td>${{$payment->amount}}</td>
							</tr>	
							@if($payment->discount_amount)
								<tr>
									<td><b>Discount</b></td>
									<td>-${{$payment->discount_amount}}</td>
								</tr>
							@endif
							<tr>
								<td><b>Tax</b></td>
								<td>${{$payment->sales_tax}}</td>
							</tr>	
							<tr>
								<td><b>Shipping</b></td>
								<td>${{$payment->shipping_fee ?? 0}}</td>
							</tr>
							@if($payment->credit_applied)
								<tr>
									<td><b>Credit</b></td>
									<td>-${{$payment->credit_applied ?? 0}}</td>
								</tr>
							@endif
							<tr>
								<td><b>Total</b></td>
								<td>
									${{$payment->total_amount}}
									<br>
									@if ($payment->refunds->count() > 0)
										<span style="color:blue">${{$payment->refunds->sum('refunded_sales') + $payment->refunds->sum('refunded_tax')}} has already been refunded</span>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Paid</b></td>
								<td>{!!$payment->paid ? '<span style="color:green">Yes</span>' : '<span style="color:red">No</span>'!!}</td>
							</tr>
						</tbody>
					</table>
					{!! Form::model($payment, ['method' => 'PUT', 'route' => ['payments.update', $payment->id], 'class' => 'form-horizontal', 'id' => 'update_'.$payment->id]) !!}
					
					    <div class="radio{{ $errors->has('paid') ? ' has-error' : '' }}">
					        <label>
					            {!! Form::radio('paid',1,  null) !!} Payment Received
					        </label>
					        <label>
					            {!! Form::radio('paid', 0,  null) !!} Payment Not Received
					        </label>
					        <small class="text-danger">{{ $errors->first('paid') }}</small>
					    </div>
					
					    <div class="btn-group pull-right">
					        {!! Form::submit("Update", ['class' => 'btn btn-primary']) !!}
					    </div>
					
					{!! Form::close() !!}
				</div>
				<div class="col-md-6">
					<h3>Refund Entire Payment</h3>
					@if ($payment->refunds->count() == 0)
						
					    {!! Form::open(['method' => 'POST', 'route' => 'refunds.store', 'class' => 'form-horizontal', 'id' => 'refund_'.$payment->id]) !!}
					    
					        @include('admin.refunds.form_partial')    	

					        <div class="btn-group pull-right">
					            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
					            {!! Form::submit("Issue Refund", ['class' => 'btn btn-success']) !!}
					        </div>
					    
					    {!! Form::close() !!}	
					@else
						<h4>${{$payment->refunds->sum('refunded_sales') + $payment->refunds->sum('refunded_tax')}} has already been refunded</h4>
					@endif

					
				</div>
			</div>
			
		</div>
		
	</div>
</div>
	

	
@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Payments')
@section('content')
	<div class="row">
		<div class="col-md-12">
			<h3>Payments</h3>
			<h3>Add Method To Handle In Person Payments & Edit Them</h3>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Method</th>
						<th>Order ID</th>
						<th>Customer</th>
						<th>Transaction ID</th>
						<th>Subtotal</th>
						<th>Discount</th>
						<th>Tax</th>
						<th>Shipping</th>
						<th>Total</th>
						<th>Paid</th>
						<th></th>
						<th>Refunded</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($payments as $payment)
						<tr>
							<td>
								@if ($payment->order)
									<a href="{{route('payments.edit', $payment->id)}}">{{$payment->id}} / {{$payment->ref_id}}</a>
								@else
									{{$payment->id}} / {{$payment->ref_id}}
								@endif
									

							</td>
							<td>{{$payment->payment_method()}}</td>
							<td>
							@if ($payment->order)
								<a href="{{route('orders.show', $payment->order->id)}}">{{$payment->order->id}} / {{$payment->order->ref_id}}</a>
							@else
								Order Deleted
							@endif
								
							</td>
							<td>
								@if ($payment->order)
									{{$payment->order->full_name}}
								@else
									Order Deleted
								@endif
								
							</td>
							<td>{{$payment->transaction_id}}</td>
							<td>${{$payment->amount}}</td>
							<td>${{$payment->discount_amount ?? 0}}</td>
							<td>${{$payment->sales_tax}}</td>
							<td>${{$payment->shipping_fee ?? 0}}</td>
							<td>${{$payment->total_amount}}</td>
							<td>{!!$payment->paid ? '<span style="color:green">Yes</span>' : '<span style="color:red">No</span>'!!}</td>
							<td>
								{!! Form::open(['method' => 'DELETE', 'route' => ['payments.destroy', 'id' => $payment->id], 'class' => 'delete']) !!}
			                        @if ($payment->order)
			                        	<a href="{{route('payments.edit', ['id'=> $payment->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
			                        @endif
			                        
			                        
			                        <div class="btn-group pull-right">
			                            {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
			                        </div>
			                    
			                    {!! Form::close() !!}
		                    </td>
		                    <td>{{$payment->refunds->sum('refunded_amount') ? "$".$payment->refunds->sum('refunded_amount') : null}}</td>
						</tr>
					@endforeach
					
				</tbody>
			</table>		
		</div>		
	</div>
	
@endsection

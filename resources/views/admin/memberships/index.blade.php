@extends('layouts.admin_josh.template')

@section('page_title', 'Memberships')

@section('content')	
	<div class="container">

	<a href="{{route('memberships.create')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create A Membership (TO DO)</a>

		<div class="row">
			<h3>Available Memberships</h3>

			<div class="col-md-12">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Membership Name</th>
							<th>Stripe Name</th>
							<th>Description</th>
							<th>Frequency</th>
							<th>Price</th>
							<th>Features</th>
							<th>Website Compare</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($memberships as $membership)
							<tr>
								<td>{{$membership->id}}</td>
								<td>
									<a href="{{route('memberships.show', $membership->id)}}">{{ucfirst($membership->name)}}</a>
								</td>
								<td>{{$membership->stripe_plan}}</td>
								<td>{{$membership->description}}</td>
								<td>{{ucfirst($membership->frequency)}}</td>
								<td>{{$membership->price}}</td>
								<td>{!!$membership->free_shipping ? "Free Shipping":""!!}</td>
								<td>{!!$membership->website_compare ? "<span style='color:green'>Visible</span>" : "Hidden"!!}</td>
								
							</tr>
						@endforeach
						
					</tbody>
				</table>
			</div>
	
		</div>

		<div class="row">
			<h3>Users</h3>
			<?php $first_membership = $memberships->first();?>

			<ul class="nav nav-tabs">
              	@foreach($memberships as $membership)
	              	<li class="{{$membership == $first_membership ? 'active': ''}}"><a data-toggle="tab" href="#{{$membership->name}}"> {{ucfirst($membership->name)}}
	                    @if ($membership->users->count())
	                        ({{$membership->users->count()}})
	                    @endif</a>
	                </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach($memberships as $membership)
	                <div id="{{$membership->name}}" class="tab-pane fade {{$membership == $first_membership ? 'in active': ''}}">
	                    <h4>{{ucfirst($membership->name)}} ${{number_format($membership->price, 2)}}</h4>
	                    @if ($membership->users->count())
							{{$membership->users->count()}} Members
							<table class="table">
								<thead>
									<tr>
										<th>User</th>
										<th>Active</th>
										<th>Stripe Sub ID</th>
										<th>Dates</th>
										<th>Periods</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@forelse ($membership->users as $user)
										<tr>
											<td><a href="{{route('users.show', $user->id)}}">{{$user->full_name}}</a></td>
											<td>{{-- {!!$user->pivot->active() ? "Yes" : "<span style='color:red'>No</span"!!} --}}</td>
											<td>{{$user->pivot->stripe_subscription_id}}</td>
											<td>
												Start Date: {{Carbon\Carbon::parse($user->pivot->start_date)->format('m/d/Y')}}<br>
												End Date: {{Carbon\Carbon::parse($user->pivot->end_date)->format('m/d/Y')}}
											</td>
											<td>
												Period Start: {{Carbon\Carbon::parse($user->pivot->current_period_start)->format('m/d/Y')}}<br>
												Period End: {{Carbon\Carbon::parse($user->pivot->current_period_end)->format('m/d/Y')}}
											</td>
											<td>
												@if (!$membership->website_compare)
													<a href="#" class="btn btn-danger">Remove User (TO DO)</a>
												@endif
												
											</td>
										</tr>
									@empty
										No Results
									@endforelse
									
								</tbody>
							</table>
						@else
							No Users
						@endif
						<hr>
	                </div>
                @endforeach
            </div>
		</div>
	</div>
	

@endsection
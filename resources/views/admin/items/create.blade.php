@extends('layouts.admin_josh.template')
@section('page_title', 'Add An Item')

@section('content')
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<a href="{{route('items.index')}}"><-- Back To All Items</a>
		    <h3>Add An Item</h3>
		    {!! Form::open(['method' => 'POST', 'route' => 'items.store', 'class' => 'form-horizontal', 'files' => true]) !!}
		    
		        @include('admin.items.form_partial')
		    	
		        <div class="btn-group pull-left">
		            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
		            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
		        </div>
		    
		    {!! Form::close() !!}
			
		</div>

	</div>
</div>

@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Items')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <h3>Items</h3>
             
            <a href="{{route('items.create')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add An Item</a>
            <br><br>

            <table class="table">
            	<thead>
            		<tr>
            			<th>ID</th>
            			<th>Image</th>
            			<th>Product Name</th>
            			<th>Size</th>
            			<th>Description</th>
            			<th>Price</th>
            			<th>Dimensions</th>
            			<th>Shipping Weight</th>
            			<th>Recurring Option</th>
            			<th>Notes</th>
            			<th></th>
            		</tr>
            	</thead>
            	<tbody>
            		@foreach ($items as $item)
            			<tr>
	            			<td>{{$item->id}}</td>
	            			<td>
	            				@if ($item->image)
	            					<a href="{{route('items.edit', ['id'=> $item->id])}}"><img src='{{asset("images/products/items/$item->image")}}' height="100"></a>
	            				@endif
	            			</td>
	            			<td>
	            				<a href="{{route('items.edit', ['id'=> $item->id])}}">{{$item->product->name}}</a>
	            			</td>
	            			<td>{{$item->size}}</td>
	            			<td>
	            				{!!$item->color ? 'Color: ' . $item->color. '<br>': ''!!} {!!$item->color ?? ''!!}
	            				{!!$item->scent ? 'Scent: ' . $item->scent. '<br>': ''!!}
	            				{!!$item->flavor ? 'Flavor: ' . $item->flavor. '<br>': ''!!}
	            				{!!$item->color ? 'Addtl Feature: ' . $item->additional_feature. '<br>': ''!!}
	            			</td>
							<td>
								${{$item->price}}<br>

								@if ($item->sale)
									<span style="color:red">On Sale For ${{$item->sale_price}}</span>
								@elseif ($item->surplus)
									<span style="color:red">Surplus For ${{$item->surplus_price}}</span>
								@endif
							</td>	            		
							<td>{{$item->length}} x {{$item->width}} x {{$item->height}} {{$item->distance_unit}} </td>	            			
							<td>{{$item->weight}} {{$item->mass_unit}}</td>	
							<td>{{$item->recurring_option ? 'Yes' : 'Not Available For Recurring Orders'}}</td>
							<td>
								{{$item->notes}}
								{!!$item->discontinued ? "<br><br><span style='color:red'>Discontinued</span>" : ''!!}
								
							</td>	
							<td>
								{!! Form::open(['method' => 'DELETE', 'route' => ['items.destroy', 'id' => $item->id], 'class' => 'delete']) !!}
	                                <a href="{{route('items.edit', ['id'=> $item->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

	                                <div class="btn-group pull-right">
	                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
	                                </div>
	                            
	                            {!! Form::close() !!}

							</td>            		
	            		</tr>
            		@endforeach
            	</tbody>
            </table>
        </div>
    </div>
@endsection
@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Item')

@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-12">
          <a href="{{route('items.index')}}"><-- Back To All Items</a>
          <h3>Edit Item</h3>
          <div class="row">
              @if ($item->image)
                  <img src='{{asset("images/products/items/$item->image")}}' height="220">
              @endif
          </div>

          <div class="row">
            {!! Form::model($item, ['method' => 'PATCH', 'route' => ['items.update', 'id' => $item->id], 'class' => 'form-horizontal', 'files' => true]) !!}
              
                  @include('admin.items.form_partial')

                  <div class="btn-group pull-left">
                      {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                      {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
                  </div>
              
              {!! Form::close() !!}           
          </div>
      </div>
    </div>
</div>
@endsection
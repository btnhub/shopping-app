<div class="row">
    <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }} col-md-6">
        {!! Form::label('product_id', 'Select A Product') !!}
        {!! Form::select('product_id', $products, null, ['id' => 'product_id', 'class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('product_id') }}</small>
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('image_file') ? ' has-error' : '' }} col-md-4">
        {!! Form::label('image_file', 'Image') !!}
        {!! Form::file('image_file') !!}
        <p class="help-block">Please upload a photo (professional) for the item</p>
        <small class="text-danger">{{ $errors->first('image_file') }}</small>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="checkbox{{ $errors->has('recurring_option') ? ' has-error' : '' }} col-md-3">
            <label for="recurring_option">
                {!! Form::checkbox('recurring_option', '1', 1, ['id' => 'recurring_option']) !!} Item Is Available For Recurring Orders
            </label>
        </div>
        <small class="text-danger">{{ $errors->first('recurring_option') }}</small>
    </div>

    <div class="form-group">
        <div class="checkbox{{ $errors->has('discontinued') ? ' has-error' : '' }} col-md-3">
            <label for="discontinued">
                {!! Form::checkbox('discontinued', '1', null, ['id' => 'discontinued']) !!} Discontinued
            </label>
        </div>
        <small class="text-danger">{{ $errors->first('discontinued') }}</small>
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('size') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('size', 'Size (include units)') !!}
        {!! Form::text('size', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('size') }}</small>
    </div>

    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('price', 'Price ($)') !!}
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('price') }}</small>
    </div>    

    <div class="form-group{{ $errors->has('organic_price') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('organic_price', 'Organic Price') !!}
        {!! Form::text('organic_price', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('organic_price') }}</small>
    </div>
</div>

<div class="row">
    <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('color', 'Color') !!}
        {!! Form::text('color', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('color') }}</small>
    </div>

    <div class="form-group{{ $errors->has('scent') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('scent', 'Scent') !!}
        {!! Form::text('scent', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('scent') }}</small>
    </div>

    <div class="form-group{{ $errors->has('flavor') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('flavor', 'Flavor') !!}
        {!! Form::text('flavor', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('flavor') }}</small>
    </div>

    <div class="form-group{{ $errors->has('additional_feature') ? ' has-error' : '' }} col-md-5">
        {!! Form::label('additional_feature', 'Additional Feature') !!}
        {!! Form::text('additional_feature', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('additional_feature') }}</small>
    </div>  

</div>

<div class="row">
    <h4>Item Dimensions</h4>
    <div class="form-group{{ $errors->has('length') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('length', 'Length') !!}
        {!! Form::text('length', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('length') }}</small>
    </div>

    <div class="form-group{{ $errors->has('width') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('width', 'Width') !!}
        {!! Form::text('width', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('width') }}</small>
    </div>

    <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('height', 'Height') !!}
        {!! Form::text('height', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('height') }}</small>
    </div>

    <div class="form-group{{ $errors->has('distance_unit') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('distance_unit', 'Distance Unit') !!}
        {!! Form::text('distance_unit', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('distance_unit') }}</small>
    </div>      
</div>


<div class="row">
    <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }} col-md-3">
        {!! Form::label('weight', 'Weight (including packaging)') !!}
        {!! Form::number('weight', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('weight') }}</small>
    </div>

    <div class="form-group{{ $errors->has('mass_unit') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('mass_unit', 'Mass Unit') !!}
        {!! Form::text('mass_unit', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('mass_unit') }}</small>
    </div>     
</div>

<div class="row">
    <div class="form-group">
        <div class="checkbox{{ $errors->has('sale') ? ' has-error' : '' }} col-md-2">
            <label for="sale">
                {!! Form::checkbox('sale', '1', null, ['id' => 'sale']) !!} On Sale
            </label>
        </div>
        <small class="text-danger">{{ $errors->first('sale') }}</small>
    </div>

    <div class="form-group{{ $errors->has('sale_price') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('sale_price', 'Sale Price ($)') !!}
        {!! Form::number('sale_price', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('sale_price') }}</small>
    </div>   
</div>

<div class="row">
    <div class="form-group">
        <div class="checkbox{{ $errors->has('surplus') ? ' has-error' : '' }} col-md-2">
            <label for="surplus">
                {!! Form::checkbox('surplus', '1', null, ['id' => 'surplus']) !!} On Surplus Sale
            </label>
        </div>
        <small class="text-danger">{{ $errors->first('surplus') }}</small>
    </div>

    <div class="form-group{{ $errors->has('surplus_price') ? ' has-error' : '' }} col-md-2">
        {!! Form::label('surplus_price', 'Surplus Price ($)') !!}
        {!! Form::number('surplus_price', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('surplus_price') }}</small>
    </div>
</div>

<div class="row">
    <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }} col-md-8">
        {!! Form::label('notes', 'Admin Notes (not seen on website)') !!}
        {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('notes') }}</small>
    </div>
</div>


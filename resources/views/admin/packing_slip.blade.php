<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Packing Slip</title>
	
	<style type="text/css">
		* { margin: 0; padding: 0; }
		body { font: 14px/1.4 Georgia, serif; }
		/*#page-wrap { width: 800px; margin: 10px auto;}*/
		#page-wrap { width: 700px; margin: 10px 50px 10px 50px;}

		textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
		table { border-collapse: collapse; }
		table td, table th { border: 1px solid black; padding: 5px; }

		#header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

		#address { width: 250px; height: 150px; float: left; }
		/*#customer { overflow: hidden; }*/

		#logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; max-height: 100px; overflow: hidden; }
		#customer-title {float: left; }

		#meta { margin-top: 1px; width: 300px; float: right; }
		#meta td { text-align: right; border: 0px;  }
		#meta td.meta-head { text-align: left; background: #eee; }
		#meta td textarea { width: 100%; height: 20px; text-align: right; }

		#items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
		#items th { background: #eee; }
		#items td{ border: 1px solid black; padding: 5px 10px 5px 10px; }
		#items tr.item-row td { border: 0; border-bottom: 1px solid gray; vertical-align: top; }
		#items td.description { width: 300px; }
		#items td.item-name { width: 75px;}
		#items td.quantity { width: 50px; }
		#items td.price { width: 50px; }
		#items td.total-cost { width: 50px; }
		#items td.total-line { border-right: 0; text-align: right; }
		#items td.total-value { border-left: 0; padding: 10px; }
		#items td.balance { background: #eee; }
		#items td.blank { border: 0; }
	</style>
</head><body>

	<div id="page-wrap">

		<div id="header">PACKING SLIP</div>
		
		<div id="identity">
		
            <div id="address" style="font-size: 16px">
				{{$company_name}}<br>
				{!!nl2br($company_address)!!}<br>
				{{$company_email}}<br>
			</div>
			{{-- 
            <div id="logo">
              <img id="image" src='{{asset("images/logo/$company_logo")}}' height="75"/>
            </div> 
            --}}
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">

            <div id="customer-title">
            	<h3>Ship To:</h3>
				{{$order->first_name}} {{$order->last_name}}<br>
				{{$order->shipping_address}}<br>
				@if ($order->shipping_address_2)
					{{$order->shipping_address_2}} <br>
				@endif
				{{$order->city}}, {{$order->state}} {{$order->zip}}, {{$order->country}}<br>
				{{$order->email}}<br>
				{{$order->phone}}<br><br>
			</div>

            <table id="meta">
                <tr>
                    <td class="meta-head">Order #</td>
                    <td>{{$order->ref_id}}</td>
                </tr>
                <tr>

                    <td class="meta-head">Order Date</td>
                    <td>{{$order->created_at->format('m/d/Y')}}</td>
                </tr>
                <tr>
                    <td class="meta-head">Shipping Method</td>
                    <td><textarea>{{$order->shipment->provider}} {{$order->shipment->servicelevel_name}}</textarea></td>
                </tr>

            </table>
		
		</div>
		
		<table id="items">
		
		  <tr>
		      <th>Item ID </th>
		      <th>Description</th>
		      <th>Price</th>
		      <th>Quantity</th>
		      <th>Total</th>
		  </tr>
		 @foreach ($cart_items as $cart_item)
		  	<tr class="item-row">
		      	<td class="item-name">{{$cart_item->model->product->ref_id}}</td>
		      	<td class="description">
		      		{{$cart_item->name}} {{$cart_item->options->organic ? '(Organic)' : ''}}<br>
		      		{!!$cart_item->model->details()!!}<br>

		      		<small>{{$cart_item->model->size ?? ''}} {!!$cart_item->options->glass ? 'Glass Container' : ''!!}</small>
		      	</td>
		      	<td class="price"><center>${{number_format($cart_item->price, 2)}}</center></td>
		      	<td class="quantity"><center>{{$cart_item->qty}}</center></td>
		      	<td class="total-cost"><center>${{number_format($cart_item->price * $cart_item->qty, 2)}}</center></td>
		 	 </tr>	
		  @endforeach 
		  
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Subtotal</td>
		      <td class="total-value"><div id="subtotal"><center>${{$order->payment->amount}}</center></div></td>
		  </tr>
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Shipping</td>
		      <td class="total-value"><div id="total"><center>${{$order->payment->shipping_fee}}</center></div></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Tax</td>

		      <td class="total-value"><center>${{$order->payment->sales_tax}}</center></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance">Total</td>
		      <td class="total-value balance"><div class="due"><center>${{$order->payment->total_amount}}</center></div></td>
		  </tr>
		
		</table>
		@if($order->payment->sales_tax <=0)
			<br>
			<small>* - We do not collect sales tax in your state, however, this sale is not exempt from sales or use tax.  Your state may require you to pay sales or use tax on this order.</small>
		@endif
	</div>
</body></html>
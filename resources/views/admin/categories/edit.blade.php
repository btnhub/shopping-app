@extends('layouts.admin_josh.template')
@section('page_title', 'Edit Category')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Edit Category</h3>
		    {!! Form::model($category, ['method' => 'PATCH', 'route' => ['categories.update', 'id' => $category->id], 'class' => 'form-horizontal']) !!}
		    
		        @include('admin.categories.form_partial')       

		        <div class="btn-group pull-right">
		            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
		            {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
		        </div>
		    
		    {!! Form::close() !!}		
		</div>
	</div>
</div>
    
@endsection
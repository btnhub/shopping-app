@extends('layouts.admin_josh.template')
@section('page_title', 'Add A Category')
@section('content')
	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<h3>Add A Category</h3>
			    {!! Form::open(['method' => 'POST', 'route' => 'categories.store', 'class' => 'form-horizontal']) !!}
			    
			        @include('admin.categories.form_partial')    	

			        <div class="btn-group pull-right">
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
			            {!! Form::submit("Add Category", ['class' => 'btn btn-success']) !!}
			        </div>
			    
			    {!! Form::close() !!}			
			</div>
		</div>
	</div>
@endsection
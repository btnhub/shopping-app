@extends('layouts.admin_josh.template')
@section('page_title', 'Categories')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Categories</h3>
                <a href="{{route('categories.create')}}" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add A Category</a></button>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Subcategories</th>
                            <th>Website Order</th>
                            {{-- <th>Date Added</th> --}}
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($categories->sortBy('website_order') as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>
                                @if ($category->image)
                                    <img src='{{ asset("images/categories/$category->image") }}' height='50'>
                                @endif
                            </td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->description}}</td>
                            <td>{{$category->subcategories->count()}}</td>
                            <td>{{$category->website_order}}</td>
                            {{-- <td>{{Carbon\Carbon::parse($category->created_at)->format('m/d/Y')}}</td> --}}
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', 'id' => $category->id], 'class' => 'delete']) !!}
                                    <a href="{{route('categories.edit', ['id'=> $category->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                    
                                    <div class="btn-group pull-right">
                                        {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                    </div>
                                
                                {!! Form::close() !!}

                                </td>
                        </tr>
                        @empty
                            <tr><td>No categories.</td></tr>

                        @endforelse
                    </tbody>
                </table>
            </div>
    
        </div>
    </div>
    
@endsection
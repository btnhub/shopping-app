<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Category Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>	

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>

<div class="form-group{{ $errors->has('website_order') ? ' has-error' : '' }}">
    {!! Form::label('website_order', 'Website Order') !!}
    {!! Form::text('website_order', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('website_order') }}</small>
</div>

<div class="form-group{{ $errors->has('website_description') ? ' has-error' : '' }}">
    {!! Form::label('website_description', 'Short Description Seen On Website') !!}
    {!! Form::text('website_description', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('website_description') }}</small>
</div>

<div class="form-group{{ $errors->has('detailed_description') ? ' has-error' : '' }}">
    {!! Form::label('detailed_description', 'Detailed Description Of Items In This Category') !!}
    {!! Form::textarea('detailed_description', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('detailed_description') }}</small>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'Image') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('image') }}</small>
</div>

<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Admin Notes (Not seen on website)') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('notes') }}</small>
</div>
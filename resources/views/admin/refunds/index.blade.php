@extends('layouts.admin_josh.template')
@section('page_title', 'Refunds')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h3>Refunds</h3>

            {{--<a href="{{route('refunds.create')}}" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-plus" aria-hidden="true">--}}
            <a href="#" class="btn btn-primary pull-left"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create A Refund</a></button>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>ID</th>
                        <th>Ref ID</th>
                        <th>User</th>
                        <th>Order</th>
                        <th>Stripe Refund ID</th>
                        <th>Refund Reason</th>
                        <th>Refund Method</th>
                        <th>Requested Amount</th>
                        <th>Deductions</th>
                        <th>Refund Amount</th>
                        <th>Processed Date</th>
                        <th>Notes</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($refunds as $refund)
                    <tr>
                        <td>{{Carbon\Carbon::parse($refund->created_at)->format('m/d/Y')}}</td>
                        <td>{{$refund->id}}</td>
                        <td>{{$refund->ref_id}}</td>
                        <td>
                           {{$refund->user->full_name}}
                        </td>
                        <td>{{$refund->order->id}} / {{$refund->order->ref_id}}</td>
                        <td>{{$refund->stripe_refund_id}}</td>
                        <td>
                            {{$refund->refund_reason}}
                            <br>
                            {{$refund->reason_details}}
                        </td>
                        <td>{{$refund->method}}</td>
                        <td>${{$refund->requested_amount ?? 0}}</td>
                        <td>${{$refund->deductions ?? 0}}</td>
                        <td>${{$refund_amount ?? 0}}</td>
                        <td>{{Carbon\Carbon::parse($refund->processed_date)->format('m/d/Y')}}</td>
                        <td>{{$refund->notes}}</td>
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['refunds.destroy', 'id' => $refund->id], 'class' => 'delete']) !!}
                                <a href="{{route('refunds.edit', ['id'=> $refund->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                
                                {{--
                                <div class="btn-group pull-right">
                                    {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
                                </div>
                                --}}
                            {!! Form::close() !!}

                            </td>
                    </tr>
                    @empty
                        <tr><td>No refunds.</td></tr>

                    @endforelse
                </tbody>
            </table>            
        </div>

    </div>
</div>
    
@endsection
{!! Form::hidden('user_id', $payment->user_id) !!}
{!! Form::hidden('payment_id', $payment->id) !!}
{!! Form::hidden('order_id', $payment->order_id) !!}

<div class="form-group{{ $errors->has('refund_reason') ? ' has-error' : '' }}">
    {!! Form::label('refund_reason', 'Refund Reason') !!}
    {!! Form::text('refund_reason', null, ['class' => 'form-control', 'placeholder' => 'Brief Explanation Of Refund Reason']) !!}
    <small class="text-danger">{{ $errors->first('refund_reason') }}</small>
</div>
<div class="form-group{{ $errors->has('reason_details') ? ' has-error' : '' }}">
    {!! Form::label('reason_details', 'Refund Reason Details (Optional)') !!}
    {!! Form::textarea('reason_details', null, ['class' => 'form-control', 'size' => '30x2']) !!}
    <small class="text-danger">{{ $errors->first('reason_details') }}</small>
</div>	

<?php $payment_methods = DB::table('payment_methods')->pluck('title', 'id')->toArray(); ?>

<div class="radio{{ $errors->has('refund_method') ? ' has-error' : '' }}">
    <b>Refund Method (Recommended: Refund Using The Same Method The Payment Was Made)</b>
    @foreach ($payment_methods as $id => $title)
        <label>
        {!! Form::radio('refund_method',$id,  $payment->payment_method_id == $id ? 1 : null) !!} {{$title}}
    </label>
    @endforeach
    
    <small class="text-danger">{{ $errors->first('refund_method') }}</small>
</div>
<div class="form-group">
    <div class="checkbox{{ $errors->has('refund_shipping') ? ' has-error' : '' }}">
        <label for="refund_shipping">
            {!! Form::checkbox('refund_shipping', '1', null, ['id' => 'refund_shipping']) !!} Refund Shipping Cost (${{$payment->shipping_fee}})
        </label>
    </div>
    <small class="text-danger">{{ $errors->first('refund_shipping') }}</small>
</div>
<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Additional Notes') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control', 'size' => '30x2']) !!}
    <small class="text-danger">{{ $errors->first('notes') }}</small>
</div>

@extends('layouts.admin_josh.template')
@section('page_title', 'Create A Refund')
@section('content')
	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<h3>Create A Refund</h3>
			    {!! Form::open(['method' => 'POST', 'route' => 'refunds.store', 'class' => 'form-horizontal']) !!}
			    
			        @include('admin.refunds.form_partial')    	

			        <div class="btn-group pull-right">
			            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
			            {!! Form::submit("Create Refund", ['class' => 'btn btn-success']) !!}
			        </div>
			    
			    {!! Form::close() !!}			
			</div>
		</div>
	</div>
@endsection
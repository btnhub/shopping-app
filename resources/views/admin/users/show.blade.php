@extends('layouts.admin_josh.template')

@section('page_title', "User Details")

@section('content')
<div class="container">
	<div class="row">
		<h3>{{$user->full_name}}</h3>
		<div class="col-md-6">
			<table class="table table-striped">
				<tbody>
					<tr>
						<td><b>Name:</b> </td>
						<td>{{$user->full_name}} </td>
					</tr>
					<tr>
						<td><b>Email:</b> </td>
						<td>{{$user->email}} </td>
					</tr>
					<tr>
						<td><b>Phone:</b> </td>
						<td>{{$user->phone}} </td>
					</tr>
					<tr>
						<td><b>Last Login:</b> </td>
						<td>{{$user->last_login_date->diffForHumans()}} </td>
					</tr>
					<tr>
						<td><b>Membership:</b></td>
						<td>
							@if ($user->memberships->count())
								{{ucwords($user->memberships->implode('name', ', '))}} 
								<br><br>
								TODO!!!
								DELETE MEMBERSHIP BUTTON
							@endif

							
							{!! Form::open(['method' => 'POST', 'route' => ['add.user.membership', 'user_id' => $user->id]]) !!}
						
							    <div class="form-group{{ $errors->has('membership_id') ? ' has-error' : '' }}">
							        {!! Form::label('membership_id', 'Add A Hidden Membership') !!}
							        {!! Form::select('membership_id',$memberships, null, ['id' => 'membership_id', 'class' => 'form-control', 'required' => 'required']) !!}
							        <small class="text-danger">{{ $errors->first('membership_id') }}</small>
							    </div>
							
							    <div class="btn-group pull-right">
							        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
							    </div>
							
							{!! Form::close() !!}
							
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<a class="btn btn-warning" href="{{ route('clone.user', ['user_id' => $user->id])}}" target="_blank">Log In As {{$user->full_name}}</a>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
		            <li class="active"><a data-toggle="tab" href="#Orders">Orders
		            		@if($user->orders->count())
			            		({{$user->orders->count()}})
			            	@endif
		                </a>
		            </li>
		            <li><a data-toggle="tab" href="#memberships">Memberships
		            		@if($user->membership_payments->count())
			            		({{$user->membership_payments->count()}})
			            	@endif
		                </a>
		            </li>
		            <li><a data-toggle="tab" href="#credits">Credits
		            	@if($user->credits->count())
		            		({{$user->credits->count()}})
		            	@endif
		          		</a>      
		            </li>
		            <li><a data-toggle="tab" href="#payments">Payments
		            		@if($user->payments->count())
		            			({{$user->payments->count()}})
		            		@endif
		                </a>
		            </li>
		        </ul>
	        	<div class="tab-content">
	                <div id="orders" class="tab-pane fade in active">
	                	<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Payment ID</th>
									<th>Status Details</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($user->orders as $order)
								<tr>
									<td>
										<a href="{{route('orders.show', $order->id)}}" target="_blank">{{$order->id}} / {{$order->ref_id}}</a> 
									</td>
									<td>
										@if (count($order->payment))
											<a href="{{route('payments.show', $order->payment->id)}}" target="_blank">{{$order->payment->id}} / {{$order->payment->ref_id}}</a>
										@else
											No Payment
										@endif
										
									</td>
									<td>
										Status: {{ucfirst($order->order_status)}}<br>
										@if ($order->cancelled)
											<span style="color:red"><b>Cancelled</b></span><br>
										@endif
										@if ($order->shipped)
											<span style="color:darkgreen">Shipped on: {{$order->shipping_date}}</span><br>
										@else
											<span style="color:darkred">Not Shipped</span><br>
										@endif
									</td>
								</tr>
								@empty
								<tr>
									<td><center>No Orders</center></td>
								</tr>
								@endforelse
							</tbody>
						</table>
	                </div>
	                <div id="memberships" class="tab-pane fade">
	                    <h4>Membership Payments</h4>
						<table class="table">
							<thead>
								<tr>
									<th>ID / Ref ID</th>
									<th>Transaction ID</th>
									<th>Membership</th>
									<th>Amount</th>
									<th>Paid</th>
									<th>Refunded</th>
									<th>Notes</th>
									<th>Delete Record</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($user->membership_payments as $membership_payment)
								<tr>
									<td>{{$membership_payment->id}} / {{$membership_payment->ref_id}}</td>
									<td>{{$membership_payment->transaction_id}}</td>
									<td>{{ucwords($membership_payment->membership->name)}}</td>
									<td>${{$membership_payment->amount}}</td>
									<td>{{$membership_payment->paid ? $membership_payment->completed_at : "Unpaid"}}</td>
									<td>
										@if ($membership_payment->refunded)
											Refunded
										@elseif($membership_payment->created_at > Carbon\Carbon::now()->subWeek())
											<a href="#" class="btn btn-danger">Refund Payment (TO DO)</a>
										@else
											Ineligible*
										@endif
										
									</td>
									<td>{{$membership_payment->notes}}</td>
									<td></td>
								</tr>
								@empty
								<tr>
									<td><center>No Payments</center></td>
								</tr>
								@endforelse
							</tbody>
						</table>
						* Cannot refund, payment is more than 1 week old			
	                
	                </div> 
	                <div id="payments" class="tab-pane fade">
	                	NEEDS WORK
	                	<table class="table">
							<thead>
								<tr>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
								</tr>
							</tbody>
						</table>
	                </div> 
	                <div id="credits" class="tab-pane fade">
	                    <p>Credits & Gift Cards</p>
	                	<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Amount</th>
									<th>Provider</th>
									<th>Expires</th>
								</tr>
							</thead>
							<tbody>
								@foreach($user->credits->sortByDesc('created_at') as $credit)
									<tr>
										<td>{{$credit->id}}</td>
										<td>${{$credit->amount}}
											@if($credit->balance() != $credit->amount)
												<br>
												<small>${{$credit->balance()}} Left</small>
											@endif
										</td>
										<td>
											@if($credit->provider_id)
						                		<a href="{{route('users.show', $credit->provider_id)}}" target="_blank">{{$credit->provider_name}}</a>
						                	@else
						                		{{$credit->provider_name}}
						                		<br>
						                		<small>{{$credit->recipient_email}}</small>
						                	@endif
						                </td>

										<td>{{$credit->expiration_date->format('m/d/Y')}}</td>
										<td>
							                {!! Form::open(['method' => 'DELETE', 'route' => ['credits.destroy', 'id' => $credit->id], 'class' => 'delete']) !!}
							                    <a href="{{route('credits.show', ['id'=> $credit->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

							                    <div class="btn-group pull-right">
							                        {!! Form::submit("X", ['class' => 'btn btn-danger']) !!}
							                    </div>
							                {!! Form::close() !!}

							            </td>
									</tr>
								@endforeach
							</tbody>
						</table>
	                </div>   
	            </div>
	        </div>		
		</div>
	</div>
</div>
	
	
@endsection
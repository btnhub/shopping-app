@extends('layouts.admin_josh.template')

@section('page_title', 'User Manager')

@section('content')	
	<div class="container">
		<div class="row">
			<h4>Users With Accounts</h4>

			<div class="col-md-12">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Last Login</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
							<tr>
								<td>{{$user->id}}</td>
								<td>
									<a href="{{route('users.show', $user->id)}}">{{$user->full_name}}</a>
								</td>
								<td>{{$user->email}}</td>
								<td>{{$user->phone}}</td>
								<td>{{Carbon\Carbon::parse($user->last_login_date)->format('m/d/Y')}}</td>
							</tr>
						@endforeach
						
					</tbody>
				</table>

				<h4>Purchases Without Accounts</h4>
				(option to delete orders without payment, i.e. user abandoned cart before completing checkout process)
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Order</th>
							<th>Order Date</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($no_account_orders as $order)
							<tr>
								<td>
									{{$order->full_name}}
								</td>
								<td>{{$order->email}}</td>
								<td>{{$order->phone}}</td>
								<td><td><a href="{{route('orders.show', $order->id)}}">{{$order->id}} / {{$order->ref_id}}</a></td>
								<td>{{Carbon\Carbon::parse($order->created_at)->format('m/d/Y')}}</td>
								<td>
									@if (!$order->payment)
										{!! Form::open(['method' => 'DELETE', 'route' => ['orders.destroy', 'id' => $order->id], 'class' => 'delete']) !!}
					                        <div class="btn-group pull-right">
					                            {!! Form::submit("Delete Order", ['class' => 'btn btn-danger']) !!}
					                        </div>
					                    {!! Form::close() !!}
									@endif
									
								</td>
							</tr>
						@endforeach
						
					</tbody>
				</table>
			</div>
	
		</div>
	</div>
	
@endsection
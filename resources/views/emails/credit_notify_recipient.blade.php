@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			@if($credit->message)
				{{$credit->message}}
				<br><br>
			@else
				A gift from us to you! Enjoy this ${{$credit->amount}} Gift Card.
			@endif

			<b>How To Redeem Your Gift Card</b><br>

			<li>Visit <a href="{{url('/')}}" target="_blank">Mountain Myst</a> and shop the best natural, handmade skin and body care products.</li>  
			<li>When you check out, use the code <b>{{$credit->credit_code}}</b> .  That's it!</li>

			<p>If you already have Mountain Myst Account, redeem this gift card at any time by <a href="{{url('/login')}}" target="_blank">logging into your account</a> and clicking on the Credits & Gift Cards link.</p>
			<br>
			<b style="color:darkred">This code expires {{$credit->expiration_date->format('m/d/Y')}}.</b>

			<br><br>

			Enjoy!
		</div>
	</div>
@endsection
@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			<b>{{$user->first_name}},</b>
			<br>
			Thank you for purchasing a <b>Mountain Myst Membership</b>!  Your purchase details are below.  You're membership discount will automatically be added to your orders when you checkout.  Be sure to log into your account <b>before</b> placing an order.  We cannot retroactively add discounts to orders once they have been placed.  Your card will automatically be charged the membership fee every {{$membership->days}} days.
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table">
					<tbody>
						<tr>
							<td><b>Plan:</b></td>
							<td>
								{{ucwords($membership->name)}} {{$membership->discount * 100}}% Off
								@if ($membership->free_shipping)
									+ Free Shipping
								@endif
							</td>
						</tr>
						<tr>
							<td><b>Price: </b></td>
							<td>${{$membership->price}}</td>
						</tr>
						<tr>
							<td><b>Frequency:</b></td>
							<td>{{ucwords($membership->frequency)}} (Every {{$membership->days}} Days)</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
@endsection
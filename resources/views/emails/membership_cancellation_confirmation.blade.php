@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			<b>{{$user->first_name}},</b>
			<br>
			Your Mountain Myst Membership has been cancelled.  You will still enjoy the benefits of membership until the end date listed below.
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table">
					<tbody>
						<tr>
							<td><b>End Date:</b></td>
							<td>
								{{Carbon\Carbon::parse($user->best_membership()->pivot->end_date)->format('m/d/Y')}}
							</td>
						</tr>
						<tr>
							<td><b>Plan:</b></td>
							<td>
								{{ucwords($membership->name)}} {{$membership->discount * 100}}% Off
								@if ($membership->free_shipping)
									+ Free Shipping
								@endif
							</td>
						</tr>
						<tr>
							<td><b>Price: </b></td>
							<td>${{$membership->price}}</td>
						</tr>
						<tr>
							<td><b>Frequency:</b></td>
							<td>{{ucwords($membership->frequency)}} (Every {{$membership->days}} Days)</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
@endsection
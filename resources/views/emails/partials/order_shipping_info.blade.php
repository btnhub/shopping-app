<div class="col-md-6">
	<table class="table striped">
		<tbody>
			<tr>
				<td><b>Order ID</b></td>
				<td>{{$order->ref_id}}</td>
			</tr>			
			<tr>
				<td><b>Subtotal</b></td>
				<td>${{number_format($order->payment->amount,2)}}</td>
			</tr>
			@if ($order->payment->discount_amount > 0)
				<tr>
					<td><b>Discounts</b></td>
					<td>${{number_format($order->payment->discount_amount ?? 0, 2)}}</td>
				</tr>
			@endif
			<tr>
				<td><b>Shipping & Handling</b></td>
				<td>${{number_format($order->payment->shipping_fee ?? 0, 2)}}</td>
			</tr>
			<tr>
				<td><b>Tax </b></td>
				<td>${{number_format($order->payment->sales_tax, 2)}}</td>
			</tr>
			@if ($order->payment->credit_applied > 0)
				<tr>
					<td><b>Credit Applied</b></td>
					<td>-${{number_format($order->payment->credit_applied, 2)}}</td>
				</tr>
			@endif
			<tr>
				<td><b>Order Total</b></td>
				<td><b>${{number_format($order->payment->total_amount, 2)}}</b></td>
			</tr>
		</tbody>
	</table>
</div>
<br><br>

<div class="col-md-6">
	<b>Shipping Information</b><br>
	{{$order->full_name}}<br>
	{{$order->shipping_address}}<br>
	@if ($order->shipping_address_2)
		{{$order->shipping_address_2}}<br>
	@endif
	{{$order->city}}, {{$order->state}} {{$order->zip}}<br>
	{{$order->country}}
	
	<br><br>
	<b>Shipping Method:</b> <br>
	{{$order->shipment->provider}} - {{$order->shipment->servicelevel_name}}
</div>
<table class="table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Feature</th>
			<th>Quantity</th>
			<th>Unit Price</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		@foreach($cart_items as $cart_item)
		<tr>
			<td>
				{{$cart_item->name}} 
				@if ($cart_item->options->organic)
					(Organic)
				@endif
				<br>
					<small>
							{{$cart_item->model->size ?? ''}} 
							{!!$cart_item->options->glass ? 'Glass Container' : ''!!}
					</small>
				
				@if ($cart_item->options->reorder_frequency)
					<small><b><em>Reorder Every {{$cart_item->options->reorder_frequency}} Days </em></b></small>
				@endif
			</td>
			<td>{!!$cart_item->model->details()!!}</td>
			<td><center>{{$cart_item->qty}}</center></td>
			<td>${{number_format($cart_item->price, 2)}}</td>
			<td>${{number_format($cart_item->price * $cart_item->qty, 2)}}</td>
		</tr>
		@endforeach
	</tbody>
</table>
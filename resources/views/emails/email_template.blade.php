<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" media="screen" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	<body>
		<table class="table" align="center">
			<tbody>
				<tr>
					<td>
						@yield('msg_body')
					</td>
				</tr>
				<tr>
					<td>
						<br><br>
						<center><a href="{{url('/')}}">
							<img src="{{asset('images/logo/mountain_myst_logo.png')}}" alt="" height="100" />
						</a></center>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>


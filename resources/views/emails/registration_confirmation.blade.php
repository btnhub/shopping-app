@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
		<b>{{$user->first_name}},</b><br>
		Thank you for creating a Mountain Myst account.  Log in to your account to view or edit recent orders, subscription plans and memberships.

		</div>
	</div>
@endsection
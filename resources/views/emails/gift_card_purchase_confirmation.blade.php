@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			Thank you purchasing a <b>Mountain Myst</b> E-Gift card!  Your purchase details are below.  We've e-mailed a gift card redemption code to {{$credit->recipient_name}}.
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table">
					<tbody>
						<tr>
							<td><b>Recipient's Name:</b></td>
							<td>{{$credit->recipient_name}}</td>
						</tr>
						<tr>
							<td><b>Recipient's Email:</b></td>
							<td>{{$credit->recipient_email}}</td>
						</tr>
						<tr>
							<td><b>Amount: </b></td>
							<td>${{$credit->amount}}</td>
						</tr>
						<tr>
							<td><b>Message: </b></td>
							<td>{{$credit->message}}</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
@endsection
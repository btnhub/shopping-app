@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			Good news, you just received a Mountain Myst E-Gift Card from {{$credit->provider_name}}! <br><br>

			<b>How To Redeem Your Gift Card</b><br>

			<li>Visit <a href="{{url('/')}}" target="_blank">Mountain Myst</a> and find the best natural, handmade skin and body care products.</li>  
			<li>When you check out, include the Gift Card Code provided below.  That's it!</li>
			<p>If you already have Mountain Myst Account, redeem this gift card at any time by <a href="{{url('/login')}}" target="_blank">logging into your account</a> and clicking on the Credits & Gift Cards link.</p>
		</div>

		<br>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table">
					<tbody>
						<tr>
							<td><b>Sender's Name:</b></td>
							<td>{{$credit->provider_name}}</td>
						</tr>
						<tr>
							<td><b>Senders's Email:</b></td>
							<td>{{$credit->provider_email}}</td>
						</tr>
						<tr>
							<td><b>Amount: </b></td>
							<td>${{$credit->amount}}</td>
						</tr>
						<tr>
							<td><b>Gift Card Code: </b></td>
							<td>{{$credit->credit_code}}</td>
						</tr>
						<tr>
							<td><b>Message: </b></td>
							<td>{{$credit->message}}</td>
						</tr>
						<tr>
							<td><b>Expiration Date: </b></td>
							<td>{{$credit->expiration_date->format('m/d/Y')}}</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
@endsection
@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			{{$order->first_name}},
			<br>

			Your recent order is on the way!  Your tracking number is {{$order->shipment->provider}} #{{$order->tracking_no}}.<br><br>
		</div>
		<div class="row">
			@include('emails.partials.order_shipping_info')
		</div>
		<br><br>
		
		<div class="row">
			<div class="col-md-8">
				<h3>Order Details</h3>
				@include('emails.partials.order_details')
			</div>
		</div>
	</div>
@endsection
@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			A new order, <a href="{{route('orders.show', $order->id)}}" target="_blank"> {{$order->ref_id}}</a>, has been created.  Please review and fill this order.  If there is a problem or delay with the order, please notify the customer immediately.<br><br>

			@if ($order->skip_grace_period)
				The customer has selected to <b><span style="color:darkred">skip the 24 hour waiting period</span></b>. <br><br>
			@endif
		</div>
		
		<div class="row">
			<div class="col-md-6">
				@include('emails.partials.order_shipping_info')
		</div>

		<br><br>
		<div class="row">
			<div class="col-md-8">
				<h3>Order Details</h3>
				@include('emails.partials.order_details')
			</div>
		</div>
	</div>

@endsection
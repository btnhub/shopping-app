@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			{{$order->first_name}},
			<br>

			Thank you for your order.

			{{-- @if ($order->skip_grace_period)
				Since you selected to skip the refund grace period, we'll get to work on it immediately.  Please note that you cannot cancel this order and receive a refund.  
			@else 
				You will have 24 hours to cancel this order and receive a full refund.  Simply log into your Mountain Myst account and cancel the order.  If you do not have a Mountain Myst account, e-mail us immediately (include the order number) and we will cancel the order and refund your payment.  
			@endif --}}
			
			{{-- <br><br> --}}

			Your order details are below:
		</div>
		<br><br>  

		<div class="row">
			@include('emails.partials.order_shipping_info')
		</div>
		<br><br>  
		
		<div class="row">
			<div class="col-md-8">
				@include('emails.partials.order_details')
			</div>
		</div>
	</div>
@endsection
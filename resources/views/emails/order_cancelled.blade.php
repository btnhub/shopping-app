@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			{{$order->first_name}},
			<br>

			Your recent order (#{{$order->ref_id}}) has just been <b>cancelled </b> and a refund will be issued shortly.  If this order was cancelled in error, please contact us immediately.<br><br>

		</div>
		<div class="row">
			@include('emails.partials.order_shipping_info')
		</div>
		
		<div class="row">
			<div class="col-md-8">
				<h3>Order Details</h3>
				@include('emails.partials.order_details')
			</div>
		</div>
	</div>
@endsection
@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table">
					<tbody>
						<tr>
							<td><b>Stripe ID:</b></td>
							<td>{{$credit->transaction_id}}</td>
						</tr>
						<tr>
							<td><b>Sender's Name:</b></td>
							<td>{{$credit->provider_name}}</td>
						</tr>
						<tr>
							<td><b>Senders's Email:</b></td>
							<td>{{$credit->provider_email}}</td>
						</tr>
						<tr>
							<td><b>Recipient's Name:</b></td>
							<td>{{$credit->recipient_name}}</td>
						</tr>
						<tr>
							<td><b>Recipient's Email:</b></td>
							<td>{{$credit->recipient_email}}</td>
						</tr>
						<tr>
							<td><b>Amount: </b></td>
							<td>${{$credit->amount}}</td>
						</tr>
						<tr>
							<td><b>Gift Card Code: </b></td>
							<td>{{$credit->credit_code}}</td>
						</tr>
						<tr>
							<td><b>Expiration Date: </b></td>
							<td>{{$credit->expiration_date->format('m/d/Y')}}</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
@endsection
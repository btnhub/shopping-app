@extends('emails.email_template')

@section('msg_body')
	<div class="container">
		<div class="row">
			<b>{{$user->first_name}},</b>
			<br>
			Unfortunately, your recent payment to renew your Mountain Myst membership was declined.  Please <a href="{{route('edit.card', $user->ref_id)}}">update your card information</a> or contact your bank in case they blocked the payment.
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table">
					<tbody>
						<tr>
							<td><b>Card:</b></td>
							<td>
								{{$user->customer->card_brand}} xxxx-{{$user->customer->card_last_four}}
							</td>
						</tr>
						<tr>
							<td><b>Plan:</b></td>
							<td>
								{{ucwords($membership->name)}} {{$membership->discount * 100}}% Off
								@if ($membership->free_shipping)
									+ Free Shipping
								@endif
							</td>
						</tr>
						<tr>
							<td><b>Price: </b></td>
							<td>${{$membership->price}}</td>
						</tr>
						<tr>
							<td><b>Frequency:</b></td>
							<td>{{ucwords($membership->frequency)}} (Every {{$membership->days}} Days)</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
	</div>
@endsection
@extends('layouts.organica.template')

@section('page_title', "Update Membership")

@section('content')

	<div class="row">
		<h3>Available Plans</h3>
		@foreach ($other_memberships as $other_membership)
			<div class="col-md-4">
				@include('membership.partials.membership_details', ['membership' => $other_membership])
			</div>
		@endforeach	
	</div>
	* Student plans are for university/college students only.  You will need to email us an official Enrollment Verification document from your school.  Your membership benefits will be activated <b>after</b> your enrollment has been verified.<br>
	<hr>
	
	<div class="row">
		<div class="col-md-4">
			<h3>Current Plan</h3>
			@include('membership.partials.membership_details', ['membership' => $current_membership])
		</div>
		<div class="col-md-4 col-md-offset-1">
			{!! Form::open(['method' => 'POST', 'route' => 'update.membership', 'class' => 'form-horizontal']) !!}
				{!! Form::hidden('subid', $stripe_subscription_id) !!}
				<div class="form-group{{ $errors->has('membership_plan') ? ' has-error' : '' }}">
				    {!! Form::label('membership_plan', 'Select A New Membership Plan') !!}
				    {!! Form::select('membership_plan',$available_memberships, null, ['id' => 'membership_plan', 'class' => 'form-control']) !!}
				    <small class="text-danger">{{ $errors->first('membership_plan') }}</small>
				</div>

				<br>

				<div class="form-group">
				    <div class="checkbox{{ $errors->has('terms_confirmation') ? ' has-error' : '' }}">
				        <label for="terms_confirmation">
				            {!! Form::checkbox('terms_confirmation', '1', null, ['id' => 'terms_confirmation', 'required']) !!} I agree to Mountain Myst's <a href="{{url('/terms')}}" target="_blank" style="text-decoration: underline">terms of use</a> and I understand that my credit/debit card will be automatically charged at the selected frequency to renew my membership.
				        </label>
				    </div>
				    <small class="text-danger">{{ $errors->first('terms_confirmation') }}</small>
				</div>
			    <div class="btn-group pull-right">
			        {!! Form::submit("Update Membership", ['class' => 'btn btn-primary']) !!}
			    </div>
			
			{!! Form::close() !!}
		</div>
	</div>
	
@endsection
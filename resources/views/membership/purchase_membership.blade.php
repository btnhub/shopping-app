@extends('layouts.organica.template')

@section('page_title', "Membership")

@section('content')
	
	<?php $customer = auth()->user()->customer ?? null; ?>
	
	<div class="col-md-6 col-md-offset-3">
		@if (auth()->user()->memberships->filter(function ($membership) {return $membership->active();})->count())
			You already have an active membership.  Click here to <a href="{{route('user.memberships', auth()->user()->ref_id)}}">view & update your membership</a>.
		@else
			{!! Form::open(['method' => 'POST', 'route' => 'purchase.membership', 'class' => 'form-horizontal', 'id' => 'payment-form', 'name' => 'membership_payment_form']) !!}

				<div class="form-group{{ $errors->has('membership_plan') ? ' has-error' : '' }}">
				    {!! Form::label('membership_plan', 'Select A Membership Plan') !!}
				    {!! Form::select('membership_plan',$plans_array, null, ['id' => 'membership_plan', 'class' => 'form-control']) !!}
				    <small class="text-danger">{{ $errors->first('membership_plan') }}</small>
				</div>

				<br>

				@if ($customer)
					<b>Your stored card  ({{$customer->card_brand}} {{$customer->card_last_four}}) will be charged. </b>  Click here to <a href="{{route('edit.card', auth()->user()->ref_id)}}">update your card</a>.

				@else
					@include('layouts.stripe_form')	
				@endif

				<br><br>
				<div class="form-group">
				    <div class="checkbox{{ $errors->has('terms_confirmation') ? ' has-error' : '' }}">
				        <label for="terms_confirmation">
				            {!! Form::checkbox('terms_confirmation', '1', null, ['id' => 'terms_confirmation', 'required']) !!} I agree to Mountain Myst's <a href="{{url('/terms')}}" target="_blank" style="text-decoration: underline">terms of use</a> and I understand that my credit/debit card will be automatically charged at the selected frequency to renew my membership.
				        </label>
				    </div>
				    <small class="text-danger">{{ $errors->first('terms_confirmation') }}</small>
				</div>
			    <div class="btn-group pull-right">
			        {!! Form::submit("Purchase Membership", ['class' => 'btn btn-primary']) !!}
			    </div>
			
			{!! Form::close() !!}
		@endif
		
	</div>
@endsection
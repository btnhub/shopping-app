@extends('layouts.organica.template')
@section('page_title', "Purchase Confirmation")

@section('content')
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		Thank you for purchasing a Mountain Myst Membership.  Your Membership discount will automatically be applied when you place an order and checkout.  Be sure to log into your Mountain Myst account <b>before</b> placing an order.  We cannot retroactively add discounts to orders once they have been placed.  Your card will automatically be charged the membership fee every {{$membership->days}} days.
		</div>
	</div>
	<br>
	<div class="col-md-6 col-md-offset-3">
		<table class="table">
			<tbody>
				<tr>
					<td><b>Plan:</b></td>
					<td>
						{{ucwords($membership->name)}} {{$membership->discount * 100}}% Off
						@if ($membership->free_shipping)
							+ Free Shipping
						@endif
					</td>
				</tr>
				<tr>
					<td><b>Price: </b></td>
					<td>${{$membership->price}}</td>
				</tr>
				<tr>
					<td><b>Frequency:</b></td>
					<td>{{ucwords($membership->frequency)}} (Every {{$membership->days}} Days)</td>
				</tr>
			</tbody>
		</table>
	</div>	
@endsection
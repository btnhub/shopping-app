<table class="table">
	<tbody>
		<tr>
			<td><b>Plan:</b></td>
			<td>
				{{ucwords($membership->name)}} {{$membership->discount * 100}}% Off
				@if ($membership->free_shipping)
					+ Free Shipping
				@endif
			</td>
		</tr>
		<tr>
			<td><b>Price: </b></td>
			<td>${{$membership->price}}</td>
		</tr>
		<tr>
			<td><b>Frequency:</b></td>
			<td>{{ucwords($membership->frequency)}} (Every {{$membership->days}} Days)</td>
		</tr>
	</tbody>
</table>
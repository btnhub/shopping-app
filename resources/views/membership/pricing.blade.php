@extends('layouts.organica.template')

@section('page_title', "Membership")

@section('header')
    {{-- https://www.w3schools.com/howto/howto_css_pricing_table.asp --}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    * {
        box-sizing: border-box;
    }

    .columns {
        float: left;
        /*width: 25%;*/
        width: 33.3%;
        padding: 8px;
    }

    .price {
        list-style-type: none;
        border: 1px solid #eee;
        margin: 0;
        padding: 0;
        -webkit-transition: 0.3s;
        transition: 0.3s;
    }

    .price:hover {
        box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
    }

    .price .header {
        background-color: #111;
        color: white;
        font-size: 25px;
    }

    .price li {
        border-bottom: 1px solid #eee;
        padding: 20px;
        text-align: center;
    }

    .price .grey {
        background-color: #eee;
        font-size: 20px;
    }

    .button {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 10px 25px;
        text-align: center;
        text-decoration: none;
        font-size: 18px;
    }

    @media only screen and (max-width: 600px) {
        .columns {
            width: 100%;
        }
    }
    </style>
@endsection

@section('content')
    <h2 style="text-align:center">Your Favorite Products At The Best Prices</h2>
    <p style="text-align:center">Select the best membership plan for you and start saving!</p>

    <?php $i = 0;?>
    <div class="row">
        @foreach ($memberships as $membership)
            <?php $i++?>
            <div class="columns">
              <ul class="price">
                <li class="header" @if (!($i % 2))
                                        style="background-color:#4CAF50"
                                    @endif >
                    {{ucwords($membership->name)}}
                    @if (stripos($membership->name, "student") !== false)
                        *
                    @endif
                </li>
                <li class="grey">
                    ${{number_format($membership->price, 0)}} <br>
                    {{ucfirst($membership->frequency)}}
                </li>
                <li>
                    <span style="font-size: 20px"><b>{{$membership->discount * 100}}% Off</b></span>
                </li>
                <li>
                    @if ($membership->free_shipping)
                        Free Shipping **
                        <br>
                        <small>No minimum purchase</small>
                    @else
                        -
                        <br><br>
                    @endif
                </li>
                <li> 
                    @if (array_key_exists($membership->name, $savings) && stripos($membership->name, "student") === false)
                        Savings start around ${{$savings[$membership->name]}} ***
                    @endif
                    <br><br>
                </li>
                <li> <br><br></li>
                <li class="grey"><a href="{{url('/membership/purchase')}}" class="button" style="color:white">{{!auth()->user() ? "Register &" :'' }} Sign Up</a></li>
              </ul>
            </div>
        @endforeach
    </div>
    
    <div class="row">
        * This plan is for university/college students only.  You will need to email us an official Enrollment Verification document from your school.  Your membership benefits will be activated <b>after</b> your enrollment has been verified.<br>

        ** Limited to shipments to the Continental USA (48 states).<br>
        *** Estimate based on ~$5 shipping fee.
        {{-- ** Limited to one free sample per month.  Purchase must be made to receive gift. --}}    
    </div>
    

@endsection
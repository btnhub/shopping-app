<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dev Site</title>
        {{-- <title>Service Unavailable</title> --}}

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    Development Site
                </div>
                <span style="color:black;font-size: 24px;">
                    <b>Looking for great natural body products? </b> <br>
                    Visit <b><a href="https://mountain-myst.com">Mountain-Myst.com</a></b> or email us at <b><a href="mailto:Contact@Mountain-Myst.com">Contact@Mountain-Myst.com</a></b>
                </span>
            </div>
        </div>
    </body>
</html>

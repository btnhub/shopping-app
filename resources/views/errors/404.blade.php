@extends('layouts.organica.template')
@section('page_title', "Error")
@section('content')
	<div class="404-area ptb-100 {{-- bg-img-2 --}}">
		{{-- <div class="container"> --}}
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
					<div class="{{-- page-not-found --}} text-center">
						<h1>404</h1>
						<h2>Oops! Page Not Found.</h2>
					</div>
				</div>
			</div>
		{{-- </div> --}}
	</div>
@endsection
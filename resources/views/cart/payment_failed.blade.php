@extends('layouts.organica.template')

@section('page_title', "Payment Failed")

@section('content')
	<div class="container">
		Unfortunately, your payment failed to process.  Click Back to try again.  			
	</div>
@stop
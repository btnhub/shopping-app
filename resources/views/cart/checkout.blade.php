@extends('layouts.organica.template')

@section('page_title', "Checkout")

@section('content')
	<div class="main-container mb-50">
		<div class="container">
			{{-- <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="checkout-title text-center mb-50">
						<h1>Checkout</h1>
					</div>
				</div>
			</div> --}}
			<div class="checkout-area">
				<div class="row">
					@if (!Auth::user())
						<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
							<div class="returning-customer-area p-20 mb-20">
								<div class="returning-customer mb-15">
									<i class="fa fa-user"></i>
									<span>Returning customer?</span>
									<span class="login-form-click">Click here to login</span>
									or register to create an account.
								</div>
								<div class="account  bg-1  p-20 login-form">
									<div class="account-form">
										<form role="form" method="POST" action="{{ route('login') }}">
                                			{{ csrf_field() }}
											<span class="form-row-first">
												<div class="{{ $errors->has('email') ? ' has-error' : '' }}">
				                                    <b>E-mail address <span>*</span></b>
				                                    <input id="email" type="email" name="email" value="{{ old('email') }}" required>

				                                    @if ($errors->has('email'))
				                                        <span class="help-block">
				                                            <strong>{{ $errors->first('email') }}</strong>
				                                        </span>
				                                    @endif
				                                </div>
											</span>
											<span class="form-row-last">
												<div class="{{ $errors->has('password') ? ' has-error' : '' }}">
				                                    <b>Password <span>*</span></b>
				                                    <input id="password" type="password" name="password" required>

				                                    @if ($errors->has('password'))
				                                        <span class="help-block">
				                                            <strong>{{ $errors->first('password') }}</strong>
				                                        </span>
				                                    @endif
				                                </div>
												
											</span>
											<div class="login-button">
												<button>Login</button>
												<a href="{{ route('password.request') }}">
		                                    	    Forgot Your Password?
		                                    	</a>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
			<div class="customer-details-area mb-35">
				<div class="row">
					{{--Customer & Shipping Details --}}
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="customer-details mb-50">
							<div class="section-title mb-10">
								<h2>Shipping Details</h2>
							</div>

							{{-- Checking for Password Error (REVISIT if creating method to create an account on this page)
							<div class="row">
								@if ($errors->has('password'))
									<div class="text-danger">
								    {{ $errors->first('password') }}<br/>
								    </div>
								@endif
							</div> --}}

							<div class="customer-details-form account-form p-20 clear bg-1">
								{!! Form::open(['method' => 'POST', 'route' => 'shipping.rates', 'class' => 'form-horizontal']) !!}
									<span class="form-row-first">
										<b>First Name <span class="required">*</span></b>
										<div class="{{ $errors->has('first_name') ? ' has-error' : '' }}">
								        	{!! Form::text('first_name', session('first_name') ?? (Auth::user()->first_name ?? null), ['class' => 'form-control']) !!}
								        	<small class="text-danger">{{ $errors->first('first_name') }}</small>						
									    </div>	
									</span>
									<span class="form-row-last">
										<b>Last Name <span class="required">*</span></b>
										<div class="{{ $errors->has('last_name') ? ' has-error' : '' }}">
								        	{!! Form::text('last_name', session('last_name') ?? (Auth::user()->last_name ?? null), ['class' => 'form-control']) !!}
								        	<small class="text-danger">{{ $errors->first('last_name') }}</small>	
									    </div>
									</span>
									<span class="form-row-first">
										<b>Email Address <span class="required">*</span></b>
										<div class="{{ $errors->has('email') ? ' has-error' : '' }}">
									        {!! Form::email('email', session('email') ?? (Auth::user()->email ?? null), ['class' => 'form-control']) !!}
									        <small class="text-danger">{{ $errors->first('email') }}</small>
									    </div>
									</span>
									<span class="form-row-last">
										<b>Phone <span class="required">*</span></b>
										<div class="{{ $errors->has('phone') ? ' has-error' : '' }}">
								        	{!! Form::text('phone', session('phone') ?? (Auth::user()->phone ?? null), ['class' => 'form-control']) !!}
									        <small class="text-danger">{{ $errors->first('phone') }}</small>
								    	</div>
									</span>
									<span>
										<b>Shipping Address <span class="required">*</span></b>
										<div class="{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
									       	{!! Form::text('shipping_address', session('shipping_address') ?? (Auth::user()->shipping_address ?? null), ['class' => 'form-control']) !!}
									        <small class="text-danger">{{ $errors->first('shipping_address') }}</small>
									    </div>
									</span>

									<span>
										<div class="{{ $errors->has('shipping_address_2') ? ' has-error' : '' }}">
									        {!! Form::text('shipping_address_2', session('shipping_address_2') ??  (Auth::user()->shipping_address_2 ?? null), ['placeholder' =>"Apartment, suite, unit etc. (optional)"]) !!}
									       	<small class="text-danger">{{ $errors->first('shipping_address_2') }}</small>
									    </div>
									</span>
									<span class="form-row-first">
										<b>City <span class="required">*</span></b>
										<div class="{{ $errors->has('city') ? ' has-error' : '' }}">
									        {!! Form::text('city', session('city') ?? (Auth::user()->city ?? null)) !!}
									        <small class="text-danger">{{ $errors->first('city') }}</small>
									    </div>
									</span>
									<span class="form-row-last">
										<b>State/Province <span class="required">*</span></b>
										<div class="{{ $errors->has('state') ? ' has-error' : '' }}">
									        {!! Form::select('state', array_merge(['' => 'Select A State/Province'] , config('codes_states')), session('state') ?? (Auth::user()->state ?? null), ['id' => 'state', 'class' => 'form-control']) !!}
									        <small class="text-danger">{{ $errors->first('state') }}</small>
									    </div>
									</span>

									<span class="form-row-last">
										<b>Zip Code <span class="required">*</span></b>
										<div class="{{ $errors->has('zip') ? ' has-error' : '' }}">
								            {!! Form::text('zip', session('zip') ?? (Auth::user()->zip ?? null)) !!}
								            <small class="text-danger">{{ $errors->first('zip') }}</small>
									    </div>
									</span>
									<span class="form-row-first">
										<b>Country <span class="required">*</span></b>
										<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
									        {!! Form::select('country',array_merge(['' => 'Select A Country'], config('codes_country')), session('country') ?? (Auth::user()->country ?? "US"), ['id' => 'country', 'class' => 'form-control', 'required' => 'required']) !!}
									        <small class="text-danger">{{ $errors->first('country') }}</small>
									    </div>
									</span>

									@if (Auth::user() && !Auth::user()->shipping_address)
										<div class="row">
											<span class="form-row-last">
										
										        <label for="save_address">
										        	<div style="display:inline">
										        	
										        	{!!Form::label('save_address', 'Save Shipping Address')!!}
										            {!! Form::checkbox('save_address', '1', null, ['id' => 'save_address']) !!} 	
										        	</div>
										            
										        </label>
										
											</span>	
										</div>
									@endif
										
								    {{-- <div class="form-group">
								        <div class="checkbox{{ $errors->has('create_account') ? ' has-error' : '' }}">
								            
								                <label for="create_account">
								                	<b>Click To Create An Account </b>
								               	</label>

												<strong class="show-password pull-left">
												                {!! Form::checkbox('create_account', '1', null, ['id' => 'create_account']) !!}
												</strong>
								        </div>
								        <small class="text-danger">{{ $errors->first('create_account') }}</small>
								    </div>
									
									<div class="account-password">
										<p>Enter a password to create an account. </p>
										
										<span class="form-row-first">
											<b>Account Password <span class="required">*</span></b>
											<input type="password" name="password"/>
										</span>
										<span class="form-row-last">
											<b>Confirm Password <span class="required">*</span></b>
											<input type="password" name="password_confirmation" />
										</span>
									</div> --}}

									
									<div class="order-button-payment">
										<input type="submit" value="Select Shipping Method" />
									</div>	
								{!! Form::close() !!}
							</div>
						</div>
					</div>
					{{--Order Details--}}
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="section-title">
							<h2>Your order</h2>
						</div>
						<div class="your-order bg-1">
							<div class="your-order-table table-responsive">
								
								<b><a href="{{route('cart.index')}}" class="pull-right">EDIT CART</a></b>
								<table>
									<thead>
										<tr>
											<th class="product-name">Product</th>
											<th class="product-total">Quantity</th>
											<th class="product-total">Total</th>

										</tr>							
									</thead>
									<tbody>
										@foreach (Cart::content() as $cart_item)
											<tr class="cart_item">
												<td class="product-name">
													<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}">
														{{$cart_item->name}} 
														{{-- @if ($cart_item->options->organic)
															(Organic)
														@endif --}}
													</a>
													<br>
													<small>
														{{$cart_item->model->size ?? ''}} 
														{{$cart_item->options->glass ? 'Glass Container' : ''}}
														<br>{{$cart_item->model->details()}}
													</small>
													@if ($cart_item->options->reorder_frequency)
														<br>
														<small><b><em>Reorder Every {{$cart_item->options->reorder_frequency}} Days </em></b></small>
													@endif
												</td>
												<td>
													{{$cart_item->qty}}
												</td>
												<td class="product-total">
													<span class="amount">${{number_format($cart_item->price * $cart_item->qty, 2)}}</span>
												</td>
											</tr>
										@endforeach
									</tbody>
									<tfoot>
										<tr class="cart-subtotal">
											<td></td>
											<th>Cart Subtotal</th>
											<td><span class="amount">${{Cart::subtotal()}}</span></td>
										</tr>
										<tr class="order-total">
											<td></td>
											<th>Order Total
												<br>
												<small>(Before Tax & Shipping)</small>
											</th>
											<td><strong><span class="amount">${{Cart::subtotal()}}</span></strong>
											</td>
										</tr>								
									</tfoot>
								</table>

								{{--Coupon--}}
								{{--
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="coupon-code-area p-20 mb-20">
										<div class="returning-customer mb-15">
											<i class="fa fa-book"></i>
											<span>Have a coupon?</span>
											<span class="code">Click here to enter your code</span>
										</div>
										<div class="code-form account-form p-20 clear bg-1">
											<form action="#">
												<span class="form-row-first">
													<input type="text" placeholder="Coupon Code"/>
												</span>
												<span class="form-row-last login-button">
													<button>Apply Coupon</button>
												</span>
											</form>
										</div>
									</div>
								</div>
							</div>
							--}}
							<div class="payment-method">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="headingThree">
											<h4 class="panel-title panel-img">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											Accepted Cards <img src="{{asset('images/stripe_credit-card-logos.png')}}" alt="" />
											</a>
											</h4>
										</div>
										<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
											<div class="panel-body payment-content">
												We accept credit/debit card payments. We use Stripe to securely process payments.
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="coupon mb-10 floatleft">
									@include('cart.partials.gift_card_form')						
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
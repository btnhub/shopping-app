@extends('layouts.organica.template')

@section('page_title', "Payment")

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>Review Cart</h3>
				@include('cart.partials.cart_items')
			</div>
			<div class="col-md-5 col-md-offset-1">
				<h3>Payment Method</h3>
			Carefully review your order before submitting this payment.<br><br>
				@if ($credit_balance >= $grand_total)
					<div class="radio{{ $errors->has('charge_customer') ? ' has-error' : '' }}">
					    <label>
					        {!! Form::radio('charge_customer',"credit",  null, ['id' => 'charge_credit', "onclick" => 'chargeMethod("credit")', 'onchange'=> 'applyCredit()']) !!} <b>Pay With Account Credit (${{$credit_balance}})</b><br><br>
					    </label>
					    <small class="text-danger">{{ $errors->first('charge_customer') }}</small>
					</div>
				@elseif ($credit_balance > 0)
					<div class="form-group">
					    <div class="checkbox{{ $errors->has('apply_credit') ? ' has-error' : '' }}">
					        <label>
					            {!! Form::checkbox('apply_credit', '1', null, ['id' => 'apply_credit', "onchange" => "applyCredit()"]) !!} <b>Pay With Account Credit (${{$credit_balance}})</b>
					        </label>
					    </div>
					    <small class="text-danger">{{ $errors->first('apply_credit') }}</small>
					</div>
				@endif

				@if ($customer && $customer->isCardValid())
					<div class="radio{{ $errors->has('charge_customer') ? ' has-error' : '' }}">
					    <label>
					        {!! Form::radio('charge_customer',1,  null, ['id' => 'charge_customer', "onclick" => "chargeMethod('customer')", 'onchange'=> 'applyCredit()']) !!} <b>Pay <span name="order_balance"></span> With Stored Card:</b> {{$customer->card_brand}} {{$customer->card_last_four}}<br><br>
					    </label>
					    <small class="text-danger">{{ $errors->first('charge_customer') }}</small>
					</div>
				@endif				


				<div class="radio{{ $errors->has('charge_customer') ? ' has-error' : '' }}">
				    <label>
				        {!! Form::radio('charge_customer',0,  null, ['id' => 'charge_customer', "onclick" => 'chargeMethod("card")', "onchange" => 'applyCredit()']) !!} <b>Pay <span name="order_balance"></span> With Card:</b> <br><br>
				    </label>
				    <small class="text-danger">{{ $errors->first('charge_customer') }}</small>
				</div>

					{!! Form::open(['method' => 'POST', 'route' => 'submit.payment', 'class' => 'form-horizontal', "id" => "stored_card", "style" => "display:none"]) !!}
						
						@if ($customer)
							{!! Form::hidden('charge_stored_card', '', ["id" =>"charge_stored_card"]) !!}
						@endif
						
					    {!! Form::hidden('apply_credit_balance', '') !!}

					    @include('cart.partials.checkout_payment_fields')
					
					{!! Form::close() !!}


				{!! Form::open(['method' => 'POST', 'route' => 'submit.payment', 'class' => 'form-horizontal', 'name' => 'pmt_form', 'id' =>"payment-form", "style" => "display:none"]) !!}
					
					{!! Form::hidden('apply_credit_balance', '') !!}

					<div id="stripe_form" class="col-md-offset-1">
						@include('layouts.stripe_form')	
						<br>

						@if ($user && $customer)
					    	<div class="form-group">
					    	    <div class="checkbox{{ $errors->has('update_card') ? ' has-error' : '' }} col-md-offset-1">
					    	        <label for="update_card">
					    	            {!! Form::checkbox('update_card', 1, null, ['id' => 'update_card']) !!} Update Card Info
					    	        </label>
					    	    </div>
					    	    <small class="text-danger">{{ $errors->first('update_card') }}</small>
					    	</div>
					    @elseif	($user && !$customer)
					    	<div class="form-group">
					    	    <div class="checkbox{{ $errors->has('save_card') ? ' has-error' : '' }}  col-md-offset-1">
					    	        <label for="save_card">
					    	            {!! Form::checkbox('save_card', '1', null, ['id' => 'save_card']) !!} Save Card Info
					    	        </label>
					    	    </div>
					    	    <small class="text-danger">{{ $errors->first('save_card') }}</small>
					    	</div>
					    @endif
					</div>
					
				    <br><br>

				    @include('cart.partials.checkout_payment_fields')
					
				{!! Form::close() !!}
				
			</div>
		</div>
	</div>

	{{-- @if (Auth::user() || !$customer || !$credit_balance) --}}
	@if ((Auth::user() && !$customer) || ($customer && !$customer->isCardValid()) || !$credit_balance)
		<script type="text/javascript">
		    document.getElementById('charge_customer').checked = true;
		    chargeMethod("card");
		</script>
	@elseif	($customer && $customer->isCardValid())
		<script type="text/javascript">
		    document.getElementById('charge_customer').checked = true;
		    chargeMethod("customer");
		</script>
	@endif
	@if ($credit_balance)
		<script type="text/javascript">
			applyCredit();
		</script>
	@endif
@endsection

<script type="text/javascript">
	function applyCredit()
	{
		var credit_checkbox = document.getElementById("apply_credit");
		var credit_radio = document.getElementById("charge_credit");
		
		if ((credit_checkbox && credit_checkbox.checked) || (credit_radio && credit_radio.checked)) {
			//check if balance can cover entire cost

			//find all fields with apply_credit_balance and change value
			var elements = document.getElementsByName('apply_credit_balance');
			for (var i = 0; i < elements.length; i++) {
			    elements[i].value = 1;
			}
			
			var grand_total = {{$grand_total}};
			var credit_balance = {{$credit_balance}};
			var difference = grand_total - credit_balance;

			if (difference > 0) {
				var order_elements = document.getElementsByName('order_balance');
				
				for (var i = 0; i < order_elements.length; i++) {
				    order_elements[i].innerHTML = "Balance $" + difference.toFixed(2);
				}
			}
		}
		
		else {
			//find all fields with order_balance and inner HTML
			var order_elements = document.getElementsByName('order_balance');
			for (var i = 0; i < order_elements.length; i++) {
			    order_elements[i].innerHTML = "";
			}

			//find all fields with apply_credit_balance and change value
			var elements = document.getElementsByName('apply_credit_balance');
			for (var i = 0; i < elements.length; i++) {
			    elements[i].value = 0;
			}
		}
	}

	function chargeMethod(method)
	{
		if (method == 'card') {
			document.getElementById('payment-form').style.display = "block";
			document.getElementById('stored_card').style.display = "none";
			document.getElementById('charge_stored_card').value = 0;
		}

		else if (method == 'customer')
		{
			document.getElementById('payment-form').style.display = "none";
			document.getElementById('stored_card').style.display = "block";
			document.getElementById('charge_stored_card').value = 1;
		}

		else
		{
			//solely using account credits
			document.getElementById('payment-form').style.display = "none";
			document.getElementById('stored_card').style.display = "block";
			document.getElementById('charge_stored_card').value = 0;	
		}
	}
</script>

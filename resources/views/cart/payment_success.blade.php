@extends('layouts.organica.template')

@section('page_title', "Payment Confirmation")

@section('content')
	<div class="container">
		<div class="alert alert-success">
			Payment Completed!
		</div>
		<div class="col-md-5">
			Your payment details are as follows:<br>
			<table class="table striped">
				<tbody>
					<tr>
						<td><b>Order ID</b></td>
						<td>{{$payment->order->ref_id}}</td>
					</tr>			
					<tr>
						<td><b>Subtotal</b></td>
						<td>${{number_format($payment->amount, 2)}}</td>
					</tr>
					{{-- <tr>
						<td><b>Processing Fee</b> </td>
						<td>${{$payment->processing_fee ?? 0}}</td>
					</tr> --}}
					@if ($payment->discount_amount > 0)
						<tr>
							<td><b>Discount Amount</b></td>
							<td>${{number_format($payment->discount_amount, 2)}}</td>
						</tr>
					@endif
					
					<tr>
						<td><b>Tax </b></td>
						<td>${{number_format($payment->sales_tax, 2)}}</td>
					</tr>
					<tr>
						<td><b>Shipping Fee</b></td>
						<td>${{number_format($payment->shipping_fee ?? 0, 2)}}</td>
					</tr>
					@if ($payment->credit_applied > 0)
						<tr>
							<td><b>Credit Applied</b></td>
							<td>-${{number_format($payment->credit_applied, 2)}}</td>
						</tr>
					@endif
					<tr>
						<td><b>Total Charged</b></td>
						<td>${{number_format($payment->total_amount, 2)}}</td>
					</tr>
							
				</tbody>
			</table>
		</div>
		<div class="col-md-5 col-md-offset-1">
			@include('cart.partials.cart_items')
		</div>
	</div>
@stop
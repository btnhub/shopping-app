@extends('layouts.organica.template')

@section('page_title', 'Cart')

@section('content')
	<?php 
        $membership_free_shipping = 0;
        if (auth()->user() && auth()->user()->best_membership())  {
            $membership_free_shipping = auth()->user()->best_membership()->free_shipping;
        }
    ?>

	<div class="cart-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="account-title mb-10 text-center">
						{{--<h1>Cart</h1>--}}
					</div>
				</div>
				<div class="col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12">
					<div class="cart-table mb-20">
						<div class="table-content table-responsive">
							<table>
								{{-- <thead>
									<tr>
										<th class="product-thumbnail"></th>
										<th class="product-name">Product</th>
										<th class="product-price">Price</th>
										<th class="product-quantity">Quantity</th>
										<th class="product-subtotal">Total</th>
									</tr>
								</thead> --}}
								<tbody>
									@forelse ($cart_items as $cart_item)
										<tr class="cart-item">
											{{-- <td class="product-thumbnail">
												@if ($cart_item->model->product->image)
													<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}">
														<img src='{{ asset("images/products/{$cart_item->model->product->image}") }}' alt="" />
													</a>	
												@endif
												
											</td> --}}
											<td class="product-name">
												<a class="product-thumbnail" href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}">
														<img src='{{ asset("images/products/{$cart_item->model->product->image}") }}' alt="" />
												</a>
												<span class="sub-total pull-right" style="color:#a0c03c; font-size: 1.25em">

													{!! Form::open(['method' => 'DELETE', 'route' => ['cart.destroy', 'row_id' => $cart_item->rowId], 'class' => 'delete product-remove pull-right', 'id' => $cart_item->rowId]) !!}
														<a href="javascript:{}" class="remove" onclick='document.getElementById("{{$cart_item->rowId}}").submit(); return false;'>x</a>
													{!! Form::close() !!}
													<br><br>
													${{number_format($cart_item->price * $cart_item->qty, 2)}}
													<br>
												</span>
												<br>
												<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}" style="font-size: 1.15em"> {{$cart_item->name}} 
													{{-- @if ($cart_item->options->organic)
														(Organic)
													@endif --}}
												</a>
												
												<br>
												Size: {{$cart_item->model->size ?? ''}} 
												{{$cart_item->options->glass ? 'Glass Container' : ''}}
												<br>
												{!!$cart_item->model->details() ?? ''!!}

												{!! Form::open(['method' => 'PATCH', 'route' => ['cart.update', $cart_item->rowId, "class" => "subtotal"]]) !!}
													<div class="pull-right">
														<span class="pull-left">
															<span class="pull-left" style="font-size:1.15em; padding: 0px 5px">
																${{number_format($cart_item->price, 2)}}
															</span>
															x <input type="number" name="qty" min="1" max="12" value="{{$cart_item->qty}}" style="margin-right: 5px">
														</span>
														<button type="submit" class="btn btn-xs btn-success pull-right"><i class='fa fa-refresh' aria-hidden='true'></i></button>
									        			{{-- {!! Form::submit("Update", ['class' => 'btn btn-success']) !!} --}}
									    			</div>
									
												{!! Form::close() !!}
												@if ($cart_item->options->reorder_frequency)
													<br>
													<small><b><em>Reorder Every {{$cart_item->options->reorder_frequency}} Days </em></b></small>
												@endif
												{{-- <br>
												{!! Form::open(['method' => 'POST', 'route' => ['wishlist.add.product',	$cart_item->model->product->ref_id]]) !!}

												    <div class="btn-group pull-right">
												        {!! Form::submit("Add To Wishlist", ['class' => 'btn btn-info']) !!}
												    </div>
												
												{!! Form::close() !!} --}}
											</td>
											{{-- <td class="product-price">
												<span class="amounte">
													${{number_format($cart_item->price, 2)}}
												</span>
											</td> --}}
											{{-- <td class="product-subtotal">
												<span class="sub-total">${{number_format($cart_item->price * $cart_item->qty, 2)}}</span>
											</td> --}}
										</tr>
									@empty
										<tr><td colspan="6"><center>No Items In Shopping Cart</center></td></tr>
									@endforelse
									{{-- Coupon & Update Entire Cart--}}
									@if ($cart_items->count() > 0)
										<tr>
											<td colspan="6" class="actions clear">
												<div class="row">
													<div class="coupon mb-10 floatright">
														@include('cart.partials.gift_card_form')
													</div>
												</div>
												<br>
												{{-- <div class="floatright mb-10">
													<input class="button cursor-not" name="update_cart" value="Update Cart" type="submit">	
												</div>
												 --}}
												
												@if ((float) str_replace(",", "", Cart::subtotal()) < config('other_constants.free_shipping_amount'))
													<div class="row">
														<div class="pull-right mb-20">
															@if ($membership_free_shipping)
																<b>Your Membership Comes With Free Shipping!</b>
															@else
																<b>You Are ${{number_format(config('other_constants.free_shipping_amount') - (float) str_replace(",", "", Cart::subtotal()), 2)}} Away From Free Shipping!</b>
															@endif
															
														</div>
													</div>
												@endif	

												{!! Form::open(['method' => 'DELETE', 'route' => 'empty.cart', 'class' => 'delete']) !!}
												    <div class="btn-group pull-right">
												        {!! Form::submit("Empty Cart", ['class' => 'btn btn-warning']) !!}
												    </div>
												{!! Form::close() !!}	
											</td>
										</tr>
									@endif
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@if ($cart_items->count() > 0)
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<!-- product area start -->
						{{--
						<div class="cart-product mb-35 tab-area">
							<div class="section-title">
								<h2>Cross-Sells </h2>
							</div>
							<div class="cart-active next-prev-style">
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-img">
											<a href="shop-single-product.html"><img src="img/product/11.jpg" alt="" /></a>
											<span class="sale">Sale</span>
										</div>
										<div class="product-item-details text-center">
											<div class="product-name-review tab-product-name-review">
												<div class="product-name mt-30 ">
													<span>Sample Category</span>
													<strong><a href="shop-single-product.html">Chaz Kangeroo Hoodie1</a></strong>
												</div>
												<div class="product-review">
													<ul>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													<span class="special-price">$ 50.00</span>
												</div>
											</div>
											<div class="add-to-cart-area clear pt-35">
												<div class="add-to-cart text-uppercase">
													<button>add to cart</button>
												</div>
												<div class="add-to-links">
													<ul>
														<li class="left">
															<a href="#"><i class="fa fa-adjust"></i></a>
														</li>
														<li class="right">
															<a href="#"><i class="fa fa-heart-o"></i></a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-img">
											<a href="shop-single-product.html"><img src="img/product/10.jpg" alt="" /></a>
											<span class="new">new</span>
										</div>
										<div class="product-item-details text-center">
											<div class="product-name-review tab-product-name-review">
												<div class="product-name mt-30 ">
													<span>Sample Category</span>
													<strong><a href="shop-single-product.html">Fusion Backpack</a></strong>
												</div>
												<div class="product-review">
													<ul>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													<span class="special-price">$ 55.00</span>
												</div>
											</div>
											<div class="add-to-cart-area clear pt-35">
												<div class="add-to-cart text-uppercase">
													<button>add to cart</button>
												</div>
												<div class="add-to-links">
													<ul>
														<li class="left">
															<a href="#"><i class="fa fa-adjust"></i></a>
														</li>
														<li class="right">
															<a href="#"><i class="fa fa-heart-o"></i></a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-img">
											<a href="shop-single-product.html"><img src="img/product/9.jpg" alt="" /></a>
											<span class="sale">Sale</span>
										</div>
										<div class="product-item-details text-center">
											<div class="product-name-review tab-product-name-review">
												<div class="product-name mt-30 ">
													<span>Sample Category</span>
													<strong><a href="shop-single-product.html">Joust Duffle Bag1</a></strong>
												</div>
												<div class="product-review">
													<ul>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													<span class="special-price">$ 40.00</span>
													<span class="old-price">
														<del>$ 44.00</del>
													</span>
												</div>
											</div>
											<div class="add-to-cart-area clear pt-35">
												<div class="add-to-cart text-uppercase">
													<button>add to cart</button>
												</div>
												<div class="add-to-links">
													<ul>
														<li class="left">
															<a href="#"><i class="fa fa-adjust"></i></a>
														</li>
														<li class="right">
															<a href="#"><i class="fa fa-heart-o"></i></a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						--}}
						<!-- product area end -->
					</div>
					{{-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> --}}
					<div class="col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12">
						<div class="cart_totals">
							<div class="section-title mb-30 text-uppercase">
								<h2>Cart Totals</h2>
							</div>
						</div>
						<div class="table-content table-responsive mb-30">
							<table>
								<tr>
									<td><strong>Subtotal</strong></td>
									<td><b>${{Cart::subtotal()}}</b></td>
								</tr>
								<tr>
									<td><strong>Tax</strong></td>
									<td>-</td>
								</tr>
								<tr>
									<td><strong>Total (before shipping & Tax)</strong></td>
									<td><b>${{Cart::subtotal()}}</b></td>
								</tr>
							</table>
						</div>
						
						{!! Form::open(['method' => 'GET', 'route' => 'get.checkout']) !!}
						
							<div class="login-button mb-30 pull-right">
								<a href="{{route('get.checkout')}}">
									<button>Proceed to Checkout</button>
								</a>
							</div>   
						
						{!! Form::close() !!}
						
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection
@extends('layouts.organica.template')

@section('page_title', "Shipping Method")

@section('content')
    
    <?php 
        $membership_free_shipping = 0;
        if (auth()->user() && auth()->user()->best_membership())  {
            $membership_free_shipping = auth()->user()->best_membership()->free_shipping;
        }
    ?>

    <div class="container">
        <div class="row">
    
            <div class="col-md-3">
                <h3>Ship To:</h3>
                {{session('first_name')}} {{session('last_name')}}<br>
                {{session('shipping_address')}}<br>
                @if (session('shipping_address_2'))
                    {{session('shipping_address_2')}}<br>
                @endif
                {{session('city')}}, {{session('state')}} {{session('zip')}}<br>
                {{session('country')}}<br>
                Email: {{session('email')}}<br>
                Phone: {{session('phone')}}<br>

            </div>
                        
            <div class="col-md-9">
                <h3><b>Select A Shipping Method</b></h3>
                {{-- Most of our items are Made-To-Order.  <b>Please allow 3-5 business days for us to make your order.</b>  <br> --}}
                <b>Please allow 1-3 business days to process your order.</b>
                {{-- Time to create your order is <b>NOT</b> included in the transit times below. --}}
                <br><br>

                {!! Form::open(['method' => 'POST', 'route' => 'payment.form', 'class' => 'form-horizontal']) !!}
                    <div>
                        @if ($pickup_locations->count())
                            <h4>Free Pickup</h4>
                            <p>For Denver Metro customers, free pickup is available at one of the local weekly markets or events listed below.  Orders must be placed by 8am on the day before pickup.  For example, for a Saturday pickup, an order must be completed by Friday @ 8am.

                            <div id="pickup_details" class="col-md-12">
                                @foreach ($pickup_locations->sortBy('website_order') as $pickup_location)
                                    <div class="col-md-4">
                                        <p><span style="font-weight: bold"><a href="{{$pickup_location->website}}" target="_blank">{{$pickup_location->event}}</a></span><br>
                                        {{$pickup_location->address}}<br>
                                        {{$pickup_location->day}} {{$pickup_location->time}}<br>
                                        
                                    </div> 
                                @endforeach  
                            </div>
                            <br><br>

                            <label>
                                {!! Form::radio('rate','free_pickup', 1, ['id' => 'free_pickup', 'onclick' => "showPickup()"]) !!} Free Pickup $0.00
                            </label> <br>
                            <div id="pickup_options" style="padding-left:3em;">
                                <?php $first = 1;?>
                                @foreach ($pickup_locations->sortBy('website_order') as $pickup_location)
                                    <label>
                                        {!! Form::radio('pickup_location',$pickup_location->id, $first, ['id' => $pickup_location->id, 'onclick' => "document.getElementById('free_pickup').checked = true"]) !!} {{$pickup_location->event}} ({{$pickup_location->day}} {{$pickup_location->time}})
                                    </label> <br>
                                    <?php $first = 0; ?>
                                @endforeach
                            </div>
                            <br>
                        @endif

                        @if ((Cart::subtotal() >= config('other_constants.free_shipping_amount') || $membership_free_shipping) && in_array(session('country'), ['US']) && !in_array(session('state'), ['AK', 'HI']))
                            <h4 style="color:darkred">Your Order Qualifies For Free Shipping!</h4>
                            <label>
                                {!! Form::radio('rate','free_shipping', 1, ['id' => 'free_shipping', 'onclick' => "hidePickup()"]) !!} Free Shipping $0.00
                            </label> <br><br>
                        @endif

                        <h4>Shipping Options</h4>
                        @foreach ($rates as $rate)
                            <label>
                                {!! Form::radio('rate', $rate->object_id,  null, ['onclick' => "hidePickup()"]) !!} 
                                    <img src="{{ $rate->provider_image_75 }}" alt="">
                                    {{ $rate->provider }} {{ $rate->servicelevel->name }} 
                                    @if ($rate->duration_terms)
                                        ({{ $rate->duration_terms }})
                                    @endif

                                    <b>${{ $rate->amount }}</b>
                            </label><br>
                        @endforeach
                        <small class="text-danger">{{ $errors->first('rate') }}</small>
                    </div>
                    <div class="login-button mt-20 mb-30">
                        {!! Form::button("Proceed To Payment", ['type' => 'submit']) !!}
                    </div>
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
@endsection

@section('scripts')
    <script type="text/javascript">
        function showPickup(){
            document.getElementById('pickup_options').style.display = 'block';
        }

        function hidePickup(){
            document.getElementById('pickup_options').style.display = 'none';
        }
    </script>
@endsection

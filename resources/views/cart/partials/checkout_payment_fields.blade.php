{{-- <b><em>You will have 24 hours to cancel this order and receive a full refund.</em></b> <br>
We will begin preparing your order after 24 hours; if you choose to skip the grace period we'll get to work immediately! <br>
<div class="form-group">
    <div class="checkbox{{ $errors->has('skip_grace_period') ? ' has-error' : '' }}">
        <label>
             {!! Form::checkbox('skip_grace_period', '1', null, ['id' => 'skip_grace_period']) !!}<b>Skip 24 Hour Refund Grace Period.</b>
        </label>
    </div>
    <small class="text-danger">{{ $errors->first('skip_grace_period') }}</small>
</div> --}}

<div class="form-group">
    <div class="checkbox{{ $errors->has('terms') ? ' has-error' : '' }}">
        <label>
            {!! Form::checkbox('terms', '1', null, ['id' => 'terms', 'required']) !!} I agree to the <a href="{{url('/terms')}}" style="text-decoration: underline" target="_blank">terms of use</a>{{--  and <a href="{{url('/refunds')}}" style="text-decoration: underline" target="_blank">refund policy</a> --}}.  
        </label>
    </div>
    <small class="text-danger">{{ $errors->first('terms') }}</small>
</div>		

{{--IF Reorder Items--}}
@if (Cart::content()->where('options.reorder_frequency','!=', null)->count())
	<div class="form-group">
	    <div class="checkbox{{ $errors->has('reorder_confirm') ? ' has-error' : '' }}">
	        <label>
	            {!! Form::checkbox('reorder_confirm', '1', null, ['id' => 'reorder_confirm', 'required']) !!} I authorize Mountain Myst to automatically reorder the selected items and charge the order (including shipping, if applicable) to my card at the specified frequency.
	        </label>
	    </div>
	    <small class="text-danger">{{ $errors->first('reorder_confirm') }}</small>
	</div>			
@endif



{{-- <small>
	<b></b>Since we're making this order just for you, all sales are final.
</small> --}}
<div class="login-button mt-20 mb-30 pull-right">
    {!! Form::button("Submit Payment", ['type' => 'submit']) !!}
</div>
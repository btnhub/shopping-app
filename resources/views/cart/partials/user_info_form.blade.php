	<h3>Contact Info</h3>
    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
        {!! Form::label('first_name', 'First Name', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('first_name', Auth::user()->first_name ?? 'Powells', ['class' => 'form-control']) !!}
        	<small class="text-danger">{{ $errors->first('first_name') }}</small>
        </div>
        
    </div>	

    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
        {!! Form::label('last_name', 'Last Name', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('last_name', Auth::user()->last_name ?? 'Bookstore', ['class' => 'form-control']) !!}
        	<small class="text-danger">{{ $errors->first('last_name') }}</small>	
        </div>
        
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::email('email', Auth::user()->email ?? 'email@example.com', ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) !!}
        <small class="text-danger">{{ $errors->first('email') }}</small>
        </div>
        
    </div>

    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
        {!! Form::label('phone', 'Phone', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('phone', Auth::user()->phone ?? "1234567789", ['class' => 'form-control']) !!}
	        <small class="text-danger">{{ $errors->first('phone') }}</small>
        </div>
        
    </div>
    @if (!Auth::user())
    	<div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
	        <div class="checkbox{{ $errors->has('create_account') ? ' has-error' : '' }}">
	            <label for="create_account">
	                {!! Form::checkbox('create_account', '1', null, ['id' => 'create_account']) !!} Create an account
	            </label>
            </div>
        </div>
        <small class="text-danger">{{ $errors->first('create_account') }}</small>
    </div>	
    @endif
    

    <hr>

    <h3>Shipping Address</h3>
    <div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
        {!! Form::label('shipping_address', 'Street Address', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('shipping_address', $shipping_info->shipping_address ?? '1005 W Burnside St', ['class' => 'form-control']) !!}
        	<small class="text-danger">{{ $errors->first('shipping_address') }}</small>
        </div>
        
    </div>

    <div class="form-group{{ $errors->has('shipping_address_2') ? ' has-error' : '' }}">
        {!! Form::label('shipping_address_2', 'Street Address 2', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('shipping_address_2', $shipping_info->shipping_address_2 ?? null, ['class' => 'form-control']) !!}
        	<small class="text-danger">{{ $errors->first('shipping_address_2') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
        {!! Form::label('city', 'City', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('city', $shipping_info->city ?? "Portland", ['class' => 'form-control']) !!}
        	<small class="text-danger">{{ $errors->first('city') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
        {!! Form::label('state', 'State', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
            {!! Form::select('state', array_merge(['' => 'Select A State'] , $states), $shipping_info->state ?? 'OR', ['id' => 'state', 'class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('state') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
        {!! Form::label('zip', 'Zip Code', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
            {!! Form::text('zip', $shipping_info->zip ?? 97209, ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('zip') }}</small>
        </div>
    </div>
    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
        {!! Form::label('country', 'Country', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 col-md-6">
        	{!! Form::text('country', $shipping_info->country ?? 'USA', ['class' => 'form-control', 'readonly']) !!}
        	<small class="text-danger">{{ $errors->first('country') }}</small>
        </div>
    </div>
<table class="table">
	<thead>
		<tr>
			<th></th>
			<th>Name</th>
			<th>Size</th>
			<th>Feature</th>
			<th>Quantity</th>
			<th>Unit Price</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		@forelse($cart_items as $cart_item)
		<tr>
			<td>
				@if ($cart_item->model->product->image)
					<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}"><img src='{{ asset("images/products/{$cart_item->model->product->image}") }}' height="75"></a>
				@endif
			</td>
			<td>
				<a href="{{route('product.details', ['cat_slug' => str_slug($cart_item->model->product->subcategory->category->name), 'sub_slug' => str_slug($cart_item->model->product->subcategory->name), 'prod_slug' => str_slug($cart_item->model->product->name), 'product_id' => $cart_item->model->product->id])}}">		
					{{$cart_item->name}}
					@if ($cart_item->options->organic)
						(Organic)
					@endif
				</a>
				@if ($cart_item->options->reorder_frequency)
					<br>
					<small><b><em>Reorder Every {{$cart_item->options->reorder_frequency}} Days </em></b></small>
				@endif
			</td>
			<td>{{$cart_item->model->size ?? ''}} {{$cart_item->options->glass ? 'Glass Container' : ''}}</td>
			<td>{{$cart_item->model->details() ?? ''}}</td>
			<td>{{$cart_item->qty}}</td>
			<td>${{number_format($cart_item->price, 2)}}</td>
			<td>${{number_format($cart_item->price * $cart_item->qty, 2)}}</td>
		</tr>
		@empty
			<tr><td>No Items In Shopping Cart</td></tr>
		@endforelse
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><b>Subtotal</b></td>
			<td><b>${{number_format($cart_subtotal,2)}}</b></td>
		</tr>
		@if (isset($grand_total))
			<tr>
			{{-- <td></td>
			<td></td>
			<td></td>
			<td></td> --}}
			<td colspan="5">{{$shipping_method->provider}} - {{$shipping_method->servicelevel->name}}</td>
			<td><b>Shipping</b></td>
			<td><b>${{number_format($cart_shipping, 2)}}</b>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><b>Tax</b></td>
			<td><b>${{number_format($cart_tax, 2)}}</b></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><b>Total</b></td>
			<td><b>${{number_format($grand_total, 2)}}</b></td>
		</tr>
		@else
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><b>Tax (before shipping)</b></td>
			<td><b>${{number_format($cart_tax, 2)}}</b></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><b>Total</b></td>
			<td><b>${{number_format($cart_total, 2)}}</b></td>
		</tr>
		@endif
	</tbody>
</table>
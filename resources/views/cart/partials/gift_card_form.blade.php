<b>Have a gift card?  Redeem it now! </b><br>
<small>Include the "-" in your code, e.g.,  GAB-CDE-FHGN</small><br><br>
{!! Form::open(['method' => 'POST', 'route' => 'redeem.credit.code']) !!}

    <input name="credit_code" class="input-text" id="credit_code" value="" placeholder="Gift Card Code" type="text" required> 
	{{-- <input class="button" name="apply_coupon" value="Apply Coupon" type="submit"> --}}		

    <div class="btn-group pull-right">
        {!! Form::submit("Redeem Gift Card", []) !!}
    </div>

{!! Form::close() !!}

<br>
@if (session('credit_code'))
	<b>Gift Card / Credit Codes Added</b> <br>
	@foreach (session('credit_code') as $credit)
		{{$credit}}<br>
	@endforeach
@endif
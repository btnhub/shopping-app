@extends('layouts.organica.template')
@section('page_title', "Purchase Confirmation")

@section('content')
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		Thank you for purchasing a Mountain Myst E-Gift Card.  Go ahead and thank yourself for being a thoughtful and amazing person.  Here are your purchase details, we've e-mailed you a purchase confirmation and e-mailed your recipient a gift card redemption code.
		</div>
	</div>
	<br>
	<div class="col-md-6 col-md-offset-3">
		<table class="table">
			<tbody>
				<tr>
					<td><b>Recipient's Name:</b></td>
					<td>{{$recipient_name}}</td>
				</tr>
				<tr>
					<td><b>Recipient's Email:</b></td>
					<td>{{$recipient_email}}</td>
				</tr>
				<tr>
					<td><b>Amount: </b></td>
					<td>${{$amount}}</td>
				</tr>
			</tbody>
		</table>
	</div>	
@endsection
@extends('layouts.organica.template')

@section('page_title', "")

@section('content')
	
	<div class="col-md-4">
		<h4>How it works</h4>
		<li>Tell us who you'd like to send a gift card to.</li>
		<li>Select how much you'd like to add to the card.</li>
		<li>We'll e-mail a gift card redemption code to your receipient while you bask in the glory of your awesome!</li>
	</div>

	<div class="col-md-7">
		<div class="contact-detail mb-50 p-20 bg-2">
			<center><h2>Send An E-Gift Card</h2></center>
			
			<hr>

			{!! Form::open(['method' => 'POST', 'route' => 'purchase.gift.card', 'class' => 'form-horizontal', 'id' => 'payment-form']) !!}
			
				<div class="col-md-5">
					<div class="form-group{{ $errors->has('recipient_name') ? ' has-error' : '' }}">
				        {!! Form::label('recipient_name', 'Recipient\'s Name') !!}
				        {!! Form::text('recipient_name', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('recipient_name') }}</small>
				    </div>

				    <div class="form-group{{ $errors->has('recipient_email') ? ' has-error' : '' }}">
				        {!! Form::label('recipient_email', 'Recipient\'s Email') !!}
				        {!! Form::email('recipient_email', null, ['class' => 'form-control', 'placeholder' => 'eg: luckiest-person@email.com']) !!}
				        <small class="text-danger">{{ $errors->first('recipient_email') }}</small>
				    </div>
				</div>
			    
				<div class="col-md-5 col-md-offset-1">
					<div class="form-group{{ $errors->has('provider_name') ? ' has-error' : '' }}">
				        {!! Form::label('provider_name', 'Your Name') !!}
				        {!! Form::text('provider_name',session('provider_name') ?? (auth()->user()->full_name ?? null), ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('provider_name') }}</small>
				    </div>

				    <div class="form-group{{ $errors->has('provider_email') ? ' has-error' : '' }}">
				        {!! Form::label('provider_email', 'Your Email') !!}
				        {!! Form::email('provider_email', session('provider_email') ?? (auth()->user()->email ?? null), ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) !!}
				        <small class="text-danger">{{ $errors->first('provider_email') }}</small>
				    </div>

				</div>
			    
			    <div class="col-md-12">
			    	<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
				        {!! Form::label('message', 'Message (optional)') !!}
				        {!! Form::textarea('message', "Here's a Mountain Myst E-Gift Card.  The best gift ever for the best person ever.  Enjoy!",  ['class' => 'form-control', 'size' => '20x4', 'maxlength' => 300]) !!}
				        <small class="text-danger">{{ $errors->first('message') }}</small>
				    </div>
			    </div>


			    <div class="row">
			    	<div class="col-md-4 col-md-offset-4">
			    		<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
					        {!! Form::label('amount', 'Gift Amount ($)') !!}
					        {!! Form::number('amount', null, ['class' => 'form-control', 'min' => 10, 'max' => 250]) !!}
					        <small>Select an amount between $10 and $250</small><br>
					        <small class="text-danger">{{ $errors->first('amount') }}</small>
					    </div>		
			    	</div>
			    	
			    </div>
			    

			    
				
				@include('layouts.stripe_form')

				<br>
				<div class="form-group">
				    <div class="checkbox{{ $errors->has('expiration_confirmation') ? ' has-error' : '' }}">
				        <label for="expiration_confirmation">
				            {!! Form::checkbox('expiration_confirmation', '1', null, ['id' => 'expiration_confirmation', 'required']) !!} I understand that gift cards expire after 1 year
				        </label>
				    </div>
				    <small class="text-danger">{{ $errors->first('expiration_confirmation') }}</small>
				</div>

				<div class="form-group">
				    <div class="checkbox{{ $errors->has('nonrefundable_confirmation') ? ' has-error' : '' }}">
				        <label for="nonrefundable_confirmation">
				            {!! Form::checkbox('nonrefundable_confirmation', '1', null, ['id' => 'nonrefundable_confirmation', 'required']) !!} I understand that gift cards are nonrefundable and are redeemable by USA & Canada residents only (we only ship to USA & Canada).
				        </label>
				    </div>
				    <small class="text-danger">{{ $errors->first('nonrefundable_confirmation') }}</small>
				</div>

				By clicking “Purchase” you confirm that you accept the <a href="{{url('/terms')}}">Terms of Service</a> and that your credit or debit card will immediately be charged. The gift card will be delivered to the recipient immediately by email.

			    <div class="login-button mt-20 mb-30 ">
				    <center><button>Purchase</button></center>
			  	</div>
			
			{!! Form::close() !!}

		</div>
	</div>
	
@endsection

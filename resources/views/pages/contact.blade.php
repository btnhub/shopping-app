@extends('layouts.organica.template')
@section('page_title', "")
@section('content')
	<!-- contuct-area start -->
	<div class="col-md-6">
		<div class="google-map-area mb-50">
			<div id="googleMap"></div>
		</div>	
	</div>
	
	<div class="contact-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="contact-detail mb-50 p-20 bg-2">
						<div class="contact-title">
							<h2>Contact Details</h2>
						</div>
							<p>Feel free to contact us with any questions or concerns.</p>
							
							{{-- <h5>EMAIL</h5> --}}
							<p><b>General Inquiry:</b> <a href="mailto:Contact@Mountain-Myst.com">Contact@Mountain-Myst.com</a></p>
							<p><b>Concerns & Website Issues:</b> <a href="mailto:Support@Mountain-Myst.com">Support@Mountain-Myst.com</a></p>
							<p><b>Orders & Subscriptions:</b> <a href="mailto:Orders@Mountain-Myst.com">Orders@Mountain-Myst.com</a></p>
							<p><b>Returns & Refunds:</b> <a href="mailto:Returns@Mountain-Myst.com">Returns@Mountain-Myst.com</a></p>
							<p><b>Membership Questions:</b> <a href="mailto:Members@Mountain-Myst.com">Members@Mountain-Myst.com</a></p>
							<br>
							<div class="col-md-6">
								<h5>Mailing Address</h5>
								{{-- <p>
									Mountain Myst<br>
									1905 15th St. # 1932<br>
									Boulder, CO 80306
								</p> --}}	
								<p>
									Mountain Myst<br>
									201 Columbine St. # 6883<br>
									Denver, CO 80206
								</p>	
							</div>
							
							<div class="col-md-6">
								<br>
								<strong><a href="https://www.facebook.com/MountainMystCO" target="_blank">
									<img src="{{asset('images/frontend/facebook_icon.png')}}" width="10%">
									fb.me/MountainMystCO</a></strong>
								<br><br>
								<strong><a href="https://www.instagram.com/mountainmystco/" target="_blank">
								<img src="{{asset('images/frontend/instagram_icon.png')}}" width="10%">
								@MountainMystCO</a></strong>
							</div>
							<br><br><br><br><br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- contuct-area end -->

	<!-- Google Map js -->
	<script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}"></script>
	{{-- API Key same as bufftutor.com --}}
	
	<script>
		function initialize() {
			var mapOptions = {
				zoom: 13,
				scrollwheel: false,
				center: new google.maps.LatLng(40.0150, -105.272148),
				disableDefaultUI: true,
		      	draggable: false,
			};

			var map = new google.maps.Map(document.getElementById('googleMap'),
				mapOptions);

			var marker = new google.maps.Marker({
				position: map.getCenter(),
				animation: google.maps.Animation.BOUNCE,
				icon: 'images/map.png',
				map: map
			});

		}

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
@endsection
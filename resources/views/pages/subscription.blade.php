<?php 
	$membership_active_field = \DB::table('settings')->where('field', 'membership_active')->first()->id;
    $membership_active = \DB::table('settings_values')->where('setting_id', $membership_active_field)->first()->value;
?>

@extends('layouts.organica.template')
@section('page_title', "Subscriptions")
@section('content')
	
	<h3>Never Go Without Your Favorite Product!</h3>
	<p>	While shopping, simply select how often you'd like to reorder an item and we'll handle the rest!  
		<li><b>There are no subscription fees.</b></li>
		<li><b>Lock in today's price!</b> The items in your subsequent orders will be charged the price at the time of purchase.</li>
	</p>

	<br>
	<h3>How it works</h3>
	<ol>
		<li>Select the frequency (every 15 days, 30 days, etc) you'd like to reorder the item <b>before</b> you add an item to your shopping cart.</li>
		<li>During checkout, you will be prompted to <a href="{{ url('/login') }}">login or create an account</a>.</li>
		<li>Checkout as normal and we will create a subscription schedule for you.<br>
			<b>Feel free to add items at different frequencies (e.g Lotion every 30 days, Lip Balm every 45 days) and we'll create multiple schedules for you automatically.</b>
		</li>
		<li>We will automatically charge your stored card at your specified time interval and ship your order shortly thereafter.</li>
	</ol>

	<br>
	<h3>How To Edit Or Cancel A Subscription</h3>
	You can edit your subscription at any time.  Simply login to your Mountain Myst account, and under Subscriptions, you can remove an item or cancel an entire order.

	<br><br>
	<h4>Please Note</h4>
	<li><b>Sale prices are not applied to subsequent orders.</b>  If you purchase an item during a sale, the regular price at the time of purchase will be applied to subsequent orders.</li>
	<li>Orders over ${{config('other_constants.free_shipping_amount')}} will automatically receive free shipping.</li>
	
	@if (isset($membership_active) && $membership_active)
		<li><a href="{{url('membership')}}">Membership benefits</a> will automatically be applied to orders.</li>
	@endif
	
	<br><br>
	
@endsection
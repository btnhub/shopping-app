@extends('layouts.organica.template')
@section('page_title', "Shipping Policy")
@section('content')

	<b>All transit times do not include time to make the product(s) in the order.   All orders require 3-7 business days to be created.  We currently only ship to USA and Canada.</b>
	<br><br>

	<h3>Shipping Methods and Rates</h3>
	Available shipping methods and rates will be calculated based on the package weight and destination.  You will be able to review and select your desired shipping method before placing your order.  Once your order has shipped, an e-mail with your tracking information will be sent to the email you provided with your order.
	We offer free (via USPS or FedEx) in the 48 states of the continental USA on orders that subtotal ${{config('other_constants.free_shipping_amount')}} (USD) after all discounts and before taxes are applied (if applicable).  During checkout, you will be able to select the Free Shipping option or any other available shipping option, however you will be responsible for the shipping charge for any other selected shipping method.  

	<br><br>
	<h3>Shipping To P.O. Boxes</h3>
	Please note that FedEx does not deliver to P.O. Boxes.  Orders to P.O. boxes will be shipped using USPS regardless of which method was selected and paid for when an order is made.  If your order will be sent to a P.O. Box, it is your responsibility to select and pay for one of the USPS options during checkout.  If a non-USPS option is selected, we will try to send by the most comparable USPS service.  We will not refund the shipping amount of any difference between the non-USPS option selected and the final service selected.  We are not responsible for any delays in delivery as a result of a violation of this policy.
	<br><br>

	<h3>International Shipping:</h3>
	We use FedEx and USPS for international orders.  We are not responsible for any delays, retained packages caused by Customs or Duty Fees or any additional shipping charges.  Import duties, taxes, and charges are not included in the items price or shipping cost. These charges are the buyers responsibility.
	<br><br>


	<h3>Change of Shipping Address</h3>
	If you have placed an order to be shipped to the wrong address please contact us immediately. You may send us an email: support@mountain-myst.com. Please provide your order number and address change request.
	<br><br>

	<b>Sending an email does not guarantee that we will catch your change of address request or that a re-route is available. The only time we can guarantee that an address change request has been fulfilled is when you receive a direct email from us confirming your address change. Re-routing fees will be applied to your order if your order has already shipped and we are successful in insuring the re-routing process. A form of payment will need to be collected to successfully solidify the re-routing process.</b>
	<br><br>

	<h3>Changes to Our Shipping Policy</h3>
	We reserve the right, in our sole discretion, to change our shipping policy at any time by posting revised terms to our Shipping Policy. It is your responsibility to check periodically for any changes we may make to this policy. Your continued use of the Services following the posting of changes to this policy or other terms means you accept the changes.
	
@endsection

@extends('layouts.organica.template')
@section('page_title', "Giving Back")
@section('content')
	
	<?php 
		$donation_setting_id = DB::table('settings')->where('field', 'donation_percent')->first()->id;
		$donation_percent =  DB::table('settings_values')->where('setting_id', $donation_setting_id)->first()->value * 100;?>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3>Giving Back In Our Own Backyard</h3>
			<p>Rather than selling extra products as discounted surplus items, we will routinely donate them to the <a href="https://bouldershelter.org/" target="_blank">Boulder Shelter For The Homeless</a>.  Acceptable items include soap, lip balm, lotion, shampoo, and deodorant. </p>

			<h4>What will be donated:</h4>
			<li> Extra product made when filling your order.</li>
			<li> 5-10% of free samples created for promotional purposes.</li>
			<li> Excess product made while developing new items.</li>
			<br>

			<a href="https://bouldershelter.org/" target="_blank"><img src="{{asset('/images/frontend/boulder_homeless_shelter-logo.jpg')}}"></a>
		</div>
		{{-- <div class="col-md-6">
			<h3>Hurricane Harvey & South Asia Floods</h3>
			
			<p><b>We will donate {{ $donation_percent}}% of your order subtotal to groups focused on providing aid to those affected by Hurricane Harvey (in Texas & Louisiana) and those affected by record monsoon rains in Bangladesh, India & Nepal that have killed over 1200 people and affected 40 million people (<a href="http://www.npr.org/sections/goatsandsoda/2017/08/29/547002884/epic-floods-not-just-in-texas-are-a-challenge-for-aid-groups" target="_blank">more info</a>).
			</b></p>
			
			<img src="{{asset('images/frontend/heart_shaped_texas_flag.png')}}" width="40%" align="left" style="padding:10px">
			<p>
			We will do our best to research organizations and donate to organizations that are transparent about how their funds are spent.  Rather than donate to large organizations such as the Red Cross, we will focus on smaller local organizations whenever possible.  
			</p>

			<p>
			For a list of organizations that we will consider and for more information on why we will not donate to the Red Cross, please visit: <a href="http://noredcross.org" target="_blank">noredcross.org</a>. Some of these organizations include:
			</p>
			
			<a href="http://organizetexas.org/" target="_blank"><b>Texas Organizing Project </b></a>
			<br>
			Dedicated to organize and advocate for devastated communities, shining a spotlight on inequalities that emerge in the restoration of lives, livelihoods, and homes, amplifying the needs of hard-hit communities, and providing legal assistance for residents wrongfully denied government support.

			<br><br>

			<a href="https://isgh.org/harvey/" target="_blank"><b>Islamic Society Of Greater Houston</b></a>
			<br>
			During the hurricane, the Islamic Society of Greater Houston opened affiliated mosques as shelters and plans to continue using their mosques as distribution centers even after evacuees have left.  <a href="http://www.cnn.com/2017/08/30/us/mosques-shelters-trnd/index.html">ISGH In The News </a>

			<br><br>

			<a href="http://irusa.org/south-asia-flood-emergency/" target="_blank"><b>IRUSA</b></a> (For South Asia Floods)
			<br>
			With local offices in the area, the IRUSA has been able to quickly provide support to areas hit by the monsoon rains in South Asia and will continue to provide assistance to survivors.  IRUSA is also providing support and assistance to survivors of Hurricane Harvey.
			
			<br><br>
			*** We will donate {{ $donation_percent}}% of your order subtotal (excluding tax & shipping).  We will not donate any portion of Membership or Gift Card purchases.
		</div> --}}
	</div>
@endsection
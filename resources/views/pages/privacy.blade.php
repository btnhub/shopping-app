@extends('layouts.organica.template')
@section('page_title', "Privacy Policy")
@section('content')
	
	This Privacy Policy applies to information collected by Mosaikz, LLC, d.b.a. Mountain Myst, (“Mountain Myst,” “we,” “our” or “us”) in connection with the products and services that Mountain Myst offers. This includes information collected online through our websites, branded pages on third party platforms (i.e, social networking services), mobile applications, and through our direct marketing campaigns or other online communications, as well as offline through our events, and mobile messaging services (collectively, “Mountain Myst Services”).<br><br>
	This Privacy Policy explains what information may be collected through the Mountain Myst Services and how such information may be used and/or shared with others. By using the this website and/or Mountain Myst Services, you agree to the collection, use, and disclosure of your information as described in this Privacy Policy.  If you do not, please do not use this website or Mountain Myst Services.<br><br>
	This Privacy Policy is hereby incorporated into and forms part of the terms and conditions of use of the Mountain Myst Services.
	<br><br>

	<h3>1. INFORMATION COLLECTION</h3>
	<h4>Information You Provide To Us</h4>
	When you use the Mountain Myst Services, we may ask you to provide certain personal information to obtain our products or use our services. For example, we may ask for information when you register to create an account, update your information, sign up for a membership or subscription, use certain features, access, upload or download content, purchase products or services, enter into promotions, fill out a survey, request customer support, or otherwise communicate with us. 
	<br><br>
	We (or our service providers or business partners) may collect information that relates to you, including personal information, such as your name, e-mail address, telephone number, phone number, mailing address, date of birth, gender, and demographic information.  We use SSL encryption to protect your personal information online.
	<br><br>
	We may also collect information you provide us in connection with participating in an online survey. 
	<br><br>
	If you make a purchase from us, you will be required to provide a credit/debit card number and related financial information (such as CVV, expiration date, and billing address), or other payment information (such as via PayPal or Apple Pay), depending on the form of payment you choose.  We use a third-party (Stripe) to process credit/debit card payments; we never see, gather or store your credit/debit card information.
	You may decide to provide us with another person's information (such as name, email, address, or phone number) so that we may recommend or send products or services to such person, or so that we may facilitate your communication with other people. Local law may require you to seek the consent of your contacts to provide their personal information to Mountain Myst (or in connection with using the Mountain Myst Services), which may in turn use that information in accordance with the terms of this Privacy Policy. Where required by law, we will obtain the required consent from the person whose information you provided to us.
	<br><br>
	You may also be given the option to link to your Facebook or other social media accounts through the Mountain Myst Services. When you do, we may automatically receive certain information from you based upon your privacy settings on those sites. This may include, but may not be limited to, name, user name, demographic information, updated address or contact information, location, interests, and publicly-observed data, such as from social media and online activity.
	<br><br>

	<h4>Information Collected Automatically</h4>
	Whenever you visit or interact with the Mountain Myst Services, Mountain Myst, as well as any third-party advertisers and/or service providers, may use a variety of technologies that automatically or passively collect information about your online activity. This information may be collected in the following ways:<br><br>
	<em>Usage Information:</em>  We may use cookies, web beacons, pixel tags, mobile analytics software, log files, or other technologies to collect certain information about your online activity, and interactions with our emails, online and mobile advertisements, and to allow us to keep track of analytics and certain statistical information which enables Mountain Myst to improve the Mountain Myst Services and provide you with more relevant content and advertising offered by Mountain Myst, or through or linked from the Mountain Myst Services.<br><br>
	For example, we may automatically or passively collect certain non-personal information from you, including your browser type, mobile device manufacturer, mobile carrier, phone number, operating system, activations, content, products or services you purchase or access, the page served and the preceding page views or websites visited, any mobile device activity and configurations, and email client. We may also automatically collect information about your use of the Mountain Myst Services, including the date and time you visit the Mountain Myst Services, use or click through to any of the Mountain Myst Services, the areas or pages of the Mountain Myst Services that you visit, the amount of time you spend viewing or using the Mountain Myst Services, the number of times you return to the Mountain Myst Services, other click-stream or site usage data, emails that you open, forward or click-through to our Mountain Myst Services, and other sites that you may visit.<br><br>

	<em>Device Information.</em> We may automatically collect your IP address or other unique identifier (“Device Identifier”) for the computer, mobile device, tablet or other device (collectively, “Device”) you use to access the Mountain Myst Services, including the hardware model and mobile network information. We may use a Device Identifier to, among other things, provide the Mountain Myst Services, help diagnose problems with our servers, analyze trends, track users’ web page, email and mobile application movements/activities, help identify or gather broad demographic information for aggregate use, and retarget online and mobile advertisements to you across computers or devices you may use.
	Geolocation Information: We may collect information about the location of your Device when you access or use the Mountain Myst Services. In addition, if you choose to turn on your Bluetooth, Wi-Fi or other geolocation functionality when you use our mobile applications, we may collect and use your geolocation information.<br><br>

	<em>Third Party Service Providers.</em>  We may use third party service providers to support the Mountain Myst Services.  Some of these service providers may use technology such as cookies, web beacons, pixel tags, log files, or other technologies to receive, collect, and store information on our behalf.<br><br>

	<em>Third Party Analytic Technologies.</em> We may use third parties’ analytics and tracking tools, such as Google Analytics, Adobe Marketing Cloud, Facebook Custom Audience and others, to help us track, segment and analyze usage of the Mountain Myst Services, and to help us or those third parties serve more targeted advertising to you on the Mountain Myst Services and across the Internet. These tools may use technology such as cookies, web beacons, pixel tags, log files, Flash cookies, or other technologies to collect and store non-personal information. They may also combine information they collect from your interaction with the Mountain Myst Services with information they collect from other sources. We do not have access to, or control over, these third parties’ use of cookies or other tracking technologies.
	<br><br>

	<h4>Combination of Information</h4>
	We may combine the information we receive from you and about you, including information you provide to us and information we automatically collect through the Mountain Myst Services, as well as information collected across other computers or devices that you may use, from other online or offline sources, and from third party sources.  This combined information may be used in any manner detailed in this Privacy Policy.
	<br><br>

	<h3>2. HOW WE USE THE INFORMATION COLLECTED</h3>
	We may use the information about you for a variety of business purposes, including for example:
	<br><br>

	<h4>For Customer Service and Transactional Purposes, such as to:</h4>
	<ol>
		<li>respond to your questions or requests and provide customer service;</li>
		<li>manage your account and registration;	</li>
		<li>troubleshoot issues, bugs or defects related to your account or activities;</li>
		<li>provide you with access to certain features of the Mountain Myst Services;</li>
		<li>validate, confirm, verify, deliver, and track your order (including to arrange for shipping, handle returns and refunds, and contact you about your orders, including by telephone), and send you related information, coupons, products or samples;</li>
		<li>verify your identity or communicate with you about your activities with respect to the Mountain Myst Services;</li>
		<li>to use and disclose your credit, debit or payment card or other financial information only to process payments and prevent fraud;</li>
		<li>contact you with regard to your use of the Mountain Myst Services and, in our discretion, changes to any Mountain Myst policy;</li>
		<li>develop new products or services and to enhance current products and services;</li>
		<li>protect the security or integrity of the Mountain Myst Services and our business, such as by protecting against and preventing fraud, unauthorized transactions, claims and other liabilities, and managing risk exposure, including by identifying potential hackers and other unauthorized users; and</li>
		<li>for other purposes as disclosed at the time you provide your personal information or otherwise with your consent.</li>
	</ol>

	<h4>For Advertising Customization and Analytics Purposes, such as to:</h4>
	<ol>
		<li>perform market research, understand the effectiveness of our competition or promotions and Mountain Myst Services analytics and operations;</li>
		<li>personalize and improve the Mountain Myst Services and our products, services, offers and advertising  made available on or outside the Mountain Myst Services (including on other sites/ applications that you visit).  For example, we may provide personalized or localized content, recommendations, features, and provide content or features that match your interests and preferences;</li>
		<li>provide you with customized content, advertisements, offers and promotions offered by Mountain Myst or on behalf of our partners and affiliates, and provide news and other information we think will be of interest to you on the Mountain Myst Services, in emails and across third-party websites;</li>
		<li>link or combine with information we receive from others to help understand your needs, use for interest-based or targeted advertising or re-targeting on your computers or other devices, and to provide you with better service; and</li>
		<li>communicate with you, either directly or through one of our partners (e.g., through Facebook Custom Audience, Google Customer Match, Twitter Match, etc.) for Mountain Myst marketing and promotional purposes via emails, notifications, or other messages, consistent with any permissions you may have communicated to us.</li>
	</ol>
	<br>

	<h3>3. SHARING OF INFORMATION </h3>
	The information collected or generated through your use of the Mountain Myst Services may be shared by you or by us as described below.  
	
	<h4>Sharing of Information by You</h4>
	Your activity in connection with the Mountain Myst Services, such as filling out surveys, reviewing and rating products, inquiring about or purchasing products, participation in online communities, “liking” or “sharing” Mountain Myst content to your social media accounts or pages or otherwise interacting with the Mountain Myst Services, may be visible to other users of the Mountain Myst Services and in some cases, publicly available.
	
	<h4>Sharing of Information by Us</h4>
	We may share the information collected from or about you in the following ways:<br><br>
	<em>With Our Affiliates.</em> We may share your information with our subsidiaries and affiliated companies for internal reasons, primarily for business and operational purposes. <br><br>

	<em>Third Party Service Providers.</em>  We may share your information with third party service providers that provide business, professional or technical support functions on our behalf. This includes, but may not be limited to, service providers that host or operate the Mountain Myst Services; payment processors; analyze data; provide customer service; postal or delivery services, and sponsors or other third parties that participate in or administer any promotions.<br><br>

	<em>Other Third Parties.</em> Your information may also be shared with our sponsors, partners, advertisers, advertising networks, advertising servers, and analytics companies or other third parties in connection with marketing, promotional, and other offers, as well as product information. Your information may also be shared with third parties we identify at the time you provide your personal information or otherwise with your consent. <br><br>

	{{-- <em>Sweepstakes, Contests, and Promotions.</em> If you choose to enter into one of our sweepstakes, contests, or other promotions (a “Promotion”) we may disclose your information to third parties or the public in connection with the administration of such Promotion, as required by law, as otherwise permitted by the Promotion’s official rules, or otherwise in accordance with the terms of this Privacy Policy.<br><br> --}}

	<em>Business Transfers.</em>  In the event that another entity acquires us or all or substantially all of our assets, or assets related to the Mountain Myst Services, your information may be disclosed to such entity as part of the due diligence process and will be transferred to such entity as one of the transferred assets. Also, if any bankruptcy or reorganization proceeding is brought by or against us, all such information may be considered an asset of ours and as such may be sold or transferred to third parties. Finally, information described in this paragraph may also be disclosed for due diligence purposes in connection with any proposed transaction of the sorts described in this paragraph.<br><br>

	<em>Legal Disclosures; Safety.</em>  Mountain Myst may transfer and disclose your information to third parties to comply with a legal obligation; when we believe in good faith that the law requires it; at the request of governmental authorities conducting an investigation; to verify or enforce our Terms of Use or other applicable policies; to detect and protect against fraud, or any technical or security vulnerabilities; to respond to an emergency; or otherwise to protect the rights, property, safety, or security of third parties, users of the Mountain Myst Services, Mountain Myst, or the public.
	<br><br>

	<h3>4. YOUR CHOICES AND OPT-OUT</h3>
	Certain parts of the Mountain Myst Services require cookies. You are free to set your browser or operating system settings to limit certain tracking or to decline cookies, but by doing so, you may not be able to use certain features through the Mountain Myst Services or take full advantage of all of our offerings. Please refer to your Web browser’s or operating system’s website or “Help” section for more information on how to delete and/or disable your browser or operating system from receiving cookies or controlling your tracking preferences. On your mobile device, you may also adjust your privacy and advertising settings to control whether you want to receive more relevant advertising.<br><br>

	Our system may not respond to Do Not Track requests or headers from some or all browsers. We may use cookies or other technologies to deliver more relevant advertising and to link data collected across other computers or devices that you may use. <br><br>

	<em>Access and Updates to Your Information:</em><br>
	If you wish to verify, correct, or update any of your personal information, you may visit your Account Page or email us at Support@Mountain-Myst.com.<br><br>

	<em>Unsubscribing from Communications by Us:</em><br>
	We provide our customers with the opportunity to update their information or opt-out of having their information used for purposes not directly related to placement, processing, fulfillment, or delivery of a product order or servicing of your product. To opt-out of marketing communications, you may:  
	<ul>
		<li>Send us an e-mail at Suport@Mountain-Myst.com,  </li>
		<li>Unsubscribe from our marketing email communications at any time by unsubscribing or following the instructions contained within the email.	</li>
	</ul>
	<br>

	<h3>5. CHILDREN'S PRIVACY</h3>
	Protecting children’s privacy is important to us. We do not direct the Mountain Myst Services to, nor do we knowingly collect any personal information from, children under the age of thirteen.  If Mountain Myst learns that a child under the age of thirteen has provided personally identifiable information through the Mountain Myst Services, we will use reasonable efforts to remove such information from our files.
	<br><br>
	
	<h3>6. SECURITY OF YOUR INFORMATION </h3>
	We use certain reasonable security measures to help protect your personal information. However, no electronic data transmission or storage of information can be guaranteed to be 100% secure.  Please note that we cannot ensure or warrant the security of any information you transmit to us. You use the Mountain Myst Services and provide us with your information at your own risk.  If we share information with a third party, as detailed in this privacy policy, we shall use all reasonable efforts to ensure that they keep the information secure and only use the information consistent with the terms of this Privacy Policy.
	<br><br>

	<h3>7. OTHER SITES </h3>
	The Mountain Myst Services may contain links to other sites that we do not own or operate. This includes links from service providers, fulfillment companies, advertisers, sponsors, or other business partners that may use our logo(s) as part of a co-branding agreement. We are not responsible for these sites or their content, products, services or privacy policies or practices. These other web sites may send their own cookies to your Device, they may independently collect data or solicit personal information and may or may not have their own published privacy policies. You should also independently assess the authenticity of any site which appears or claims that it is one of the Mountain Myst Services (including those linked to through an email or social networking page).
	<br><br>

	<h3>8. CHANGES & OTHER MOUNTAIN MYST PRIVACY POLICIES </h3>
	Any changes we may make to our Privacy Policy will be posted on this page. Please check back frequently to see any updates or changes to our Privacy Policy. If you do not agree or consent to these updates or changes, do not continue to use the Mountain Myst Services. If we make a material change to this Privacy Policy, we will provide appropriate notice to you.
	In addition to this Privacy Policy, there may be specific campaigns or promotions which will be governed by additional privacy terms or policies. We encourage you to read these additional terms or policies before participating in any such campaigns or promotions as you will be required to comply with them if you participate. Any additional privacy terms or policies will be made prominently available to you.
	 



@endsection
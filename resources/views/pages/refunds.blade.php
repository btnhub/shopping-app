@extends('layouts.organica.template')
@section('page_title', "Refunds & Returns Policy")
@section('content')

	All sales are final.  Since our Products are hand made, if you are dissatisfied with our Products, we only offer store credit within 30 days of receiving your order.  Refund requests must be made directly to Mountain Myst at Returns@Mountain-Myst.com. All refund requests must be made within thirty (30) days of the date of the order. Mountain Myst does not pay for return shipping and does not offer return labels.  Mountain Myst is not liable for Products that are damaged or lost in transit to Mountain Myst. Promptly following Mountain Myst’s receipt of your request (typically within five (5) business days), Mountain Myst will credit the amount paid for the returned Product (less any shipping and handling costs/fees related to the original purchase, which are non-refundable) to your Mountain Myst account.  If you do not have a Mountain Myst account, an account credit code will be provided that you can use during checkout to apply the credit. 
	<br><br>

	Mountain Myst will not provide a refund for a request that is received by Mountain Myst more than thirty (30) days after the date of order. Mountain Myst also does not provide a refund for returned Products that are damaged due to misuse, lack of care, mishandling, accident, abuse or other abnormal use by the customer or end user. This includes but is not limited to: failure to pick up or receive your delivery in a timely manner, not refrigerating Products, exposure to sunlight or heat after delivery.
	<br><br>
	
	The credit for your returned items cannot be exchanged for cash.
	<br><br>
	
	If you buy Mountain Myst Products from an unauthorized retailer or other unauthorized source, we cannot offer you credit for your product or otherwise assist you with any problems that you may encounter. 
	<br><br>
	
	If you are signed up for a Subscription and have items reordered on a regular time frame selected by you (e.g. every 15 days, every 30 days) and you wish to pause or end the Subscription, it is your responsibility to pause or delete the Subscription on your Account before the next charge date.  We do not refund charges for Subscriptions or recurring orders.    
	<br><br>
	
	<h4>Damages</h4>
	If your package was damaged in transit, you must notify us within 2 business days so we can make a claim with the carrier.  Do not discard the damaged packaging in the event the carrier would like to inspect the box and contents.
@endsection